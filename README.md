
# OER-Converter Frontend
The frontend will offer the core services, such as extracting legally-questionable content and replace it with with content that bear suitable licenses, in our case [CC-0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY](https://creativecommons.org/licenses/by/2.0/) and [CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/).

The frontend will run on Angular, which is a typescript-focused NodeJs application that also brings about some debugging tools and easy means for plugging in third-party content.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
