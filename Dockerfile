FROM node:16 as angular_build

WORKDIR /app

COPY package*.json ./

COPY . .

RUN npm install
RUN npm install -g @angular/cli@8.3.29
RUN  ng build --prod --source-map

FROM nginx:latest

COPY --from=angular_build /app/dist/frontend /usr/share/nginx/html

EXPOSE 80
