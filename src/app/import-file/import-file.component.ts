import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {SupportedImportFileType, SupportedImportFileTypesConfig} from '../../config/supported-import-file-types.config';
import {ToastService} from '../services/toast.service';
/**
 * This component displays a file input component, checks the selected file for
 * acceptance and then triggers an output action which passes the wrapped up selected file
 * to the parent component
 * @output file-import Propagates a file wrapper containing the loaded file upon import
 */
@Component({
  selector: 'app-import-file',
  templateUrl: './import-file.component.html',
  styleUrls: ['./import-file.component.scss']
})
export class ImportFileComponent implements OnInit {
  @Output('file-import') file_$ = new EventEmitter<FileWrapper>();

  // accesses the html material file input compnent
  @ViewChild('fileInput', {static: false}) private _fileInput: ElementRef;

  // holds the accepted mime types as a string
  public acceptedTypes: string;

  constructor(
    private _toast: ToastService
  ) { }

  /**
   * When this component is initialized, request the string of allowed mime types
   */
  ngOnInit() {
    this.acceptedTypes = this._getAcceptedTypes();
  }

  /**
   * Gets triggered once a file was selected. If selected file is not supported, a toast stating the issue is shown
   * @param file The file that the user wishes to transform/translate/make OER conform
   */
  public handleFileInput(file: File) {
    if (file) {
      if (this._isFileSupported(file)) {
        this.file_$.emit(this._buildFileWrapper(file));
      } else {
        this._fileInput.nativeElement.value = null
        this._toast.show('file is not supported');
      }
    }
  }

  /**
   * Helper function to build the wrapper around the loaded file
   * @param file The file the users loaded into the application
   */
  private _buildFileWrapper(file: File): FileWrapper {
    const result: FileWrapper = {
      type: file.type,
      file: file
    };
    return result;
  }

  /**
   * Computes a string containing the accepted types by the file input component. Based on config file which can be found
   * under /config/supported-import-file-types.config
   */
  private _getAcceptedTypes(): string {
    let result = '';
    const type = (index: number): SupportedImportFileType => {
      return SupportedImportFileTypesConfig[index];
    };
    const isLastSupportedType = (index: number): boolean => {
      return index === SupportedImportFileTypesConfig.length - 1;
    };
    for (let i = 0; i < SupportedImportFileTypesConfig.length; i++) {
      result += type(i).type;
      if (!isLastSupportedType(i)) {
        result += ',';
      }
    }
    console.log(result);
    return result;
  }

  /**
   * Helper function which booleanly answers if a given file (namely the one that the user selected)
   * is actually supported
   * @param file The file that the user wishes to transform/translate/make OER conform
   */
  private _isFileSupported(file: File): boolean {
    for (const type of SupportedImportFileTypesConfig) {
      if (type.type === file.type) {
        return true;
      }
    }
    // if functions has not returned by now, the type is not supported
    return false;
  }
}
