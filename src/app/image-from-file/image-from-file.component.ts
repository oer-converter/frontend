import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {IImageWrapper} from '../../models/image-wrapper';
import {Observable, Subscription} from 'rxjs';

/**
 * Component which takes a single image wrapper and handles user interaction with it
 */
@Component({
  selector: 'app-image-from-file',
  templateUrl: './image-from-file.component.html',
  styleUrls: ['./image-from-file.component.scss']
})
export class ImageFromFileComponent implements OnInit, OnDestroy {
  @Input() image: IImageWrapper;
  @Output('click') _onClicked: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();

  @Input('clear-selection') clearSelection$: Observable<number>;
  private _subscriptionClearSelection: Subscription;

  public selected = false;

  constructor() { }

  /**
   * Only subscribe if an observable was passed upon initialization
   */
  ngOnInit() {
    if (this.clearSelection$) {
      this._subscriptionClearSelection = this.clearSelection$.subscribe(this._onClearingSelection.bind(this));
    }
  }

  /**
   * Unsubscribe upon component destruction to avoid memory leaks
   */
  ngOnDestroy(): void {
    if (this._subscriptionClearSelection) {
      this._subscriptionClearSelection.unsubscribe();
      this._subscriptionClearSelection = undefined;
    }
  }

  /**
   * When an image was clicked on, add stylesheet to it make this selection visible
   * and propagate the image upon the event, that this particular
   * image was clicked on
   */
  public onClick(): void {
    this.selected = true;
    this._onClicked.emit(this.image);
  }

  /**
   * Setting selection state to false if ID matches own external ID
   * @param idToClear The external ID from the IImageWrapper to is to be cleared
   */
  private _onClearingSelection(idToClear: number) {
    if (this.image.internalId === idToClear) {
      this.selected = false;
    }
  }

}
