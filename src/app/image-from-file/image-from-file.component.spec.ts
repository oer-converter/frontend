import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageFromFileComponent } from './image-from-file.component';

describe('ImageFromFileComponent', () => {
  let component: ImageFromFileComponent;
  let fixture: ComponentFixture<ImageFromFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageFromFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
