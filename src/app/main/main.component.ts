/**
 * thanks to
 * - https://medium.com/better-programming/convert-a-base64-url-to-image-file-in-angular-4-5796a19fdc21 for giving an example on how to
 * convert the base64 encoded string from the zip file can be converted to an image
 */

import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';
import {ZipService} from '../services/zip.service';
import {DomSanitizer, SafeUrl, Title} from '@angular/platform-browser';
import {IImageWrapperEdited, IImageWrapper} from '../../models/image-wrapper';
import {HttpService} from '../services/http.service';
import {FileHandler, ISaveToFileProgress} from '../../classes/file-handler.abstract';
import {MatBottomSheet, MatBottomSheetRef, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import {
  DialogFileGettingReadyComponent,
  DialogFileGettingReadyOptions
} from '../dialog-file-getting-ready/dialog-file-getting-ready.component';
import {DialogStartComponent, DialogStartData, DialogStartReturnData} from '../dialog-start/dialog-start.component';
import {MainConfig} from '../../config/main.config';
import {SessionServiceService} from '../services/session-service.service';
import {IMakeOerParams} from '../make-oer/make-oer.component';
import {
  DialogEditImageComponent,
  IDialogEditImageComponentOptions,
  IDialogEditImageReturnParams
} from '../compare-images/dialog-edit-image/dialog-edit-image.component';
import {Utils} from '@hexxag0nal/utils';
import {BackendService} from '../services/backend.service';
import {
  DialogSuggestionsFromDatabaseComponent,
  DialogSuggestionsFromDatabaseComponentOptions, DialogSuggestionsFromDatabaseComponentResult
} from '../dialog-suggestions-from-database/dialog-suggestions-from-database.component';
import {IImageGalleryShowInfoOptions} from '../image-gallery/image-gallery.component';
import {BottomSheetGenericComponent, IBottomSheetGenericOptions} from '../bottom-sheet-generic/bottom-sheet-generic.component';
import {Router} from '@angular/router';
import {ToastService} from '../services/toast.service';
import {filterErrorsAndWarnings} from '@angular/compiler-cli';
import {CcByLicense, CcBySaLicense, CcZeroLicense, License} from '../../assets/libs/office-open-xml-js/license';
import {DialogAskForFeedbackComponent} from '../dialog-ask-for-feedback/dialog-ask-for-feedback.component';

/**
 * This component is the main component and acts as the running application. It properly shows and hides relevant components,
 * according to the current app's state, manages the app's state and pushes and takes data to and from child components
 */
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  private _fileHandler: FileHandler;


  // private _subjectImagesFromFile: Subject<IImageWrapper[]> = new Subject<IImageWrapper[]>();
  public images$: Observable<IImageWrapper[]> = this._session.observeImagesFromFile();
  private _subscriptionImages: Subscription;

  // do it this way, because otherwise (if directly binding to the observable via async pipe) it will only work once and then not again
  public images: IImageWrapper[];

  public selectedImageFromFile$: Observable<IImageWrapper> = this._session.observeSelectedImageFromFile();

  public selectedImageFromExternalApi$: Observable<IImageWrapper> = this._session.observeSelectedImageFromExternalApi();

  private _subjectSavingFileProgress: Subject<ISaveToFileProgress> = new Subject<ISaveToFileProgress>();
  public savingFileProgress$: Observable<ISaveToFileProgress> = this._subjectSavingFileProgress.asObservable();

  public showImportComponent = true;
  public showMainScreen = true;

  private _selectedImageFromFile: IImageWrapper;
  private _selectedImageFromApi: IImageWrapper;

  public showInfoOptions: IImageGalleryShowInfoOptions[] = [
    {
      text: 'Size',
      value: image => {
        let refDimension = image.dimension;
        if (image.edited) {
          refDimension = image.edited.dimension;
        }
        // tslint:disable-next-line:max-line-length
        return refDimension.width + 'x' + refDimension.height + ' (~' + refDimension.ratio.toFixed(MainConfig.imageRatioFractionDigits) + ')';
      }
    }
  ];

  private _bottomSheetRef: MatBottomSheetRef;
  private _subjectReplaceByImageFromExternalApi: Subject<IBottomSheetPassedData> = new Subject<IBottomSheetPassedData>();
  private _subscriptionReplaceByImageFromExternalApi: Subscription;
  private _subjectReplaceByImageFromMachine: Subject<IBottomSheetPassedData> = new Subject<IBottomSheetPassedData>();
  private _subscriptionReplaceByImageFromMachine: Subscription;
  private _subjectPublishAsOer: Subject<IBottomSheetPassedData> = new Subject<IBottomSheetPassedData>();
  private _subscriptionPublishAsOer: Subscription;
  private _subjectLeaveAsIsButExcludeFromOverallLicense: Subject<IBottomSheetPassedData> = new Subject<IBottomSheetPassedData>();
  private _subscriptionLeaveAsIsButExcludeFromOverallLicense: Subscription;

  private _subscriptionDialogAskForFeedbackAfterClose: Subscription;

  constructor(
    private _zip: ZipService,
    private _sanitizer: DomSanitizer,
    private _http: HttpService,
    private _dialogFileGettingReady: MatDialog,
    private _dialogImportFile: MatDialog,
    private _title: Title,
    private _session: SessionServiceService,
    private _backend: BackendService,
    private _bottomSheetFurtherActions: MatBottomSheet,
    private _router: Router,
    private _toast: ToastService,
    private _dialogAskForFeedback: MatDialog
  ) { }

  ngOnInit() {
    this._session.startSessionProperly();

    this._subscriptionImages = this.images$.subscribe(images => {
      this.images = images;
    });


    // set the title. checks if version is to be shown
    this._title.setTitle(MainConfig.name + (MainConfig.showVersion ? ' ' + this._session.getCurrentToolVersion() : ''));
    if (this._session.shallStartFromBeginning()) {
      this._openStartDialog(true);
    } else {
      // now the images need to be fetched from the session service since the start dialog won't resolve them
      this.showImportComponent = false;
      this.showMainScreen = true;
      this._fileHandler = this._session.getFileHandler();
      // I have no idea why, but in order for the remaining images to be shown, this line is crucial, even though they are subscribed
      // to on the images$ observable
      this.images = this._session.getImages();
    }
    this._subscriptionReplaceByImageFromExternalApi = this._subjectReplaceByImageFromExternalApi.subscribe(passedData => {
      this._nextStep(passedData.image, '/search-api');
    });
    this._subscriptionPublishAsOer = this._subjectPublishAsOer.subscribe(passedData => {
      this._nextStep(passedData.image, 'publish-as-oer');
    });
    this._subscriptionReplaceByImageFromMachine = this._subjectReplaceByImageFromMachine.subscribe(passedData => {
      this._nextStep(passedData.image, 'replace-by-from-machine');
    });
    this._subscriptionLeaveAsIsButExcludeFromOverallLicense = this._subjectLeaveAsIsButExcludeFromOverallLicense.subscribe(passedData => {
      this._nextStep(passedData.image, 'exclude');
    });
  }

  ngOnDestroy() {
    Utils.safeUnsubscribe(
      this._subscriptionLeaveAsIsButExcludeFromOverallLicense,
      this._subscriptionPublishAsOer,
      this._subscriptionReplaceByImageFromExternalApi,
      this._subscriptionReplaceByImageFromMachine,
      this._subscriptionImages,
      this._subscriptionDialogAskForFeedbackAfterClose
    );
  }

  /**
   * Handles the dismissal of the bottom sheet, propagation of the currently selected image from file and the navigation to the
   * correct destination
   * @param image The IImageWrapper that was selected
   * @param route The destination
   */
  private _nextStep(image: IImageWrapper, route: string): void {
    this._dismissAndResetBottomSheet();
    this.propagateSelectedImage(image);
    this._router.navigate([route]);
  }

  /**
   * Is triggered when an image was chosen. It then propagates the event via the next subject/observable
   * @param image The image wrapper containing the image that was chosen to be toReplace/edited
   */
  public onSelectImageFromExternalSearch(image: IImageWrapper): void {
    // something fires the select event, couldn't determine what, but we can block out Events
    if (!(image instanceof Event)) {
      // save a pointer to the currently selected image that will replace the image from file
      this._selectedImageFromApi = image;
      // propagate the selected image
      this._session.nextSelectedImageFromExternalApi(image);
    }
  }

  public async onSaveFile(): Promise<void> {
    await this._openDialogFileGettingReady();
    // Take the most specific license by default
    let _targetLicense: License = new CcBySaLicense(4);
    if (this._session.getTargetLicense() === 2) {
      _targetLicense = new CcByLicense(4);
    } else if (this._session.getTargetLicense() === 1) {
      _targetLicense = new CcZeroLicense();
    }
    // todo: when overhauling the code, do not forget to tow the download functionality out of the filehandler
    //  and let some superior instance do the job to download. filehandler should only give back the file as blob.
    await this._fileHandler.saveToFile(
      this._session.getLegalDisclaimerText() + this._session.getTextOfExcludedImages(),
      _targetLicense,
      this._subjectSavingFileProgress
    );
    this._backend.sendReceipt(await this._fileHandler.getReceipt());
    const _blobReceipt = new Blob([await this._fileHandler.getReceiptAsCsv()], {
      type: 'text/csv',
      endings: 'native'
    });
    await Utils.saveAs(_blobReceipt, `receipt-${this._session.getCurrentGUID()}.csv`);
    this._session.markSessionAsDone();
    this._openDialogAskForFeedback();
  }

  public async onMakeOerAction(params: IMakeOerParams): Promise<void> {
    const _indexInArray = this.images.findIndex(elem => elem === this._selectedImageFromFile);
    this.images.splice(_indexInArray, 1);
    this._session.nextImagesFromFile(this.images);
    // this._subjectSelectedImageFromFile.next();
    this._session.nextSelectedImageFromFile();
    this._session.clearSelectedImages();
    this._fileHandler.EnqueuePublishAsOer(this._selectedImageFromFile, params);
  }

  private async _openDialogFileGettingReady(): Promise<void> {
    const dialogRef = this._dialogFileGettingReady.open(DialogFileGettingReadyComponent, {
      data: {
        progress$: this.savingFileProgress$
      },
      disableClose: true

      // This tells that the data field within the MatDialogConfig is of type DialogFileGettingReadyOptions
    } as MatDialogConfig & {data: DialogFileGettingReadyOptions});

    return new Promise((resolve) => {
      const _subscriptionAfterOpened = dialogRef.afterOpened().subscribe(() => {
        // unsubscribe directly to avoid memory leaks due to open subscriptions
        _subscriptionAfterOpened.unsubscribe();
        resolve();
      });
    });
  }

  private async _openStartDialog(disableClose: boolean = false): Promise<void> {
    const dialogRef = this._dialogImportFile.open<DialogStartComponent, any, DialogStartReturnData>(DialogStartComponent, {
      data: {
        sessionService: this._session
      },
      disableClose: disableClose,
      /*height: '80%',
      minWidth: '50%'*/

      // This tells that the data field within the MatDialogConfig is of type DialogFileGettingReadyOptions
    } as MatDialogConfig & {data: DialogStartData});

    const _promises: Promise<any>[] = [];

    _promises.push(new Promise((resolve) => {
      const _subscriptionAfterOpened = dialogRef.afterOpened().subscribe(() => {
        // unsubscribe directly to avoid memory leaks due to open subscriptions
        _subscriptionAfterOpened.unsubscribe();
        resolve();
      });
    }));

    _promises.push(new Promise(async resolve => {
      const _subscriptionAfterClosed = dialogRef.afterClosed().subscribe(async (result) => {
        _subscriptionAfterClosed.unsubscribe();

        this.showImportComponent = false;
        this.showMainScreen = true;

        this._fileHandler = this._session.getFileHandler();

        this._session.nextImagesFromFile(result.images);
        this._session.doNotStartFromBeginning();
        resolve();
      });
    }));

    await Promise.all(_promises);
  }

  public apiReachabilityTest() {
    console.log('Checking if api is reachable');
    this._http.get('http://localhost:4201/');
  }

  private async _openDialogSuggestionsFromDatabase(): Promise<void> {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this._dialogFileGettingReady.open<DialogSuggestionsFromDatabaseComponent, DialogSuggestionsFromDatabaseComponentOptions, DialogSuggestionsFromDatabaseComponentResult>(DialogSuggestionsFromDatabaseComponent, {
      data: {
        fileHandler: this._fileHandler,
        image: this._selectedImageFromFile
      },
      disableClose: false

      // This tells that the data field within the MatDialogConfig is of type DialogFileGettingReadyOptions
    });

    const _promises: Promise<any>[] = [];

    _promises.push(new Promise((resolve) => {
      const _subscriptionAfterOpened = dialogRef.afterOpened().subscribe((result) => {
        // unsubscribe directly to avoid memory leaks due to open subscriptions
        _subscriptionAfterOpened.unsubscribe();
        resolve();
      });
    }));

    _promises.push(new Promise(async resolve => {
      const _subscriptionAfterClosed = dialogRef.afterClosed().subscribe(async (result) => {
        _subscriptionAfterClosed.unsubscribe();

        // if there is no result, the user probably just closed the dialog
        if (result) {
          if (result.replacedBy) {
            this.onSelectImageFromExternalSearch(result.replacedBy);
          } else if (result.madeOer) {
            this.onMakeOerAction(result.madeOer);
          }
        }

        resolve();
      });
    }));

    await Promise.all(_promises);
  }

  /**
   * Opens the bottom sheet for picking further action for the selected image
   */
  public openBottomSheetActions(image: IImageWrapper): void {
    if (image.apiHistory) {
      this._openDialogSuggestionsFromDatabase();
    }
    if (!(image instanceof MouseEvent)) {
      const dataBottomSheet: IBottomSheetGenericOptions<IBottomSheetPassedData> = {
        data: {
          image: image
        },
        list: [
          {
            text: 'Use external API Search',
            subText: 'Replace image with an image from an external image search',
            onClick: this._subjectReplaceByImageFromExternalApi
          },
          {
            text: 'Upload replacement from your machine',
            subText: 'Upload a new image that will replace your chosen image',
            onClick: this._subjectReplaceByImageFromMachine
          },
          {
            text: 'Publish as OER',
            subText: 'Manually add the necessary information to the picture to adhere to the OER specifications',
            onClick: this._subjectPublishAsOer
          },
          {
            text: 'Leave as is',
            subText: 'But exclude this image from the overall license of the resulting file.',
            onClick: this._subjectLeaveAsIsButExcludeFromOverallLicense
          }
        ]
      };
      this._bottomSheetRef = this._bottomSheetFurtherActions.open(BottomSheetGenericComponent, {data: dataBottomSheet});
    }
  }

  /**
   * As the name suggests, it dismisses the bottom sheet and resets the reference
   */
  private _dismissAndResetBottomSheet(): void {
    if (this._bottomSheetRef) {
      this._bottomSheetRef.dismiss();
      this._bottomSheetRef = undefined;
    }
  }

  /**
   * Tell the session about the selected image to avoid serializing it in a way it can be appended to the url
   */
  public propagateSelectedImage(image: IImageWrapper): void {
    this._session.nextSelectedImageFromFile(image);
  }

  private _openDialogAskForFeedback(): void {
    this._subscriptionDialogAskForFeedbackAfterClose = this._dialogAskForFeedback.open(DialogAskForFeedbackComponent)
      .afterClosed().subscribe(answer => {
        if (answer) {
          this._router.navigate(['feedback']);
        } else {
          this._reloadApp();
        }
      });
  }

  /**
   * Reload the entire app
   */
  private _reloadApp(): void {
    /*this._router.routeReuseStrategy.shouldReuseRoute = () => false;
    this._router.onSameUrlNavigation = 'reload';
    this._router.navigate(['']);*/
    window.location.reload();
  }

  /**
   * Just a shortcut and public accessor to reload the entire app
   */
  public loadNewFile(): void {
    this._reloadApp();
  }


}

// Do not export since it shall only be used here
interface IBottomSheetPassedData {
  image: IImageWrapper;
}

/**
 * A helper function that removes the currently selected image from file from the list
 * @param session The SessionService
 */
export function removeCurrentlySelectedImageFromImagesFromFile(session: SessionServiceService) {
  const _indexInArray = session.getImages().findIndex(elem => elem === session.getCurrentlySelectedImageFromFile());
  session.getImages().splice(_indexInArray, 1);
  session.nextImagesFromFile(session.getImages());
}

/**
 * Helper function that derives the height and the width of an image
 */
export function getDimensionFromImageSource(source: string): Promise<{height: number; width: number}> {
  return new Promise(((resolve, reject) => {
    var img = new Image();
    img.onload = function() { 
      resolve({height: img.width, width: img.height}); 
    }
    img.src = source;
  }));
}


