import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SessionServiceService} from '../services/session-service.service';
import {Router} from '@angular/router';
import {IOerFormularValues} from '../oer-formular/oer-formular.component';
import {Utils} from '@hexxag0nal/utils';
import {IImageWrapper, IImageWrapperEdited} from '../../models/image-wrapper';
import {
  DialogEditImageComponent,
  IDialogEditImageComponentOptions,
  IDialogEditImageReturnParams
} from '../compare-images/dialog-edit-image/dialog-edit-image.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Subscription} from 'rxjs';
import {getDimensionFromImageSource, removeCurrentlySelectedImageFromImagesFromFile} from '../main/main.component';

@Component({
  selector: 'app-replace-by-image-from-machine',
  templateUrl: './replace-by-image-from-machine.component.html',
  styleUrls: ['./replace-by-image-from-machine.component.scss']
})
export class ReplaceByImageFromMachineComponent implements OnInit {
  @ViewChild('fileInput', {static: false}) fileInput: ElementRef;

  public imageFromMachine: IImageWrapper;

  private _editExternalImageDialogRef: MatDialogRef<DialogEditImageComponent>;
  private _subscriptionEditExternalImageDialogAfterClose: Subscription;

  constructor(
    private _session: SessionServiceService,
    private _router: Router,
    private _dialogEditExternalImage: MatDialog
  ) { }

  ngOnInit() {
  }

  public goToMain(): void {
    this._router.navigate(['']);
  }

  public getToolbarTitle(): string {
    return this._session.getToolName() + ' ' + this._session.getCurrentToolVersion();
  }

  public async replaceByImageFromMachine(form: IOerFormularValues): Promise<void> {
    if (form.license) {
      this.imageFromMachine.license = form.license.enum;
      this.imageFromMachine.licenseVersion = form.license.version;
      this.imageFromMachine.licenseUrl = form.license.deeds;
      this.imageFromMachine.title = form.title;
      this.imageFromMachine.author = form.author;
      removeCurrentlySelectedImageFromImagesFromFile(this._session);
      // tslint:disable-next-line:max-line-length
      this._session.getFileHandler().EnqueueAddImageFromBase64(await this._session.getCurrentlySelectedImageFromFile(), this.imageFromMachine);
      this._session.nextSelectedImageFromFile();
      this._session.nextSelectedImageFromExternalApi();
      this.goToMain();
    }
  }

  public openFileUpload(): void {
    this.fileInput.nativeElement.click();
  }

  public async onFileUploaded(file: File): Promise<void> {
    const dataUrl = await Utils.readFileAsDataUrl(file);

    const dim = await getDimensionFromImageSource(dataUrl.toString());

    const image: IImageWrapper = {
      base64: dataUrl,
      dimension: {
        height: dim.height,
        width: dim.width,
        ratio: dim.width / dim.height
      }
    };

    this.openEditExternalImageDialog(image);
  }

  /**
   * Triggers the dialog that exposes the image and some editing tools to the user
   */
  public openEditExternalImageDialog(image: IImageWrapper): void {
    // tslint:disable-next-line:max-line-length
    this._editExternalImageDialogRef = this._dialogEditExternalImage.open<DialogEditImageComponent, IDialogEditImageComponentOptions>(DialogEditImageComponent, {
      // disableClose: true,
      height: '92.3%',
      // setting it to 100% causes the dialog to be stuck on the left side
      width: '99.999999%',
      data: {
        image: image,
        targetAspectRatio: this._session.getCurrentlySelectedImageFromFile().dimension.ratio,
        useBase64DataUrl: true
      }
    });

    this._subscriptionEditExternalImageDialogAfterClose = this._editExternalImageDialogRef.afterClosed().subscribe((imageState: IDialogEditImageReturnParams) => {
      if (imageState) {
        (imageState as IImageWrapperEdited).editor = this._session.getCurrentUserRealName();
        image.edited = imageState;
        // since it was edited, it should be uploaded to the db, regardless if it was a suggestion
        image.doNotUpload = false;
        this.imageFromMachine = image;
      }
    });
  }

}
