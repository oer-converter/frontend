import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplaceByImageFromMachineComponent } from './replace-by-image-from-machine.component';

describe('ReplaceByImageFromMachineComponent', () => {
  let component: ReplaceByImageFromMachineComponent;
  let fixture: ComponentFixture<ReplaceByImageFromMachineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplaceByImageFromMachineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplaceByImageFromMachineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
