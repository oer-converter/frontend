import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageGalleryComponent } from './image-gallery.component';
import {MatButtonModule, MatCardModule, MatTooltipModule, MatCheckboxModule} from '@angular/material';



@NgModule({
  declarations: [ImageGalleryComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatTooltipModule,
    MatCheckboxModule
  ],
  exports: [ImageGalleryComponent]
})
export class ImageGalleryModule { }
