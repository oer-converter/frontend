import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IImageWrapper} from '../../models/image-wrapper';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit {
  @Input('images') images: IImageWrapper[];
  @Output('select') _selectedImage: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();

  @Input('card-actions') cardActions: IImageGalleryCardActionsOptions[];
  @Output() click: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();
  @Input('show-info') showInfo: IImageGalleryShowInfoOptions[];


  // *****************
  // *****************
  // mohibChecked = true;
  // checkedArray: Array<any> = [];


  public onMohibChecked(i) {
    // i.moShow = false;
    this.images[i].moShow = false;
    // this.mohibChecked = !this.mohibChecked;
  }
  // *****************
  // *****************


  constructor() { }

  ngOnInit() {
  }

  /**
   * Executed when an image from the results is selected
   * @param image The selected image
   */
  public onSelect(image: IImageWrapper): void {
    if (!(image instanceof MouseEvent)) {
      this._selectedImage.next(image);
    }
  }
}

export interface IImageGalleryShowInfoOptions {
  text: string;
  value: (image: IImageWrapper) => string;
  link?: (image: IImageWrapper) => string;
}

export interface IImageGalleryCardActionsOptions<SUBJECT_DATA = any> {
  text: string;
  subject: Subject<SUBJECT_DATA>;
  tooltip?: string;
}
