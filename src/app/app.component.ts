import {Component, EventEmitter, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  constructor() {
  }
}

export function hasObservers(observable: Subject<any> | EventEmitter<any>): boolean {
  return observable.observers.length > 0;
}
