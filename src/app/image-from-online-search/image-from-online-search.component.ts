import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {IImageWrapper} from '../../models/image-wrapper';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-image-from-online-search',
  templateUrl: './image-from-online-search.component.html',
  styleUrls: ['./image-from-online-search.component.scss', '../image-from-file/image-from-file.component.scss']
})
export class ImageFromOnlineSearchComponent implements OnInit, OnDestroy {
  @Input() image: IImageWrapper;
  @Output('click') _onClicked: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();

  @Input('clear-selection') clearSelection$: Observable<string> = new Observable<string>();
  private _subscriptionClearSelection: Subscription;

  public selected = false;

  constructor() { }

  ngOnInit(): void {
    if (this.clearSelection$) {
      this._subscriptionClearSelection = this.clearSelection$.subscribe(this._onClearingSelection.bind(this));
    }
  }

  ngOnDestroy(): void {
    if (this._subscriptionClearSelection) {
      this._subscriptionClearSelection.unsubscribe();
      this._subscriptionClearSelection = undefined;
    }
  }

  /**
   * When an image was clicked on, add stylesheet to it to make this selection visible
   * and propagate the image upon the event, that this particular
   * image was clicked on
   */
  public onClick(): void {
    this.selected = true;
    this._onClicked.emit(this.image);
  }

  /**
   * Setting selection state to false if ID matches own external ID
   * @param idToClear The external ID from the IImageWrapper to is to be cleared
   */
  private _onClearingSelection(idToClear: string) {
    if (this.image.externalId === idToClear) {
      this.selected = false;
    }
  }
}
