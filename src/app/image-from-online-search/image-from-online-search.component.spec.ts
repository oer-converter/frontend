import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageFromOnlineSearchComponent } from './image-from-online-search.component';

describe('ImageFromOnlineSearchComponent', () => {
  let component: ImageFromOnlineSearchComponent;
  let fixture: ComponentFixture<ImageFromOnlineSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageFromOnlineSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFromOnlineSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
