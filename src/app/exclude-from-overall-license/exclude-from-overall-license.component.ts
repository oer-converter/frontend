import { Component, OnInit } from '@angular/core';
import {SessionServiceService} from '../services/session-service.service';
import {Router} from '@angular/router';
import {IImageWrapper} from '../../models/image-wrapper';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {removeCurrentlySelectedImageFromImagesFromFile} from '../main/main.component';

@Component({
  selector: 'app-exclude-from-overall-license',
  templateUrl: './exclude-from-overall-license.component.html',
  styleUrls: ['./exclude-from-overall-license.component.scss']
})
export class ExcludeFromOverallLicenseComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private _session: SessionServiceService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      description: new FormControl(null, Validators.required)
    });
  }

  public goToMain(): void {
    this._router.navigate(['']);
  }

  public getToolbarTitle(): string {
    return this._session.getToolName() + ' ' + this._session.getCurrentToolVersion();
  }

  public getCurrentlySelectedImageFromFile(): IImageWrapper {
    return this._session.getCurrentlySelectedImageFromFile();
  }

  public excludeImage(): void {
    console.log('excluding it!');
    removeCurrentlySelectedImageFromImagesFromFile(this._session);
    this._session.excludeImageFromOverallLicense(this.formGroup.get('description').value.toString());
    this.goToMain();
  }


//   /**---------------------------------------------------------------------------------- */
//   /** UI responds with "Modification was successful" message to make App more responsive */


//   public showMessageToUser = false;

//   feedbackAction() {
//     setTimeout(() => {
//       this.showMessageToUser = true;
//     }, 1);

//   setTimeout(() => {
//     this.goToMain();
//   }, 1000);
// }

//   /** ----------------------------------------------------------------------------------- */

}
