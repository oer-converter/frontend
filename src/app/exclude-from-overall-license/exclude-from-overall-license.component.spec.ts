import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcludeFromOverallLicenseComponent } from './exclude-from-overall-license.component';

describe('ExcludeFromOverallLicenseComponent', () => {
  let component: ExcludeFromOverallLicenseComponent;
  let fixture: ComponentFixture<ExcludeFromOverallLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcludeFromOverallLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcludeFromOverallLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
