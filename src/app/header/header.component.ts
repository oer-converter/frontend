import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {hasObservers} from '../app.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output('go-back') onGoBack: EventEmitter<void> = new EventEmitter<void>();
  public showGoBackButton = false;

  @Output('save-file') onSaveFile: EventEmitter<void> = new EventEmitter<void>();
  public showSaveFileButton = false;

  @Output('load-file') onLoadFile: EventEmitter<void> = new EventEmitter<void>();
  public showLoadFileButton = false;

  @Output('show-selected-image-from-file') onShowSelectedImageFromFile: EventEmitter<void> = new EventEmitter<void>();
  public showButtonShowSelectedImageFromFile = false;

  @Input() title: string;

  @Input('more-buttons') moreButtons: IMoreButtons[];

  constructor() { }

  ngOnInit() {
    this.showGoBackButton = hasObservers(this.onGoBack);
    this.showSaveFileButton = hasObservers(this.onSaveFile);
    this.showLoadFileButton = hasObservers(this.onLoadFile);
    this.showButtonShowSelectedImageFromFile = hasObservers(this.onShowSelectedImageFromFile);
  }

}

export interface IMoreButtons {
  tooltip?: string;
  callback: () => {};
  buttonText?: string;
  buttonIcons?: string[];
}
