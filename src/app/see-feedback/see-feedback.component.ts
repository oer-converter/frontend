import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SessionServiceService} from '../services/session-service.service';
import {BackendService} from '../services/backend.service';
import {IFeedbackResponse} from '@oer-converter/api-types';
import {IMoreButtons} from '../header/header.component';
import * as json2csv from 'json2csv';
import {Utils} from '@hexxag0nal/utils';

@Component({
  selector: 'app-see-feedback',
  templateUrl: './see-feedback.component.html',
  styleUrls: ['./see-feedback.component.scss']
})
export class SeeFeedbackComponent implements OnInit {
  public feedback: Promise<IFeedbackResponse[]>;

  public downloadFeedbackButton: IMoreButtons = {
    tooltip: 'Download feedback to your machine',
    callback: this.downloadFeedback.bind(this),
    buttonIcons: ['get_app']
  };

  constructor(
    private _router: Router,
    private _session: SessionServiceService,
    private _backend: BackendService
  ) { }

  ngOnInit() {
    this.feedback = this._backend.getFeedback();
  }

  public goToMain(): void {
    this._router.navigate(['']);
  }

  public getToolbarTitle(): string {
    return 'Feedback for ' + this._session.getToolName();
  }

  public async downloadFeedback(): Promise<void> {
    const fields = ['name', {label: 'feedback', value: 'freeText'}, {label: 'given on', value: 'time'}];

    const json2CSVParser = new json2csv.Parser({delimiter: ';', fields: fields});

    try {
      const _blobFeedback = new Blob([json2CSVParser.parse(await this.feedback)], {
        type: 'text/csv',
        endings: 'native'
      });
      await Utils.saveAs(_blobFeedback, `feedback-${new Date().toISOString()}.csv`);
    } catch (err) {
      console.error(err);
    }
  }

}
