import {Injectable} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {UUID} from '@hexxag0nal/uuid';
import {FileHandler} from '../../classes/file-handler.abstract';
import {MainConfig} from '../../config/main.config';
import {IImageWrapper} from '../../models/image-wrapper';
import {Observable, Subject, ReplaySubject} from 'rxjs';
import {Licenses} from '../../models/licenses';

@Injectable({
  providedIn: 'root'
})
/**
 * Exposes function to manipulate the current session
 */
export class SessionServiceService {
  /**
   * A subject that can be triggered and fed with the currently selected image from file
   */
  private _subjectSelectedImageFromFile: Subject<IImageWrapper> = new ReplaySubject<IImageWrapper>(1);
  /**
   * A subject that can be triggered and fed with the currently selected image from the external search
   */
  private _subjectSelectedImageFromExternalApi: Subject<IImageWrapper> = new ReplaySubject<IImageWrapper>(1);
  /**
   * A subject that can be triggered to clear the selection of the images
   */
  private _subjectClearSelectedImage: Subject<void> = new Subject<void>();
  /**
   * A subject that can be triggered and fed with the current images loaded from the file
   */
  private _subjectImagesFromFile: Subject<IImageWrapper[]> = new Subject<IImageWrapper[]>();
  /**
   * The current document's GUID
   */
  private _currentDocumentGUID: UUID;
  /**
   * This is the name of the author of the file, not the user that converts the file!
   */
  private _currentFileAuthor: string;

  /**
   * Current version of the tool
   */
  private readonly _currentToolVersion: string;

  /**
   * Whether or not the load indicator that shows that some action is going on in the background shall be shown
   */
  private _showLoadIndicator: boolean = false;
  /**
   * Number of asynchronous actions going on in the background
   */
  private _asyncActionsAwaiting: {[key: string]: number} = {};

  /**
   * The text that shall be put onto the proper slide
   */
  private _legalDisclaimerText: string;

  /**
   * Info about the current user using the tool
   */
  private _currentUserInfo = {
    realName: '',
    screenName: '',
  };

  /**
   * Current file handler
   */
  private _currentFileHandler: FileHandler;
  /**
   * Currently extracted images
   */
  private _currentImages: IImageWrapper[];

  /**
   * Used to check whether the session was instantiated by any other component than the main one
   */
  private _hasStartedProperly: boolean = false;

  /**
   * Used to check whether the start component/dialog is to be shown or if the main screen can be used without invoking that dialog
   */
  private _startFromBeginning: boolean = true;

  /**
   * The currently selected image from file
   */
  private _currentlySelectedFromFile: IImageWrapper;

  /**
   * The targeted license of the resulting file
   */
  private _targetLicense: Licenses

  /**
   * Whether the user is done with the current session
   */
  private _doneWithSession: boolean = false;

  /**
   * A bit hacky and not very proper, but the best way achieve this little quick because I'm already hours above the time I get paid for
   */
  private _excludedImages: string[] = [];

  /**
   * @param _loadIndicatorSpinner The load indicator service
   */
  constructor(
    private _loadIndicatorSpinner: NgxSpinnerService
  ) {
    this._currentUserInfo.realName = 'Jane Doe';
    this._currentToolVersion = require( '../../../package.json').version;
  }

  /**
   * Used to check whether the session was instantiated by any other component than the main one
   */
  public startSessionProperly(): void {
    this._hasStartedProperly = true;
  }

  /**
   * Used to check whether the session was instantiated by any other component than the main one
   */
  public hasStartedProperly(): boolean {
    return this._hasStartedProperly;
  }

  /**
   * Check if the tool shall start from the beginning
   */
  public shallStartFromBeginning(): boolean {
    return this._startFromBeginning;
  }

  /**
   * Tell the session that the tool shall start from the beginning again
   */
  public doStartFromBeginning(): void {
    this._startFromBeginning = true;
  }

  /**
   * Tell the session that the tool shall not start from the beginning again if main screen is hit again
   */
  public doNotStartFromBeginning(): void {
    this._startFromBeginning = false;
  }

  /**
   * Add another asynchronous action to wait for
   * @param name The name of the spinner. 'primary' is the default one set by the ngx-spinner if none is specified
   */
  public awaitAsyncAction(name: string = 'primary'): void {
    if (!this._asyncActionsAwaiting.hasOwnProperty(name)) {
      this._asyncActionsAwaiting[name] = 0;
    }
    this._asyncActionsAwaiting[name]++;
    this._assesActionStates();
  }

  /**
   * Tell that one asynchronous action was completed
   * * @param name The name of the spinner. 'primary' is the default one set by the ngx-spinner if none is specified
   */
  public doneAsyncAction(name: string = 'primary'): void {
    // for crash-safety reasons
    if (this._asyncActionsAwaiting.hasOwnProperty(name) && this._asyncActionsAwaiting[name] > 0) {
      this._asyncActionsAwaiting[name]--;
    }
    this._assesActionStates();
  }

  /**
   * Determine if the load indicator needs to be shown
   */
  private _assesActionStates(): void {
    for (const nameOfSpinner in this._asyncActionsAwaiting) {
      if (this._asyncActionsAwaiting[nameOfSpinner] > 0) {
        this._enableLoadIndicator(nameOfSpinner);
      } else {
        this._disableLoadIndicator(nameOfSpinner);
      }
    }
  }

  /**
   * Show the load indicator
   */
  private _enableLoadIndicator(name: string): void {
    this._loadIndicatorSpinner.show(name);
  }

  /**
   * Hide the load indicator
   */
  private _disableLoadIndicator(name: string): void {
    this._loadIndicatorSpinner.hide(name);
  }

  /**
   * Get the current user's real name
   */
  public getCurrentUserRealName(): string {
    return this._currentUserInfo.realName;
  }

  /**
   * Set the current user's real name
   * @param name The name to be set
   */
  public setCurrentUserRealName(name: string): void {
    this._currentUserInfo.realName = name;
  }

  /**
   * Set the current document's GUID
   * @param guid The GUID to be set
   */
  public setCurrentGUID(guid: UUID): void {
    this._currentDocumentGUID = guid;
  }

  /**
   * Get the current document's GUID
   */
  public getCurrentGUID(): UUID {
    return this._currentDocumentGUID;
  }

  /**
   * Get the name of the tool
   */
  public getToolName(): string {
    return MainConfig.nameToolCustom || 'OER Conversion Tool';
  }

  /**
   * Set the file author of the current file
   * @param author The author's name
   */
  public setCurrentFileAuthor(author: string): void {
    this._currentFileAuthor = author;
  }

  /**
   * Get the file author of the current file
   */
  public getCurrentFileAuthor(): string {
    return this._currentFileAuthor;
  }

  /**
   * Get the tool's version
   */
  public getCurrentToolVersion(): string {
    return this._currentToolVersion;
  }

  /**
   * Get the disclaimer text
   */
  public getLegalDisclaimerText(): string {
    return this._legalDisclaimerText;
  }

  /**
   * Set the disclaimer text
   * @param text The disclaimer text to be put onto the proper slide
   */
  public setLegalDisclaimerText(text: string): void {
    this._legalDisclaimerText = text;
  }

  /**
   * Set the current file handler
   * @param fileHandler The file handler instance that is currently used
   */
  public setFileHandler(fileHandler: FileHandler): void {
    this._currentFileHandler = fileHandler;
  }

  /**
   * Get the current file handler
   */
  public getFileHandler(): FileHandler {
    return this._currentFileHandler;
  }

  /**
   * Set the extracted images
   * @param images The images that were extracted
   */
  public setImages(images: IImageWrapper[]): void {
    this._currentImages = images;
    this._subjectImagesFromFile.next(images);
  }

  public getImages(): IImageWrapper[] {
    return this._currentImages;
  }

  /**
   * Returns an observable for the selected image from file
   */
  public observeSelectedImageFromFile(): Observable<IImageWrapper> {
    return this._subjectSelectedImageFromFile.asObservable();
  }

  /**
   * Announce the next selected image from file (actually, it's obviously the current one, but the function is named 'next' thus
   * I named the functions like that as well
   * @param image The next image being selected from file
   */
  public nextSelectedImageFromFile(image?: IImageWrapper): void {
    this._subjectSelectedImageFromFile.next(image);
    this._currentlySelectedFromFile = image;
  }

  /**
   * Get the currently selected image from file
   */
  public getCurrentlySelectedImageFromFile(): IImageWrapper {
    /*return new Promise<IImageWrapper>((resolve, reject) => {
      let image_: IImageWrapper;
      // since under the hood the subject is implemented as a ReplaySubject with buffer size of 1 and the button that triggers this very
      // method is only enabled once there is an active selection of an image from the file, we can assume that that the following trick
      // of subscribing and directly unsubscribing works the same way as if we'd observe the regular way (at a prior point in time)
      // and then save the value to a variable. But this just seems nicer since we can not forget to unsubscribe it upon the
      // service's destroyance.
      this.observeSelectedImageFromFile().subscribe((image: IImageWrapper) => {
        image_ = image;
      }).unsubscribe();
      // var will be initialized due to the while-loop
      if (image_ === null) {
        // then no image is currently selected
        reject();
      } else {
        // var will be initialized due to the while-loop
        resolve(image_);
      }
    });*/

    // The only reason it is implemented as Promise is that there was a previous XXX that went broken. In order to not break anything,
    // I decided to make it async again
    /*return new Promise(((resolve, reject) => {
      if (this._currentlySelectedFromFile) {
        resolve(this._currentlySelectedFromFile);
      } else {
        reject();
      }
    }));*/
    return this._currentlySelectedFromFile;
  }

  /**
   * Returns an observable for the selected image from external API
   */
  public observeSelectedImageFromExternalApi(): Observable<IImageWrapper> {
    return this._subjectSelectedImageFromExternalApi.asObservable();
  }

  /**
   * Announce the next selected image from external api's search results (actually, it's obviously the current one, but the function is
   * named 'next' thus I named the functions like that as well
   * @param image The next image being selected from file
   */
  public nextSelectedImageFromExternalApi(image?: IImageWrapper): void {
    this._subjectSelectedImageFromExternalApi.next(image);
  }

  /**
   * Get the currently selected image from external API
   */
  public async getCurrentlySelectedImageFromExternalApi(): Promise<IImageWrapper> {
    return new Promise<IImageWrapper>((resolve, reject) => {
      let image_: IImageWrapper;
      // since under the hood the subject is implemented as a ReplaySubject with buffer size of 1 and the button that triggers this very
      // method is only enabled once there is an active selection of an image from the external API, we can assume that that the following
      // trick of subscribing and directly unsubscribing works the same way as if we'd observe the regular way (at a prior point in time)
      // and then save the value to a variable. But this just seems nicer since we can not forget to unsubscribe it upon the
      // service's destroyance.
      this.observeSelectedImageFromExternalApi().subscribe((image: IImageWrapper) => {
        image_ = image;
      }).unsubscribe();
      // var will be initialized due to the while-loop
      console.log('sadasd', image_)
      if (image_ == null) {
        // then no image is currently selected
        reject();
      } else {
        // var will be initialized due to the while-loop
        resolve(image_);
      }
    });
  }

  /**
   * Returns an observable for the clearance of selected images
   */
  public observeClearanceOfSelectedImages(): Observable<void> {
    return this._subjectClearSelectedImage.asObservable();
  }

  /**
   * Announce the clearance of any selected image
   */
  public clearSelectedImages(): void {
    this._subjectClearSelectedImage.next();
  }

  /**
   * Returns an observable for the images loaded from file
   */
  public observeImagesFromFile(): Observable<IImageWrapper[]> {
    return this._subjectImagesFromFile.asObservable();
  }

  /**
   * Announce the next images loaded from file (actually, it's obviously the current one, but the function is
   * named 'next' thus I named the functions like that as well
   * @param images The next images being loaded from file
   */
  public nextImagesFromFile(images?: IImageWrapper[]): void {
    this._currentImages = images;
    this._subjectImagesFromFile.next(images);
  }

  /**
   * Set the target license for the resulting file
   */
  public setTargetLicense(license: Licenses): void {
    this._targetLicense = license;
  }

  /**
   * Get the target license for the resulting file
   */
  public getTargetLicense(): Licenses {
    return this._targetLicense;
  }

  /**
   * Check if the session is done/has ended
   */
  public isSessionDone(): boolean {
    return this._doneWithSession;
  }

  /**
   * Mark the current session as done
   */
  public markSessionAsDone(): void {
    this._doneWithSession = true;
  }

  /**
   * Exclude an image from the overall license, i.e. a logo
   * @param description I.e. 'Logo of the RWTH Aachen'
   */
  public excludeImageFromOverallLicense(description: string): void {
    this._excludedImages.push(description);
  }

  /**
   * Get a string that says which images are excluded from the overall license
   */
  public getTextOfExcludedImages(): string {
    if (this._excludedImages.length === 0) {
      return undefined;
    } else {
      let result = 'Excluded';
      if (this._excludedImages.length === 1) {
        result += ' is ';
      } else {
        result += ' are ';
      }
      for (let i = 0; i < this._excludedImages.length; i++) {
        result += this._excludedImages[i];
        if (i !== this._excludedImages.length - 1) {
          result += ', ';
        }
      }
      result += '.\nThe rights are reserved by the respective owners.';
      return result;
    }
  }
}
