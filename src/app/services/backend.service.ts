import { Injectable } from '@angular/core';
import {Receipt, History} from '../../models/receipt.model';
import {HttpService} from './http.service';
import {RequestMadeOerAction, RequestReplacementAction, ResponseGetMadeOerImages, ResponseGetReplacementImages} from '../../models/backend';
import {URL} from '@hexxag0nal/universal-ressource';
import {MainConfig} from '../../config/main.config';
import {MD5} from '@hexxag0nal/crypto';
import {HttpErrorResponse} from '@angular/common/http';
import {DataUrl} from '@hexxag0nal/data-url';
import {
  IFeedbackRequest,
  IFeedbackResponse,
  IRequestRecognizeImageBase64DataUrl,
  ITensorFlowImageClassifications
} from '@oer-converter/api-types';

@Injectable({
  providedIn: 'root'
})
/**
 * Abstracts the communication with the backend
 */
export class BackendService {
  /**
   * The address of the backend
   */
  private _apiUrl = new URL(MainConfig.backend.url + ':' + MainConfig.backend.port);

  /**
   * @param _http The HttpService of ours
   */
  constructor(private _http: HttpService) {
  }

  /**
   * Send the receipt to the backend
   * @param receipt The receipt to be sent
   */
  public async sendReceipt(receipt: Receipt): Promise<void> {
    const history: History = receipt.getHistory();
    for (const replacementAction of history.actions.replacement) {
      if (replacementAction.doNotUpload) {
        continue;
      }
      const _requestParams: RequestReplacementAction = {
        document: {
          guid: receipt.getDocumentGUID().toString(),
          internalPath: replacementAction.wasReplaced.internalPath,
          mimeType: receipt.getDocumentMIMEType().toString()
        },
        replacedImage: {
          checksum: replacementAction.wasReplaced.checksum
        },
        replacingImage: {
          sourceApi: replacementAction.isReplacing.sourceApi,
          sourceUrl: replacementAction.isReplacing.sourceUrl,
          authorUserName: replacementAction.isReplacing.authorUserName,
          authorRealName: replacementAction.isReplacing.authorRealName,
          width: replacementAction.isReplacing.width,
          height: replacementAction.isReplacing.height,
          checksum: replacementAction.isReplacing.checksum,
          archiveUrl: replacementAction.isReplacing.archiveUrl,
          license: {
            // the backend expects the name of the CC-0 license like that
            name: replacementAction.isReplacing.license.name === 'CC-0' ? 'CC0' : replacementAction.isReplacing.license.name,
            version: replacementAction.isReplacing.license.version
          },
          edited: replacementAction.isReplacing.edited ? {
            width: replacementAction.isReplacing.edited.width,
            height: replacementAction.isReplacing.edited.height,
            dataUrlBase64: replacementAction.isReplacing.edited.dataUrlBase64.toString(),
            checksum: replacementAction.isReplacing.edited.checksum
          } : undefined
        }
      };
      await this._http.post(this._apiUrl.toString() + 'replace/image', _requestParams);
    }
    for (const madeOerAction of history.actions.publishAsOer) {
      if (madeOerAction.doNotUpload) {
        continue;
      }
      const _requestParams: RequestMadeOerAction = {
        document: {
          guid: receipt.getDocumentGUID().toString(),
          mimeType: receipt.getDocumentMIMEType().toString()
        },
        dataUrlBase64: madeOerAction.dataUrlBase64.toString(),
        checksum: madeOerAction.checksum,
        width: madeOerAction.width,
        height: madeOerAction.height,
        license: {
          name: madeOerAction.license.name === 'CC-0' ? 'CC0' : madeOerAction.license.name,
          version: madeOerAction.license.version
        },
        author: madeOerAction.author,
        title: madeOerAction.title
      };
      await this._http.post(this._apiUrl.toString() + 'oer/image', _requestParams);
    }
  }

  /**
   * Check if an image with a given checksum was already replaced by some other image in the past
   * @param checksum The checksum of the image we want to check if it was replaced before
   */
  public async checkIfImageByChecksumWasReplacedBefore(checksum: MD5): Promise<ResponseGetReplacementImages[]> {
    // let wasNotFound = false;
    // TODO: adapt this when implemented the 404 response for a checksum that is not known
    /*const result = await this._http.get(this._apiUrl.toString() + `replace/image/${checksum}`).catch((reason: HttpErrorResponse) => {
      wasNotFound = reason.status === 404;
    });*/
    const result: ResponseGetReplacementImages[]  = await this._http.get(this._apiUrl.toString() + `replace/image/${checksum}`);
    return ((result && result.length !== 0) ? result : null);
  }

  /**
   * Check if an image with a given checksum was declared as OER in the past
   * @param checksum The checksum of the image we want to check if it was replaced before
   */
  public async checkIfImageByChecksumWasMadeOerBefore(checksum: MD5): Promise<ResponseGetMadeOerImages[]> {
    let notFound: boolean = false;
    const result: ResponseGetMadeOerImages[]  = await this._http.get(this._apiUrl.toString() + `oer/image/${checksum}`).catch((reason: HttpErrorResponse) => {
      if (reason.status === 404) {
        notFound = true;
      }
    });
    return ((!notFound && result && result.length !== 0) ? result : null);
  }

  /**
   * Get search parameters based on the content of the image
   * @param base64DataUrl This data url contains the image in base64 encoded form
   */
  public async getImageClassifications(base64DataUrl: DataUrl): Promise<ITensorFlowImageClassifications> {
    return (await this._http.post<ITensorFlowImageClassifications>(this._apiUrl.toString() + 'recognize/image', {
      dataUrlBase64: base64DataUrl.toString()
    } as IRequestRecognizeImageBase64DataUrl)).body;
  }

  /**
   * Get the feedback for the tool
   */
  public async getFeedback(): Promise<IFeedbackResponse[]> {
    return await this._http.get(this._apiUrl.toString() + `feedback`);
  }

  /**
   * Save a feedback to the backend
   */
  public async saveFeedback(text: string, name?: string): Promise<void> {
    await this._http.post<any, IFeedbackRequest>(this._apiUrl.toString() + 'feedback', {freeText: text, name: name});
  }
}
