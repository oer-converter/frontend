import { Injectable } from '@angular/core';
import {ToastOptions} from '../../models/ToastOptions';
import {MatSnackBar} from '@angular/material';
import {ToastsConfig} from '../../config/toasts.config';

@Injectable({
  providedIn: 'root'
})
/**
 * Exposes functions to show toasts
 */
export class ToastService {

  /**
   * @param _snackBar The MatSnackBar provided by angular
   */
  constructor(
    private _snackBar: MatSnackBar
  ) { }

  /**
   * Show a toast
   * @param message The message that the toast shall bear
   * @param opts Some further options
   */
  public show(message: string, opts?: ToastOptions): void {
    if (opts) {
      // set to default values if missing from opts
      if (!opts.duration) {
        opts.duration = ToastsConfig.duration;
      }

      this._snackBar.open(message, opts.actionMessage, opts);
    } else {
      this._snackBar.open(message, null, {duration: ToastsConfig.duration});
    }
  }
}
