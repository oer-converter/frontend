import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, take } from 'rxjs/operators'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Feedback } from '../../classes/feedback.model';

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json'
//   })
// }

@Injectable({
  providedIn: 'root'
})

// @Injectable()
export class FeedbackService {
  selectedFeedback: Feedback;
  feedbacks: Feedback[];
  // readonly baseURL= 'http://localhost:3000/feedbacks';
  baseURL= 'http://localhost:3000/feedbacks';


  constructor(private http: HttpClient) { }

  postFeedback(feedModel: Feedback) {
    console.log(feedModel)
    return this.http.post(this.baseURL, feedModel);
  }




  // // posts feedback data to database...
  // addFeedback(feedback):Observable<any> {
  //   return this.http.post(this.baseUrl, feedback, httpOptions);
  // }




}
