/*
https://medium.com/@dazcyril/unzip-files-in-the-browser-with-angular-7-fcdc3040f3c1
 */

// TODO: merge with JsZipHelper #107

import { Injectable } from '@angular/core';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver';
import {JsZipHelper} from '../../classes/js-zip-helper';

@Injectable({
  providedIn: 'root'
})
export class ZipService {

  constructor() {
  }

  public getContent(file: File): Promise<JSZip> {
    return JsZipHelper.getContent(file, {base64: true});
  }

  /**
   * This will save an image encoded in Base64 into the zip file. If name/filePath already exists, it will be overwritten!
   * @param refToZip A reference to the JSZip object returned by getContent()
   * @param image Object holding the name as full filePath + image name and the image encoded in Base64
   */
  public addBase64Image(refToZip: JSZip, image: {nameAndFullPath: string, imageAsBase64: string}): void {
    JsZipHelper.addBase64Image(refToZip, image);
  }

  /**
   * Same as addBase64Image but takes an url instead of the image as Base64
   * @param refToZip A reference to the JSZip object returned by getContent()
   * @param image Object holding the name as full filePath + image name and the image url
   */
  public addImageFromUrl(refToZip: JSZip, image: {nameAndFullPath: string, urlToImage: string}): void {
    // check if no error occured by checking if the filePath is correct
    const _imgPath = image.nameAndFullPath;
    if (refToZip.files.hasOwnProperty(_imgPath)) {
      const _pathArray = _imgPath.split('/');
      // the slice thingy puts only the last element (which would be the file name) in a new array and then calling
      // the zeroest element makes it a string, such that the split method can be used
      const _fileNameArray = (_pathArray.slice(-1)[0]).split('.');
      // since _fileNameArray will always have [0]: file name and [1]: file ending, it is safe to use it this way
      const _fileEndingOfTheImageFileThatIsToBeReplaced = _fileNameArray[1];
      const _base64ImagePrefix = `data:image/${_fileEndingOfTheImageFileThatIsToBeReplaced};base64,`;
      // const _pureBase64 = this._selectedImageFromFile.base64

      // big props and credits to
      // Balvinder Singh, https://medium.com/@erbalvindersingh/fetch-an-image-from-url-in-angular-4-and-store-locally-4928d3b504fc
      //
      // creating a new image, load the external image into that img
      // Then create a canvas, load the image into the canvas and convert the canvas into a base64 string
      // TODO: use Utils.imageUrlToBase64DataUrl()
      let img = new Image();
      img.crossOrigin = 'anonymous';
      img.src = image.urlToImage;
      img.onload = () => {
        let canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        let ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);

        // TODO: use the DataUrl object from Utils

        // [0] will be base64 prefix, something like 'data:image/png;base64', whereas [1] contains the image encoded as base64
        const _base64Image = (canvas.toDataURL(`image/${_fileEndingOfTheImageFileThatIsToBeReplaced}`)).split(',')[1];

        // release the occupied references
        img = null;
        canvas = null;
        ctx = null;

        this.addBase64Image(refToZip, {nameAndFullPath: _imgPath, imageAsBase64: _base64Image});
      };
    }
  }

  /**
   * Saving the zip to a file, meaning that the user will be prompted a 'save file as'-window for the altered file
   * @param refToZip A reference to the JSZip object returned by getContent()
   * @param options An object holding the file name with the correct file ending that the 'save file as'-window will use as default
   */
  public async saveToFile(refToZip: JSZip, options?: {fileName?: string}): Promise<void> {
    return JsZipHelper.zipToFile(refToZip, options);
  }
}
