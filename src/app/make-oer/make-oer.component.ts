import {Component, OnInit} from '@angular/core';
import {EImageRole, IImageWrapper} from '../../models/image-wrapper';
import {IShowFromFileOptions} from '../compare-images/show-from-file/show-from-file.component';
import {IPublishAsOerActionParams} from '../../classes/file-handler.abstract';
import {SessionServiceService} from '../services/session-service.service';
import {Router} from '@angular/router';
import {IOerFormularValues} from '../oer-formular/oer-formular.component';

interface ILicensesDropDownMenu {
  name: string;
  version: string;
  deeds: string;
  enum: number;
}

// for now, those interfaces are equivalent, but I guess it might change in the future
export interface IMakeOerParams extends IPublishAsOerActionParams {}

@Component({
  selector: 'app-make-oer',
  templateUrl: './make-oer.component.html',
  styleUrls: ['./make-oer.component.scss']
})
export class MakeOerComponent implements OnInit {

  public readonly showFromFileComponentOptions: IShowFromFileOptions = {
    hideSizeInfo: true
  };

  constructor(
    private _session: SessionServiceService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  public makeOer(form?: IOerFormularValues): void {
    // A license is always present. Used to differ it from the SubmitEvent which is always fired shortly thereafter
    if (form.license) {
      const shortcutChosenLicense: ILicensesDropDownMenu = form.license;
      const images = this._session.getImages();
      const selectedImageFromFile = this._session.getCurrentlySelectedImageFromFile();
      const _indexInArray = images.findIndex(elem => elem === selectedImageFromFile);
      images.splice(_indexInArray, 1);
      this._session.nextImagesFromFile(images);
      this._session.nextSelectedImageFromFile();
      const params: IMakeOerParams = {
        author: form.author,
        title: form.title,
        affiliation: form.authorAffiliation,
        license: {
          enum: shortcutChosenLicense.enum,
          version: shortcutChosenLicense.version
        },
        description: form.description,
        originUrl: form.originUrl,
        originName: form.originName,
        authorUrl: form.authorUrl,
        role: form.imageRole
      };
      if (form.imageRole === EImageRole.screenshot) {
        params.screenshotData = {
          date: form.screenshot.date,
          description: form.screenshot.description
        };
      }
      this._session.getFileHandler().EnqueuePublishAsOer(selectedImageFromFile, params);
      this.goToMain();
    }
  }

  public getCurrentlySelectedImageFromFile(): IImageWrapper {
    return this._session.getCurrentlySelectedImageFromFile();
  }

  public goToMain(): void {
    this._router.navigate(['']);
  }

  public getToolbarTitle(): string {
    return this._session.getToolName() + ' ' + this._session.getCurrentToolVersion();
  }
}
