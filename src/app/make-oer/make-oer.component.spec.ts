import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeOerComponent } from './make-oer.component';

describe('MakeOerComponent', () => {
  let component: MakeOerComponent;
  let fixture: ComponentFixture<MakeOerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeOerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeOerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
