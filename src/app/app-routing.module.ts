import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainComponent} from './main/main.component';
import {SearchComponent} from './search-external-api/search/search.component';
import {SessionProperlyStartedGuard} from './route-guards/session-properly-started.guard';
import {MakeOerComponent} from './make-oer/make-oer.component';
import {ReplaceByImageFromMachineComponent} from './replace-by-image-from-machine/replace-by-image-from-machine.component';
import {FeedbackComponent} from './feedback/feedback.component';
import {ReadyForFeedbackGuard} from './route-guards/ready-for-feedback.guard';
import {SeeFeedbackComponent} from './see-feedback/see-feedback.component';
import {ExcludeFromOverallLicenseComponent} from './exclude-from-overall-license/exclude-from-overall-license.component';

const routes: Routes = [
  /*{ path: '', redirectTo: '/main', pathMatch: 'full' },*/
  { path: '', component: MainComponent },
  { path: 'search-api', component: SearchComponent, canActivate: [SessionProperlyStartedGuard]},
  { path: 'publish-as-oer', component: MakeOerComponent, canActivate: [SessionProperlyStartedGuard]},
  { path: 'replace-by-from-machine', component: ReplaceByImageFromMachineComponent, canActivate: [SessionProperlyStartedGuard]},
  { path: 'exclude', component: ExcludeFromOverallLicenseComponent, canActivate: [SessionProperlyStartedGuard]},
  { path: 'feedback', component: FeedbackComponent, canActivate: [ReadyForFeedbackGuard]},
  { path: '__read-feedback__', component: SeeFeedbackComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
