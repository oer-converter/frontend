import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomSheetGenericComponent } from './bottom-sheet-generic.component';

describe('BottomSheetGenericComponent', () => {
  let component: BottomSheetGenericComponent;
  let fixture: ComponentFixture<BottomSheetGenericComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomSheetGenericComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSheetGenericComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
