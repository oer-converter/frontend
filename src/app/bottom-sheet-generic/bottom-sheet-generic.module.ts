import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BottomSheetGenericComponent, IBottomSheetGenericListOptions, IBottomSheetGenericOptions} from './bottom-sheet-generic.component';
import {MatButtonToggleModule, MatListModule} from '@angular/material';



@NgModule({
  declarations: [BottomSheetGenericComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonToggleModule
  ],
  exports: [BottomSheetGenericComponent],
})
export class BottomSheetGenericModule { }
