import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';
import {Subject} from 'rxjs';
import {Test} from 'tslint';

@Component({
  selector: 'app-bottom-sheet-generic',
  templateUrl: './bottom-sheet-generic.component.html',
  styleUrls: ['./bottom-sheet-generic.component.scss']
})
export class BottomSheetGenericComponent<DATA = any, SUBJECT_DATA = DATA> implements OnInit {

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: IBottomSheetGenericOptions,
    /*private _bottomSheetRef: MatBottomSheetRef<BottomSheetGenericComponent<DATA>>*/
  ) { }

  ngOnInit() {
  }

}

export interface IBottomSheetGenericOptions<DATA = any, SUBJECT_DATA = DATA> {
  data: DATA;
  list: IBottomSheetGenericListOptions<SUBJECT_DATA>[];
}

export interface IBottomSheetGenericListOptions<SUBJECT_DATA = void> {
  onClick: Subject<SUBJECT_DATA>;
  text: string;
  subText: string;
}
