import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFromExternalApiComponent } from './show-from-external-api.component';

describe('ShowFromExternalApiComponent', () => {
  let component: ShowFromExternalApiComponent;
  let fixture: ComponentFixture<ShowFromExternalApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowFromExternalApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFromExternalApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
