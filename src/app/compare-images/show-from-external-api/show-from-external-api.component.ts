import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {IImageWrapper} from '../../../models/image-wrapper';
import {LicensesHumanReadable} from '../../../models/licenses';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DialogEditImageComponent, IDialogEditImageComponentOptions} from '../dialog-edit-image/dialog-edit-image.component';
import {subscriptionLogsToBeFn} from 'rxjs/internal/testing/TestScheduler';
import {MainConfig} from '../../../config/main.config';

@Component({
  selector: 'app-show-from-external-api',
  templateUrl: './show-from-external-api.component.html',
  styleUrls: ['./show-from-external-api.component.scss', '../compare-images.component.scss']
})
export class ShowFromExternalApiComponent implements OnInit, OnDestroy {
  @Input('image') image$: Observable<IImageWrapper>;
  @Output('edit-image') editImageEventEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Input('disable-edit-button') isEditButtonDisabled$: Observable<boolean>;

  @ViewChild('ImageViewer', {static: false}) refImageViewer: ElementRef;
  @ViewChild('Image', {static: false}) refImage: ElementRef;

  private _imageRatio = 1; // init with default ratio of 1
  private _imageViewerRatio = 1; // init with default ratio of one

  private _subscriptionImage: Subscription;

  public humanReadableLicenses = LicensesHumanReadable;

  public imgHeight: string = '100%';

  private _dialogEditImageRef: MatDialogRef<DialogEditImageComponent>;

  private _currentImage: IImageWrapper = null;

  public mainConfig = MainConfig;

  constructor(
    private _sanitizer: DomSanitizer,
    private _rawDialog: MatDialog
  ) { }

  /**
   * subscribe to the image$ observable. Update held info about selected image.
   */
  ngOnInit() {
    const _setImageViewerRatio = () => {
      const _elemImageViewer: HTMLElement = (this.refImageViewer.nativeElement as HTMLElement);
      this._imageViewerRatio = _elemImageViewer.clientWidth / _elemImageViewer.clientHeight;
    };
    this._subscriptionImage = this.image$.subscribe(image => {
      if (image) {
        this._currentImage = image;
        this._imageRatio = image.dimension.ratio;
        this.imgHeight = '100%';
        // Since the whole div is hidden by an ngIf* statement, it needs to be initialized upon the first time this subscription is fired.
        // Thus, putting the work a millisecond into the future is already enough to find the requested element. Without the timeout, it
        // would be 'undefined' or 'null' the first time this subscription is triggered. So I'll use the timeout variant if it is still
        // 'null' or 'undefined', but recede from using it once it was already initialized. Merely for performance reasons, even though I do
        // not think that is matters, judging by the scale.
        if (this.refImageViewer) {
          // if it is NOT 'null' nor 'undefined'
          _setImageViewerRatio();
        } else {
          // if it is 'null' or 'undefined'
          setTimeout(() => {
            _setImageViewerRatio();
          }, 1);
        }
        // set this one ms farther into the future, otherwise the height of the img will get stuck
        setTimeout(() => {
          this.imgHeight = (this.refImage.nativeElement as HTMLImageElement).height + 'px';
        }, 3);
      }
    });
  }

  /**
   * unsubscribe to avoid memory leakage
   */
  ngOnDestroy(): void {
    this._subscriptionImage.unsubscribe();
  }

  /**
   * Determines the needed CSS class. Those classes make the image stay within the boundaries of the divs but keep their ratio
   */
  public getCssClass(): string {
    if (this._imageRatio < this._imageViewerRatio) {
      return 'image-ratio-smaller-than-one';
    }
    return 'image-ratio-at-least-one';
  }

  public triggerOpenEditImageDialog(): void {
    this.editImageEventEmitter.emit();
  }
}
