import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {IImageWrapper} from '../../models/image-wrapper';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DialogWarningImagesRatioMismatchComponent} from '../dialog-warning-images-ratio-mismatch/dialog-warning-images-ratio-mismatch.component';
import {DialogEditImageComponent, IDialogEditImageComponentOptions} from './dialog-edit-image/dialog-edit-image.component';
import {MainConfig} from '../../config/main.config';

@Component({
  selector: 'app-compare-images',
  templateUrl: './compare-images.component.html',
  styleUrls: ['./compare-images.component.scss']
})
export class CompareImagesComponent implements OnInit, OnDestroy {
  @Input('image-from-file') imageFromFile$: Observable<IImageWrapper>;
  private _subImageFromFile: Subscription;
  private _selectedFromFile: IImageWrapper;

  @Input('image-from-api') imageFromApi$: Observable<IImageWrapper>;
  private _subImageFromApi: Subscription;
  private _selectedFromAPi: IImageWrapper;

  @Output('replace') replaceActionEventEmitter: EventEmitter<void> = new EventEmitter<void>();

  @Output('edit-external-image') editExternalImageEventEmitter: EventEmitter<void> = new EventEmitter<void>();

  public buttonActionReplaceImageDisabled = true;
  private _isSelectedFromFile = false;
  private _isSelectedFromApi = false;

  private _subDialogWarningRatioMismatchAfterClose: Subscription;
  private _ignoreRatioMismatch = false;

  public subjectIsEditButtonOfImageFromExternalApiDisabled: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(
    private _dialogWarningRatioMismatch: MatDialog,
  ) { }

  ngOnInit() {
    this._subImageFromFile = this.imageFromFile$.subscribe((image: IImageWrapper) => {
      if (image) {
        this._selectedFromFile = image;
        this._isSelectedFromFile = true;
        this.subjectIsEditButtonOfImageFromExternalApiDisabled.next(false);
        // if both images were selected, enable the replace button
        if (this._isSelectedFromFile && this._isSelectedFromApi) {
          this.buttonActionReplaceImageDisabled = false;
        }
      }
    });
    this._subImageFromApi = this.imageFromApi$.subscribe((image: IImageWrapper) => {
      if (image) {
        this._selectedFromAPi = image;
        this._isSelectedFromApi = true;
        // if both images were selected, enable the replace button
        if (this._isSelectedFromFile && this._isSelectedFromApi) {
          this.buttonActionReplaceImageDisabled = false;
        }
      }
    });
  }

  ngOnDestroy(): void {
    // cancel subscription upon destroy cycle (if still subscribed) to avoid memory leaks
    this._unsubImageSelections();
    this._subDialogWarningRatioMismatchAfterClose.unsubscribe();
  }

  private _unsubImageSelections(): void {
    this.subjectIsEditButtonOfImageFromExternalApiDisabled.next(true);
    if (this._subImageFromApi) {
      this._subImageFromApi.unsubscribe();
      this._subImageFromApi = null;
    }
    if (this._subImageFromFile) {
      this._subImageFromFile.unsubscribe();
      this._subImageFromFile = null;
    }
  }

  public replaceImage(): void {
    if (!this._ignoreRatioMismatch && !this._ratiosDoMatch()) {
      // if the ratios do not match, and it shall not be ignored, then prompt the user a dialog to let him know about the issue and decide
      this._openDialogWarningRatioMismatch();
    } else {
      this.replaceActionEventEmitter.emit();
      this._resetStates();
    }
  }

  private _ratiosDoMatch(): boolean {
    const _ratioImgFromFile = this._selectedFromFile.dimension.ratio;
    // If there is info about an edit, then use the new ratio, otherwise use the original one
    // tslint:disable-next-line:max-line-length
    const _ratioImgFromExtApi = this._selectedFromAPi.edited ? this._selectedFromAPi.edited.dimension.ratio : this._selectedFromAPi.dimension.ratio;

    // tslint:disable-next-line:max-line-length
    return _ratioImgFromExtApi >= (_ratioImgFromFile - MainConfig.imageRatioTolerance) && _ratioImgFromExtApi <= (_ratioImgFromFile + MainConfig.imageRatioTolerance);
  }

  private _openDialogWarningRatioMismatch(): void {
    const _dialogRef = this._dialogWarningRatioMismatch.open(DialogWarningImagesRatioMismatchComponent);

    this._subDialogWarningRatioMismatchAfterClose = _dialogRef.afterClosed().subscribe((choice: boolean) => {
      if (choice) {
        this.subjectIsEditButtonOfImageFromExternalApiDisabled.next(true);
        this._ignoreRatioMismatch = true;
        this.replaceImage();
      }
    });
  }

  /**
   * Reset states such that we achieve a clean slate and the button is disabled again
   * @private
   */
  private _resetStates(): void {
    this._selectedFromAPi = null;
    this._isSelectedFromApi = false;
    this._selectedFromFile = null;
    this._isSelectedFromFile = false;
    this.buttonActionReplaceImageDisabled = true;
    this._ignoreRatioMismatch = false;
  }
}
