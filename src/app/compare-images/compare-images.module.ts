import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompareImagesComponent} from './compare-images.component';
import {ShowFromExternalApiComponent} from './show-from-external-api/show-from-external-api.component';
import {ShowFromFileComponent} from './show-from-file/show-from-file.component';
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatTooltipModule
} from '@angular/material';
import {ImageCropperModule} from 'ngx-image-cropper';
import {DialogEditImageComponent} from './dialog-edit-image/dialog-edit-image.component';
import {LoadingSpinnerModule} from '../loading-spinner/loading-spinner.module';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    ImageCropperModule,
    MatButtonModule,
    MatDialogModule,
    MatTooltipModule,
    LoadingSpinnerModule
  ],
  declarations: [CompareImagesComponent, ShowFromExternalApiComponent, ShowFromFileComponent, DialogEditImageComponent],
  exports: [CompareImagesComponent]
})

export class CompareImagesModule {}
