import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IImageWrapper} from '../../../models/image-wrapper';
import {Observable, Subscription} from 'rxjs';
import {MainConfig} from '../../../config/main.config';

export interface IShowFromFileOptions {
  hideSizeInfo?: boolean;
}

@Component({
  selector: 'app-show-from-file',
  templateUrl: './show-from-file.component.html',
  styleUrls: ['./show-from-file.component.scss', '../compare-images.component.scss']
})
export class ShowFromFileComponent implements OnInit, OnDestroy {
  @Input('image') image$: Observable<IImageWrapper>;

  @Input('options') componentOptions: IShowFromFileOptions;

  @ViewChild('ImageViewer', {static: false}) refImageViewer: ElementRef;

  private _imageRatio = 1; // init with default ratio of 1
  private _imageViewerRatio = 1; // init with default ratio of one

  private _subscriptionImage: Subscription;

  public mainConfig = MainConfig;

  constructor() { }

  /**
   * subscribe to the image$ observable. Update held info about selected image.
   */
  ngOnInit() {
    const _setImageViewerRatio = () => {
      const _elemImageViewer: HTMLElement = (this.refImageViewer.nativeElement as HTMLElement);
      this._imageViewerRatio = _elemImageViewer.clientWidth / _elemImageViewer.clientHeight;
    };
    this._subscriptionImage = this.image$.subscribe(image => {
      if (image) {
        this._imageRatio = image.dimension.ratio;
        // Since the whole div is hidden by an ngIf* statement, it needs to be initialized upon the first time this subscription is fired.
        // Thus, putting the work an ms into the future is already enough to find the requested element. Without the timeout, it would be
        // 'undefined' or 'null' the first time this subscription is triggered. So I'll use the timeout variant if it is still 'null'
        // or 'undefined', but recede from using it once it was already initialized. Merely for performance reasons, even though I do not
        // think that is matters, judging by the scale.
        if (this.refImageViewer) {
          // if it is NOT 'null' nor 'undefined'
          _setImageViewerRatio();
        } else {
          // if it is 'null' or 'undefined'
          setTimeout(() => {
            _setImageViewerRatio();
          });
        }
      }
    });
  }

  /**
   * unsubscribe to avoid memory leakage
   */
  ngOnDestroy(): void {
    this._subscriptionImage.unsubscribe();
  }

  /**
   * Determines the needed CSS class. Those classes make the image stay within the boundaries of the divs but keep their ratio
   */
  public getCssClass(): string {
    if (this._imageRatio < this._imageViewerRatio) {
      return 'image-ratio-smaller-than-one';
    }
    return 'image-ratio-at-least-one';
  }

}
