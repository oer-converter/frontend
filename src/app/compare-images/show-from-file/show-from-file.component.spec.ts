import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFromFileComponent } from './show-from-file.component';

describe('ShowFromFileComponent', () => {
  let component: ShowFromFileComponent;
  let fixture: ComponentFixture<ShowFromFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowFromFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
