import {Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable, Subscription} from 'rxjs';
import {EditingAction, IImageWrapper} from '../../../models/image-wrapper';
import {ImageCroppedEvent, ImageTransform} from 'ngx-image-cropper';
import {SessionServiceService} from '../../services/session-service.service';
import {DataUrl} from '@hexxag0nal/data-url';

/**
 * Props to Freaky Jolly @ https://www.freakyjolly.com/angular-image-uploader-with-cropper-and-preview-tutorial-with-example/
 */


export interface IDialogEditImageComponentOptions {
  image: IImageWrapper;
  targetAspectRatio: number;
  useBase64DataUrl?: boolean;
}

export interface IDialogEditImageReturnParams {
  base64: DataUrl;
  dimension: {
    width: number;
    height: number;
    ratio: number;
  };
  action: EditingAction[];
}

@Component({
  selector: 'app-dialog-edit-image',
  templateUrl: './dialog-edit-image.component.html',
  styleUrls: ['./dialog-edit-image.component.scss']
})
export class DialogEditImageComponent implements OnInit {
  @ViewChild('Image', {static: false}) refImage: ElementRef;

  public selectedImageFromExternalApi$: Observable<IImageWrapper>;

  /**
   * When 'true' is passed, then the image has already been edited before and should have the corresponding field on the IImageWrapper object
   */
  @Input('dirty') _passedImageIsAlreadyEdited: boolean;

  public imgCssClass: string = null;
  public imgSrc: {url: string; base64: DataUrl} = {
    url: null,
    base64: null
  };
  public imgTransform: ImageTransform = {
    scale: 1,
    rotate: 0,
    flipH: false,
    flipV: false
  };
  public canvasRotation = 0;
  public cropperAspectRatio = 1;

  imageState: IDialogEditImageReturnParams = {
    base64: null,
    dimension: {
      height: null,
      width: null,
      ratio: null
    },
    action: []
  };

  imageChangedEvent: any = '';
  croppedImage: any = '';

  constructor(
    private _dialogEditImage: MatDialogRef<DialogEditImageComponent>,
    @Inject(MAT_DIALOG_DATA) private _dataPassedToDialog: IDialogEditImageComponentOptions,
    private _session: SessionServiceService
  ) { }

  ngOnInit() {
    this._session.awaitAsyncAction('edit-dialog-loading-image');
    const _data = this._dataPassedToDialog;
    this.imgSrc.url = _data.useBase64DataUrl ? _data.image.base64.toString() : _data.image.source;
    if (this._dataPassedToDialog.image.dimension.ratio >= 1) {
      this.imgCssClass = 'image-ratio-at-least-one';
    } else {
      this.imgCssClass = 'image-ratio-smaller-than-one';
    }

    // set aspect ratio of the cropper area
    // just safety check
    if (this._dataPassedToDialog!.targetAspectRatio) {
      this.cropperAspectRatio = this._dataPassedToDialog.targetAspectRatio;
    }
  }

  /**
   * Fired when the cropper is done loading the image. Stops the async action animation
   */
  public imageLoaded(): void {
    this._session.doneAsyncAction('edit-dialog-loading-image');
  }

  /**
   * Fired when the image is cropped
   */
  public imageCropped(event: ImageCroppedEvent): void {
    this.imageState.base64 = DataUrl.fromString(event.base64);
    this.imageState.dimension.height = event.height;
    this.imageState.dimension.width = event.width;
    this.imageState.dimension.ratio = event.width / event.height;
  }

  public cropperReady(): void {
    // cropper ready
    // this._session.doneAsyncAction();
  }

  public rotate90Clockwise(): void {
    this.canvasRotation = (this.canvasRotation + 1) % 4;
  }

  public rotate90CounterClockwise(): void {
    this.canvasRotation = (this.canvasRotation - 1) % 4;
  }

  public processEditAndCloseDialog(): void {
    if (this.canvasRotation !== 0) {
      this.imageState.action.push('rotated');
    }

    this.imageState.action.push('cropped');

    this._dialogEditImage.close(this.imageState);
  }
}
