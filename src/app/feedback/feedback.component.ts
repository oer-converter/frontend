import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { SessionServiceService } from '../services/session-service.service';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ToastService } from '../services/toast.service';
import { BackendService } from '../services/backend.service';
// added by Mo...
import {coerceNumberProperty} from '@angular/cdk/coercion';
import { FeedbackService } from '../services/feedback.service';



@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
  providers: [FeedbackService]
})

export class FeedbackComponent implements OnInit {
  // @Output() addFeedback: EventEmitter<any> = new EventEmitter();
  // public formGroup: FormGroup;
  
  // // feedback format .... added my Mohib ************
  // feedback = {
  //   wasHelpful: '',
  //   uiIsGood: '',
  //   iWillUseIt: '',
  //   recommendOthers: '',
  //   suggestions: ''
  // }
  // //************************ */


  private _questions: string = 'How helpful was this tool? (1 - not really << to >> 5 - very much):\n\n\n' +
    'How would you rate the user interface? (1 - did not like it << to >> 5 - really liked it):\n\n\n' +
    'Would you use this tool to publish your educational ressources as OER? (yes <<>> no):\n\n\n' +
    'Would you recommend this tool to anybody else? (yes <<>> no):\n\n\n' +
    'Do you have any other ideas or thoughts regarding the tool? Something that could be improved? (free text):';

  constructor(
    private _session: SessionServiceService,
    private _toast: ToastService,
    private _backend: BackendService,
    public feedbackService: FeedbackService   // service added my Mohib.
  ) { }

  ngOnInit() {
    this.resetForm();

    // this.formGroup = new FormGroup({
    //   freeText: new FormControl(this._questions, Validators.required),
    //   name: new FormControl()
    // });
  }


  // ********************************************************************
  resetForm(form?: NgForm) {
    if (form)
      form.reset();

      this.feedbackService.selectedFeedback = {
        _id: "",
        wasHelpful: "",
        uiIsGood: "",
        iWillUseIt: "",
        recommendOthers: "",
        suggestions: ""
      }
  }


// ********************************************************************





  public reloadApp(): void {
    window.location.reload();
  }

  public getToolbarTitle(): string {
    return this._session.getToolName() + ' ' + this._session.getCurrentToolVersion();
  }

  // public async submitFeedback(): Promise<void> {
  //   console.log('feedback', this.formGroup.value);
  //   await this._backend.saveFeedback(this.formGroup.value['freeText'], this.formGroup.value['name']);
  //   const duration = 1500;
  //   this._toast.show('Thank you for giving your feedback. You\'ll be redirected shortly...', { actionMessage: 'No, problem', duration: duration, action: this.reloadApp });
  //   setTimeout(this.reloadApp, duration);
  // }

  /** ----------------------------------------------------------------------------------- */
  /** Mohib added this code for showing response to user after giving their feedback */
  public showMessageToUser = false;

  feedbackAction() {
    setTimeout(() => {
      this.showMessageToUser = true;
    }, 100);

    setTimeout(() => {
      this.reloadApp();
    }, 500);
  }
  /** ----------------------------------------------------------------------------------- */
  autoTicks = false;
  max = 5;
  min = 1;
  yes = 1;
  no = 0;
  showTicks = true;
  step = 1;
  thumbLabel = true;
  value = 0;

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;

  //-----------------------------------------------------------------------
  // onSubmit() {
  //   this.feedbackService.postFeedback(this.feedbackService.selectedFeedback).subscribe((res) => {
  //     this.resetForm();
  //   });
  // }

onSubmit(feedbackForm: NgForm) {
  this.feedbackService.postFeedback(feedbackForm.value).subscribe((res) => {
    this.resetForm();
  });
}


// onSubmit(f: NgForm) {
//   console.log(f.value);  // { first: '', last: '' }
//   console.log(f.valid);  // false
// }

// // ------------------------------------------------------------------------

}
