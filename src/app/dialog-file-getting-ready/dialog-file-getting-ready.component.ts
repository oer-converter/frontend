import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs';
import {FileHandlerActionEnum, ISaveToFileProgress} from '../../classes/file-handler.abstract';

/**
 * Describes possible options that can or must be passed to this dialog
 */
export interface DialogFileGettingReadyOptions {
  /**
   * Contains the progress for zipping, replacing, adding slides etc.
   */
  progress$: Observable<ISaveToFileProgress>;
}

@Component({
  selector: 'app-dialog-file-getting-ready',
  templateUrl: './dialog-file-getting-ready.component.html',
  styleUrls: ['./dialog-file-getting-ready.component.scss']
})
export class DialogFileGettingReadyComponent implements OnInit {
  public adapterFileHandlerActionEnum = FileHandlerActionEnum;

  constructor(
    private _dialogRef: MatDialogRef<DialogFileGettingReadyComponent>,
    @Inject(MAT_DIALOG_DATA) public passedData: DialogFileGettingReadyOptions
  ) { }

  ngOnInit() {
    this.passedData.progress$.subscribe(action => {
      if (action.action === FileHandlerActionEnum.READY_TO_DOWNLOAD) {
        // close the dialog when the file is zipped and ready to be saved to a computer
        this._closeDialog();
      }
    });
  }

  private _closeDialog(): void {
    this._dialogRef.close();
  }

}
