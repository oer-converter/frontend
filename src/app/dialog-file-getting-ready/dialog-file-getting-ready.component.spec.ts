import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogFileGettingReadyComponent } from './dialog-file-getting-ready.component';

describe('DialogFileGettingReadyComponent', () => {
  let component: DialogFileGettingReadyComponent;
  let fixture: ComponentFixture<DialogFileGettingReadyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogFileGettingReadyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFileGettingReadyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
