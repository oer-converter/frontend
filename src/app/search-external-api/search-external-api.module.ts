import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatTooltipModule
} from '@angular/material';
// tslint:disable-next-line:max-line-length
import { DialogShowSelectedImageFromFileComponent } from './dialog-show-selected-image-from-file/dialog-show-selected-image-from-file.component';
import {SearchComponent} from './search/search.component';
import {ResultsComponent} from './results/results.component';
import {LoadingSpinnerModule} from '../loading-spinner/loading-spinner.module';
import {HeaderModule} from '../header/header.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ImageGalleryModule} from '../image-gallery/image-gallery.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BottomSheetGenericModule} from '../bottom-sheet-generic/bottom-sheet-generic.module';



@NgModule({
  declarations: [SearchComponent, ResultsComponent, DialogShowSelectedImageFromFileComponent],
  imports: [
    CommonModule,
    MatTooltipModule,
    MatDialogModule,
    LoadingSpinnerModule,
    HeaderModule,
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    ImageGalleryModule,
    FlexLayoutModule,
    MatButtonModule,
    BottomSheetGenericModule,
    MatInputModule
  ],
  entryComponents: [DialogShowSelectedImageFromFileComponent]
})
export class SearchExternalApiModule { }
