import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {IImageWrapper} from '../../../models/image-wrapper';
import {FormControl, FormGroup} from '@angular/forms';
import {CcImageSearchResult, CcImageSearchService} from '../services/cc-image-search.service';
import {Observable, Subject} from 'rxjs';
import {ExternalImageSearchApis} from '../../../config/external-image-search-api.config';
import {MainConfig} from '../../../config/main.config';
import {ToastService} from '../../services/toast.service';
import {Licenses, LicensesHumanReadable} from '../../../models/licenses';
import {IStandardizedImageSearchResults} from '../../../models/standardized-image-search-results';
import {FlickrImageSearchService, IFlickrImageSearchResult} from '../services/flickr-image-search.service';
import {SessionServiceService} from '../../services/session-service.service';
import {BackendService} from '../../services/backend.service';
import {IResponseRecognizeImage} from '@oer-converter/api-types';
import {Router} from '@angular/router';
import {DialogShowSelectedImageFromFileComponent} from '../dialog-show-selected-image-from-file/dialog-show-selected-image-from-file.component';
import {MatDialog} from '@angular/material';

/**
 * This component will provide means to look for suitable replacements for the passed image inside the image wrapper online
 * @input image image wrapper which contains the image that is to be toReplace
 */
@Component({
  selector: 'app-search-image-online',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output('select') _selectedImage: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();
  @Input('clear-selection') clearSelection$: Observable<string> = new Observable<string>();

  public searchStandardizedResultsSubject: Subject<IStandardizedImageSearchResults[]> = new Subject<IStandardizedImageSearchResults[]>();

  public searchFormGroup: FormGroup;

  public externalImageSearchApis: {key: string; name: string}[] = new Array<{key: string; name: string}>();

  public showPagination: boolean = false;
  public currentPage: number = 1;
  public hasNextPage: boolean = false;

  public licenses: number[] = [];
  public licensesHumanReadable = LicensesHumanReadable;

  constructor(
    private _toastie: ToastService,
    private _ccImgSearch: CcImageSearchService,
    private _flickrImgSearch: FlickrImageSearchService,
    public session: SessionServiceService,
    private _backend: BackendService,
    private _router: Router,
    private _dialogSelectedImageFromFile: MatDialog,
    private _session: SessionServiceService
  ) { }

  /**
   * Init the form and its options
   */
  ngOnInit() {
    // init form
    const _defaultTargetApiConfigKey = MainConfig.defaultTargetApi;
    const _defaultTargetApiConfigObject = ExternalImageSearchApis[_defaultTargetApiConfigKey];

    // Create an array containing the object-keys of the apis to make all the available API be preselected in the form
    const _availableApis: string[] = [];
    // tslint:disable-next-line:forin
    for (const key in ExternalImageSearchApis) {
      // just a safety-check
      if (ExternalImageSearchApis.hasOwnProperty(key)) {
        _availableApis.push(key);
      }
    }

    // tslint:disable-next-line:forin
    for (const license in Licenses) {
      const index = Number(license);
      // ignore the 'UNKNOWN' license type
      if (!isNaN(index) && Licenses[index].localeCompare('UNKNOWN') !== 0 && index <= this._session.getTargetLicense()) {
        this.licenses.push(index);
      }
    }

    this.searchFormGroup = new FormGroup({
      query: new FormControl(),
      // select all available APIs
      targetApi: new FormControl(_availableApis),
      license: new FormControl(this.licenses)
    });
    // init array that holds the names of the available target apis
    for (const key in ExternalImageSearchApis) {
      // this condition isn't really necessary but it is recommended to check first
      if (ExternalImageSearchApis.hasOwnProperty(key)) {
        this.externalImageSearchApis.push({name: ExternalImageSearchApis[key].name, key: key});
      }
    }
  }

  // trigger the listening specific API components by sending the activated one a search string
  /**
   * Submits the query to the chosen targets
   */
  public async submitQuery(): Promise<void> {
    this.session.awaitAsyncAction();
    const _targetedApi: string = this.searchFormGroup.get('targetApi').value;
    const _targetedLicenses: Licenses[] = this.searchFormGroup.get('license').value;
    const _queryString: string = this.searchFormGroup.get('query').value;
    const _currentPage: number = this.currentPage;
    const results: IStandardizedImageSearchResults[] = [];
    if (_targetedApi.indexOf('cc') !== -1) {
      const _rawResult: CcImageSearchResult = await this._ccImgSearch.query(_queryString, {
        license: _targetedLicenses,
        page: _currentPage
      });
      if (_rawResult.page_count > 1) {
        this.showPagination = true;
        this.hasNextPage = _rawResult.page_count > _currentPage;
      } else {
        this.showPagination = false;
      }
      const _convertedResults: IStandardizedImageSearchResults = this._ccImgSearch
        .convertToStandardizedApiResults(_rawResult, _currentPage, _targetedLicenses);
      results.push(_convertedResults);
    }
    if (_targetedApi.indexOf('flickr') !== -1) {
      const _rawResult: IFlickrImageSearchResult = await this._flickrImgSearch.query(_queryString, {
        license: _targetedLicenses,
        page: _currentPage
      });
      if (Number(_rawResult.photos.pages) > 1) {
        this.showPagination = true;
        this.hasNextPage = Number(_rawResult.photos.pages) > _currentPage;
      } else {
        this.showPagination = false;
      }
      const _convertedResults: IStandardizedImageSearchResults = await this._flickrImgSearch
        .convertToStandardizedApiResults(_rawResult, _currentPage, _targetedLicenses);
      results.push(_convertedResults);
    }
    if (results.length > 0) {
      this.searchStandardizedResultsSubject.next(results);
    }
    this.session.doneAsyncAction();
  }

  /**
   * Cascade up the selected image up to the main component
   * @param image The image that was selected from the results
   */
  public onSelectedImage(image: IImageWrapper): void {
    this._selectedImage.emit(image);
  }

  /**
   * Calls the API to get predictions what might be on the given image and uses the first prediction (the one with the highest confidence)
   * to search for via the external APIs
   */
  public async getAutoImageResults(): Promise<void> {
    const selectedImageFromFile: IImageWrapper = await this.session.getCurrentlySelectedImageFromFile();
    this.session.awaitAsyncAction();
    const imagePrediction: IResponseRecognizeImage = await this._backend.getImageClassifications(selectedImageFromFile.base64);
    this.searchFormGroup.get('query').setValue(imagePrediction[0].className);
    // now submit the form
    this.submitQuery();
    this.session.doneAsyncAction();
  }

  /**
   * Just redirects to the main screen
   */
  public goToMain(): void {
    this._router.navigate(['']);
  }

  public showSelectedImageFromFile(): void {
    this._dialogSelectedImageFromFile.open(DialogShowSelectedImageFromFileComponent);
  }
}
