import { Component, OnInit } from '@angular/core';
import {IImageWrapper} from '../../../models/image-wrapper';
import {SessionServiceService} from '../../services/session-service.service';
import {MainConfig} from '../../../config/main.config';

@Component({
  selector: 'app-dialog-show-selected-image-from-file',
  templateUrl: './dialog-show-selected-image-from-file.component.html',
  styleUrls: ['./dialog-show-selected-image-from-file.component.scss']
})
export class DialogShowSelectedImageFromFileComponent implements OnInit {

  constructor(
    private _session: SessionServiceService
  ) { }

  ngOnInit() {
  }

  public getCurrentImageFromFile(): IImageWrapper {
    return this._session.getCurrentlySelectedImageFromFile();
  }

  public getSizeString(): string {
    const refDimension = this.getCurrentImageFromFile().dimension;
    return refDimension.width + 'x' + refDimension.height + ' (~' + refDimension.ratio.toFixed(MainConfig.imageRatioFractionDigits) + ')';
  }
}
