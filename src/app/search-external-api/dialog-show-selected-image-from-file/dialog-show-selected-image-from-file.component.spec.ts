import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogShowSelectedImageFromFileComponent } from './dialog-show-selected-image-from-file.component';

describe('DialogShowSelectedImageFromFileComponent', () => {
  let component: DialogShowSelectedImageFromFileComponent;
  let fixture: ComponentFixture<DialogShowSelectedImageFromFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogShowSelectedImageFromFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogShowSelectedImageFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
