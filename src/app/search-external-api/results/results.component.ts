import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {config, Observable, Subject, Subscription} from 'rxjs';
import {IImageWrapper, IImageWrapperEdited} from '../../../models/image-wrapper';
import {IStandardizedImageSearchResults} from '../../../models/standardized-image-search-results';
import {IImageGalleryCardActionsOptions, IImageGalleryShowInfoOptions} from '../../image-gallery/image-gallery.component';
import {LicensesHumanReadable} from '../../../models/licenses';
import {MainConfig} from '../../../config/main.config';
import {MatBottomSheet, MatBottomSheetRef, MatDialog, MatDialogRef} from '@angular/material';
import {BottomSheetGenericComponent, IBottomSheetGenericOptions} from '../../bottom-sheet-generic/bottom-sheet-generic.component';
import {Utils} from '@hexxag0nal/utils';
import {SessionServiceService} from '../../services/session-service.service';
import {Router} from '@angular/router';
import {DialogWarningImagesRatioMismatchComponent} from '../../dialog-warning-images-ratio-mismatch/dialog-warning-images-ratio-mismatch.component';
import {
  DialogEditImageComponent,
  IDialogEditImageComponentOptions, IDialogEditImageReturnParams
} from '../../compare-images/dialog-edit-image/dialog-edit-image.component';

@Component({
  selector: 'app-search-image-online-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
/**
 * This component shows the results from an image search
 */
export class ResultsComponent implements OnInit, OnDestroy {
  /**
   * The results from the query
   */
  @Input('standardized-results') standardizedResults$: Observable<IStandardizedImageSearchResults[]>;
  /**
   * Triggered when an image from the result is selected
   */
  @Output('select') _selectedImage: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();
  /**
   * Triggered, when all selections shall be cleared, e.g. when an image was replaced
   */
  @Input('clear-selection') clearSelection$: Observable<void> = new Observable<void>();

  /**
   * Subscription to the results
   */
  private _subscriptionStandardizedResults: Subscription;
  /**
   * Refrence to the selected image from the results
   */
  private _selectedImageLocalRef: IImageWrapper;

  /**
   * All the images from the result
   */
  private _imagesFromExternalApi: IImageWrapper[];

  /**
   * Get the images from the result
   */
  get images() {
    return this._imagesFromExternalApi;
  }

  // Need a new subject which will propagate the ID of the image which's selection is to be cleared
  /**
   * Subject for clearing selections
   */
  private _subjectClearSelectedImage: Subject<string> = new Subject<string>();
  /**
   * The corresponding supscription
   */
  private _subscriptionClearSelectedImage;

  public showInfoOptions: IImageGalleryShowInfoOptions[] = [
    {
      text: 'Title',
      value: image => image.title || 'unknown'
    },
    {
      text: 'Author',
      value: image => image.author || 'unknown'
    },
    {
      text: 'License',
      value: (image) => LicensesHumanReadable[image.license] + ' ' + image.licenseVersion,
      link: image => image.licenseUrl
    },
    {
      text: 'Size',
      value: image => {
        let refDimension = image.dimension;
        if (image.edited) {
          refDimension = image.edited.dimension;
        }
        // tslint:disable-next-line:max-line-length
        return refDimension.width + 'x' + refDimension.height + ' (~' + refDimension.ratio.toFixed(MainConfig.imageRatioFractionDigits) + ')';
      }
    }
  ];

  private _bottomSheetRef: MatBottomSheetRef;

  private _subjectUseImageAsIs: Subject<IBottomSheetPassedData> = new Subject<IBottomSheetPassedData>();
  private _subscriptionUseImageAsIs: Subscription;
  private _subjectEditImageBeforeUsage: Subject<IBottomSheetPassedData> = new Subject<IBottomSheetPassedData>();
  private _subscriptionEditImageBeforeUsage: Subscription;
  public cardActionsOptions: IImageGalleryCardActionsOptions<IBottomSheetPassedData>[] = [
    {
      text: 'Use \'as is\'',
      subject: this._subjectUseImageAsIs,
      tooltip: 'Use the image \'as is\' without further modifications'
    },
    {
      text: 'Edit before usage',
      subject: this._subjectEditImageBeforeUsage,
      tooltip: 'Zoom in, crop or rotate the image before using it as replacement'
    }
  ];

  private _subscriptionDialogWarningRatioMismatchAfterClose: Subscription;

  private _editExternalImageDialogRef: MatDialogRef<DialogEditImageComponent>;
  private _subscriptionEditExternalImageDialogAfterClose: Subscription;

  /**
   * Should not be used in angular
   */
  constructor(
    private _bottomSheetFurtherActions: MatBottomSheet,
    private _session: SessionServiceService,
    private _router: Router,
    private _dialogWarningRatioMismatch: MatDialog,
    private _dialogEditExternalImage: MatDialog
  ) { }

  /**
   * Executing this upon initialization
   */
  ngOnInit() {
    if (this.standardizedResults$) {
      this._subscriptionStandardizedResults = this.standardizedResults$.subscribe(this.onNewResults.bind(this));
    }
    this._subscriptionEditImageBeforeUsage = this._subjectEditImageBeforeUsage.subscribe(passedData => {
      this.openEditExternalImageDialog(passedData.image);
      this._dismissAndResetBottomSheet();
    });
    this._subscriptionUseImageAsIs = this._subjectUseImageAsIs.subscribe(passedData => {
      this._replaceImage(passedData.image);
      this._dismissAndResetBottomSheet();
    });
  }

  /**
   * Executing this upon destruction
   */
  ngOnDestroy(): void {
    Utils.safeUnsubscribe(
      this._subscriptionStandardizedResults,
      this._subscriptionClearSelectedImage,
      this._subscriptionUseImageAsIs,
      this._subscriptionEditImageBeforeUsage,
      this._subscriptionDialogWarningRatioMismatchAfterClose,
      this._subscriptionEditExternalImageDialogAfterClose
    );
    // to make sure that no image is selected when this component is destroyed
    this._session.nextSelectedImageFromExternalApi();
  }

  /**
   * Looks for the selected image and removes it from the array such that an image can only be toReplace once.
   * It also enqueues the action to replace the image upon downloading the complete file
   * @param replaceBy The image that was chosen and that is to replace an image from file
   * @param ignoreRatios If set to true, it will not be checked if the ratios mismatch
   */
  private async _replaceImage(replaceBy: IImageWrapper, ignoreRatios: boolean = false): Promise<void> {
    if (!ignoreRatios && !this._ratiosDoMatch(replaceBy)) {
      this._openDialogWarningRatioMismatch(replaceBy);
    } else {
      const _indexInArray = this._session.getImages().findIndex(elem => elem === this._session.getCurrentlySelectedImageFromFile());
      this._session.getImages().splice(_indexInArray, 1);
      this._session.nextImagesFromFile(this._session.getImages());
      if (replaceBy.edited) {
        this._session.getFileHandler().EnqueueAddImageFromBase64(await this._session.getCurrentlySelectedImageFromFile(), replaceBy);
      } else {
        this._session.getFileHandler().EnqueueAddImageFromUrl(await this._session.getCurrentlySelectedImageFromFile(), replaceBy);
      }
      this._session.nextSelectedImageFromFile();
      this._session.nextSelectedImageFromExternalApi();
      this.goToMain();
    }
  }

  /**
   * Just redirects to the main screen
   */
  public goToMain(): void {
    this._router.navigate(['']);
  }

  /**
   * As the name suggests, it dismisses the bottom sheet and resets the reference
   */
  private _dismissAndResetBottomSheet(): void {
    if (this._bottomSheetRef) {
      this._bottomSheetRef.dismiss();
      this._bottomSheetRef = undefined;
    }
  }

  /**
   * Executed when an image from the results is selected
   * @param image The selected image
   */
  public onSelect(image: IImageWrapper): void {
    if (!(image instanceof MouseEvent)) {
      // If there is a local ref, check the external ID against the just selected one
      const isDifferentImage = (this._selectedImageLocalRef && this._selectedImageLocalRef.externalId !== image.externalId);
      if (!this._selectedImageLocalRef || isDifferentImage) {
        if (this._selectedImageLocalRef) {
          this._subjectClearSelectedImage.next(this._selectedImageLocalRef.externalId);
        }
        this._selectedImageLocalRef = image;
        this._selectedImage.next(image);
      }
    }
  }

  /**
   * If new results came in, e.g. the user sent another query or changed pages
   * @param results The new results
   */
  public onNewResults(results: IStandardizedImageSearchResults[]): void {
    const tmpResults = [];
    for (const resultsFromSingleApi of results) {
      tmpResults.push(...resultsFromSingleApi.results);
    }
    this._imagesFromExternalApi = tmpResults;
  }

  /**
   * Opens the bottom sheet for picking further action for the selected image
   */
  public openBottomSheetActions(image: IImageWrapper): void {
    if (!(image instanceof MouseEvent)) {
      const dataBottomSheet: IBottomSheetGenericOptions<IBottomSheetPassedData> = {
        data: {
          image: image
        },
        list: [
          {
            text: 'Use \'as is\'',
            subText: 'Use the image \'as is\' without further modifications',
            onClick: this._subjectUseImageAsIs
          },
          {
            text: 'Edit before usage',
            subText: 'Zoom in, crop or rotate the image before using it as replacement',
            onClick: this._subjectEditImageBeforeUsage
          }
        ]
      };
      this._bottomSheetRef = this._bottomSheetFurtherActions.open(BottomSheetGenericComponent, {data: dataBottomSheet});
    }
  }

  /**
   * Open a warning that the ratios do not match
   */
  private _openDialogWarningRatioMismatch(image: IImageWrapper): void {
    const _dialogRef = this._dialogWarningRatioMismatch.open(DialogWarningImagesRatioMismatchComponent);

    this._subscriptionDialogWarningRatioMismatchAfterClose = _dialogRef.afterClosed().subscribe((choice: boolean) => {
      if (choice) {
        this._replaceImage(image, true);
      }
    });
  }

  /**
   * Check whether the ratios match
   */
  private _ratiosDoMatch(image: IImageWrapper): boolean {
    const _ratioImgFromFile = this._session.getCurrentlySelectedImageFromFile().dimension.ratio;
    // If there is info about an edit, then use the new ratio, otherwise use the original one
    // tslint:disable-next-line:max-line-length
    const _ratioImgFromExtApi = image.edited ? image.edited.dimension.ratio : image.dimension.ratio;

    // tslint:disable-next-line:max-line-length
    return _ratioImgFromExtApi >= (_ratioImgFromFile - MainConfig.imageRatioTolerance) && _ratioImgFromExtApi <= (_ratioImgFromFile + MainConfig.imageRatioTolerance);
  }

  /**
   * Triggers the dialog that exposes the image and some editing tools to the user
   */
  public openEditExternalImageDialog(image: IImageWrapper): void {
    // tslint:disable-next-line:max-line-length
    this._editExternalImageDialogRef = this._dialogEditExternalImage.open<DialogEditImageComponent, IDialogEditImageComponentOptions>(DialogEditImageComponent, {
      // disableClose: true,
      height: '92.3%',
      // setting it to 100% causes the dialog to be stuck on the left side
      width: '99.999999%',
      data: {
        image: image,
        targetAspectRatio: this._session.getCurrentlySelectedImageFromFile().dimension.ratio
      }
    });

    this._subscriptionEditExternalImageDialogAfterClose = this._editExternalImageDialogRef.afterClosed().subscribe((imageState: IDialogEditImageReturnParams) => {
      if (imageState) {
        (imageState as IImageWrapperEdited).editor = this._session.getCurrentUserRealName();
        image.edited = imageState;
        // since it was edited, it should be uploaded to the db, regardless if it was a suggestion
        image.doNotUpload = false;
        // once it was edited, it shall be replaced
        this._replaceImage(image);
      }
    });
  }
}

// Do not export since it shall only be used here
interface IBottomSheetPassedData {
  image: IImageWrapper;
}
