import { Injectable } from '@angular/core';
import {HttpService} from '../../services/http.service';
import {CcImageSearchImageItem, CcImageSearchQueryOptions, CcImageSearchResult} from './cc-image-search.service';
import {Licenses} from '../../../models/licenses';
import {CcImageSearchQuery} from '../../../models/cc-image-search-query';
import {ExternalImageSearchApis} from '../../../config/external-image-search-api.config';
import {IStandardizedImageSearchResults} from '../../../models/standardized-image-search-results';
import {IImageWrapper} from '../../../models/image-wrapper';

/**
 * see https://www.flickr.com/services/api/flickr.photos.licenses.getInfo.html for licenses' IDs
 */
const flickrLicenses = {
  0: {
    name: 'All Rights Reserved',
    url: '',
    version: ''
  },
  1: {
    name: 'Attribution-NonCommercial-ShareAlike License',
    url: 'https://creativecommons.org/licenses/by-nc-sa/2.0/',
    version: '2.0'
  },
  2: {
    name: 'Attribution-NonCommercial License',
    url: 'https://creativecommons.org/licenses/by-nc/2.0/',
    version: '2.0'
  },
  3: {
    name: 'Attribution-NonCommercial-NoDerivs License',
    url: 'https://creativecommons.org/licenses/by-nc-nd/2.0/',
    version: '2.0'
  },
  4: {
    name: 'Attribution License',
    url: 'https://creativecommons.org/licenses/by/2.0/',
    version: '2.0'
  },
  5: {
    name: 'Attribution-ShareAlike License',
    url: 'https://creativecommons.org/licenses/by-sa/2.0/',
    version: '2.0'
  },
  6: {
    name: 'Attribution-NoDerivs License',
    url: 'https://creativecommons.org/licenses/by-nd/2.0/',
    version: '2.0'
  },
  7: {
    name: 'No known copyright restrictions',
    url: 'https://www.flickr.com/commons/usage/',
    version: ''
  },
  8: {
    name: 'United States Government Work',
    url: 'http://www.usa.gov/copyright.shtml',
    version: ''
  },
  9: {
    name: 'Public Domain Dedication (CC0)',
    url: 'https://creativecommons.org/publicdomain/zero/1.0/',
    version: '1.0'
  },
  10: {
    name: 'Public Domain Mark',
    url: 'https://creativecommons.org/publicdomain/mark/1.0/',
    version: '1.0'
  }
};

/**
 * Parameter for a flickr query
 */
export interface IFlickrImageSearchQueryOptions {
  /**
   * Targeted widht
   */
  width?: number;
  /**
   * Targeted height
   */
  height?: number;
  /**
   * Targeted ratio
   */
  ratio?: number;
  /**
   * Targeted licenses
   */
  license?: Licenses[];
  /**
   * Targeted page of the results
   */
  page?: number;
}

/**
 * Image result item
 */
export interface IFlickrPublicSearchImageItem {
  /**
   * Internal id
   */
  id: string;
  /**
   * owner/creator/author
   */
  owner: string;
  /**
   * Some sort of secret/salt
   */
  secret: string;
  /**
   * The flickr server hosting the imgae
   */
  server: string;
  /**
   * The number of the farm the server is belonging to
   */
  farm: number;
  /**
   * Title of the image
   */
  title: string;
  /**
   * Whether the image can be seen by the public
   */
  ispublic: number;
  /**
   * Whether the image can be seen by friends of the owner
   */
  isfriend: number;
  /**
   * Whether the image can be seen by the family of the owner
   */
  isfamily: number;
}

/**
 * Detailed info on an flickr image
 */
export interface IFlickrGetInfoOnImageResult {
  /**
   * The image
   */
  photo: {
    id: string;
    secret: string;
    server: string;
    farm: number;
    dateuploaded: string;
    isfavorite: number;
    license: string | number;
    safety_level: 0;
    rotation: number;
    originalsecret: string;
    originalformat: string;
    owner: {
      nsid: string;
      username: string;
      realname: string;
      location: string;
      iconserver: string;
      iconfarm: number;
      path_alias: string;
    };
    title: {
      _content: string;
    };
    description: {
      _content: string;
    };
    visibility: {
      ispublic: number;
      isfriend: number;
      isfamily: number;
    };
    dates: {
      posted: string;
      taken: string;
      takengranularity: number;
      takenunknown: number;
      lastupdate: string;
    };
    views: number;
    editability: {
      cancomment: number;
      canaddmeta: number;
    };
    publiceditability: {
      cancomment: number;
      canaddmeta: number;
    };
    usage: {
      candownload: number;
      canblog: number;
      canprint: number;
      canshare: number;
    };
    comments: {
      _content: number;
    };
    notes: any;
    people: {
      haspeople: number;
    };
    tags: any;
    urls: {
      url: {
        type: string;
        _content: string
      }[]
    };
    media: string;
  };
  /**
   * meta data stats
   */
  stat: string;
}

/**
 * Results of the search
 */
export interface IFlickrImageSearchResult {
  /**
   * Results and meta data
   */
  photos: {
    page: number;
    pages: string;
    perpage: number;
    total: string;
    photo: IFlickrPublicSearchImageItem[];
  };
  /**
   * Stats
   */
  stat: string;
}

/**
 * Info about an image's size
 */
export interface IFlickrImageSizesSearchResult {
  /**
   * Available sizes
   */
  sizes: {
    canblog: number;
    canprint: number;
    candownload: number;
    size: IFlickrImageSizeItem[]
  };
  /**
   * Additional stats
   */
  stat: string;
}

/**
 * A singular size item
 */
export interface IFlickrImageSizeItem {
  label: string;
  width: number;
  height: number;
  source: string;
  url: string;
  media: string;
}

/**
 * See https://www.flickr.com/services/api/flickr.photos.search.html
 */
export interface IFlickrImageSearchQuery {
  text?: string;
  api_key: string;
  license?: string;
  method: 'flickr.photos.search';
  format: 'json';
  nojsoncallback: 1;
  per_page?: number;
  page?: number;
}

/**
 * See https://www.flickr.com/services/api/flickr.photos.getInfo.html
 */
export interface IFlickrInfoOnImageSearchQuery {
  api_key: string;
  method: 'flickr.photos.getInfo';
  format: 'json';
  nojsoncallback: 1;
  photo_id: string;
}

/**
 * See https://www.flickr.com/services/api/explore/flickr.photos.getSizes
 */
export interface IFlickrSizesOfImageSearchQuery {
  api_key: string;
  method: 'flickr.photos.getSizes';
  format: 'json';
  nojsoncallback: 1;
  photo_id: string;
}

@Injectable({
  providedIn: 'root'
})
/**
 * Abstracts communication with the flickr API
 */
export class FlickrImageSearchService {

  /**
   * Makes use of the HTTPService
   * @param _http The http service
   */
  constructor(
    private _http: HttpService
  ) { }

  /**
   * Query the flickr API
   * @param query The actual query
   * @param options Some further options
   */
  public async query(query: string, options?: IFlickrImageSearchQueryOptions): Promise<IFlickrImageSearchResult> {
    // this is the query object that will be sent to the API
    const _queryObject: IFlickrImageSearchQuery = {
      text: query,
      api_key: ExternalImageSearchApis['flickr'].publicKey,
      method: 'flickr.photos.search',
      format: 'json',
      nojsoncallback: 1,
      per_page: 20,
      page: (options && options.page) ? options.page : 1
    };
    if (!options || !options.license || options.license.length === 0) {
      _queryObject.license = '4,5,9';
    } else {
      let _convertedLicenses: string = '';
      for (const license of options.license) {
        // we glue the converted licenses together since the API expects a comma separated string, not an array
        _convertedLicenses += this.licenseTypeToFlickrId(license) + ',';
      }
      _convertedLicenses = _convertedLicenses.slice(0, _convertedLicenses.length - 1);
      _queryObject.license = _convertedLicenses;
    }

    const url = ExternalImageSearchApis['flickr'].url + '?' + this._http.convertObjectToQueryString(_queryObject);
    const result: IFlickrImageSearchResult = await this._http.get(url);

    return result; // ignore options for now
  }

  /**
   * Convert to standardized results
   * @param flickrImageSearchResult The results of the query
   * @param currentPage The result's current page
   * @param licenses The result's licenses
   */
  public async convertToStandardizedApiResults(flickrImageSearchResult: IFlickrImageSearchResult, currentPage: number, licenses?: Licenses[]): Promise<IStandardizedImageSearchResults> {
    return {
      pageCount: Number(flickrImageSearchResult.photos.pages),
      resultCount: Number(flickrImageSearchResult.photos.total),
      pageCurrent: Number(flickrImageSearchResult.photos.page),
      pageSize: flickrImageSearchResult.photos.perpage,
      license: licenses,
      targetApi: 'flickr',
      results: await this.convertToImageWrapper(flickrImageSearchResult.photos.photo)
    };
  }

  /**
   * Convert the results to the internally used format that is used by the tool
   * @param imageItems The images from the query results
   */
  public async convertToImageWrapper(imageItems: IFlickrPublicSearchImageItem[]): Promise<IImageWrapper[]> {
    const results: IImageWrapper[] = [];
    for (const imageItem of imageItems) {
      // get additional info on the image
      const _queryObjectInfo: IFlickrInfoOnImageSearchQuery = {
        api_key: ExternalImageSearchApis['flickr'].publicKey,
        method: 'flickr.photos.getInfo',
        format: 'json',
        nojsoncallback: 1,
        photo_id: imageItem.id
      };
      const urlInfo = ExternalImageSearchApis['flickr'].url + '?' + this._http.convertObjectToQueryString(_queryObjectInfo);
      const imageInfo: IFlickrGetInfoOnImageResult = await this._http.get(urlInfo);
      // get info on the size and the url of the original
      const _queryObjectSizes: IFlickrSizesOfImageSearchQuery = {
        api_key: ExternalImageSearchApis['flickr'].publicKey,
        method: 'flickr.photos.getSizes',
        format: 'json',
        nojsoncallback: 1,
        photo_id: imageItem.id
      };
      const urlSizes = ExternalImageSearchApis['flickr'].url + '?' + this._http.convertObjectToQueryString(_queryObjectSizes);
      const imageSizes: IFlickrImageSizesSearchResult = await this._http.get(urlSizes);
      let _owner = imageInfo.photo.owner.username;
      if (imageInfo.photo.owner.realname && imageInfo.photo.owner.realname !== '') {
        _owner += ` (${imageInfo.photo.owner.realname})`;
      }

      // the original size seems to always be the last item, and the thumbnails seems to always be the third, but for safety's sake
      // we'll loop through the sizes and check the label
      // Read from top to bottom, if a size does note exist (maybe because the original is already small) then the next smaller
      // size will be used as thumbnail. The image labelled 'Thumbnail' should always exist
      const sizesForThumbnail = [
        'Medium 800',
        'Medium 640',
        'Medium',
        'Small 400',
        'Small 320',
        'Small',
        'Thumbnail'
      ];
      let _refThumbnailImage: IFlickrImageSizeItem;
      for (const size of sizesForThumbnail) {
        _refThumbnailImage = imageSizes.sizes.size.find((item: IFlickrImageSizeItem) => item.label === size);
        if (_refThumbnailImage != null) { break; }
      }
      const _refOriginalImage: IFlickrImageSizeItem =
        imageSizes.sizes.size.find((item: IFlickrImageSizeItem) => item.label === 'Original');

      results.push({
        externalId: imageItem.id,
        base64: null,
        license: this.licenseIdToType(Number(imageInfo.photo.license)),
        licenseVersion: flickrLicenses[imageInfo.photo.license].version,
        licenseUrl: flickrLicenses[imageInfo.photo.license].url,
        description: imageInfo.photo.description._content,
        title: imageInfo.photo.title._content,
        source: _refOriginalImage.source,
        thumbnailSource: _refThumbnailImage.source,
        author: _owner,
        dimension: {
          height: _refOriginalImage.height,
          width: _refOriginalImage.width,
          ratio: _refOriginalImage.width / _refOriginalImage.height
        }
      });
    }
    return results;
  }

  /**
   * Convert a license by id to the proper enum
   * @param licenseId The license
   */
  public licenseIdToType(licenseId: number): Licenses {
    switch (licenseId) {
      case 9:
        return Licenses.CC_ZERO;
      case 4:
        return Licenses.CC_BY;
      case 5:
        return Licenses.CC_BY_SA;
      default:
        return Licenses.UNKNOWN;
    }
  }

  /**
   * Convert the license to an id such that flickr understand the license
   * @param license The license to be converted to an id
   */
  public licenseTypeToFlickrId(license: Licenses): number {
    switch (license) {
      case Licenses.CC_BY: {
        return 4;
      }
      case Licenses.CC_BY_SA: {
        return 5;
      }
      case Licenses.CC_ZERO: {
        return 9;
      }
      default: {
        throw new Error('License not supported by CC');
      }
    }
  }
}
