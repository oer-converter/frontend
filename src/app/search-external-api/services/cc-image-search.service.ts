import {Injectable} from '@angular/core';
import {HttpService} from '../../services/http.service';
import {CcImageSearchQuery} from '../../../models/cc-image-search-query';
import {ExternalImageSearchApis} from '../../../config/external-image-search-api.config';
import {IImageWrapper} from '../../../models/image-wrapper';
import {Licenses} from '../../../models/licenses';
import {IStandardizedImageSearchResults} from '../../../models/standardized-image-search-results';
import {getDimensionFromImageSource} from '../../main/main.component';

/**
 * Describes a cc image search query
 */
export interface CcImageSearchQueryOptions {
  /**
   * Targeted width
   */
  width?: number;
  /**
   * Targeted height
   */
  height?: number;
  /**
   * Targeted ratio
   */
  ratio?: number;
  /**
   * Targeted license
   */
  license?: Licenses[];
  /**
   * Targeted page of the results
   */
  page?: number;
}

/**
 * The result of an cc image search query
 */
export interface CcImageSearchResult {
  /**
   * Number of pages of the result
   */
  page_count: number;
  /**
   * Total number of results
   */
  result_count: number;
  /**
   * The results itself
   */
  results: CcImageSearchImageItem[];
}

/**
 * A result item of the cc image search query
 */
export interface CcImageSearchImageItem {
  /**
   * The title of the image
   */
  title: string;
  /**
   * Internal id
   */
  id: string;
  /**
   * The creator/author
   */
  creator: string;
  /**
   * The url to the creator's/author's page
   */
  creator_url: string;
  /**
   * The url to the image upon the corresponding website
   */
  url: string;
  /**
   * Url to a thumbnail
   */
  thumbnail: string;
  /**
   * Name of the provider that hosts the image
   */
  provider: string;
  /**
   * The actual source url for the image
   */
  source: string;
  /**
   * The name of the license
   */
  license: string;
  /**
   * The version of the license
   */
  license_version: string;
  /**
   * The url to the license's deeds
   */
  license_url: string;
  foreign_landing_url: string;
  /**
   * Description
   */
  detail: string;
  fields_matched: string[];
  /**
   * The image's height
   */
  height?: number;
  /**
   * The image_s width
   */
  width?: number;
}

@Injectable({
  providedIn: 'root'
})
/**
 * Documentation of API: https://api.creativecommons.engineering/#operation/image_search
 */
export class CcImageSearchService {

  constructor(
    private _http: HttpService
  ) { }

  /**
   * Query the cc image search api
   * @param query The query
   * @param options Some further options
   */
  public async query(query: string, options?: CcImageSearchQueryOptions): Promise<CcImageSearchResult> {
    // this is the query object that will be sent to the API
    const _queryObject: CcImageSearchQuery = {
      q: query,
      page: (options && options.page) ? options.page : 1
    };
    if (!options || !options.license || options.license.length === 0) {
      _queryObject.license = 'cc0,by,by-sa';
    } else {
      let _convertedLicenses: string = '';
      for (const license of options.license) {
        // we glue the converted licenses together since the API expects a comma separated string, not an array
        _convertedLicenses += this.licenseTypeToCcAbbr(license) + ',';
      }
      // cut off the last comma
      _convertedLicenses = _convertedLicenses.slice(0, _convertedLicenses.length - 1);
      _queryObject.license = _convertedLicenses;
    }
    const url = ExternalImageSearchApis['cc'].url + '?' + this._http.convertObjectToQueryString(_queryObject);
    const apiResult: CcImageSearchResult = await this._http.get(url);
    // sometimes, the images don't have the height and/or the width
    for (const result of apiResult.results) {
      if (!result.height || !result.width) {
        const dim = await getDimensionFromImageSource(result.url);
        result.height = dim.height;
        result.width = dim.width;
      }
    }
    return apiResult; // ignore options for now

    /*if (!options) {
      return result;
    } else {
      if (options.ratio) {
        result = result.filter(image => image['width'] / image['height'] === options.ratio);
      }
      if (options.height) {
        result = result.filter(image => image['height'] === options.height);
      }
      if (options.width) {
        result = result.filter(image => image['width'] === options.width);
      }
      return result;
    }*/
  }

  /**
   * Convert the results into the internally used format
   * @param imageItems
   */
  public convertToImageWrapper(imageItems: CcImageSearchImageItem[]): IImageWrapper[] {
    const results: IImageWrapper[] = [];
    for (const imageItem of imageItems) {
      results.push({
        externalId: imageItem.id,
        base64: null,
        license: this.licenseStringToType(imageItem.license),
        licenseVersion: imageItem.license_version,
        licenseUrl: imageItem.license_url,
        description: imageItem.detail,
        title: imageItem.title,
        source: imageItem.url,
        author: imageItem.creator,
        dimension: {
          height: imageItem.height,
          width: imageItem.width,
          ratio: imageItem.width / imageItem.height
        }
      });
    }
    return results;
  }

  /**
   * Convert to a standardized result format
   * @param ccImageSearchResult The results
   * @param currentPage The current page
   * @param licenses The used licenses
   */
  public convertToStandardizedApiResults(ccImageSearchResult: CcImageSearchResult, currentPage: number, licenses?: Licenses[]): IStandardizedImageSearchResults {
    return {
      pageCount: ccImageSearchResult.page_count,
      resultCount: ccImageSearchResult.result_count,
      pageCurrent: currentPage,
      pageSize: ccImageSearchResult.results.length,
      license: licenses,
      targetApi: 'cc',
      results: this.convertToImageWrapper(ccImageSearchResult.results)
    };
  }

  /**
   * Convert a license by name and version to the proper enum
   * @param licenseString
   */
  public licenseStringToType(licenseString: string): Licenses {
    switch (licenseString) {
      case 'cc0':
        return Licenses.CC_ZERO;
      case 'by':
        return Licenses.CC_BY;
      case 'by-sa':
        return Licenses.CC_BY_SA;
      default:
        return Licenses.UNKNOWN;
    }
  }

  /**
   * Since cc and the tool used different abbreviations for the licenses, this function converts the enum to the proper cc abbreviation
   * @param license
   */
  public licenseTypeToCcAbbr(license: Licenses): string {
    switch (license) {
      case Licenses.CC_BY: {
        return 'by';
      }
      case Licenses.CC_BY_SA: {
        return 'by-sa';
      }
      case Licenses.CC_ZERO: {
        return 'cc0';
      }
      default: {
        throw new Error('License not supported by CC');
      }
    }
  }
}
