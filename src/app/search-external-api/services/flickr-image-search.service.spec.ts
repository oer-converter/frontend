import { TestBed } from '@angular/core/testing';

import { FlickrImageSearchService } from './flickr-image-search.service';

describe('FlickrImageSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlickrImageSearchService = TestBed.get(FlickrImageSearchService);
    expect(service).toBeTruthy();
  });
});
