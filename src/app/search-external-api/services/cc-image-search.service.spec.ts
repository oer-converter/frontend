import { TestBed } from '@angular/core/testing';

import { CcImageSearchService } from './cc-image-search.service';

describe('CcImageSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CcImageSearchService = TestBed.get(CcImageSearchService);
    expect(service).toBeTruthy();
  });
});
