import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWarningImagesRatioMismatchComponent } from './dialog-warning-images-ratio-mismatch.component';

describe('DialogWarningImagesRatioMismatchComponent', () => {
  let component: DialogWarningImagesRatioMismatchComponent;
  let fixture: ComponentFixture<DialogWarningImagesRatioMismatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogWarningImagesRatioMismatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWarningImagesRatioMismatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
