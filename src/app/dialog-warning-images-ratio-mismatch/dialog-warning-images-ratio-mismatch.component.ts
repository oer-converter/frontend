import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogFileGettingReadyOptions} from '../dialog-file-getting-ready/dialog-file-getting-ready.component';

@Component({
  selector: 'app-dialog-warning-images-ratio-mismatch',
  templateUrl: './dialog-warning-images-ratio-mismatch.component.html',
  styleUrls: ['./dialog-warning-images-ratio-mismatch.component.scss']
})
export class DialogWarningImagesRatioMismatchComponent implements OnInit {

  constructor(
    private _dialogRef: MatDialogRef<DialogWarningImagesRatioMismatchComponent>,
    @Inject(MAT_DIALOG_DATA) public passedData: DialogFileGettingReadyOptions
  ) { }

  ngOnInit() {
  }

}
