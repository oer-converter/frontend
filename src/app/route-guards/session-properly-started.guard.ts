import { Injectable } from '@angular/core';
import {CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionServiceService} from '../services/session-service.service';
import {ToastService} from '../services/toast.service';

@Injectable({
  providedIn: 'root'
})
export class SessionProperlyStartedGuard implements CanActivate {
  constructor(
    private _session: SessionServiceService,
    private _router: Router,
    private _toast: ToastService
  ) {
  }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    if (!this._session.hasStartedProperly()) {
      return this.denyAccess('You must start this tool from the beginning');
    } else if (!this._session.getCurrentlySelectedImageFromFile()) {
      return this.denyAccess('You can\'t go to this page because you have not selected an image from the uplaoded file')
    } else {
      return true;
    }
  }

  denyAccess(toastMsg?: string): boolean {
    this._router.navigate(['']);
    if (toastMsg) {
      this._toast.show(toastMsg, {actionMessage: 'Got it'});
    }
    return false;
  }
}
