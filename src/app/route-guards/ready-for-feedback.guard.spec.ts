import { TestBed, async, inject } from '@angular/core/testing';

import { ReadyForFeedbackGuard } from './ready-for-feedback.guard';

describe('ReadyForFeedbackGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReadyForFeedbackGuard]
    });
  });

  it('should ...', inject([ReadyForFeedbackGuard], (guard: ReadyForFeedbackGuard) => {
    expect(guard).toBeTruthy();
  }));
});
