import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionServiceService} from '../services/session-service.service';

@Injectable({
  providedIn: 'root'
})
export class ReadyForFeedbackGuard implements CanActivate {
  constructor(
    private _session: SessionServiceService,
    private _router: Router
  ) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this._session.isSessionDone()) {
      this._router.navigate(['']);
    }
    return this._session.isSessionDone();
  }

}
