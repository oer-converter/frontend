import { TestBed, async, inject } from '@angular/core/testing';

import { SessionProperlyStartedGuard } from './session-properly-started.guard';

describe('SessionProperlyStartedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionProperlyStartedGuard]
    });
  });

  it('should ...', inject([SessionProperlyStartedGuard], (guard: SessionProperlyStartedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
