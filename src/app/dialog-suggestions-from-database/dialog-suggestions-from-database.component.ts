import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FileHandler, IPublishAsOerActionParams} from '../../classes/file-handler.abstract';
import {IImageWrapper, IImageWrapperEdited} from '../../models/image-wrapper';
import {ResponseGetMadeOerImages, ResponseGetReplacementImages} from '../../models/backend';
import {Crypto} from '@hexxag0nal/crypto';
import {DataUrl} from '@hexxag0nal/data-url';
import {licenseNameToEnum, Licenses, LicensesHumanReadable} from '../../models/licenses';

@Component({
  selector: 'app-dialog-suggestions-from-database',
  templateUrl: './dialog-suggestions-from-database.component.html',
  styleUrls: ['./dialog-suggestions-from-database.component.scss']
})
export class DialogSuggestionsFromDatabaseComponent implements OnInit {

  public suggestions: {
    replacedBy: ResponseGetReplacementImages[],
    madeOer: ResponseGetMadeOerImages[]
  } = {
    replacedBy: [],
    madeOer: []
  };

  public licenses: typeof Licenses = Licenses;
  public licensesHumanlyReadable = LicensesHumanReadable;

  constructor(
    private _dialogRef: MatDialogRef<DialogSuggestionsFromDatabaseComponent, DialogSuggestionsFromDatabaseComponentResult>,
    @Inject(MAT_DIALOG_DATA) public passedData: DialogSuggestionsFromDatabaseComponentOptions,
  ) { }

  ngOnInit() {
    // we do not need to check if .apiHistory exists, because this dialog is only opened in such a situation
    const apiHistory = this.passedData.image.apiHistory;
    if (apiHistory.replacedBy) {
      this.suggestions.replacedBy = apiHistory.replacedBy;
    }
    if (apiHistory.madeOer) {
      this.suggestions.madeOer = apiHistory.madeOer;
    }
  }

  public async onClickReplacingImage(choice: ResponseGetReplacementImages): Promise<void> {
    const base64DataUrl: DataUrl = await DataUrl.fromImageUrl(choice.source_url);
    // tslint:disable-next-line:max-line-length
    const isEditedVariant: boolean = !!choice.edited_data_url_base64 && !!choice.edited_height && !!choice.edited_width;
    const edited: IImageWrapperEdited = !isEditedVariant ? undefined : {
      editor: choice.author_real_name || choice.author_user_name || 'unknown',
      dimension: {
        height: choice.edited_height,
        width: choice.edited_width,
        ratio: choice.edited_width / choice.edited_height
      },
      base64: choice.edited_data_url_base64,
      action: ['unknown']
    };
    const result: DialogSuggestionsFromDatabaseComponentResult = {
      replacedBy: {
        doNotUpload: true,
        checksum: Crypto.md5(base64DataUrl.toString()),
        license: licenseNameToEnum(choice.license_name),
        licenseVersion: choice.license_version,
        licenseUrl: choice.license_deeds,
        source: choice.source_url,
        author: choice.author_real_name || choice.author_user_name || 'unknown',
        base64: base64DataUrl,
        dimension: {
          height: choice.height,
          width: choice.width,
          ratio: choice.width / choice.height
        },
        edited: edited
      }
    };
    this._dialogRef.close(result);
  }

  public async onClickMadeOerSuggestion(choice: ResponseGetMadeOerImages): Promise<void> {

    /*const result: DialogSuggestionsFromDatabaseComponentResult = {
      madeOer: {
        doNotUpload: true,
        base64: choice.data_url_base64,
        license: licenseNameToEnum(choice.license_name),
        licenseVersion: choice.license_version,
        licenseUrl: choice.license_deeds,
        title: choice.title,
        // @ts-ignore //TODO: remove the ts-ignore and see
        author: {
          name: choice.author_name,
          url: choice.author_url,
          affiliation: choice.author_affiliation
        },
        dimension: {
          height: choice.height,
          width: choice.width,
          ratio: choice.width / choice.height
        },
        checksum: choice.checksum
      }
    };*/

    const result: DialogSuggestionsFromDatabaseComponentResult = {
      madeOer: {
        author: choice.author_name,
        authorUrl: choice.author_url,
        title: choice.title,
        license: {
          enum: licenseNameToEnum(choice.license_name),
          version: choice.license_version
        },
        affiliation: choice.author_affiliation,
        doNotUpload: true
      }
    };

    this._dialogRef.close(result);
  }
}

export interface DialogSuggestionsFromDatabaseComponentOptions {
  fileHandler: FileHandler;
  image: IImageWrapper;
}

/**
 * Describes the result of the suggestions dialog. Will have only one of these set.
 */
export interface DialogSuggestionsFromDatabaseComponentResult {
  madeOer?: IPublishAsOerActionParams;
  replacedBy?: IImageWrapper;
}
