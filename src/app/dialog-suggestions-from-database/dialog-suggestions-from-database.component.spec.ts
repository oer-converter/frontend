import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSuggestionsFromDatabaseComponent } from './dialog-suggestions-from-database.component';

describe('DialogSuggestionsFromDatabaseComponent', () => {
  let component: DialogSuggestionsFromDatabaseComponent;
  let fixture: ComponentFixture<DialogSuggestionsFromDatabaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSuggestionsFromDatabaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSuggestionsFromDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
