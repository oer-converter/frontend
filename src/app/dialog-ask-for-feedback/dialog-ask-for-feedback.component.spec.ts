import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAskForFeedbackComponent } from './dialog-ask-for-feedback.component';

describe('DialogAskForFeedbackComponent', () => {
  let component: DialogAskForFeedbackComponent;
  let fixture: ComponentFixture<DialogAskForFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAskForFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAskForFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
