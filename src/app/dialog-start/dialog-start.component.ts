import {Component, Inject, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatStepper} from '@angular/material';
import {DialogFileGettingReadyOptions} from '../dialog-file-getting-ready/dialog-file-getting-ready.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS, StepperSelectionEvent} from '@angular/cdk/stepper';
import {MainConfig} from '../../config/main.config';
import {Subject} from 'rxjs';
import {IImageWrapper} from '../../models/image-wrapper';
import {FileHandler, IFileHanderParameters} from '../../classes/file-handler.abstract';
import {PptxHandler} from '../../classes/pptx-handler';
import {SessionServiceService} from '../services/session-service.service';
import {DomSanitizer} from '@angular/platform-browser';
import {DocxHandler} from '../../classes/docx-handler';
import {DataUrl} from '@hexxag0nal/data-url';
import {Utils} from '@hexxag0nal/utils';
import {PDFDict, PDFDocument, PDFName, PDFPageLeaf, PDFRef, PDFStream} from 'pdf-lib';
import * as Pako from 'pako';
import {deflate} from 'zlib';
import * as zlib from 'zlib';
import {saveAs} from 'file-saver';


// big fat props to https://stackoverflow.com/a/50769855/7618184 !!!!
import * as pdfjsLib from 'pdfjs-dist/build/pdf';
import {PdfHandler} from '../../classes/pdf-handler';
import {ToastService} from '../services/toast.service';
import {Licenses, LicensesHumanReadable} from '../../models/licenses';
// using the worker in the node_modules does not work, probably because it can't be accessed
pdfjsLib.GlobalWorkerOptions.workerSrc = '/assets/pdf.worker.min.js';


@Component({
  selector: 'app-dialog-import-file',
  templateUrl: './dialog-start.component.html',
  styleUrls: ['./dialog-start.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class DialogStartComponent implements OnInit {
  @ViewChild('stepper', {static: false}) stepper: MatStepper;

  // private _session: SessionServiceService;
  public _session: SessionServiceService;


  public stepperFormGroup: FormGroup;

  private _fileHandler: FileHandler;
  private _images: Promise<IImageWrapper[]>;

  private _loadSearchDatabaseComponent = false;

  public licenses: number[] = [];
  public licensesHumanReadable = LicensesHumanReadable;


  constructor(
    private _dialogRef: MatDialogRef<DialogStartComponent, DialogStartReturnData>,
    @Inject(MAT_DIALOG_DATA) public passedData: DialogStartData,
    private _formBuilder: FormBuilder,
    // private _session: SessionServiceService,
    private _sanitizer: DomSanitizer,
    private _toast: ToastService
  ) { }

  ngOnInit() {
    this._session = this.passedData.sessionService;
    this.stepperFormGroup = new FormGroup({
      chosenFile: new FormControl(null, Validators.required),
      userName: new FormControl('unknown', Validators.required),
      targetLicense: new FormControl(null, Validators.required),
      disclaimerSlideText: new FormControl(null, Validators.required)
    });

    // tslint:disable-next-line:forin
    for (const license in Licenses) {
      const index = Number(license);
      // ignore the 'UNKNOWN' license type
      if (!isNaN(index) && Licenses[index].localeCompare('UNKNOWN') !== 0) {
        this.licenses.push(index);
      }
    }
  }

  public async onFileImport(file: FileWrapper): Promise<void> {
    // this._dialogRef.close(file);


    this._session.awaitAsyncAction();
    const _fileHandlerParams: IFileHanderParameters = {file: file, domSanitizer: this._sanitizer};
    switch (file.type) {
      case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        this._fileHandler = new PptxHandler(_fileHandlerParams) as PptxHandler;
        break;
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        this._fileHandler = new DocxHandler(_fileHandlerParams) as DocxHandler;
        break;
      case 'application/pdf':
        // see this https://resources.infosecinstitute.com/pdf-file-format-basic-structure/ to understand PDFs
        // or https://www.oreilly.com/library/view/pdf-explained/9781449321581/ch04.html
        // or https://web.archive.org/web/20141010035745/http://gnupdf.org/Introduction_to_PDF

        /*
        A full description of the progress on supporting PDFs and what is still to be done can be found in the file pdf-handler.ts
         */
        // tslint:disable-next-line:max-line-length
        this._toast.show('PDF files are just rudimentary supported. You can see the images in them but the resulting file for download will be the same that you uploaded.', {
          duration: 30000,
          actionMessage: 'Got it!'
        });
        this._fileHandler = new PdfHandler(_fileHandlerParams) as PdfHandler;

        break;
      default:
        throw new Error(`File of type '${file.type}' is not supported`);
    }

    this._session.setCurrentGUID(await this._fileHandler.getDocumentGUID());
    this._session.setFileHandler(this._fileHandler);
    const images: IImageWrapper[] = await this._fileHandler.getImages();

    this._images = Promise.resolve(images);

    this._session.doneAsyncAction();

    this.stepperFormGroup.get('chosenFile').setValue(file);

    this._session.setCurrentFileAuthor(await this._fileHandler.getFileAuthorName(/*await (this._fileHandler as PptxHandler).getZip()*/));

    this.stepper.next();
  }

  public onStepChange(event: StepperSelectionEvent) {
    switch (event.selectedIndex) {
      case 3:
        this.processAndUpdateDisclaimerTextTemplate();
        break;
      case 4:
        this._session.setLegalDisclaimerText(this.stepperFormGroup.get('disclaimerSlideText').value.toString());
        break;
      case 5:
        this._loadSearchDatabaseComponent = true;
        break;
      default:
        break;
    }

    if (event.selectedIndex !== 5) {
      this._loadSearchDatabaseComponent = false;
    }
  }

  public processAndUpdateDisclaimerTextTemplate(): void {
    const processedText = MainConfig.disclaimerSlide.defaultText
      .replace('__ORIGINAL_AUTHOR__', this._session.getCurrentFileAuthor())
      .replace('__TOOL_NAME__', this._session.getToolName)
      .replace('__USER_NAME__', this.stepperFormGroup.get('userName').value.toString())
      // if i don't bind the _session, the 'this' is not defined and throws an error
      .replace('__TOOL_VERSION__', this._session.getCurrentToolVersion.bind(this._session))
      .replace('__LICENSE_FULL_NAME__', this.getLicenseFullName(this.stepperFormGroup.get('targetLicense').value))
      .replace('__LICENSE_SHORT_NAME__', this.getLicenseShortName(this.stepperFormGroup.get('targetLicense').value))
    ;
    this.stepperFormGroup.get('disclaimerSlideText').setValue(processedText);
  }

  // *********************************
  // *** Formats available for use ****
  public getFileFormats(format: any): string {
    switch (format) {
      case 1:
        return '.pptx';
      case 2:
        return '.docx';
      case 3:
        return '.pdf';
    }
  }
  // *********************************

  public getLicenseFullName(license: Licenses): string {
    switch (license) {
      case 1:
        return 'CC0 1.0 Universal, Public Domain Dedication';
      case 2:
        return 'Attribution 4.0 International';
      case 3:
        return 'Attribution-ShareAlike 4.0 International';
    }
  }

  public getLicenseShortName(license: Licenses): string {
    switch (license) {
      case 1:
        return 'CC0 1.0';
      case 2:
        return 'CC BY 4.0';
      case 3:
        return 'CC BY-SA 4.0';
    }
  }

  public async closeStartDialog(): Promise<void> {
    this.processAllInputData();

    this._dialogRef.close({
      fileHandler: this._fileHandler,
      images: await this._images
    });
  }

  public processAllInputData(): void {
    // just to be 100% sure that all the input data will be processed
    this._session.setCurrentUserRealName(this.stepperFormGroup.get('userName').value.toString());
    this._session.setTargetLicense(this.stepperFormGroup.get('targetLicense').value);
    // maybe the next line is only needed until the two extra steps are working again, I don't know for sure
    this._session.setLegalDisclaimerText(this.stepperFormGroup.get('disclaimerSlideText').value.toString());
    // this.processAndUpdateDisclaimerTextTemplate();
    // this._session.setLegalDisclaimerText(this.stepperFormGroup.get('disclaimerSlideText').value.toString());
  }
}

export interface DialogStartData {
  // I don't know why, but injecting the service the usual way results in error when getting the tools version.
  sessionService: SessionServiceService;
}

export interface DialogStartReturnData {
  fileHandler: FileHandler;
  images: IImageWrapper[];
}
