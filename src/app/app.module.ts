import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ImportFileComponent } from './import-file/import-file.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatBottomSheetModule,
  MatButtonModule, MatCardModule, MatDatepickerModule,
  MatDialogModule, MatExpansionModule,
  MatIconModule,
  MatInputModule, MatListModule, MatNativeDateModule, MatProgressBarModule,
  MatSelectModule,
  MatSnackBarModule, MatStepperModule, MatTabsModule,
  MatToolbarModule, MatTooltipModule
} from '@angular/material';
import { DisplayImagesFromFileComponent } from './display-images-from-file/display-images-from-file.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import { ImageFromFileComponent } from './image-from-file/image-from-file.component';
import { SearchComponent } from './search-external-api/search/search.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { DialogFileGettingReadyComponent } from './dialog-file-getting-ready/dialog-file-getting-ready.component';
import { DialogStartComponent } from './dialog-start/dialog-start.component';
import { MakeOerComponent } from './make-oer/make-oer.component';
// tslint:disable-next-line:max-line-length
import { DialogWarningImagesRatioMismatchComponent } from './dialog-warning-images-ratio-mismatch/dialog-warning-images-ratio-mismatch.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { ResultsComponent } from './search-external-api/results/results.component';
import { ImageFromOnlineSearchComponent } from './image-from-online-search/image-from-online-search.component';
import { DialogEditImageComponent } from './compare-images/dialog-edit-image/dialog-edit-image.component';
import { SearchDatabaseForOccurencesComponent } from './search-database-for-occurences/search-database-for-occurences.component';
import { DialogSuggestionsFromDatabaseComponent } from './dialog-suggestions-from-database/dialog-suggestions-from-database.component';
import {CompareImagesModule} from './compare-images/compare-images.module';
import {HeaderModule} from './header/header.module';
import {LoadingSpinnerModule} from './loading-spinner/loading-spinner.module';
import {ImageGalleryModule} from './image-gallery/image-gallery.module';
import {BottomSheetGenericComponent} from './bottom-sheet-generic/bottom-sheet-generic.component';
import {BottomSheetGenericModule} from './bottom-sheet-generic/bottom-sheet-generic.module';
import {SearchExternalApiModule} from './search-external-api/search-external-api.module';
import { OerFormularComponent } from './oer-formular/oer-formular.component';
import { ReplaceByImageFromMachineComponent } from './replace-by-image-from-machine/replace-by-image-from-machine.component';
import { DialogAskForFeedbackComponent } from './dialog-ask-for-feedback/dialog-ask-for-feedback.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { SeeFeedbackComponent } from './see-feedback/see-feedback.component';
import { ExcludeFromOverallLicenseComponent } from './exclude-from-overall-license/exclude-from-overall-license.component';

import { FormsModule } from '@angular/forms';
import {MatSliderModule} from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDividerModule} from '@angular/material/divider';
import {MatRadioModule} from '@angular/material/radio';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';



@NgModule({
  declarations: [
    AppComponent,
    ImportFileComponent,
    MainComponent,
    DisplayImagesFromFileComponent,
    ImageFromFileComponent,
    MainHeaderComponent,
    DialogFileGettingReadyComponent,
    DialogStartComponent,
    MakeOerComponent,
    DialogWarningImagesRatioMismatchComponent,
    ImageFromOnlineSearchComponent,
    SearchDatabaseForOccurencesComponent,
    DialogSuggestionsFromDatabaseComponent,
    OerFormularComponent,
    ReplaceByImageFromMachineComponent,
    DialogAskForFeedbackComponent,
    FeedbackComponent,
    SeeFeedbackComponent,
    ExcludeFromOverallLicenseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatInputModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatToolbarModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatDialogModule,
    MatProgressBarModule,
    MatTabsModule,
    ScrollingModule,
    MatStepperModule,
    MatExpansionModule,
    MatTooltipModule,
    CompareImagesModule,
    MatCardModule,
    MatBottomSheetModule,
    MatListModule,
    HeaderModule,
    LoadingSpinnerModule,
    ImageGalleryModule,
    BottomSheetGenericModule,
    SearchExternalApiModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MatSliderModule,
    MatFormFieldModule,
    MatDividerModule,
    MatRadioModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogFileGettingReadyComponent,
    DialogStartComponent,
    DialogWarningImagesRatioMismatchComponent,
    DialogEditImageComponent,
    DialogSuggestionsFromDatabaseComponent,
    BottomSheetGenericComponent,
    DialogAskForFeedbackComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
