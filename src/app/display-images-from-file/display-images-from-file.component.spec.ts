import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayImagesFromFileComponent } from './display-images-from-file.component';

describe('DisplayImagesFromFileComponent', () => {
  let component: DisplayImagesFromFileComponent;
  let fixture: ComponentFixture<DisplayImagesFromFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayImagesFromFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayImagesFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
