import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IImageWrapper} from '../../models/image-wrapper';
import {Observable, Subject} from 'rxjs';

/**
 * Displays all available images, the images are being passed
 * @input images An array of image wrappers
 * @output select Triggered when an image was clicked on/chosen. Then that image is being propagated upwards
 */
@Component({
  selector: 'app-display-images-from-file',
  templateUrl: './display-images-from-file.component.html',
  styleUrls: ['./display-images-from-file.component.scss']
})
export class DisplayImagesFromFileComponent implements OnInit {
  @Input('images') images: IImageWrapper[];
  @Output('select') _selectedImage: EventEmitter<IImageWrapper> = new EventEmitter<IImageWrapper>();

  private _selectedImageLocalRef: IImageWrapper;

  /**
   * Subject for clearing selections
   */
  private _subjectClearSelectedImage: Subject<number> = new Subject<number>();

  constructor() {
  }

  ngOnInit() {
  }

  /**
   * Executed when an image from the results is selected
   * @param image The selected image
   */
  public onSelect(image: IImageWrapper): void {
    if (!(image instanceof MouseEvent)) {
      /*// If there is a local ref, check the interal ID against the just selected one
      const isDifferentImage = (this._selectedImageLocalRef && this._selectedImageLocalRef.internalId !== image.internalId);
      if (!this._selectedImageLocalRef || isDifferentImage) {
        if (this._selectedImageLocalRef) {
          this._subjectClearSelectedImage.next(this._selectedImageLocalRef.internalId);
        }
        this._selectedImageLocalRef = image;
        this._selectedImage.next(image);
      }*/
      this._selectedImage.next(image);
    }
  }
}
