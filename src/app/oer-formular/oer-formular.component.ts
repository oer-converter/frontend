import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Licenses, LicensesBaseHref, LicensesHumanReadable} from '../../models/licenses';
import {SessionServiceService} from '../services/session-service.service';
import {EImageRole} from '../../models/image-wrapper';
import {MAT_DATE_LOCALE} from '@angular/material';

interface ILicensesDropDownMenu {
  name: string;
  version: string;
  deeds: string;
  enum: number;
}

export interface IOerFormularValues {
  author?: string;
  authorAffiliation?: string;
  authorUrl?: string;
  description?: string;
  license: ILicensesDropDownMenu;
  originName?: string;
  originUrl?: string;
  title?: string;
  imageRole: EImageRole;
  screenshot?: {
    date: Date;
    description: string;
  };
}

@Component({
  selector: 'app-oer-formular',
  templateUrl: './oer-formular.component.html',
  styleUrls: ['./oer-formular.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'us-US'},
  ]
})
export class OerFormularComponent implements OnInit {
  @Output() submit: EventEmitter<IOerFormularValues> = new EventEmitter<IOerFormularValues>();

  @Input('button-text') buttonText: string;

  @Input('submit-button-disabled') disableButton: boolean;

  public rootFormGroup: FormGroup;

  public licenses: ILicensesDropDownMenu[];
  public licensesEnumAccessor = Licenses;

  public imageRoles = EImageRole;

  constructor(
    private _session: SessionServiceService
  ) { }

  ngOnInit() {
    this._compileLicensesObject();
    const _urlRegex = /^[A-Za-z][A-Za-z\d.+-]*:\/*(?:\w+(?::\w+)?@)?[^\s/]+(?::\d+)?(?:\/[\w#!:.?+=&%@\-/]*)?$/;
    this.rootFormGroup = new FormGroup({
      author: new FormControl(),
      license: new FormControl(this.licenses[0]),
      title: new FormControl(),
      authorAffiliation: new FormControl(),
      description: new FormControl(),
      // props to Victor Dias for commenting @ https://stackoverflow.com/a/50509022/7618184
      originUrl: new FormControl(null, Validators.pattern(_urlRegex)),
      originName: new FormControl(),
      authorUrl: new FormControl(null, Validators.pattern(_urlRegex)),
      imageRole: new FormControl(this.imageRoles.photo, Validators.required),
      screenshot: new FormGroup({
        date: new FormControl(),
        description: new FormControl()
      })
    });
  }

  public onAuthorInputChange(newInput: any): void {
    // somehow the InputEvent is not recognized by ts, and the @types/dom-inputevent does not seem to work with me
    if (newInput && newInput.data && newInput.data === '.') {
      // by checking if the new input is a dot we do not need to see if the text input contains '...' upon every single change
      if (this.rootFormGroup.get('author').value === '...') {
        this.rootFormGroup.get('author').setValue(this._session.getCurrentUserRealName());
      }
    }
  }

  private _compileLicensesObject(): void {
    const arr: ILicensesDropDownMenu[] = [];
    // start at 1 to skip the 'unknown' license
    for (let i = 1; i <= this._session.getTargetLicense(); i++) {
      // if i = 1 then we're looking at CC0 and it only hast version 1.0
      const _version = i === 1 ? '1.0' : '4.0';
      arr.push({
        name: LicensesHumanReadable[i],
        version: _version,
        deeds: LicensesBaseHref[LicensesHumanReadable[i]] + _version,
        enum: i
      });
    }
    this.licenses = arr;
  }

}
