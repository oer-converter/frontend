import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OerFormularComponent } from './oer-formular.component';

describe('OerFormularComponent', () => {
  let component: OerFormularComponent;
  let fixture: ComponentFixture<OerFormularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OerFormularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OerFormularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
