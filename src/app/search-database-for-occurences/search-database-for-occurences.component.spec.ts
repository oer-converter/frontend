import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDatabaseForOccurencesComponent } from './search-database-for-occurences.component';

describe('SearchDatabaseForOccurencesComponent', () => {
  let component: SearchDatabaseForOccurencesComponent;
  let fixture: ComponentFixture<SearchDatabaseForOccurencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDatabaseForOccurencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDatabaseForOccurencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
