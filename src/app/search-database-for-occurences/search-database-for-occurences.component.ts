import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IImageWrapper} from '../../models/image-wrapper';
import {HttpService} from '../services/http.service';
import {MD5} from '@hexxag0nal/crypto';
import {ResponseGetReplacementImages} from '../../models/backend';
import {PptxHandler} from '../../classes/pptx-handler';
import {SessionServiceService} from '../services/session-service.service';
import {Crypto} from '@hexxag0nal/crypto';
import {BackendService} from '../services/backend.service';

@Component({
  selector: 'app-search-database-for-occurences',
  templateUrl: './search-database-for-occurences.component.html',
  styleUrls: ['./search-database-for-occurences.component.scss']
})
/**
 * This component will search the database
 */
export class SearchDatabaseForOccurencesComponent implements OnInit, OnChanges {
  /**
   * The images
   */
  @Input('images') images: IImageWrapper[];

  /**
   *
   */
  private _wereReplacedBefore: {[checksum: string]: ResponseGetReplacementImages[]};

  constructor(
    private _http: HttpService,
    private _session: SessionServiceService,
    private _backend: BackendService
  ) { }

  /**
   *
   */
  ngOnInit() {
  }

  /**
   * Listens for changes in the model
   * @param changes The new images
   */
  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (this.images) {
      this._session.awaitAsyncAction();
      // this._wereReplacedBefore = {};
      for (const image of this.images) {
        const _fileHandler: PptxHandler = this._session.getFileHandler() as PptxHandler;
        // const fileEnding: string = image.source.split('.')[1];
        // tslint:disable-next-line:max-line-length
        // TODO: move this into the data url class, expose a function that creates an object or string from the extension, the data and isBase64
        // tslint:disable-next-line:max-line-length
        // const base64String = `data:${MimeType.fromFileExtension(fileEnding).toString()};base64,` +  (await (await _fileHandler.getZip()).file(image.source).async('base64'));

        const checksum: MD5 = Crypto.md5(image.base64.toString());

        console.log('CC', checksum)

        // check for replacement suggestions
        const resultReplacements = await this._backend.checkIfImageByChecksumWasReplacedBefore(checksum);
        console.log('BB', resultReplacements)
        if (resultReplacements) {
          // this._wereReplacedBefore[checksum] = resultReplacements;
          if (!image.apiHistory) {
            image.apiHistory = {
              replacedBy: resultReplacements
            };
          } else {
            image.apiHistory.replacedBy = resultReplacements;
          }
        }

        // check for made oer suggestion
        const resultsMadeOer = await this._backend.checkIfImageByChecksumWasMadeOerBefore(checksum);
        if (resultsMadeOer) {
          // this._wereReplacedBefore[checksum] = resultReplacements;
          if (!image.apiHistory) {
            image.apiHistory = {
              madeOer: resultsMadeOer
            };
          } else {
            image.apiHistory.madeOer = resultsMadeOer;
          }
        }
      }
      this._session.doneAsyncAction();
    }
  }
}
