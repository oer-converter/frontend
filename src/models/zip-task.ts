import { Observable } from 'rxjs';
import {ZipTaskProgress} from './zip-task-progress';

/**
 * Info about tasks operating on a zip file
 */
export interface ZipTask {
  /**
   * The progress
   */
  progress: Observable<ZipTaskProgress>;
  /**
   * Additional data
   */
  data: Observable<Blob>;
}
