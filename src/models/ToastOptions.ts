import {MatSnackBarConfig} from '@angular/material';

/**
 * Options for a toast/snackbar
 */
export interface ToastOptions extends MatSnackBarConfig {
  /**
   * The message that is shown
   */
  actionMessage?: string;
  /**
   * The function that shall be executed if the user clicks the proper button on the toast/snackbar
   */
  action?: () => void;
}
