/**
 * This interface will be used when converting specific results from their API into an object
 * that is understood throughout the tool
 */
import {IImageWrapper} from './image-wrapper';
import {Licenses} from './licenses';

export interface IStandardizedImageSearchResults {
  /**
   * Number of pages that are available for this query
   */
  pageCount: number;
  /**
   * Number of total results for this query
   */
  resultCount: number;
  /**
   * Current page's number
   */
  pageCurrent: number;
  /**
   * Number of items on this page
   */
  pageSize: number;
  /**
   * The images from the result. Already converted to default format
   */
  results: IImageWrapper[];
  /**
   * The licenses this query was made against
   */
  license?: Licenses[];
  /**
   * The API which was targeted for this query
   */
  targetApi: 'cc' | 'flickr';
}
