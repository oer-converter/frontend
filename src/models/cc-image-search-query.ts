/**
 * see here https://api.creativecommons.engineering/#operation/image_search
 */

export interface CcImageSearchQuery {
  q?: string;
  license?: string;
  license_type?: string;
  page?: number;
  page_size?: number;
  creator?: string;
  tags?: string;
  title?: string;
  filter_dead?: boolean;
  source?: string[];
  extension?: string;
  categories?: string[];
  size?: 'small' | 'medium' | 'large';
  qa?: boolean;
}
