/**
 * Describes the necessary fields in the body of such a request. Is an exact copy of replace-image.request.interface.ts of the backend
 */
import {DataUrl} from '@hexxag0nal/data-url';

export interface RequestReplacementAction {
  /**
   * Info regarding the document the action was performed on
   */
  document: {
    /**
     * The document's GUID
     */
    guid: string;
    /**
     * The internal path of the file that was replaced
     */
    internalPath: string;
    /**
     * The document's MIME type
     */
    mimeType: string;
  }
  /**
   * Info about the image that is being replaced
   */
  replacedImage: {
    /**
     * The checksum of the image that is being replaced
     */
    checksum: string;
  };
  /**
   * Info about the image that replaces
   */
  replacingImage: {
    /**
     * The name of the source api that was used to find this image
     */
    sourceApi: string;
    /**
     * The url where the image can be found
     */
    sourceUrl: string;
    /**
     * The author's user name, if given
     */
    authorUserName?: string;
    /**
     * The author's real name, if given
     */
    authorRealName?: string;
    /**
     * The width of the original image
     */
    width: number;
    /**
     * The height of the original image
     */
    height: number;
    checksum: string;
    archiveUrl?: string;
    /**
     * The license this image was published under
     */
    license: {
      /**
       * The name of the license used
       */
      name: string;
      /**
       * The version of the license used
       */
      version: string;
    };
    /**
     * If the image chosen was cropped, resized or cut-out, then this field needs to be accessible
     */
    edited?: {
      /**
       * The new width
       */
      width: number;
      /**
       * The new height
       */
      height: number;
      /**
       * The edited image as a data url
       */
      dataUrlBase64: string;
      checksum: string;
    }
  }
}


/**
 * Describes the necessary fields in the body of such a request. Is an exact copy of made-image-oer.request.interface.ts of the backend
 */
export interface RequestMadeOerAction {
  dataUrlBase64: string;
  checksum: string;
  width: number;
  height: number;
  license: {
    name: string;
    version: string;
  };
  author?: {
    name?: string;
    url?: string;
    affiliation?: string;
  };
  title?: string;
  document: {
    guid: string;
    mimeType: string;
  };
}

/**
 * Describes the answer to a request to list all replacement images to a given checksum
 */
export interface ResponseGetReplacementImages {
  source_api: string;
  source_url: string;
  author_real_name?: string;
  author_user_name?: string;
  width: number;
  height: number;
  license_name: string;
  license_version: string;
  license_deeds: string;
  edited_data_url_base64?: DataUrl;
  edited_width?: number;
  edited_height?: number;
}

/**
 * Describes the response to for a request to get all media made oer with a given checksum
 */
export interface ResponseGetMadeOerImages {
  id: number;
  data_url_base64: string;
  checksum: string;
  width: number;
  height: number;
  media_type: string;
  author_name?: string;
  author_url?: string;
  author_affiliation?: string;
  title?: string;
  license_name: string;
  license_version: string;
  license_deeds: string;
}
