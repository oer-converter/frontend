/**
 * This file contains all relevant definitions for the receipt
 */


import {GUID, UUID} from '@hexxag0nal/uuid';
import {MimeType} from '@hexxag0nal/mime-type';
import {MD5} from '@hexxag0nal/crypto';
import {DataUrl} from '@hexxag0nal/data-url';
import {Document} from './document.model';

/**
 * A receipt object
 */
export class Receipt {
  /**
   * current replacement actions
   */
  private _currentSessionReplacementActions: IMediumReplacedAction[] = [];
  /**
   * current declared oer actions
   */
  private _currentSessionPublishAsOerActions: IImagePublishedAsOerAction[] = [];

  /**
   * The document
   */
  private _document: Document;

  /**
   * @param document The document
   * @param history History
   * @param options Some further options
   */
  constructor(document: Document, history?: History, options?: ReceiptOptions) {
    this._document = document;
    if (history) {
      this._currentSessionReplacementActions.concat(history.actions.replacement);
      this._currentSessionPublishAsOerActions.concat(history.actions.publishAsOer);
    }
    if (options) {
    }
  }

  /**
   * Announce a new replacement action
   * @param action The action parameters
   */
  public addMediumReplacedAction(action: IMediumReplacedAction): void {
    this._currentSessionReplacementActions.push(action);
  }

  /**
   * Announce a new action, that an image was declared OER
   * @param action The action parameters
   */
  public addImagePublishedAsOerAction(action: IImagePublishedAsOerAction): void {
    this._currentSessionPublishAsOerActions.push(action);
  }

  /**
   * Serialize to string
   */
  public  toString(): string {
    return JSON.stringify({
      document: this._document,
      history: this.getHistory()
    } as IReceiptSerialized);
  }


  static parseFromString(string: string): Receipt {
    const _parsed: IReceiptSerialized = JSON.parse(string);
    return new Receipt(_parsed.document, _parsed.history);
  }
  // As of now, there are not session states planned, but keep the code if it is needed in the future
/*
  /!**
   * End the current session and push it to the history. Identical to startNewSession()
   *!/
  public endSession(): void {
    this._history.push(this._createSessionObjectFromCurrentSession());
  }

  /!**
   * End the current session and push it to the history. Identical to endSession()
   *!/
  public startNewSession(): void {
    this.endSession();
  }*/
  /**
   * Check if the history is empty
   */
  private _isHistorySessionEmpty(): boolean {
    return this._currentSessionPublishAsOerActions.length === 0 && this._currentSessionReplacementActions.length === 0;
  }

  /**
   * Get the known history
   */
  public getHistory(): History {
    return {
      actions: {
        publishAsOer: this._currentSessionPublishAsOerActions,
        replacement: this._currentSessionReplacementActions
      }
    } as IHistory;
  }

  /**
   * Create a string that resembles the content of a CSV file
   * @param printReduced Whether or not only a reduced amount of info shall be put into the csv string
   */
  public toCsv(printReduced: boolean = true): string {
    let prettyString: string = '';
    const receiptHeaderText: string = 'Lorem ipsum dolor sit amet, consectetur...\n';

    const tableReplacementActionTitle = 'Replacement actions:\n';
    const tableReplacementActionHeader: string = 'replaced medium;; ;new medium;;;;;;;;;;;\ninternal path;checksum; ;checksum;width;height;provider;license;name of author;username of author;is an edited variant of some original;original width;original height;original checksum;\n';
    let tableReplacementActionBody: string = '';
    for (const action of this._currentSessionReplacementActions) {
      const height: string = action.isReplacing.edited ? action.isReplacing.edited.height.toString() : action.isReplacing.height.toString();
      const originalHeight: string = action.isReplacing.edited ? action.isReplacing.height.toString() : '';

      const width: string = action.isReplacing.edited ? action.isReplacing.edited.width.toString() : action.isReplacing.width.toString();
      const originalWidth: string = action.isReplacing.edited ? action.isReplacing.width.toString() : '';

      const license: string = `${action.isReplacing.license.name} ${action.isReplacing.license.version}`;

      const checksum: string = action.isReplacing.edited ? action.isReplacing.edited.checksum : action.isReplacing.checksum;
      const originalChecksum: string = action.isReplacing.edited ? action.isReplacing.checksum : '';

      tableReplacementActionBody += `${action.wasReplaced.internalPath};${action.wasReplaced.checksum}; ;${checksum};${width};${height};${action.isReplacing.sourceApi};${license};${action.isReplacing.authorRealName || ''};${action.isReplacing.authorUserName || ''};${action.isReplacing.edited != null};${originalWidth};${originalHeight};${originalChecksum}\n`;
    }
    const tableReplacementAction = tableReplacementActionTitle + tableReplacementActionHeader + tableReplacementActionBody;

    const tableOerActionTitle = 'Made OER actions:\n';
    const tableOerActionHeader: string = 'title;checksum;width;height;author name; author url;author affiliation;license\n';
    let tableOerActionBody: string = '';
    for (const action of this._currentSessionPublishAsOerActions) {
      const license: string = `${action.license.name} ${action.license.version}`;
      const authorName = action.author ? action.author.name || '' : '';
      const authorUrl = action.author ? action.author.url || '' : '';
      const authorAffiliation = action.author ? action.author.affiliation || '' : '';
      // tslint:disable-next-line:max-line-length
      tableOerActionBody += `${action.title || ''};${action.checksum};${action.width};${action.height};${authorName};${authorUrl};${authorAffiliation};${license}\n`;
    }
    const tableOerAction = tableOerActionTitle + tableOerActionHeader + tableOerActionBody;

    prettyString += receiptHeaderText + '\n' + tableReplacementAction + '\n' + tableOerAction;
    return prettyString;
  }

  /**
   * Get the document's GUID
   */
  public getDocumentGUID(): GUID {
    return this._document.getGuid();
  }

  /**
   * Get the documents MIME type
   */
  public getDocumentMIMEType(): MimeType {
    return this._document.getMimeType();
  }
}

// Think about this later
/*export class PrettyReceiptTemplate {
}

export interface PrettyReceiptTemplateOptions {
  header?: string;
}*/

/**
 * This interface describes a single receipt object that keeps track of changes and what not during one session
 */
export interface IHistory {
  /**
   * Actions that took place so far
   */
  actions: {
    /**
     * Replacement actions that took place
     */
    replacement: IMediumReplacedAction[];
    /**
     * OER declarations that took place
     */
    publishAsOer: IImagePublishedAsOerAction[];
  };
}

/**
 * To make it available under the other name
 */
export type History = IHistory;

/**
 * Parameters for a replacement action
 */
export interface IMediumReplacedAction {
  /**
   * The medium that was replaced
   */
  wasReplaced: IReplacedMedium;
  /**
   * The new medium that replaces
   */
  isReplacing: IReplacingMedium;
  /**
   * Whether or not this action shall be announced to the backend
   */
  doNotUpload?: boolean;
}

/**
 * Parameters for a replaced medium
 */
export interface IReplacedMedium {
  /**
   * The internal path within the file
   */
  internalPath: string;
  /**
   * Its checksum
   */
  checksum: MD5;
}

/**
 * Additional info on a replacing medium
 */
export interface IReplacingMedium extends IReplacingMediumReduced {
  /**
   * The data url encoded as base64
   */
  dataUrlBase64: DataUrl;
  /**
   * Parameters for an edited version
   */
  edited?: IEditedVariantReplacingMedium;
  /**
   * The source url of the replacing medium
   */
  sourceUrl: string;
  /**
   * The license it was published under
   */
  license: ILicense;
}

/**
 * Additional info on a replacing medium's edited variant
 */
export interface IEditedVariantReplacingMedium extends IEditedVariantReplacingMediumReduced {
  dataUrlBase64: DataUrl;
}

/**
 * Additional info on a medium declared as OER
 */
export interface IImagePublishedAsOerAction extends IImagePublishedAsOerActionReduced {
  /**
   * The data url encoded as base64
   */
  dataUrlBase64: DataUrl;
  /**
   * The license it was published under
   */
  license: ILicense;
}

/**
 * Minimum info on a medium declared as OEr
 */
export interface IImagePublishedAsOerActionReduced {
  /**
   * Path within the file
   */
  internalPath: string;
  /**
   * The medium's title
   */
  title?: string;
  /**
   * The checksum
   */
  checksum: MD5;
  /**
   * The width
   */
  width: number;
  /**
   * The height
   */
  height: number;
  /**
   * The author
   */
  author?: {
    /**
     * The author's name
     */
    name?: string;
    /**
     * An url to the author's web presence
     */
    url?: string;
    /**
     * The author's affiliation
     */
    affiliation?: string;
  };
  /**
   * The license it was published under
   */
  license: ILicenseReduced;
  /**
   * Whether or not this action shall be announced to the backend
   */
  doNotUpload?: boolean;
}

/**
 * To make it avaliable under this name
 */
export type ReceiptOptions = IReceiptOptions;

/**
 * Options for the receipt
 */
interface IReceiptOptions {
}

/**
 * Minimum info on a replaced medium
 */
export interface IReplacedMediumReduced {
  /**
   * The path within the file
   */
  internalPath: string;
  /**
   * The checksum
   */
  checksum: MD5;
}

/**
 * Minimum info on a replacing medium
 */
export interface IReplacingMediumReduced {
  /**
   * An url to a webarchive.org permalink
   */
  archiveUrl?: string;
  /**
   * The width
   */
  width: number;
  /**
   * The height
   */
  height: number;
  /**
   * The checksum
   */
  checksum: MD5;
  /**
   * The source API
   */
  sourceApi: string;
  /**
   * The author's username
   */
  authorUserName?: string;
  /**
   * The author's real name
   */
  authorRealName?: string;
  /**
   * The license it was published under
   */
  license: ILicenseReduced;
  /**
   * Info about an edited variant
   */
  edited?: IEditedVariantReplacingMediumReduced;
}

/**
 * Additional info on a license
 */
interface ILicense extends ILicenseReduced {
  /**
   * The url to the license's deeds
   */
  deedsUrl: string;
}

/**
 * Minimum info on a license
 */
interface ILicenseReduced {
  /**
   * The name
   */
  name: string;
  /**
   * The version
   */
  version: string;
}

/**
 * Minimum info on a replacing medium's edited variant
 */
export interface IEditedVariantReplacingMediumReduced {
  /**
   * The height
   */
  height: number;
  /**
   * The width
   */
  width: number;
  /**
   * The checksum
   */
  checksum: MD5;
}

/**
 * Describes the fields in a reduced receipt, i.e. for printing receipt that do not data urls (because they're saved
 * in the db anyway).
 */
export interface IHistoryReduced {

}

/**
 * Parameters for a serialized receipt
 */
export interface IReceiptSerialized {
  /**
   * The history
   */
  history?: IHistory;
  /**
   * The corresponding document
   */
  document: Document;
}
