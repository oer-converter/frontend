import {UUID} from '@hexxag0nal/uuid';
import {MimeType} from '@hexxag0nal/mime-type';

export class Document {
  /**
   * The GUID/UUID
   */
  private readonly guid: UUID;
  /**
   * The MIME type
   */
  private readonly mimeType: MimeType;

  constructor(guid: UUID, mimeType: MimeType) {
    this.guid = guid;
    this.mimeType = mimeType;
  }

  public getGuid(): UUID {
    return this.guid;
  }

  public getMimeType(): MimeType {
    return this.mimeType;
  }
}
