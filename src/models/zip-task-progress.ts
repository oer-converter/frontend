/**
 * Describing the progress on a zip task
 */
export interface ZipTaskProgress {
  /**
   * tasks done
   */
  active: boolean;
  /**
   * Current task
   */
  current?: number;
  /**
   * Total tasks
   */
  total?: number;
}
