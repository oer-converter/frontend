import {Licenses} from './licenses';
import {ResponseGetMadeOerImages, ResponseGetReplacementImages} from './backend';
import {MD5} from '@hexxag0nal/crypto';
import {DataUrl} from '@hexxag0nal/data-url';

export type EditingAction = 'cropped' | 'rotated' | 'unknown';

export enum EImageRole {
  photo,
  screenshot,
  graph
}

export interface IImageWrapperEdited {
  /**
   * The name that shall be displayed if the image was edited
   */
  editor?: string;
  /**
   * The new dimensions
   */
  dimension: {
    height: number;
    width: number;
    ratio: number;
  };
  /**
   * The actions that were performed to get this result. Necessary for licensing stuff
   */
  action: EditingAction[];
  /**
   * The base64 DataUrl that resembles the edited variant of the image
   */
  base64: DataUrl;
}

// TODO: differ between images from file and from search and maybe implement real objects
/**
 * Describes an image object. Suits the images from search as well the ones from file
 */
export interface IImageWrapper {
  /**
   * the id given the API result, if any ID was given
   */
  externalId?: string;
  /**
   * internal substitute ID if no external ID is given (e.g. images from file)
   */
  internalId?: number;
  /**
   * The image as base64 data-url
   */
  base64: DataUrl;
  /**
   * The license the image was published under
   */
  license?: Licenses;
  /**
   * The license's version the image was published under
   */
  licenseVersion?: string;
  licenseUrl?: string;
  /**
   * A description of the image
   */
  description?: string;
  /**
   * The title of the image
   */
  title?: string;
  /**
   * The source of the image
   */
  source?: string;
  /**
   * The source of a thumbnail
   */
  thumbnailSource?: string;
  author?: string;
  /**
   * The dimensions of the image
   */
  dimension: {
    height: number;
    width: number;
    ratio: number;
  };
  /**
   * An edited variant of the image
   */
  edited?: IImageWrapperEdited;
  // The following line does not make sense, since the ref to the ImageElement does not already exist upon creation.
  // Leaving that line to remind me that I've tried it that way.
  // htmlImageElement?: HTMLImageElement;
  checksum?: MD5;
  apiHistory?: {
    replacedBy?: ResponseGetReplacementImages[];
    madeOer?: ResponseGetMadeOerImages[];
  };
  doNotUpload?: boolean;

  // Mohib added for testing. lets see if works or not... 
  moShow?: boolean;

}