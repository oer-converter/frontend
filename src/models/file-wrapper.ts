/**
 * Wraps the file and some info
 */
interface FileWrapper {
  /**
   * The file's MIME type
   */
  type: string;
  /**
   * The file itself
   */
  file: File;
}
