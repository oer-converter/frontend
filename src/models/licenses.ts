/**
 * Don't forget to adapt the human readable version if changed
 */
export enum Licenses {
  UNKNOWN,
  CC_ZERO,
  CC_BY,
  CC_BY_SA
}

/**
 * The licenses in a format readable for humans (e.g. as string)
 */
export const LicensesHumanReadable: string[] = [
  'Unknown',
  'CC-0',
  'CC-BY',
  'CC-BY-SA'
];

/**
 * The base urls to the licenses' deeds, only the version needs to be appended
 */
export const LicensesBaseHref = {
  'Unknown': '',
  'CC-0': 'https://creativecommons.org/publicdomain/zero/',
  'CC-BY': 'https://creativecommons.org/licenses/by/',
  'CC-BY-SA': 'https://creativecommons.org/licenses/by-sa/'
};

/**
 * Get the correct enum for a given license name
 * @param name The name of the license
 */
export function licenseNameToEnum(name: string): Licenses {
  switch (name.toLowerCase()) {
    case 'cc0' || 'cc-0':
      return Licenses.CC_ZERO;
    case 'unknown':
      return Licenses.UNKNOWN;
    case 'cc-by':
      return Licenses.CC_BY;
    case 'cc-by-sa':
      return Licenses.CC_BY_SA;
  }
}
