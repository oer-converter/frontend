/**
 * Used by angular to serve variables based on the the environment
 */
export const environment = {
  /**
   * This is a production environment
   */
  production: true
};
