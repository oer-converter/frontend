/**
 * Defines the possible options for adding new source slides
 */

export  interface IOpenXmlPresentationAddSourceSlidesOptions {
  /**
   * Any value smaller than 1 indicates infinitely many sources per slide (resulting in exactly one slide)
   * If the number of provided sources is greater than the max number, then the remaining sources will be put onto another slide
   */
  readonly maxNumberOfSourcesPerSlide?: number;
}
