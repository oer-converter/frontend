import {ITupleXY} from './ITupleXY';

/**
 * Dimension options
 */
export interface IShapeDimensions {
  /**
   * Offset from relative anchor point
   */
  offset: ITupleXY;
  /**
   * Basically the width and the height of the shape
   */
  ext: ITupleXY;
}
