/**
 * Defines the field for a slide object to have. Specialized slides inherit from this interface.
 * There are some keywords available for writing the template strings, that will be replaced by actual content. Available so far:
 * - __NEXT_SLIDE_LAYOUT_NUMBER__: When passing a new slide layout to be added, this can be used to reference the new number.
 *   For example: If there are eigth layouts so far, and a ninth is being added, then __NEXT_SLIDE_LAYOUT_NUMBER__ will resolve to '9'
 */
export interface ISlide {
  /**
   * The title for the slide, even it may not be displayed
   */
  // title: string;
  /**
   * Contains info on the slide content itself
   */
  slide: {
    /**
     * XML serialized string for the slide###.xml
     */
    xmlString: string;
    /**
     * XML serialized string for the slide###.xml.rels. Can i.e. be used to manipulate targets to media files or layouts
     */
    xmlRelsString: string;
  };
  /**
   * Contains info on the layout of the slide (if a new layout is added instead of a preexisting one being used)
   */
  layout?: {
    /**
     * XML serialized string for slideLayout###.xml. Can be used to add new layouts. Not mandatory for a new slide as it can simply use one of
     * those preexisting layouts that come with the OpenXML file
     */
    xmlLayoutString: string;
    /**
     * XML serialized string for slideLayout###.xml.rels
     */
    xmlLayoutRelsString: string;
  };
}
