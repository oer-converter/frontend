import {IOpenXmlPresentationAddSourceSlidesOptions} from '../interfaces/IOpenXmlPresentationAddSourceSlidesOptions';

export const OpenXmlPresentationAddSourceSlidesDefaultOptions: IOpenXmlPresentationAddSourceSlidesOptions = {
  maxNumberOfSourcesPerSlide: 0
};
