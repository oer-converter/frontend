import * as JSZip from 'jszip';
import {IOpenXmlPresentationConfig, OpenXmlPresentationConfig} from './config';
import {JsZipHelper} from '../../../classes/js-zip-helper';
import {JSZipObject} from 'jszip';
import {ISlide} from './interfaces/ISlide';
import {IOpenXmlPresentationAddSourceSlidesOptions} from './interfaces/IOpenXmlPresentationAddSourceSlidesOptions';
import {isNull, isUndefined} from 'util';
import {ISource} from './interfaces/ISource';
import {IReference} from './interfaces/IReference';
import {IMapSlideXmlRid} from './interfaces/IMapSlideXmlRid';
import {IPairRidReference} from './interfaces/IPairRidReference';
import {IShapeDimensions} from './interfaces/IShapeDimensions';
import {ITupleXY} from './interfaces/ITupleXY';
import {Licenses, LicensesBaseHref} from '../../../models/licenses';
import {MimeType} from '@hexxag0nal/mime-type';
import {InvalidArgumentError, NotYetImplemented} from '@hexxag0nal/error';
import {URI} from '@hexxag0nal/universal-ressource';

/**
 * This holds the namespaces that are used throughout the tool
 */
const xmlNS = {
  drawingml: {
    main: 'http://schemas.openxmlformats.org/drawingml/2006/main'
  },
  officeDocument: {
    relationships: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships',
    docPropsVTypes: 'http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes',
    customProperties: 'http://schemas.openxmlformats.org/officeDocument/2006/custom-properties'
  },
  presentationml: {
    main: 'http://schemas.openxmlformats.org/presentationml/2006/main',
  },
  package: {
    relationships: 'http://schemas.openxmlformats.org/package/2006/relationships',
    contentTypes: 'http://schemas.openxmlformats.org/package/2006/content-types'
  },
  relationships: {
    customProperties: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties'
  }
};

/**
 * This objects holds the schemata that are used throughout the tool
 */
const xmlSchemata = {
  slide: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide',
  hyperlink: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink'
};

/**
 * Describes the parameters needed when creating a new paragraph
 */
interface ICreateParagraphParameters {
  /**
   * The texts this paragraph shall contain
   */
  texts: IParagraphTextParameters[];
}

/**
 * Describes properties of texts that shall be appended to a paragraph node
 */
interface IParagraphTextParameters {
  /**
   * The text that will be written to a paragraph
   */
  text: string;
  /**
   * You can give the text some properties if wanted
   */
  properties?: {
    /**
     * If provided, the text will be made a hyperlink to the specified target
     */
    hyperlink?: {
      /**
       * The url of the target
       */
      url: string;
      /**
       * The xml.rels file of the correpsonding slideX.xml as document
       */
      xmlRel: Document;
    }
  };
}

/**
 * This class exposed functions to manipulate or create OOXML parts
 */
export class OpenXmlPresentationJs {
  /**
   * Add a source slide
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param title The title of the slide
   * @param sources The sources that shall be put onto that slide
   */
  private static async addSourceSlide(refToZip: JSZip, title: string, sources: ISource[]): Promise<JSZip> {
    // creating ppt/slideLayouts/slideLayout#NUMBER#.xml
    const _slideLayoutXmlContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
      // tslint:disable-next-line:max-line-length
      `<p:sldLayout xmlns:a="${xmlNS.drawingml.main}" xmlns:r="${xmlNS.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main" type="obj" preserve="1"><p:cSld name="Titel und Inhalt"><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="2" name="Titel 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{07A39B65-9269-4EFE-862B-28239DA065F3}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="title"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:r><a:rPr lang="de-DE"/><a:t>Mastertitelformat bearbeiten</a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="3" name="Inhaltsplatzhalter 2"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{834AD2E2-43DB-44C3-A861-F81DF7C5A913}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph idx="1"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:pPr lvl="0"/><a:r><a:rPr lang="de-DE"/><a:t>Mastertextformat bearbeiten</a:t></a:r></a:p><a:p><a:pPr lvl="1"/><a:r><a:rPr lang="de-DE"/><a:t>Zweite Ebene</a:t></a:r></a:p><a:p><a:pPr lvl="2"/><a:r><a:rPr lang="de-DE"/><a:t>Dritte Ebene</a:t></a:r></a:p><a:p><a:pPr lvl="3"/><a:r><a:rPr lang="de-DE"/><a:t>Vierte Ebene</a:t></a:r></a:p><a:p><a:pPr lvl="4"/><a:r><a:rPr lang="de-DE"/><a:t>Fünfte Ebene</a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="4" name="Datumsplatzhalter 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{9D0D1EE0-497A-44F7-A1A9-CD89EEEA72BC}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="dt" sz="half" idx="10"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{5D8EFFE2-AA75-411E-B166-DA043D84DB8C}" type="datetimeFigureOut"><a:rPr lang="de-DE" smtClean="0"/><a:t>09.01.2020</a:t></a:fld><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Fußzeilenplatzhalter 4"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{C4F906FE-D013-4E7C-86F4-70426A07A45A}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ftr" sz="quarter" idx="11"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="6" name="Foliennummernplatzhalter 5"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{658B3698-3230-483C-94C7-B54187DC40E8}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="sldNum" sz="quarter" idx="12"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{9DE91646-6837-40F9-92BD-80A4980E4DD1}" type="slidenum"><a:rPr lang="de-DE" smtClean="0"/><a:t>‹Nr.›</a:t></a:fld><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="2237627844"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sldLayout>`;
    // const _docSlideLayoutXml: Document = await new DOMParser().parseFromString(_slideLayoutXmlContent, 'application/xml');



    // creating ppt/slides/slide#NUMBER#.xml
    const _templateSlideXmlContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
      // tslint:disable-next-line:max-line-length
      `<p:sld xmlns:a="${xmlNS.drawingml.main}" xmlns:r="${xmlNS.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="4" name="Titel 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{182C4918-090F-4E93-9E46-65C1AF21D9E6}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="title"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Inhaltsplatzhalter 4"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{6B155D67-C27B-488C-87C8-3811EA56C936}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph idx="1"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="3284794006"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>`;
    const _docSlideXml: Document = await new DOMParser().parseFromString(_templateSlideXmlContent, 'application/xml');
    // [0] will be the title box, [1] the content box
    const _elemsSp = _docSlideXml.getElementsByTagName('p:sp');
    const _elemSpTitleBox = _elemsSp[0];
    const _elemSpContentBox = _elemsSp[1];
    const _elemTxBodyOfTitleBox = _elemSpTitleBox.getElementsByTagName('p:txBody')[0];
    const _elemTxBodyOfContentBox = _elemSpContentBox.getElementsByTagName('p:txBody')[0];
    // since we just created the slide#NUMMBER#.xml, the only <a:p> block in both boxes is the empty placeholder which has no text
    const _elemEmptyPOfTxBodyOfContentBox = _elemTxBodyOfContentBox.getElementsByTagName('a:p')[0];
    const _elemEmptyPOfTxBodyOfTitleBox = _elemTxBodyOfTitleBox.getElementsByTagName('a:p')[0];

    // remove the node that tells powerpoint that this box is empty
    _elemTxBodyOfContentBox.removeChild(_elemEmptyPOfTxBodyOfContentBox);
    _elemTxBodyOfTitleBox.removeChild(_elemEmptyPOfTxBodyOfTitleBox);

    // set the title
    _elemTxBodyOfTitleBox.appendChild(this._createParagraph({texts: [{text: title}]}).paragraph);

    const _slideLayoutXmlRelsContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
      // tslint:disable-next-line:max-line-length
      '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster" Target="../slideMasters/slideMaster1.xml"/></Relationships>';
    // find the number/position of the slide layout with the title 'Title and Content'/'Titel und Inhalt'

    // we assume that slide master 1 holds all the default layouts offered by powerpoint
    const _positionOfDesiredSlideLayout: number = await this._getPositionOfSlideLayoutByName(refToZip, OpenXmlPresentationConfig.slideLayoutName, 1);
   /* const _slideXmlRelsContent = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
      `<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout" Target="../slideLayouts/slideLayout__NEXT_SLIDE_LAYOUT_NUMBER__.xml"/></Relationships>`;*/
    const _slideXmlRelsContent = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
      `<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout" Target="../slideLayouts/slideLayout${_positionOfDesiredSlideLayout}.xml"/></Relationships>`;
    let _docSlideXmlRels = await new DOMParser().parseFromString(_slideXmlRelsContent, 'application/xml');


    // add the sources to the content box
    for (const source of sources) {
      const params: ICreateParagraphParameters = {
        texts: [
          {
            text: `${OpenXmlPresentationConfig.prefixRefAbbr}${source.refAbbreviation}${OpenXmlPresentationConfig.postfixRefAbbr}: Image `
          }
        ]
      };
      if (source.title) {
        params.texts.push({
          text: `"${source.title}" `
        });
      }
      if (source.author) {
        params.texts.push({
          text: 'by '
        });
        if (source.authorUrl) {
          params.texts.push({
            text: source.author + ' ',
            properties: {
              hyperlink: {
                url: source.authorUrl,
                xmlRel: _docSlideXmlRels
              }
            }
          });
        } else {
          params.texts.push({text: source.author + ' '});
        }
      }
      // license is always given
      params.texts.push({
          text: `under the license `
        },
        {
        text: `${source.licenseHumanReadable} ${source.licenseVersion}`,
        properties: {
          hyperlink: {
            url: LicensesBaseHref[source.licenseHumanReadable] + source.licenseVersion,
            xmlRel: _docSlideXmlRels
          }
        }
      });
      if (source.originName) {
        params.texts.push({
            text: ` via `,
          },
          {
            text: source.originName,
            properties: {
              hyperlink: {
                url: source.originUrl,
                xmlRel: _docSlideXmlRels
              }
            }
          });
      } else if (source.originUrl) {
        params.texts.push({
            text: ` via `,
          },
          {
            text: source.originUrl,
            properties: {
              hyperlink: {
                url: source.originUrl,
                xmlRel: _docSlideXmlRels
              }
            }
          });
      }
      // if image was edited, add info about it as seen on p. 117 of "Freie Unterrichtsmaterialien" by Jören Muuß-Merholz
      if (source.edited) {
        params.texts.push({
          text: `; Edited: ${source.edited.actions.join(', ')} by ${source.edited.editor}`
        });
      }
      const _paragraphObject = this._createParagraph(params);
      _elemTxBodyOfContentBox.appendChild(_paragraphObject.paragraph);
      _docSlideXmlRels = _paragraphObject.xmlRels;
    }
    // console.log('ppt/slides/slide#NUMBER#.xml', `slide${currentNumberOfSlides + 1}.xml`, _docSlideXml);

    // creating the slide object with the layout and the content of the slides, such that the object can be passed to the function
    // that appends a new slide
    const _xmlSerializer = new XMLSerializer();
    const slide: ISlide = {
      slide: {
        xmlString: _xmlSerializer.serializeToString(_docSlideXml),
        xmlRelsString: _xmlSerializer.serializeToString(_docSlideXmlRels)
      }
      // do not add the
      /*,
      layout: {
        xmlLayoutString: _slideLayoutXmlContent,
        xmlLayoutRelsString: _slideLayoutXmlRelsContent
      }*/
    };

    return this.appendSlide(refToZip, slide);
  }

  /**
   * Easy accessor for the config file
   */
  private static readConfig(): IOpenXmlPresentationConfig {
    return OpenXmlPresentationConfig;
  }

  /**
   * Checks if multiple slides are needed since they exceed the maximum number of sources per slide and then calls addSourceSlide
   * the proper number of times
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param sources The sources that shall be put onto slides
   * @param options Some options
   */
  // tslint:disable-next-line:max-line-length
  public static async addSourceSlides(refToZip: JSZip, sources: ISource[], options?: IOpenXmlPresentationAddSourceSlidesOptions): Promise<JSZip> {

    let _currentRefToZip: JSZip = refToZip;

    let _numberOfSlidesNeeded: number;
    let _numberOfSourcesPerSlide: number;

    const _processedCreditStrings: (string[])[] = [];
    if (!options) {
      // if no options were provided, fill in default values set in the config file
      _numberOfSlidesNeeded = sources.length / OpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
      _numberOfSourcesPerSlide = OpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
    } else {
      // if options were provided
      if (
        options.maxNumberOfSourcesPerSlide === null ||
        isNull(options.maxNumberOfSourcesPerSlide) ||
        isUndefined(options.maxNumberOfSourcesPerSlide) ||
        options.maxNumberOfSourcesPerSlide === undefined
      ) {
        // if no max number option was provided, use the default value set in the config file
        _numberOfSlidesNeeded = sources.length / OpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
        _numberOfSourcesPerSlide = OpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
      } else {
        // but if a max number option was indeed provided, calc how many slides are needed in the end
        _numberOfSourcesPerSlide = options.maxNumberOfSourcesPerSlide;
        if (options.maxNumberOfSourcesPerSlide < 1) {
          // this happens if the number of allowed sources per slide is smaller than 1, because that implies that all sources shall be put
          // onto a single slide
          _numberOfSlidesNeeded = 1;
        } else {
          // otherwise calc how many slides are needed
          _numberOfSlidesNeeded = Math.ceil(sources.length / options.maxNumberOfSourcesPerSlide);
        }
      }
    }

    let _isFirstSlide = true;
    for (let i = 0; i < _numberOfSlidesNeeded; i++) {
      // the following call to splice will remove as many sources as are allowed on a single slide. If there are less sources than allowed,
      // then splice still works as expected and will not throw any error. If the number of sources per slide is smaller than 1 (meaning
      // that all sources shall be on a single slide), then set _numberOfSourcesPerSlide to the length of the sources array
      if (_numberOfSourcesPerSlide < 1) {
        _numberOfSourcesPerSlide = sources.length;
      }

      let title = '';

      // prefill the title with an empty string. If this is the first slide or the title is to be put on all the additional slides, then
      // this field will be changed to that, further below in the code
      /*const _slide: ISlideSource = {
        sources: sources.splice(0, _numberOfSourcesPerSlide)
      };*/
      const _sourcesToPass = sources.splice(0, _numberOfSourcesPerSlide);
      // check whether the title shall only be put onto the first of the slides
      if (!OpenXmlPresentationConfig.titleOnlyOnFirstSourceSlide) {
        // in this case the title ought to be on all the new slides
        title = OpenXmlPresentationConfig.titleOfSourceSlides;
      } else {
        // in this case the title shall only be on the first of the slides
        if (_isFirstSlide) {
          title = OpenXmlPresentationConfig.titleOfSourceSlides;
        }
        // once the first slide was processed, set it to false to indicate that the other slides are not the first one
        _isFirstSlide = false;
      }
      _currentRefToZip = await this.addSourceSlide(_currentRefToZip, title, _sourcesToPass);
    }
    return _currentRefToZip;
  }

  /**
   * This function adds those little texts around the images to show the ref to the source
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param references The references that shall be added
   */
  public static async addBibRefsToImages(refToZip: JSZip, references: IReference[]): Promise<JSZip> {
    const _slidesPath = 'ppt/slides/';
    const _relsPath = _slidesPath + '_rels/';
    const _relpathsAndRef: {relPath: string, ref: IReference}[] = [];
    // const _relPathsFromXml: string[] = [];
    // convert the references' full path to a path relative to the slide xml files, not the slide rel xml files!
    references.forEach((ref: IReference) => {
      const pathOfRefAsArray = ref.file.split('/');
      // results for example in 'image2' and 'media' respectively
      const _file = pathOfRefAsArray.pop();
      const _folder = pathOfRefAsArray.pop();
      // get the last to levels
      // _relPathsFromXml.push(`../${_folder}/${_file}`);
      _relpathsAndRef.push({
        relPath: `../${_folder}/${_file}`,
        ref: ref
      });
    });

    // will hold the promises, but already linked to the corresponding slideX
    const _mapSlideXmlRidPromises: {[key: string]: Promise<IPairRidReference[]>} = {};
    const _mapSlideXmlRid: IMapSlideXmlRid = {};

    // this will hold all the promises that are generated
    const _wait: Promise<string>[] = [];

    refToZip.folder(_relsPath).forEach((relPath: string, file: JSZipObject) => {
      // since relPath looks like 'slideX.xml.rels', we want it to look like 'slideX' instead, which is the first element if we split the
      // relPath by the dots
      const _slideX = relPath.split('.').shift();
      const _f = new Promise<any>(async (resolve, reject) => {
        // init a string array that cna be written to
        const _mapsRidsReferences: IPairRidReference[] = new Array<IPairRidReference>();
        // read every slide rel and look for occurrences of the images affected by the references
        const _xml: string = await refToZip.file(_relsPath + relPath).async('text');
        const _doc: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
        const _elemsRelationship = _doc.getElementsByTagName('Relationship');
        for (let i = 0; i < _elemsRelationship.length; i++) {
          // check the attribute 'Target' from each Relationship node for equivalence with any of the given reference file paths and
          // save the Id
          for (const relPathAndRefOfAffectedImage of _relpathsAndRef) {
            if (_elemsRelationship[i].getAttribute('Target') === relPathAndRefOfAffectedImage.relPath) {
              const _rId = _elemsRelationship[i].getAttribute('Id');
              _mapsRidsReferences.push({
                rId: _rId,
                ref: relPathAndRefOfAffectedImage.ref
              });
            }
          }
        }
        resolve(_mapsRidsReferences);
      });
      // push it to the _wait queue such that we can wait for it to finish
      _wait.push(_f);
      // reference the file name with the result of _f, this object will be processed later such that it will contain the absolute
      // results instead of their promises
      _mapSlideXmlRidPromises[_slideX] = _f;
    });

    // wait for all the async function before we clean up the mess in __test
    await Promise.all(_wait);
    // since we waited for all the promises to finish, we should expect a result at every key
    for (const slide in _mapSlideXmlRidPromises) {
      // safety-check
      if (_mapSlideXmlRidPromises.hasOwnProperty(slide)) {
        // save the results instead of their promises
        _mapSlideXmlRid[slide] = await _mapSlideXmlRidPromises[slide];
      }
    }

    const _xmlSerializer = new XMLSerializer();

    // now that we've mapped the slides, rIds and refs, we need to add the text fields to the slides
    for (const slide in _mapSlideXmlRid) {
      // safety-check if property really exists. If it does, check if the array is not empty
      if (_mapSlideXmlRidPromises.hasOwnProperty(slide) && _mapSlideXmlRid[slide].length) {
        const _slideXmlFullPath = _slidesPath + slide + '.xml';
        const _xml: string = await refToZip.file(_slideXmlFullPath).async('text');
        const _docSlide: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
        // shortcut to the current pairs
        const _currentPairsOfRidsAndRefs: IPairRidReference[] = _mapSlideXmlRid[slide];
        // shortcut to the spTree for easy appending of the text nodes which contain the ref abbr
        const _elemSpTree = _docSlide.getElementsByTagName('p:spTree')[0];
        // all <p:pic> nodes in the slide
        const _elemsPic = _elemSpTree.getElementsByTagName('p:pic');
        // all the rIds
        const _rIds: string[] = [];
        for (const map of _mapSlideXmlRid[slide]) {
          _rIds.push(map.rId);
        }
        for (let i = 0; i < _elemsPic.length; i++) {
          // shortcut
          const _elemPic = _elemsPic[i];
          // find the child <a:blip r:embed="rIdX"> (there is only one <a:blip> child)
          const _elemBlip = _elemPic.getElementsByTagName('a:blip')[0];
          // shortcut, basically holds rIdX as string
          const _attrEmbed: string = _elemBlip.getAttribute('r:embed');
          if (_rIds.includes(_attrEmbed)) {
            // if the emebed/rId of this <pic> element matches, then get its position data
            const _elemOff = _elemPic.getElementsByTagName('a:off')[0];
            // There seems to be only one <a:off>, but multiple <a:ext> in this <pic>. The needed <a:ext> is always the sibling of the only
            // <a:off>. Ergo, use it as shortcut.
            const _elemExt = _elemOff.parentElement.getElementsByTagName('a:ext')[0];
            // save the dimension data
            const _picDimensions: IShapeDimensions = {
              offset: {
                x: Number(_elemOff.getAttribute('x')),
                y: Number(_elemOff.getAttribute('y')),
              },
              ext: {
                x: Number(_elemExt.getAttribute('cx')),
                y: Number(_elemExt.getAttribute('cy'))
              }
            };
            // calculate offset of text field
            const _offsetReferenceAbbreviation: ITupleXY = {
              x: _picDimensions.offset.x + _picDimensions.ext.x,
              y: _picDimensions.offset.y + _picDimensions.ext.y
            };


            // just another shortcut for better readability
            const _currentRid: string = _attrEmbed;
            // preset it with -1, thus it can indicate a malfunction if it still appears in the saved presentation
            let _refNumber = '-1';
            for (const _pairOfRidsAndRefs of _currentPairsOfRidsAndRefs) {
              if (_pairOfRidsAndRefs && _pairOfRidsAndRefs.rId === _currentRid) {
                _refNumber = _pairOfRidsAndRefs.ref.refAbbreviation;
              }
            }

            // template for a text field which will later hold the ref abbr
            // tslint:disable-next-line:max-line-length
            const _templateTextField = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' + `<p:sp xmlns:p="${xmlNS.presentationml.main}" xmlns:r="${xmlNS.officeDocument.relationships}" xmlns:a="${xmlNS.drawingml.main}"><p:nvSpPr><p:cNvPr id="2" name="Textfeld 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{08035DC6-F5EC-4E63-B802-D1666C78ECDE}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="8034500" y="5423946"/><a:ext cx="442750" cy="369332"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="none" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:r><a:rPr lang="de-DE" dirty="0"/><a:t>${this.readConfig().prefixRefAbbr}${_refNumber}${this.readConfig().postfixRefAbbr}</a:t></a:r></a:p></p:txBody></p:sp>`;
            const _docTextField: Document = await new DOMParser().parseFromString(_templateTextField, 'application/xml');
            const _deltas: ITupleXY = {
              x: 7591759,
              y: 5054614
            };
            const _elemOffOfNewTextField = _docTextField.getElementsByTagName('a:off')[0];
            // this ref is not yet used but keep it for dev purposes maybe?
            // const _elemExtOfNewTextfield = _docTextField.getElementsByTagName('a:ext')[0];
            _elemOffOfNewTextField.setAttribute('x', '' + _offsetReferenceAbbreviation.x);
            _elemOffOfNewTextField.setAttribute('y', '' + _offsetReferenceAbbreviation.y);

            // do not append it to the top spTree, but instead make it a sibling of the <pic> which contains the image that was replaced.
            // This way it is made sure that the positional values will have the same relative anchor point as the <pic> in question has
            _elemPic.parentElement.appendChild(_docTextField.childNodes[0]);
          }
        }
        // write every slideX.xml and chain them together
        refToZip = refToZip.file(_slideXmlFullPath, _xmlSerializer.serializeToString(_docSlide));
      }
    }

    return refToZip;
  }

  /**
   * Creates a claimer/disclaimer slide at the very end
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param legalText The text that is to be displayed on the disclaimer slide
   */
  public static async addLicensingSlide(refToZip: JSZip, legalText: string) {
    const paragraphOfLegalText: Node = this._createParagraph({texts: [{text: legalText}]}).paragraph;
    const _logoFileEnding: string = 'png';
    const _slideTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
`<p:sld xmlns:a="${xmlNS.drawingml.main}" xmlns:r="${xmlNS.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="9" name="Textfeld 8"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{CCE18A2D-B512-4BBE-9251-22F0483AE87A}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="4762500" y="3099435"/><a:ext cx="5847370" cy="1477328"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="none" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/></p:txBody></p:sp><p:pic><p:nvPicPr><p:cNvPr id="3" name="Grafik 2" descr="Badge showing the CC license"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{5781B322-F30A-4896-B4D7-258C6631DD77}"/></a:ext></a:extLst></p:cNvPr><p:cNvPicPr><a:picLocks noChangeAspect="1"/></p:cNvPicPr><p:nvPr/></p:nvPicPr><p:blipFill><a:blip r:embed="rId2"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></p:blipFill><p:spPr><a:xfrm><a:off x="695462" y="3205880"/><a:ext cx="3589371" cy="1264437"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr></p:pic></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="3243645703"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>`;
// `<p:sld xmlns:a="${xmlNamespaces.drawingml.main}" xmlns:r="${xmlNamespaces.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="9" name="Textfeld 8"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{CCE18A2D-B512-4BBE-9251-22F0483AE87A}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="4762500" y="3099435"/><a:ext cx="5847370" cy="1477328"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="none" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:r><a:rPr lang="de-DE" dirty="0"/><a:t>This </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>work</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> was </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>originally</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>created</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>by</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> AUTHOR_NAME.</a:t></a:r></a:p><a:p><a:r><a:rPr lang="de-DE" dirty="0"/><a:t>Updated </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>by</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> TOOL_NAME, TOOL_USER_NAME.</a:t></a:r></a:p><a:p><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>It</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>is</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>licensed</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>under</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" b="1" dirty="0"/><a:t>Attribution 4.0 International (CC BY 4.0)</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t>.</a:t></a:r></a:p><a:p><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>Excluded</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>are</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>the</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> RWTH Aachen </a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0" err="1"/><a:t>University‘s</a:t></a:r><a:r><a:rPr lang="de-DE" dirty="0"/><a:t> logo LISTING</a:t></a:r></a:p><a:p><a:r><a:rPr lang="de-DE" dirty="0"/><a:t>OF THINGS TO BE EXCLUDED USE USER INPUT</a:t></a:r></a:p></p:txBody></p:sp><p:pic><p:nvPicPr><p:cNvPr id="3" name="Grafik 2" descr="Badge showing the CC license"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{5781B322-F30A-4896-B4D7-258C6631DD77}"/></a:ext></a:extLst></p:cNvPr><p:cNvPicPr><a:picLocks noChangeAspect="1"/></p:cNvPicPr><p:nvPr/></p:nvPicPr><p:blipFill><a:blip r:embed="rId2"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></p:blipFill><p:spPr><a:xfrm><a:off x="695462" y="3205880"/><a:ext cx="3589371" cy="1264437"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr></p:pic></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="3243645703"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>`;
    // const _slideLayoutTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
    //                              `<p:sldLayout xmlns:a="${xmlNamespaces.drawingml.main}" xmlns:r="${xmlNamespaces.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main" type="blank" preserve="1"><p:cSld name="Leer"><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="2" name="Datumsplatzhalter 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{2867BF52-E6EE-405F-BDDD-169BD15BAFC6}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="dt" sz="half" idx="10"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{23DE2ADA-BD7E-4710-B194-26057D380146}" type="datetimeFigureOut"><a:rPr lang="de-DE" smtClean="0"/><a:t>07.02.2020</a:t></a:fld><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="3" name="Fußzeilenplatzhalter 2"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{4612F097-FA61-4A69-9B44-D6BEB8CCD560}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ftr" sz="quarter" idx="11"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="4" name="Foliennummernplatzhalter 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{E2FCB645-646F-4A85-9413-9CC9B40C7880}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="sldNum" sz="quarter" idx="12"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{2FABD243-BEF3-4EC9-B856-04D5075317EF}" type="slidenum"><a:rPr lang="de-DE" smtClean="0"/><a:t>‹Nr.›</a:t></a:fld><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="2628325194"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sldLayout>`;

    const _slideTemplateDoc = new DOMParser().parseFromString(_slideTemplate, 'application/xml')
    const _elemTxBodyInTemplate = _slideTemplateDoc.getElementsByTagNameNS(xmlNS.presentationml.main, 'txBody')[0];
    _elemTxBodyInTemplate.appendChild(paragraphOfLegalText);

    // tslint:disable-next-line:max-line-length
    const _ccLicenseImageAsBase64 = 'iVBORw0KGgoAAAANSUhEUgAAAWAAAAB8CAMAAABg3JoqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAOw4AADsOAcy2oYMAAACKUExURQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgqKAAAAAsLCxAQEBUWFSAgICAhICosKzAwMDU4NUBAQEBDQEpOS1BQUFVZVWBgYGBkYGpva3BwcHV6doCAgICFgIqRi4+Pj5Wclp+fn5+noKqyq6+vr7+/v8/Pz9/f3+/v7////2r+204AAAANdFJOUwAgMGBwgI+fr8/f7/uUB9TzAAAO40lEQVR42u2d6YKjuBGAPclsspsJlw2YGwyG5vL7v15szCGkkhBCeLIz6NfudLfAn0qlulQ+nU6nf/zx1w/lGLLHn//6durGPw+6e43vL77fDw67Ev72lN9LWnwdQ/aIL0/C305/KMp/D7z7jCfhf5/+VJT4QLHPKBTlr9N/FOUQ4L2Govw4PfXEAWKvoSrKAfgAfAD+8LiFgWNdzqp+tq6eF8o4QYrnlNfnlKpuPecM0/8DwEWa3uI4jG+3j9rQxc0548b8xbltWzCPmFJ14p8IOI2d52LPPqLl3T4iuxeV4jFd76JTOjplSiv8GYCL+HqmvNBWQVpmcWY5pUKI747KmPIcfhhwcbuqTM9bddL98FpLfr+zFnHhqAtTnm8fBFx4Okd04xzuo3sXWXSIV80Z8Exp3T8EuPBUzgDSHojT2dpqbhRlVV2WeeLb82fzH7jFdTal7UdZXlXPKSNXm+3K20cAh+qKGN1ZtqK4IU83k7p9oKOMECA676MLC12wfD5llZjIxwn2B5xaK8OgjlS+017W/PoBjMqfHn1buyXsqgWmbKNpSm9vwIG6OtCsxhLV78QCxNuJsbFK4KYtYWa0Ketp1S67Ai5I8dXsTgk+92rblGWW+CaJ2JMFeORrlg/GyEbEIT9fLWsZU9buVsI8gG+47UBorG5HZTahieV4eMEwn8ti8XoFk1dL3Ae+ZsOe8jHqietugG9z9aBFNfVzlj6mJmScdenw/Gj+3Doy3QR7AZfvwcXgsNhLfJ/7gn9biAGeq18tYUtR48smPAiblmMfvLMcTEwnJ4MtwZxzUHl++1getbbq8FwLeM7XXV7xeo54q/NcXHq+JfyxDezxOYfOHHRO9OAaTf8o9b4D4ACFRT/C5yaTIZHwcMDhR/24jCXlB/QdHa+R324xubaFEGDUu8D3KGMk0rTEoCAIYRsXEVfDgx5WqXP2e8LE+dZl5NqunyQVcbb0z4plA05RvuWDf1SyCF9p9sNoMEQ0W8JjC7A2345tjhqafgluC10y4DvqnzaPNaNG1IQubq3dKMKGmE/kurcaS2UOFsT87yoNMzGx08YVtSRYgIfzpVO/7WPdaBGrWNgNGnZzBcw/vBjw7JxluQaA4KNesQLqnnZJ74gAnhxU3ATlGshLi0bX+t3s0rUQbMj22wcS4eLtNWno37U+6OzbgJIIJAIOEb4PkeFOEwiqYYsqwJ3r6EY5vPA5XQsHgHhGlHCKLUGE6YARBWy3QoCng0jIwOnqjqgCzB4GlcZ7zbQWsBHIEZGW0V0a4CkabQjyfbTGtohqryFI67vKZqOhiTBJo1AJAZ4iGIrmR1liI+ddRRydgSzA8fTU+iE66slWuwtrCJOleyiWREtb1v5jNVCwYQysjc6xohFa2JIEuDjT317IHrYEAOuwJ1GTwVPiwTQaV3LNTNLQb0ahzoldUcgBHChUT0nQlLiLaoiG5lchg6IjVI41G2UgA4xAdOlaIYuIAngSYPuxbRji8dSAoiF4ALdwICQlzZJBQ8zOvSnUUeEfxZECOIbUvNDIxUX4SjEReQD3NELQMzQhhT63VRpgAyciuo4CePTh3Mfjp4mwhWvBNYBt8JQLsV1ZR2NC2o5K6LU1/LkXGYBjCRYEgUMVO+NKMcAR6Gq8tY4P+hhza3/8QYs9V5cB+LrNhaO4G6EQ4GoLYGtR60QUb2qMuNaY2lAlAC7GlGsjAXAtaqlRjAg+wBn4SIs0jHw4YJeTh1ArYqeBgGOGANdl9iowApy7tsqzJK8aqgir617tjm/RVYDfv3QGz5acNJmJgGgJWMIihzUI2KKZEO1UUOTmGF1zdH+ihpLfCEUAP8QAVyDgM6TWfSjgXAPmsSYQuAIB67CD1EbzqDTy6GpedzLXZ62YHVFskuAcPPIvcH7PZDxkBwlOYQ3RutRgU4bnA7QKMnnWnw9gqGeFDr6CIW7COW0Z5RAlbhtvP+QCMArRGOTnMskcJ/S340ldCADeZEU4IGCf42gGXLlalplmQc5ja1MDpjn0E9QAKcVS+OctjoYPAva4vadxuzYLal0A8AXy4iK07s9GPelm0g+mO5VD24ASXhdMhRXm84Pato1sp+f/2TRAIehBoa5yk/ejhkPZ5pJaFwCsk+p9otgHTZOp9BMz1fv8rIZm2m2hekuHIW7ZBLhheOgxnKVugcVP4F0S4ZviuhnwnaVGx6Dp6xMaL9gj+iEx+voHza0hhbbO1QgpsV4ewBV84hek6JigyWQDEAyRolwAcApZwcOOmc4Hs5dlMtyXY3in5VFF7LRcBHBC0Zc6ccpF0DwlkC6rhUrBAMAx8MQacBzb+WmAbLGaVkq10k7T6Wf+EmCTErv1CGltgENjKkNJ8EeuDVkBgEMyjjR+HpMejqx4gsIihX+aAOCaVktW0DfnVGMx8UUNKVco6AoADgDbx6fnN9hnDbbjVlb+0XXEAuCEGvzQiZMTsfqivGrqErnEVXKsmTBgE7AKye3a0E1RQMMUIjpCWw24P3gterlSzUpSg3URtrS6CA+QVpcaXhvYGcwSZkHADlWE2YAjutmdkq/b0viiIYpKMDnOAGwCVktEZadxSfDK9S+gfOQy4F6A4apOi7RBa0rlVEsSuH3JUxEaoINdapWj0vKUR6wtZPVoj2UC9lnR0TsgELUG6YeWPKbXV3cwrAgNMBZtOuBavhUx1kKSiTkW4IxdNHsFPgpZX2mX0AqkMgDHgEzmdEVgKAx/AMex+v1CyvIxAOcaezcXoLorZ0edPb+eOEQmBO7KsQDXgJzmSM1cPhNuRGX7eE4jEy+y1OHsIB3wIGzWkt7Bt0WduU+OmmG6GbacQyRcpMAOAFywXGUDNTU7xDnxQStCPIYNeF7/gins4lABD8LGuhly5tBq0OuL3A1mAc6hvJo7+4RmPQX2hhqK/npGBFh5ItdRYwVK+9IAj3dDWA7BkDXXuMqWRv0s1ECAFa4Eq+EUM6vaZszBRWg+I6rath4Sd6htZW55xStUBEMBPDq5Hte2ULJlvo296eY1K+CuUUQGM8VbA/xRCayO2F0NC5hyjJPn6C2CMTfocG6L5cqacUvoYp1lIMAOuPkgh/ItVPWSlzl6+2ItJKZSz4RvL3MI21Xhug7eDiaJcF8ZCHAMbiAgKTdE36sFK90VqzwBCBt0YxApKuDZzBbPDZ+pGkG4vQgEuKCUVuKXcaa7ibgnpGWgqSx8Xw65sWfCiFuk4YnDtY7e9LqUVatsCT2I1hSeoF1b8MYGMxVtV7CjvKGPD3Jnz01qomgLbWHEm1m9TX9iRIQ90WRTqZJyEb+rCgJ2qEtbvj+J9jTG8dhD7hvvHxGfP9mmggmB64D0XUHapi59TaxVUDrr1OCPZXVtXc56TnFuiTXVlezy64Ya12nBHxkbVTAhcANm09Q29RMriE6Nmmka+JxqsOW14fpgXVnp6zx4AhFbO305y52ugpVrGCy2MlS3Nf6jAPYUeg5O/Dbi5s6h6VUu3lmCjNLvdGtPHBhwIeWSHKaB9a/tI6W3X1VF+9syVu2yvZss5RLMVeFJVHAJsCa3jdotuIAbeUsb7BBirF48GT2zKIDvSzbiegFW5bXJDi1UdapnZ3sL7njWpVlVL6Gk16Xd9LwqCjUftmY0kgV40mJpHHrBLb1LbG9+j8MgCG9pIXFOGuC7IuWq5+Rfq/ev33JQb9tPamnDXUSkKN77OgCD6cZNajiS0RjpFwWMXFcWttUyeQ0Af0HASEBP8EIt0irL+ToAs5SEEGGE77k4ALPDK9r6rgbI1S71t1UQC53/QvjWIZf9EMnpMf+rAFaRocNueiJon6EKOFB/u6G8Ac+/smYifKE1wltIxZog32J9q/1fYRCA0RI3lLDJaRBPqdhuuQqeuNXvBVinBf1dnv4R84vhSDorVg7AwKk0jw5GS4ireXYfSWcV5wMw2CHKYlQ8EHix1D6aLnSUA7ACZh9wMmTeHEieE+nCUDkAo6rzi8mGKCRoK5/I8M4y6Kl6AJ5v7uVUmPHqKJ8lkU2murs1QvXMXVcOwPMR8mYGeVK8xWXza2ruMIZeBq//HpWS8fo/Y/20dpSVeTKrbOym0nYHjEUY03UieLlTInPCwyCKuHK09Dh/zDoB8+KtgdBqAhXi7QAYL8IPVuCd1y8VMjyM+U3HF0sNKXxzaa1tmAOtmi/nFd3N/oDJKHmgc+It5PPFr5K+irr9sTa2i6c2a7d116ugLfMKugtofAAwWZHOUWpEFGsUliINsKFpmvHuRjFUdnXl89GsiQbv8MfK2wqpwy9ZJf2SAQOBxhvzy2EvZPWSJL494Kkaxhwqu5K+tqBcPWU+ctSmvnOvuRrWDWapgMFkcOyBNsH5Cn0jfCrLPsMAvyuG8vc/ZkInXCervaAmWZa9J3hthqREG3vtClg5w/UMt9i7Xs76U5pV/Xy2nOAG/14ozb9AAEejvHaSV9pCJ1xvLuBf5PjSFrZP61YmH7D417hIO94QwInv+1HZTpd/unOuETnhlKl2tIzM2b/VyB75AGDRb7MEvgxUmhUxffgabxO1Zkw1NU2ESHXy1j3+pwArqpAQ831XuqiZhjW5KcUmRb5jrK9jbt6KyKe3kdgBMOGY8eD1JEd3OsDdF79Us7th5UPshCNCg52ScXvfRZNoCp+4fmtdJX0RSA/uIIdcdxAN8qUJnnBIlKP/er6oX67qFYuo5LnLJ87f468gL+IdkhcoYHcyW99tLVyhGW3bNhFVUeLFuu1nAT+lmEsX35xdQr8oYBPdwMKAXYSh/w4/YLc+7Q8D5mCcentl3lDAEd4sSAiwhkTjetP6pRia92ilmcKnlb9veZRKqDT2dsxrGu+Gmc9hdndMq82AO4vhfTx2KiLrdsZgAbrSdMRJ4G/O11f1/qByi/uT7WXnnJBBqx0SB/w28ZqyrHsrIkEMvk4d+z8L8DB0vXOXPzEMWm23OOBZ8f7rtkODTpXDfQQ+C/iDY9arr0Ts3g2AFTufvqai1wrajL7x+wDuWol3A8ux2vOO42t9OT/J88Q3B7vNljXx3w/w33YcgA/AB+BjHIAPwL/q+HH6fkDYk+8f/wPFRlq+wgB4JQAAAABJRU5ErkJggg==';
    // data:image/png;base64,

    // refToZip = refToZip.folder('ppt/media/');
    const _regex = /^image\d+\./;
    const _currentNumberOfImages = refToZip.folder('ppt/media').file(_regex).length;
    // The image of the cc license that'll be written is a png file
    // tslint:disable-next-line:max-line-length
    refToZip = JsZipHelper.addBase64Image(refToZip, {nameAndFullPath: `ppt/media/image${_currentNumberOfImages + 1}.${_logoFileEnding}`, imageAsBase64: _ccLicenseImageAsBase64});


    // we assume that slide master 1 holds all the default layouts offered by powerpoint
    // tslint:disable-next-line:max-line-length
    const _positionOfDesiredSlideLayout: number = await this._getPositionOfSlideLayoutByName(refToZip, OpenXmlPresentationConfig.slideLayoutName, 1);
    /*const _slideRelsTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
       `<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="../media/image${_currentNumberOfImages + 1}.png"/><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout" Target="../slideLayouts/slideLayout7.xml"/></Relationships>`;*/
    const _slideRelsTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
      `<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="../media/image${_currentNumberOfImages + 1}.png"/><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout" Target="../slideLayouts/slideLayout${_positionOfDesiredSlideLayout}.xml"/></Relationships>`;

    const _docLicenseSlideXml: Document = await new DOMParser().parseFromString(_slideTemplate, 'application/xml');
    const slide: ISlide = {
      slide: {
        xmlString: new XMLSerializer().serializeToString(_slideTemplateDoc),
        xmlRelsString: _slideRelsTemplate
      }
      // xmlLayoutString: _slideLayoutTemplate,
    };

    // check whether or not the file extension of the cc license logo is already registered
    const _logoMimeType: MimeType = MimeType.fromFileExtension(_logoFileEnding);
    const _defaultExtension = new DefaultContentType({type: _logoMimeType, extension: _logoFileEnding});
    if (!(await OpenXmlPresentationJs._doesDefaultExtensionAlreadyExist(refToZip, _defaultExtension))) {
      refToZip = await OpenXmlPresentationJs._addDefaultExtension(refToZip, _defaultExtension);
    }

    return this.appendSlide(refToZip, slide);
  }


  /**
   * Appends a slide via the given templates
   * @param refToZip refToZip The JSZip object resembling the pptx zip file
   * @param slide Object that contains info on the slide
   */
  public static async appendSlide(refToZip: JSZip, slide: ISlide): Promise<JSZip> {
    let currentNumberOfSlides;

    // manipulating docProps/app.xml
    let _xml = await refToZip.file('docProps/app.xml').async('text');
    const _docAppXml: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    const _elemSlidesNumber = _docAppXml.getElementsByTagName('Slides')[0];
    currentNumberOfSlides = Number(_elemSlidesNumber.innerHTML);
    // increment number of slides by one
    _elemSlidesNumber.innerHTML = String(currentNumberOfSlides + 1);
    const _elems_vt_variant = _docAppXml.getElementsByTagName('vt:variant');
    // it seems that the <vt:i4> elemnt in question always is the last one
    // I can't rely on looking at the preceding elemnt and look at it's text content, since 'Slide Titles' and 'Folientitel' indicate that
    // the exact content depends on the most recent power point instance's, that saved this pptx, language.
    const _numberOfVariantsFound = _elems_vt_variant.length;
    const currentVariant = _elems_vt_variant[_numberOfVariantsFound - 1];
    // save the number of slide titles for future usea
    const currentNumberOfSlideTitles = Number(currentVariant.firstChild.textContent);
    // since we add a slide title to the proper xml file, we need to increase the counter in this xml file by 1
    currentVariant.firstChild.textContent = String(currentNumberOfSlideTitles + 1);
    const _elemTitlesOfParts = _docAppXml.getElementsByTagName('TitlesOfParts')[0];
    const _elem_vt_lpstr = document.createElementNS(xmlNS.officeDocument.docPropsVTypes, 'vt:lpstr');
    _elem_vt_lpstr.textContent = this.readConfig().titleOfSourceSlides; // 'Source';
    // appending the vt:lpstr tag at the correct position
    _elemTitlesOfParts.firstChild.appendChild(_elem_vt_lpstr);
    // update the array's size
    const _listSize = Number((_elemTitlesOfParts.firstChild as Element).getAttribute('size'));
    (_elemTitlesOfParts.firstChild as Element).setAttribute('size', (_listSize + 1).toString());
    // console.log('docProps/app.xml', _docAppXml);


    // manipulating docProps/core.xml
    _xml = await refToZip.file('docProps/core.xml').async('text');
    const _docCoreXml: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    if (this.readConfig().changeInfoLastModifiedBy) {
      let _elemLastModifiedBy = _docCoreXml.getElementsByTagName('cp:lastModifiedBy')[0];

      if(!_elemLastModifiedBy){
        _elemLastModifiedBy = _docCoreXml.createElement('cp:lastModifiedBy')
      }
      
      _elemLastModifiedBy.innerHTML = this.readConfig().newLastModiefiedBy;
    }
    if (this.readConfig().changeInfoTimeModified) {
      // write an ISO string
      _docCoreXml.getElementsByTagName('dcterms:modified')[0].innerHTML = new Date().toISOString();
    }
    if (this.readConfig().incrementRevision) {
      // increment the revision number
      const currentRevsionNumber = _docCoreXml.getElementsByTagName('cp:revision')[0].innerHTML;
      _docCoreXml.getElementsByTagName('cp:revision')[0].innerHTML = String((Number(currentRevsionNumber) + 1));
    }
    // console.log('docProps/core.xml', _docCoreXml);




    // manipulating ppt/_rels/presentation.xml.rels
    _xml = await refToZip.file('ppt/_rels/presentation.xml.rels').async('text');
    const _docPresentationXmlRels: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    const _elemRelationships = _docPresentationXmlRels.getElementsByTagName('Relationships')[0];
    const numberOfRelationships: number = _elemRelationships.children.length;
    // the name space used here is not the same as xmlNamespaces.r but since it seems to be only used here, and does not have a prefix, I decided
    // to not make it part of the xmlNamespaces object
    const _elemRelationship = document.createElementNS(xmlNS.package.relationships, 'Relationship');

    // after power point repaired a file that was created with a prior build, the rId for the new slide was changed to
    // the rId of the previously last slide + 1. So I should do that as well.
    // Additionally I observed that the order of the <Relationship> don't seem to matter this time
    const _prefix = 'rId';
    let _idOfLastSlide: number;
    _elemRelationships.childNodes.forEach((node: ChildNode) => {
      const elem: Element = node as Element;
      if (elem.getAttribute('Target') === `slides/slide${currentNumberOfSlides}.xml`) {
        const _rawRId: string = elem.getAttribute('Id');
        _idOfLastSlide = Number(_rawRId.slice(_prefix.length, _rawRId.length));
      }
    });
    // loop again through all the children, this time to increment rIds if the rId is
    // at least greater than the rId of the last slide, since the new slide will be added afterwards
    _elemRelationships.childNodes.forEach((node: ChildNode, key: number) => {
      const elem: Element = node as Element;
      const _rawRId: string = elem.getAttribute('Id');
      const _id = Number(_rawRId.slice(_prefix.length, _rawRId.length));
      if (_id > _idOfLastSlide) {
        const _rawCurrentRId: string = elem.getAttribute('Id');
        const _idOfCurrentElement = Number(_rawCurrentRId.slice(_prefix.length, _rawRId.length));
        elem.setAttribute('Id', `${_prefix}${_idOfCurrentElement + 1}`);
      }
    });

    _elemRelationship.setAttribute('Id', `rId${_idOfLastSlide + 1}`);
    _elemRelationship.setAttribute('Type', xmlSchemata.slide);
    _elemRelationship.setAttribute('Target', `slides/slide${currentNumberOfSlides + 1}.xml`);
    _elemRelationships.appendChild(_elemRelationship);
    // console.log('ppt/_rels/presentation.xml.rels', _docPresentationXmlRels);


    // manipulating [Content_Types].xml
    _xml = await refToZip.file('[Content_Types].xml').async('text');
    const _docContentTypesXml: Document = new DOMParser().parseFromString(_xml, 'application/xml');
    // there always is only one <Types>
    const _elemTypes = _docContentTypesXml.getElementsByTagName('Types')[0];

    // add <Override> tag for the slide
    const _elemOverrideSlide = document.createElementNS(xmlNS.package.contentTypes, 'Override');
    // set the attributes
    _elemOverrideSlide.setAttribute('PartName', `/ppt/slides/slide${currentNumberOfSlides + 1}.xml`);
    _elemOverrideSlide.setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.presentationml.slide+xml');

    // insert this right after the currently last slide, so find the next sibling of the last slide element such that the new
    // node can be inserted before
    let insertSlideBeforeThisNode: Node;
    let insertSlideLayoutBeforeThisNode: Node;
    let currentNumberOfSlideLayouts = 0;
    const regExpSlideLayouts = new RegExp('^\\/ppt\\/slideLayouts\\/slideLayout(\\d)+\\.xml$');
    await _elemTypes.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
      // look for the correct <Override> tag
      if ((node as Element).getAttribute('PartName') === `/ppt/slides/slide${currentNumberOfSlides}.xml`) {
        insertSlideBeforeThisNode = node.nextSibling;
      }
      // in order to save resources, also use this loop to check how many slide layouts there are in the pptx
      if (regExpSlideLayouts.test((node as Element).getAttribute('PartName'))) {
        currentNumberOfSlideLayouts++;
      }
    });

    // insert the <Override> at the correct position
    _elemTypes.insertBefore(_elemOverrideSlide, insertSlideBeforeThisNode);


    // check if a slide layout was passed at all
    if (slide.layout) {
      // add <Override> tag for the slide layout
      const _elemOverrideSlideLayout = document.createElementNS('http://schemas.openxmlformats.org/package/2006/content-types', 'Override');
      // set the attributes
      _elemOverrideSlideLayout.setAttribute('PartName', `/ppt/slideLayouts/slideLayout${currentNumberOfSlideLayouts + 1}.xml`);
      _elemOverrideSlideLayout.setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml');

      // insert this right after the currently last slide, so find the next sibling of the last slide element such that the new
      // node can be inserted before
      await _elemTypes.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        if ((node as Element).getAttribute('PartName') === `/ppt/slideLayouts/slideLayout${currentNumberOfSlideLayouts}.xml`) {
          insertSlideLayoutBeforeThisNode = node.nextSibling;
        }
      });
      // insert the <Override> at the correct position
      _elemTypes.insertBefore(_elemOverrideSlideLayout, insertSlideLayoutBeforeThisNode);
    }
    // console.log('[Content_Types].xml', _docContentTypesXml);


    // manipulating ppt/slideMasters/slideMaster1.xml
    // create the var that'll hold the document here and init it with null, thus it can safely be checked if this document exists and be
    // used if necessary
    let _docSlideMaster1Xml: Document = null;
    // only needed when a layout was passed withing the slide:ISlide argument
    if (slide.layout) {
      // add the slide master 1 to the newly created slide by referencing the new layout in the <p:sldLayoutIdLst> element
      _xml = await refToZip.file('ppt/slideMasters/slideMaster1.xml').async('text');
      _docSlideMaster1Xml = await new DOMParser().parseFromString(_xml, 'application/xml');
      const _elemSlideLayoutIdList = _docSlideMaster1Xml.getElementsByTagName('p:sldLayoutIdLst')[0];
      const _elemSlideLayoutId = document.createElementNS('http://schemas.openxmlformats.org/presentationml/2006/main', 'p:sldLayoutId');
      let longIdOfLastSlideLayout: number;
      _elemSlideLayoutIdList.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        if ((node as Element).getAttribute('r:id') === `rId${currentNumberOfSlideLayouts}`) {
          longIdOfLastSlideLayout = Number((node as Element).getAttribute('id'));
        }
      });
      _elemSlideLayoutId.setAttribute('id', `${longIdOfLastSlideLayout + 1}`);
      _elemSlideLayoutId.setAttributeNS(xmlNS.officeDocument.relationships, 'r:id', `rId${currentNumberOfSlideLayouts + 1}`);
      _elemSlideLayoutIdList.appendChild(_elemSlideLayoutId);
    }
    // console.log('ppt/slideMasters/slideMaster1.xml', _docSlideMaster1Xml);




    // manipulating ppt/slideMasters/_rels/slideMaster1.xml.rels
    // create the var that'll hold the document here and init it with null, thus it can safely be checked if this document exists and be
    // used if necessary
    let _docSlideMaster1XmlRels: Document = null;
    // this, too, is only needed if a layout was passed
    if (slide.layout) {
      // add the slide master 1 to the newly created slide by referencing the new layout as a <Relationship> element
      _xml = await refToZip.file('ppt/slideMasters/_rels/slideMaster1.xml.rels').async('text');
      _docSlideMaster1XmlRels = await new DOMParser().parseFromString(_xml, 'application/xml');
      const _elemRelationships2 = _docSlideMaster1XmlRels.getElementsByTagName('Relationships')[0];
      _elemRelationships2.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        const rIdRaw: string = (node as Element).getAttribute('Id');
        const rIdAsNumber: number = Number(rIdRaw.slice(_prefix.length, rIdRaw.length));
        if (rIdAsNumber > currentNumberOfSlideLayouts) {
          (node as Element).setAttribute('Id', `${_prefix}${rIdAsNumber + 1}`);
        }
      });
      const _elemRelationship2 = document.createElementNS('http://schemas.openxmlformats.org/package/2006/relationships', 'Relationship');
      _elemRelationship2.setAttribute('Id', `${_prefix}${currentNumberOfSlideLayouts + 1}`);
      _elemRelationship2.setAttribute('Type', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout');
      _elemRelationship2.setAttribute('Target', `../slideLayouts/slideLayout${currentNumberOfSlideLayouts + 1}.xml`);
      _elemRelationships2.appendChild(_elemRelationship2);
    }
    // console.log('ppt/slideMasters/_rels/slideMaster1.xml.rels', _docSlideMaster1XmlRels);






    // manipulating ppt/presentation.xml
    // adding a new <p:sldId> element as child to <p:sldIdLst> for the new slide
    _xml = await refToZip.file('ppt/presentation.xml').async('text');
    const _docPresentationXml: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    const _elemSlideIdsList = _docPresentationXml.getElementsByTagName('p:presentation')[0].getElementsByTagName('p:sldIdLst')[0];
    const _elemSlideId = document.createElementNS(xmlNS.presentationml.main, 'p:sldId');

    // In contrast to previous observations, the IDs are not always 256 + number of slide. I guess that, when a slide is deleted that
    // was not the last one, the next slide won't have the ID of the deleted one but the currently highest ID + 1, regardless
    // of its position in the presentation. Thus, we need to find that maximal ID as base ref

    let _maxId = 256 + currentNumberOfSlides; // let's use the old approach as fallback
    const _elemsSlideId: Array<Element> = Array.from(_elemSlideIdsList.children);
    for (const _currentElementOfSlideIds of _elemsSlideId) {
      const _idOfCurrentElement = Number(_currentElementOfSlideIds.getAttribute('id'));
      if (_idOfCurrentElement > _maxId) {
        _maxId = _idOfCurrentElement;
      }
    }

    // the folowing line was used for an old observation. Not deleting it yet, but out of usage.
    // _elemSlideId.setAttribute('id', (_idOffset + currentNumberOfSlides).toString());

    _elemSlideId.setAttribute('id', (_maxId + 1).toString());
    // set the r:id attribute to the value given before in ppt/_rels/presentation.xml.rels
    _elemSlideId.setAttributeNS(xmlNS.officeDocument.relationships, 'r:id', `rId${_idOfLastSlide + 1}`);

    // since some rIds were increased, we need to increase those here as well. We need to check the <p:notesMasterIdLst> and the
    // <p:handoutMasterIdLst>. So lets fetch these lists and concat them in order to be processed
    const _elemsNotesMasterId: Array<Element> = Array.from(_docPresentationXml.getElementsByTagName('p:notesMasterId'));
    const _elemsHandoutMasterId: Array<Element> = Array.from(_docPresentationXml.getElementsByTagName('p:handoutMasterId'));
    const _elemsToCheck = _elemsNotesMasterId.concat(_elemsHandoutMasterId);
    // @ts-ignore it complains that it is not of array or string type but i guess that's a mistake since
    // the HTMLCollectionOf<> can be used like an array otherwise
    for (const elemToCheck of _elemsToCheck) {
      const _rawCurrentRId: string = (elemToCheck as Element).getAttribute('r:id');
      const _idOfCurrentElement = Number(_rawCurrentRId.slice(_prefix.length, _rawCurrentRId.length));
      // if the rId if the current element is equal or greater to the new rId of the new slide, then increase it by
      // one (as done before in ppt/_rels/presentation.xml.rels)
      if (_idOfCurrentElement >= _idOfLastSlide + 1) {
        (elemToCheck as Element).setAttributeNS(
          xmlNS.officeDocument.relationships,
          'r:id',
          `${_prefix}${_idOfCurrentElement + 1}`
        );
      }
    }
    _elemSlideIdsList.appendChild(_elemSlideId);
    // console.log('ppt/presentation.xml', _docPresentationXml);

    // creating ppt/slideLayouts/_rels/slideLayout#NUMBER#.xml
    // todo: this seems dependent on the number of master slides, see #34
    // again, only necessary and possible if layout was passed
    let _slideLayoutXmlRelsContent;
    if (slide.layout) {
      _slideLayoutXmlRelsContent = slide.layout.xmlLayoutRelsString;
    }
    // console.log('ppt/slideLayouts/slideLayout#NUMBER#.xml', `slideLayout${currentNumberOfSlideLayouts + 1}.xml`, _docSlideLayoutXml);


    // creating ppt/slides/_rels/slide#NUMBER#.xml.rels
    // target the slide layout xml that we've created a few steps before, if any
    let _slideXmlRelsContent = slide.slide.xmlRelsString;
    // In order to target, we need to check for special keywords that might need to be replaced (see ISlide tsDoc for more info)
    const mapOfRegExpAndReplaceValue = [[/__NEXT_SLIDE_LAYOUT_NUMBER__/, `${currentNumberOfSlideLayouts + 1}`]];
    // pair[0] will contain the pattern, pair[1] the value it is to be replaced with/by
    for (const pair of mapOfRegExpAndReplaceValue) {
      _slideXmlRelsContent = _slideXmlRelsContent.replace(pair[0] as RegExp, pair[1] as string);
    }

    const _docSlideXmlRels: Document = await new DOMParser().parseFromString(_slideXmlRelsContent, 'application/xml');
    // console.log('ppt/slides/_rels/slide#NUMBER#.xml.rels', `slide${currentNumberOfSlides + 1}.xml.rels`, _docSlideXmlRels);

    const _xmlSerializer = new XMLSerializer();
    // writing all the files to the zip;
    refToZip = refToZip
      .file(`ppt/slides/slide${currentNumberOfSlides + 1}.xml`, slide.slide.xmlString)
      .file(`ppt/slides/_rels/slide${currentNumberOfSlides + 1}.xml.rels`, _slideXmlRelsContent)
      .file('docProps/app.xml', _xmlSerializer.serializeToString(_docAppXml))
      .file('ppt/_rels/presentation.xml.rels', _xmlSerializer.serializeToString(_docPresentationXmlRels))
      .file('[Content_Types].xml', _xmlSerializer.serializeToString(_docContentTypesXml))
      .file('ppt/presentation.xml', _xmlSerializer.serializeToString(_docPresentationXml));

    // we could also check if a layout was passed, because then they following documents ought to be created, but for safety's sake
    // we'll check the existence of those documents directly
    if (_docSlideMaster1XmlRels) {
      refToZip = refToZip.file('ppt/slideMasters/_rels/slideMaster1.xml.rels', _xmlSerializer.serializeToString(_docSlideMaster1XmlRels));
    }
    if (_docSlideMaster1Xml) {
      refToZip = refToZip.file('ppt/slideMasters/slideMaster1.xml', _xmlSerializer.serializeToString(_docSlideMaster1Xml));
    }
    if (_slideLayoutXmlRelsContent) {
    refToZip = refToZip.file(`ppt/slideLayouts/_rels/slideLayout${currentNumberOfSlideLayouts + 1}.xml.rels`, _slideLayoutXmlRelsContent);
    }
    if (slide.layout && slide.layout.xmlLayoutString) {
      refToZip = refToZip.file(`ppt/slideLayouts/slideLayout${currentNumberOfSlideLayouts + 1}.xml`, slide.layout.xmlLayoutString);
    }


    return refToZip;
  }

  /**
   * Creates a paragraph node and the corresponding xml.rels for a slide
   * Also see https://docs.microsoft.com/en-us/dotnet/api/system.windows.documents.paragraph?view=netframework-4.8
   * @param params The params for the new paragraph node
   */
  private static _createParagraph(params: ICreateParagraphParameters): {paragraph: Node, xmlRels: Document} {
    // the paragraph node that will be returned at the end of this function
    const _newElemP = document.createElementNS(xmlNS.drawingml.main, 'a:p');
    let _newXmlRels: Document = null;
    for (const textObject of params.texts) {
      const _newElemT = document.createElementNS(xmlNS.drawingml.main, 'a:t');
      _newElemT.innerHTML = textObject.text;
      // tslint:disable-next-line:max-line-length
      // TODO RECEIPT_ALTERNATIVE: when there was a typo and instead of a:rPr it a.rPr, an element a:.arPr was created and NOT REMOVED by powerpoint upon reparation!
      const _newElemRpr = document.createElementNS(xmlNS.drawingml.main, 'a:rPr');
      _newElemRpr.setAttribute('lang', 'en-US');
      _newElemRpr.setAttribute('dirty', '0');
      // check if additinal params are passed
      if (textObject.properties) {
        // check for making it a hyperlink
        if (textObject.properties.hyperlink) {
          // find out the new rId
          const _docSlideRel: Document = textObject.properties.hyperlink.xmlRel;
          const _newRId: number = _docSlideRel.getElementsByTagName('Relationship').length + 1;
          const _newElemRelationship = document.createElementNS(xmlNS.package.relationships, 'Relationship');
          _newElemRelationship.setAttribute('Id', `rId${_newRId}`);
          _newElemRelationship.setAttribute('TargetMode', 'External');
          _newElemRelationship.setAttribute('Target', textObject.properties.hyperlink.url);
          _newElemRelationship.setAttribute('Type', xmlSchemata.hyperlink);
          // append the new <Relationship> element as a child to the root <Relationships> element
          _docSlideRel.getElementsByTagName('Relationships')[0].appendChild(_newElemRelationship);
          // set the ref to the document of the xml rels such that it can be returned at the end of this function
          _newXmlRels = _docSlideRel;

          // now create the proper hlinkClick node with the correct r:id
          const _newElemHlinkClick = document.createElementNS(xmlNS.drawingml.main, 'a:hlinkClick');
          _newElemHlinkClick.setAttributeNS(xmlNS.officeDocument.relationships, 'r:id', `rId${_newRId}`);
          _newElemRpr.appendChild(_newElemHlinkClick);
        }
      }
      const _newElemR = document.createElementNS(xmlNS.drawingml.main, 'a:r');
      _newElemR.append(_newElemRpr, _newElemT);
      _newElemP.appendChild(_newElemR);
    }

    // return the new paragraph node
    return {
      paragraph: _newElemP,
      xmlRels: _newXmlRels
    };
  }

  /**
   * Add custom data to the OOXML file
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param name The name of the variable holding the data
   * @param value The actual data
   */
  public static async addCustomDataAsString(refToZip: JSZip, name: string, value: string): Promise<JSZip> {
    // check if custom.xml does not already exist yet
    if (!(await this._getCustomXmlFileContent(refToZip))) {
      await this._createCustomXmlAndDependencies(refToZip);
    }
    const _customXmlContent = await refToZip.file('docProps/custom.xml').async('text');
    const _docCustomXml: Document = new DOMParser().parseFromString(_customXmlContent, 'application/xml');
    const _elemProperties: Element = _docCustomXml.getElementsByTagName('Properties')[0];
    // since it starts counting at 2, make sure that you add 2 to the current number
    const _newIdProperty: string = _elemProperties.getElementsByTagName('property').length + 2 + '';
    // create the new property that will later hold the string we want to save
    const _elemNewProperty = document.createElementNS(xmlNS.officeDocument.customProperties, 'property');
    _elemNewProperty.setAttribute('fmtid', '{D5CDD505-2E9C-101B-9397-08002B2CF9AE}');
    _elemNewProperty.setAttribute('pid', _newIdProperty);
    _elemNewProperty.setAttribute('name', name);
    _elemProperties.appendChild(_elemNewProperty);

    // create the string and append it to the <property>
    const _elemNewLpwStr = document.createElementNS(xmlNS.officeDocument.docPropsVTypes, 'vt:lpwstr');
    _elemNewLpwStr.innerHTML = value;
    _elemNewProperty.appendChild(_elemNewLpwStr);

    // write the changes to the file
    refToZip = refToZip.file('docProps/custom.xml', new XMLSerializer().serializeToString(_docCustomXml));

    return refToZip;
  }

  /**
   * Get custom data embedded into the OOXML file
   * @param refToZip
   * @param name
   */
  public static async getCustomData(refToZip: JSZip, name: string): Promise<string> {
    const _customXmlContent = await this._getCustomXmlFileContent(refToZip);
    let _returnVal: string = null;
    if (_customXmlContent) {
      // if the result is not empty, proceed
      const _docCustomXml: Document = new DOMParser().parseFromString(_customXmlContent, 'application/xml');
      const _elemProperties: Element = _docCustomXml.getElementsByTagName('Properties')[0];
      let _foundName = false;
      _elemProperties.childNodes.forEach(((child: HTMLElement, key: number) => {
        if (!_foundName && child.getAttribute('name') === name) {
          _returnVal = (child.getElementsByTagName('vt:lpwstr')[0]).innerHTML;
          // since it is not possible to leave the loop via break (because we're within a callback fnc), I have to do it this way
          _foundName = true;
        }
      }));
    }
    return _returnVal;
  }

  /**
   * The the content of the file inside the OOXML file that holds the data for custom data
   * @param refToZip The JSZip object resembling the pptx zip file
   * @private
   */
  private static async _getCustomXmlFileContent(refToZip: JSZip): Promise<JSZipObject> {
    return JsZipHelper.getFileContent(refToZip, 'docProps/custom.xml');
  }

  /**
   * Create the file inside the OOXML file that holds the custom data and also create its dependencies
   * @param refToZip The JSZip object resembling the pptx zip file
   * @private
   */
  private static async _createCustomXmlAndDependencies(refToZip: JSZip): Promise<JSZip> {
    // check if it already exists
    if (await this._getCustomXmlFileContent(refToZip)) {
      throw new Error('custom.xml already exists.');
    } else {
      // tslint:disable-next-line:max-line-length
      const _basicTemplateCustomXml = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<Properties xmlns="${xmlNS.officeDocument.customProperties}" xmlns:vt="${xmlNS.officeDocument.docPropsVTypes}"></Properties>`;
      const _pathCustomXmlFile = 'docProps/custom.xml';
      const _xmlSerializer = new XMLSerializer();
      refToZip = await JsZipHelper.createFile(refToZip, _pathCustomXmlFile, _basicTemplateCustomXml);

      // add <Relationship> to /_rels/.rels
      const _relsContent = await refToZip.file('_rels/.rels').async('text');
      const _docRels: Document = new DOMParser().parseFromString(_relsContent, 'application/xml');
      const _elemRelationships = _docRels.getElementsByTagName('Relationships')[0];
      const _newRelationshipId: string = 'rId' + (_elemRelationships.getElementsByTagName('Relationship').length + 1);
      const _elemNewRelationship = document.createElementNS(xmlNS.package.relationships, 'Relationship');
      _elemNewRelationship.setAttribute('Id', _newRelationshipId);
      _elemNewRelationship.setAttribute('Type', xmlNS.relationships.customProperties);
      _elemNewRelationship.setAttribute('Target', 'docProps/custom.xml');
      _elemRelationships.appendChild(_elemNewRelationship);
      // write to file
      refToZip = refToZip.file('_rels/.rels', _xmlSerializer.serializeToString(_docRels));

      // add <Override> to /[Content_Types].xml
      const _contentTypesXmlContent = await refToZip.file('[Content_Types].xml').async('text');
      const _docContentTypesXml: Document = new DOMParser().parseFromString(_contentTypesXmlContent, 'application/xml');
      const _elemTypes = _docContentTypesXml.getElementsByTagName('Types')[0];
      const _elemNewOverride = document.createElementNS(xmlNS.package.contentTypes, 'Override');
      _elemNewOverride.setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.custom-properties+xml');
      _elemNewOverride.setAttribute('PartName', '/docProps/custom.xml');
      _elemTypes.appendChild(_elemNewOverride);
      // write to file
      refToZip = refToZip.file('[Content_Types].xml', _xmlSerializer.serializeToString(_docContentTypesXml));

      return refToZip;
    }
  }

  /**
   * Add a default extension to the OOXML file
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param defaultExtension The extension that shall be added
   * @private
   */
  private static async _addDefaultExtension(refToZip: JSZip, defaultExtension: DefaultContentType): Promise<JSZip> {
    const _contentTypesXmlContent = await refToZip.file('[Content_Types].xml').async('text');
    const _docContentTypesXml: Document = new DOMParser().parseFromString(_contentTypesXmlContent, 'application/xml');
    // It only seems to matter that the <Default> nodes precede the <Override> nodes, but the order
    // within does not matter (even though powerpoint bring them all in order upon each save)
    const _elemTypes = _docContentTypesXml.getElementsByTagName('Types')[0];
    const _lastElemDefault = Array.from<Node>(_docContentTypesXml.getElementsByTagName('Types')[0].getElementsByTagName('Default')).shift();
    // thanks for the swift explanation to Barmar @ https://stackoverflow.com/a/23401695/7618184
    _elemTypes.insertBefore(defaultExtension.toElement(), _lastElemDefault.nextSibling);

    // write to file
    refToZip = await refToZip.file('[Content_Types].xml', new XMLSerializer().serializeToString(_docContentTypesXml));
    return refToZip;
  }

  /**
   * Check if a given extension is already known
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param defaultExtensionContentType The extension that is to be checked
   * @private
   */
  // tslint:disable-next-line:max-line-length
  private static async _doesDefaultExtensionAlreadyExist(refToZip: JSZip, defaultExtensionContentType: DefaultContentType): Promise<boolean> {
    const _contentTypesXml = await refToZip.file('[Content_Types].xml').async('text');
    const _contentTypesDoc: Document = await new DOMParser().parseFromString(_contentTypesXml, 'application/xml');
    const _elemTypes = _contentTypesDoc.getElementsByTagName('Types')[0];
    for (const child of Array.from(_elemTypes.getElementsByTagName('Default'))) {
      if (
        child.getAttribute('Extension') === defaultExtensionContentType.getExtension() &&
        child.getAttribute('ContentType') === defaultExtensionContentType.getContentType().toString()
      ) {
        return true;
      }
    }
    // if the for loop went through, then the default extension is not known yet
    return false;
  }

  /**
   * Get the number/position of a layout by its name
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param layoutName The name of the seeked layout
   * @param slideMaster The slide master that shall be search through. If none is given, all masters will be searched
   * @private
   */
  // tslint:disable-next-line:max-line-length
  private static async _getPositionOfSlideLayoutByName(refToZip: JSZip, layoutName: MultiLanguageSlideLayoutTitle, slideMaster?: number): Promise<number> {
    //
    // little helper to search for the desired name in the given layouts
    //
    const helperSearchThroughLayouts = (async (layouts: string[] | number[]): Promise<number> => {
      for (const layout of layouts) {
        // if it is a string, we assume it will contain the full path. If it is number, we assume it's the layout's number/position
        const _pathToLayout: string = typeof layout === 'string' ? layout : `ppt/slideLayouts/slideLayout${layout}.xml`;
        const _slideLayoutXml = await refToZip.file(_pathToLayout).async('text');
        const _slideLayoutDoc: Document = new DOMParser().parseFromString(_slideLayoutXml, 'application/xml');
        const _elemCSld = _slideLayoutDoc.getElementsByTagName('p:cSld')[0];
        for (const _name of layoutName) {
          if (_name === _elemCSld.getAttribute('name')) {
            if (typeof layout === 'string') {
              const _splitAtSlash = layout.split('/');
              // the file name with extension will be at the end of the split
              const _splitAtDot = _splitAtSlash.pop().split('.');
              // now remove the preceding 'slideLayout' portion of the string
              return Number(_splitAtDot[0].substring(11, _splitAtDot[0].length));
            } else if (typeof layout === 'number') {
              // since it is a number, we assume that it resembles the layout's position/number
              return layout;
            }
          }
        }
      }
      // if it looped through all the available slide layouts, then the desired one couldn't be found
      return Promise.reject(`Couldn't find slide layout '${layoutName.shift()}' or any of its variants.`);
    });
    //
    // end of little helper function
    //

    if (!slideMaster) {
      // if no slide master was passed, then push all the layouts to the array of available layouts that shall be searched
      const _availableSlideLayouts: JSZipObject[] = refToZip.folder('ppt/slideLayouts').file(/^slideLayout(\d)+\.xml$/);
      // map the items which are JSZipObject to their name
      return helperSearchThroughLayouts(_availableSlideLayouts.map(x => x['name']));
    } else {
      // if a slide master was passed, only look through the layouts associated with it

      // first check if the slide master provided does really exist. If it exists, it will also have a corresponding .rels.
      // since we ultimately need the content of that .rels, we can check whether or not the master exists by checking if its .rels
      // exists.
      const _slideMasterRelsString = await refToZip.file(`ppt/slideMasters/_rels/slideMaster${slideMaster}.xml.rels`).async('text');
      if (!_slideMasterRelsString) {
        // if null or empty, then the slide master does not exist
        return Promise.reject(`slide master number ${slideMaster} does not exist.`);
      } else {
        // parsed content to Document object
        const _slideMasterRelsDoc: Document = new DOMParser().parseFromString(_slideMasterRelsString, 'application/xml');
        const _elemRelationships = _slideMasterRelsDoc.getElementsByTagName('Relationships')[0];
        const _slideLayoutRegex = new RegExp(/slideLayouts\/slideLayout(\d)+\.xml$/);
        const _relatedLayoutNumbers: number[] = [];
        for (const elemRelationship of Array.from(_elemRelationships.childNodes)) {
          if (elemRelationship instanceof Element && _slideLayoutRegex.test(elemRelationship.getAttribute('Target'))) {
            const _target = elemRelationship.getAttribute('Target');
            // find the position of the number by checking against a regex
            // see https://www.tutorialspoint.com/typescript/typescript_string_search.htm
            const _posFirstdigit = _target.search(/(\d)+/);
            // we don't need to imageFromMachine if the search found a digit at all, because then the _slideLayoutRegex.imageFromMachine()
            // would have been false already
            // push all the single digits in here and then glue/implode/concat/join them to a string and then parse it to number
            const _digits: number[] = [];
            // now we need to check if there are more digits following
            for (let i = _posFirstdigit; true; i++) {
              if (!isNaN(Number(_target.charAt(i)))) {
                // then it is a number!
                _digits.push(Number(_target.charAt(i)));
              } else {
                // if it is not a number, then there are no more digits following and the loop can be finished
                break;
              }
            }
            _relatedLayoutNumbers.push(Number(_digits.join('')));
          }
        }

        // now we've got all the layout numbers that are associated with the desired slide master
        return helperSearchThroughLayouts(_relatedLayoutNumbers);
      }
    }
  }

  /**
   * Get the slide layout based on its number/position
   * @param refToZip The JSZip object resembling the pptx zip file
   * @param position The number/position of the layout
   * @private
   */
  private static async _getSlideLayoutByPosition(refToZip: JSZip, position: number): Promise<Document> {
    const _layoutXmlContent = await refToZip.file(`ppt/slideLayouts/slideLayout${position}.xml`).async('text');
    return new DOMParser().parseFromString(_layoutXmlContent, 'application/xml');
  }

  /**
   * Get the number of available slide layouts
   * @param refToZip The JSZip object resembling the pptx zip file
   * @private
   */
  private static async _getNumberOfSlideLayouts(refToZip: JSZip): Promise<number> {
    return refToZip.folder('ppt/slideLayouts').file(/^slideLayout(\d)+\.xml$/).length;

  }

  /**
   * Get the name of the author of this OOXML file
   * @param refToZip The JSZip object resembling the pptx zip file
   */
  public static async getAuthorName(refToZip: JSZip): Promise<string> {
    const _coreXmlContent = await refToZip.file(`docProps/core.xml`).async('text');
    const _coreXmlDoc = new DOMParser().parseFromString(_coreXmlContent, 'application/xml');
    return Promise.resolve(_coreXmlDoc.getElementsByTagName('dc:creator')[0].innerHTML || 'unknown');
  }
}

/**
 * Resembles a content type
 */
export abstract class ContentType {
  /**
   * The MIME type
   */
  protected _contentType: MimeType;

  /**
   * @param contentType The MIME type
   */
  constructor(contentType: MimeType) {
    this._contentType = contentType;
  }

  /**
   * Get the MIME type
   */
  public getContentType(): MimeType {
    return this._contentType;
  }

  /**
   * Return an xml Element that can be attached to Nodes
   */
  public abstract toElement(): Element;

  /**
   * Return the xml Element as a string
   */
  public abstract toXmlString(): string;
}

/**
 * Resembles a default content type
 */
export class DefaultContentType extends ContentType {
  /**
   * The file extension
   */
  protected _defaultExtension: string;

  /**
   * @param source The Element or options this object shall be created from
   */
  constructor(source: Element | DefaultExtensionContentTypeOptions) {
    if (source instanceof Element) {
      const element: Element = source;
      const _nodeName = element.nodeName;
      const _extension = element.getAttribute('Extension');
      const _mimeString: string = element.getAttribute('ContentType');
      const _isValidMime: boolean = MimeType.isValid(element.getAttribute('ContentType'));
      if (_nodeName === 'Default' && _extension && _isValidMime) {
        // return new DefaultContentType(_extension, new MimeType(_mimeString));
        super(new MimeType(_mimeString));
        this._defaultExtension = _extension;
      } else {
        throw new InvalidArgumentError(`Element ${element} cannot be converted into a DefaultExtensionContentType`);
      }
    } else if (source instanceof Object && source.extension && source.type) {
      super(source.type);
      this._defaultExtension = source.extension;
    } else {
      throw new InvalidArgumentError(`${source} cannot be converted into a DefaultExtensionContentType`);
    }
  }

  /**
   * Return an xml Element that can be attached to Nodes
   */
  public toElement(): Element {
    const _elemDefault: Element = document.createElementNS(xmlNS.package.contentTypes, 'Default');
    _elemDefault.setAttribute('Extension', this._defaultExtension);
    _elemDefault.setAttribute('ContentType', this._contentType.toString());
    return _elemDefault;
  }

  /**
   * Return the xml Element as a string
   */
  public toXmlString(): string {
    return new XMLSerializer().serializeToString(this.toElement());
  }

  /**
   * Return just the file extension
   */
  public getExtension(): string {
    return this._defaultExtension;
  }
}

/**
 * Resembles an override content type
 */
export class OverrideContentType extends ContentType {
  /**
   * The name of the part
   */
  protected _partName: URI;

  /**
   * @param partName The name of the part
   * @param contentType The MIME type
   */
  constructor(partName: URI, contentType: MimeType) {
    super(contentType);
    this._partName = partName;
  }

  /**
   * Return an xml Element that can be attached to Nodes
   */
  public toElement(): Element {
    throw new NotYetImplemented();
  }

  /**
   * Return the xml Element as a string
   */
  public toXmlString(): string {
    return new XMLSerializer().serializeToString(this.toElement());
  }

}

/**
 * Describes the options for a default extension
 */
export interface DefaultExtensionContentTypeOptions {
  /**
   * The file extension
   */
  extension: string;
  /**
   * The MIME type
   */
  type: MimeType;
}

/**
 * Is used to identify a slide layout via name, since the name will be in the same language the presentation was created in
 */
export type MultiLanguageSlideLayoutTitle = Array<string>;
