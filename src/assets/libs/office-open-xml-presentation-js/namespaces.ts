/**
 * This holds the namespaces that are used
 */
export const xmlNamespaces = {
  drawingml: {
    main: 'http://schemas.openxmlformats.org/drawingml/2006/main'
  },
  officeDocument: {
    relationships: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships',
    docPropsVTypes: 'http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes',
    customProperties: 'http://schemas.openxmlformats.org/officeDocument/2006/custom-properties'
  },
  presentationml: {
    main: 'http://schemas.openxmlformats.org/presentationml/2006/main',
  },
  package: {
    relationships: 'http://schemas.openxmlformats.org/package/2006/relationships',
    contentTypes: 'http://schemas.openxmlformats.org/package/2006/content-types'
  },
  relationships: {
    customProperties: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties'
  }
};
