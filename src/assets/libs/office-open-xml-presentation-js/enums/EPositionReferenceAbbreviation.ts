export enum EPositionReferenceAbbreviation {
  UPPER_LEFT_CORNER = 'upper left corner',
  UPPER_RIGHT_CORNER = 'upper right corner',
  LOWER_LEFT_CORNER = 'lower left corner',
  LOWER_RIGHT_CORNER = 'lower right corner'
}
