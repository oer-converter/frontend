import * as JSZip from 'jszip';
import {xmlNamespaces} from '../office-open-xml-presentation-js/namespaces';
import {JsZipHelper} from '../../../classes/js-zip-helper';
import {JSZipObject} from 'jszip';
import {DataUrl} from '@hexxag0nal/data-url';
import {MainConfig} from '../../../config/main.config';
import {MimeType} from '@hexxag0nal/mime-type';
import {IImageInFile} from './interfaces';
import {IAddImageFromUrlParameters} from '../../../classes/file-handler.abstract';
import {AddImageAsBase64Parameters} from './interfaces';
import {GetFileReadMode} from './types';
import {InvalidArgumentError, NotYetImplemented} from '@hexxag0nal/error';
import {URI} from '@hexxag0nal/universal-ressource';

export abstract class OfficeOpenXmlJs {
  protected zipFile: JSZip;
  protected xmlSerializer: XMLSerializer;

  protected constructor(refToZip: JSZip) {
    this.zipFile = refToZip;
  }

  /**
   * Add custom data to the OOXML file
   * @param name The name of the variable holding the data
   * @param value The actual data
   */
  public async addCustomDataAsString(name: string, value: string): Promise<void> {
    // check if custom.xml does not already exist yet
    if (!(await this._getCustomXmlFileContent())) {
      await this._createCustomXmlAndDependencies(this.zipFile);
    }
    const _customXmlContent = await this.zipFile.file('docProps/custom.xml').async('text');
    const _docCustomXml: Document = new DOMParser().parseFromString(_customXmlContent, 'application/xml');
    const _elemProperties: Element = _docCustomXml.getElementsByTagName('Properties')[0];
    // since it starts counting at 2, make sure that you add 2 to the current number
    const _newIdProperty: string = _elemProperties.getElementsByTagName('property').length + 2 + '';
    // create the new property that will later hold the string we want to save
    const _elemNewProperty = document.createElementNS(xmlNamespaces.officeDocument.customProperties, 'property');
    _elemNewProperty.setAttribute('fmtid', '{D5CDD505-2E9C-101B-9397-08002B2CF9AE}');
    _elemNewProperty.setAttribute('pid', _newIdProperty);
    _elemNewProperty.setAttribute('name', name);
    _elemProperties.appendChild(_elemNewProperty);

    // create the string and append it to the <property>
    const _elemNewLpwStr = document.createElementNS(xmlNamespaces.officeDocument.docPropsVTypes, 'vt:lpwstr');
    _elemNewLpwStr.innerHTML = value;
    _elemNewProperty.appendChild(_elemNewLpwStr);

    // write the changes to the file
    this.zipFile = this.zipFile.file('docProps/custom.xml', new XMLSerializer().serializeToString(_docCustomXml));
  }

  /**
   * Get custom data embedded into the OOXML file
   * @param name The name of the variable holding the custom data
   */
  public async getCustomData(name: string): Promise<string> {
    const _customXmlContent = await this._getCustomXmlFileContent();
    let _returnVal: string = null;
    if (_customXmlContent) {
      // if the result is not empty, proceed
      const _docCustomXml: Document = new DOMParser().parseFromString(_customXmlContent, 'application/xml');
      const _elemProperties: Element = _docCustomXml.getElementsByTagName('Properties')[0];
      let _foundName = false;
      _elemProperties.childNodes.forEach(((child: HTMLElement, key: number) => {
        if (!_foundName && child.getAttribute('name') === name) {
          _returnVal = (child.getElementsByTagName('vt:lpwstr')[0]).innerHTML;
          // since it is not possible to leave the loop via break (because we're within a callback fnc), I have to do it this way
          _foundName = true;
        }
      }));
    }
    return _returnVal;
  }

  /**
   * The the content of the file inside the OOXML file that holds the data for custom data
   */
  protected async _getCustomXmlFileContent(): Promise<JSZipObject> {
    return JsZipHelper.getFileContent(this.zipFile, 'docProps/custom.xml');
  }

  /**
   * Create the file inside the OOXML file that holds the custom data and also create its dependencies
   * @param refToZip The JSZip object resembling the pptx zip file
   */
  protected async _createCustomXmlAndDependencies(refToZip: JSZip): Promise<JSZip> {
    // check if it already exists
    if (await this._getCustomXmlFileContent()) {
      throw new Error('custom.xml already exists.');
    } else {
      // tslint:disable-next-line:max-line-length
      const _basicTemplateCustomXml = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<Properties xmlns="${xmlNamespaces.officeDocument.customProperties}" xmlns:vt="${xmlNamespaces.officeDocument.docPropsVTypes}"></Properties>`;
      const _pathCustomXmlFile = 'docProps/custom.xml';
      const _xmlSerializer = new XMLSerializer();
      refToZip = await JsZipHelper.createFile(refToZip, _pathCustomXmlFile, _basicTemplateCustomXml);

      // add <Relationship> to /_rels/.rels
      const _relsContent = await refToZip.file('_rels/.rels').async('text');
      const _docRels: Document = new DOMParser().parseFromString(_relsContent, 'application/xml');
      const _elemRelationships = _docRels.getElementsByTagName('Relationships')[0];
      const _newRelationshipId: string = 'rId' + (_elemRelationships.getElementsByTagName('Relationship').length + 1);
      const _elemNewRelationship = document.createElementNS(xmlNamespaces.package.relationships, 'Relationship');
      _elemNewRelationship.setAttribute('Id', _newRelationshipId);
      _elemNewRelationship.setAttribute('Type', xmlNamespaces.relationships.customProperties);
      _elemNewRelationship.setAttribute('Target', 'docProps/custom.xml');
      _elemRelationships.appendChild(_elemNewRelationship);
      // write to file
      refToZip = refToZip.file('_rels/.rels', _xmlSerializer.serializeToString(_docRels));

      // add <Override> to /[Content_Types].xml
      const _contentTypesXmlContent = await refToZip.file('[Content_Types].xml').async('text');
      const _docContentTypesXml: Document = new DOMParser().parseFromString(_contentTypesXmlContent, 'application/xml');
      const _elemTypes = _docContentTypesXml.getElementsByTagName('Types')[0];
      const _elemNewOverride = document.createElementNS(xmlNamespaces.package.contentTypes, 'Override');
      _elemNewOverride.setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.custom-properties+xml');
      _elemNewOverride.setAttribute('PartName', '/docProps/custom.xml');
      _elemTypes.appendChild(_elemNewOverride);
      // write to file
      refToZip = refToZip.file('[Content_Types].xml', _xmlSerializer.serializeToString(_docContentTypesXml));

      return refToZip;
    }
  }

  /**
   * Add a default extension to the OOXML file
   * @param defaultExtension The extension that shall be added
   */
  protected async _addDefaultExtension(defaultExtension: DefaultContentType): Promise<JSZip> {
    const _contentTypesXmlContent = await this.zipFile.file('[Content_Types].xml').async('text');
    const _docContentTypesXml: Document = new DOMParser().parseFromString(_contentTypesXmlContent, 'application/xml');
    // It only seems to matter that the <Default> nodes precede the <Override> nodes, but the order
    // within does not matter (even though powerpoint bring them all in order upon each save)
    const _elemTypes = _docContentTypesXml.getElementsByTagName('Types')[0];
    const _lastElemDefault = Array.from<Node>(_docContentTypesXml.getElementsByTagName('Types')[0].getElementsByTagName('Default')).shift();
    // thanks for the swift explanation to Barmar @ https://stackoverflow.com/a/23401695/7618184
    _elemTypes.insertBefore(defaultExtension.toElement(), _lastElemDefault.nextSibling);

    // write to file
    this.zipFile = await this.zipFile.file('[Content_Types].xml', new XMLSerializer().serializeToString(_docContentTypesXml));
    return this.zipFile;
  }

  /**
   * Check if a given extension is already known
   * @param defaultExtensionContentType The extension that is to be checked
   */
  // tslint:disable-next-line:max-line-length
  protected async _doesDefaultExtensionAlreadyExist(defaultExtensionContentType: DefaultContentType): Promise<boolean> {
    const _contentTypesXml = await this.zipFile.file('[Content_Types].xml').async('text');
    const _contentTypesDoc: Document = await new DOMParser().parseFromString(_contentTypesXml, 'application/xml');
    const _elemTypes = _contentTypesDoc.getElementsByTagName('Types')[0];
    for (const child of Array.from(_elemTypes.getElementsByTagName('Default'))) {
      if (
        child.getAttribute('Extension') === defaultExtensionContentType.getExtension() &&
        child.getAttribute('ContentType') === defaultExtensionContentType.getContentType().toString()
      ) {
        return true;
      }
    }
    // if the for loop went through, then the default extension is not known yet
    return false;
  }

  /**
   * Get the name of the author of this OOXML file
   */
  public async getAuthorName(): Promise<string> {
    const _coreXmlContent = await this.zipFile.file(`docProps/core.xml`).async('text');
    const _coreXmlDoc = new DOMParser().parseFromString(_coreXmlContent, 'application/xml');
    return Promise.resolve(_coreXmlDoc.getElementsByTagName('dc:creator')[0].innerHTML || 'unknown');
  }

  /**
   * Get the zip file of the OOXML document
   */
  public getZip(): JSZip {
    return this.zipFile;
  }

  /**
   * An alias for getZip(), if no filename is provided. If a filename was passed, then that file will be given back
   * @param filepath The name and path of the file within the zipfile that shall be read
   * @param readMode The mode the file shall be read in.
   */
  public async getFile(filepath?: string, readMode: GetFileReadMode = 'text'): Promise<JSZip> {
    if (!filepath) {
      return this.getZip();
    } else {
      return await this.getZip().file(filepath).async(readMode);
    }
  }

  public updateFile(file: JSZip): void {
    this.zipFile = file;
  }

  public updateZip(zip: JSZip): void {
    this.updateFile(zip);
  }

  /**
   * Extract the images as Base64 DataUrls
   */
  protected async _getImages(pathStart: string/*not implemented yet options?: IGetImagesOptions*/): Promise<IImageInFile[]> {
    const _images: IImageInFile[] = [];
    const _imagesLoading: Promise<void>[] = [];
    this.zipFile.folder(pathStart).forEach((relativePath: string, file) => {
      _imagesLoading.push(new Promise<void>(async (resolve) => {
        const fileName: string = relativePath;
        const fileEnding: string = fileName.split('.')[1];
        // check if this type of media is supported by the tool
        let _isIncluded = false;
        for (const type of MainConfig.supportedMediaFormats.images) {
          if (type === fileEnding) {
            _isIncluded = true;
            break;
          }
        }
        // if file is not supported, then don't process its data
        if (!_isIncluded) {
          return resolve();
        }
        const mimeType: MimeType = MimeType.fromFileExtension(fileEnding);
        const fullPath = pathStart + fileName;
        _images.push({
          dataBase64: new DataUrl(mimeType, await this.zipFile.file(fullPath).async('base64'), true),
          path: fullPath,
          id: Number(fileName.match(/\d+/g))
        });
        resolve();
      }));
    });
    await Promise.all(_imagesLoading);
    // Not needed for now
    /*if (options) {
      if (options.sorted === 'id ascending') {}
    }*/
    return _images;
  }

  /**
   * Add an image by its url to the presentation
   * @param image Image that is to be added
   */
  public async addImageFromUrl(image: IAddImageFromUrlParameters): Promise<void> {
    await this._checkExtension(image.nameAndFullPath);
    return this._addImageFromUrl(image);
  }

  /**
   * Same as _addBase64Image but takes an url instead of the image as Base64
   * @param image Object holding the name as full filePath + image name and the image url
   */
  private async _addImageFromUrl(image: IAddImageFromUrlParameters): Promise<void> {
    // check if no error occured by checking if the filePath is correct
    const _imgPath = image.nameAndFullPath;
    if (this.zipFile.files.hasOwnProperty(_imgPath)) {
      const _pathArray = _imgPath.split('/');
      // the slice thingy puts only the last element (which would be the file name) in a new array and then calling
      // the zeroest element makes it a string, such that the split method can be used
      const _fileNameArray = (_pathArray.slice(-1)[0]).split('.');
      // since _fileNameArray will always have [0]: file name and [1]: file ending, it is safe to use it this way
      const _fileEndingOfTheImageFileThatIsToBeReplaced = _fileNameArray[1];
      // const _base64ImagePrefix = `data:image/${_fileEndingOfTheImageFileThatIsToBeReplaced};base64,`;
      // const _pureBase64 = this._selectedImageFromFile.base64

      /**
       * big props and credits to
       * Balvinder Singh, https://medium.com/@erbalvindersingh/fetch-an-image-from-url-in-angular-4-and-store-locally-4928d3b504fc
       **/

        // creating a new image, load the external image into that img
        // Then create a canvas, load the image into the canvas and convert the canvas into a base64 string
      let img = new Image();
      img.crossOrigin = 'anonymous';
      img.src = image.urlToImage;

      return new Promise((resolve, reject) => {
        img.onload = () => {

          /**
           * Props to alessandrio @ https://stackoverflow.com/a/43809873/7618184
           */

          let canvas = document.createElement('canvas');
          const _actualWidth = img.width;
          const _actualHeight = img.height;
          const _targetWidth = image.dimensions.width;
          const _targetHeight = image.dimensions.height;
          let _scale = Math.min(_targetWidth / _actualWidth, _targetHeight / _actualHeight);
          // make sure an replacing image will only be scaled down. If it is scaled up, it will consume more space but looks just
          // as bad as scaling it on-the-fly by the presentation software, i.e. PowerPoint
          _scale = Math.min(1, _scale);
          const _scaledWidth = _actualWidth * _scale;
          const _scaledHeigth = _actualHeight * _scale;
          canvas.width = _scaledWidth;
          canvas.height = _scaledHeigth;
          let ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0, _scaledWidth, _scaledHeigth);

          // [0] will be base64 prefix, something like 'data:image/png;base64', whereas [1] contains the image encoded as base64
          const _base64Image = (canvas.toDataURL(`image/${_fileEndingOfTheImageFileThatIsToBeReplaced}`)).split(',')[1];

          // release the occupied references
          img = null;
          canvas = null;
          ctx = null;

          // thanks to Benjamin Gruenbaum, Patrick Roberts @ https://stackoverflow.com/a/22519785/7618184
          this._addBase64Image({nameAndFullPath: _imgPath, imageAsBase64: _base64Image}).then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          });
        };
      });
    }
  }

  /**
   * This will save an image encoded in Base64 into the zip file. If name/filePath already exists, it will be overwritten!
   * @param image Object holding the name as full filePath + image name and the image encoded in Base64
   */
  public async addBase64Image(image: AddImageAsBase64Parameters): Promise<void> {
    await this._checkExtension(image.nameAndFullPath);
    return this._addBase64Image(image);
  }

  private async _checkExtension(filePath: string): Promise<void> {
    // find the file ending. The first split divides by the '/' and then grabs the last one.
    // The second split divides by '.' and then grabs the last one. This should be the file ending
    const fileEnding: string = (filePath.split('/').pop()).split('.').pop();
    // check whether or not the file extension of the cc license logo is already registered
    const _logoMimeType: MimeType = MimeType.fromFileExtension(fileEnding);
    const _defaultExtension = new DefaultContentType({type: _logoMimeType, extension: fileEnding});
    if (!(await this._doesDefaultExtensionAlreadyExist(_defaultExtension))) {
      this.zipFile = await this._addDefaultExtension(_defaultExtension);
    }
  }

  /**
   * This will save an image encoded in Base64 into the zip file. If name/filePath already exists, it will be overwritten!
   * @param image Object holding the name as full filePath + image name and the image encoded in Base64
   */
  private async _addBase64Image(image: AddImageAsBase64Parameters): Promise<void> {
    // return JsZipHelper.addBase64Image(this.zipFile, {nameAndFullPath: image.nameAndFullPath, imageAsBase64: image.imageAsBase64});
    this.zipFile = this.zipFile.file(image.nameAndFullPath, image.imageAsBase64, {base64: true});
  }

  public abstract getImages(): Promise<IImageInFile[]>;

  protected serializeXml(root: Node): string {
    if (!this.xmlSerializer) {
      this.xmlSerializer = new XMLSerializer();
    }
    return this.xmlSerializer.serializeToString(root);
  }

  public async getNumberOfImages(pathToMedia: string): Promise<number> {
    const _regex = /^image\d+\./;
    return await this.zipFile.folder(pathToMedia + 'media').file(_regex).length;
  }
}


/**
 * Resembles a content type
 */
export abstract class ContentType {
  /**
   * The MIME type
   */
  protected _contentType: MimeType;

  /**
   * @param contentType The MIME type
   */
  constructor(contentType: MimeType) {
    this._contentType = contentType;
  }

  /**
   * Get the MIME type
   */
  public getContentType(): MimeType {
    return this._contentType;
  }

  /**
   * Return an xml Element that can be attached to Nodes
   */
  public abstract toElement(): Element;

  /**
   * Return the xml Element as a string
   */
  public abstract toXmlString(): string;
}

/**
 * Resembles a default content type
 */
export class DefaultContentType extends ContentType {
  /**
   * The file extension
   */
  protected _defaultExtension: string;

  /**
   * @param source The Element or options this object shall be created from
   */
  constructor(source: Element | DefaultExtensionContentTypeOptions) {
    if (source instanceof Element) {
      const element: Element = source;
      const _nodeName = element.nodeName;
      const _extension = element.getAttribute('Extension');
      const _mimeString: string = element.getAttribute('ContentType');
      const _isValidMime: boolean = MimeType.isValid(element.getAttribute('ContentType'));
      if (_nodeName === 'Default' && _extension && _isValidMime) {
        // return new DefaultContentType(_extension, new MimeType(_mimeString));
        super(new MimeType(_mimeString));
        this._defaultExtension = _extension;
      } else {
        throw new InvalidArgumentError(`Element ${element} cannot be converted into a DefaultExtensionContentType`);
      }
    } else if (source instanceof Object && source.extension && source.type) {
      super(source.type);
      this._defaultExtension = source.extension;
    } else {
      throw new InvalidArgumentError(`${source} cannot be converted into a DefaultExtensionContentType`);
    }
  }

  /**
   * Return an xml Element that can be attached to Nodes
   */
  public toElement(): Element {
    const _elemDefault: Element = document.createElementNS(xmlNamespaces.package.contentTypes, 'Default');
    _elemDefault.setAttribute('Extension', this._defaultExtension);
    _elemDefault.setAttribute('ContentType', this._contentType.toString());
    return _elemDefault;
  }

  /**
   * Return the xml Element as a string
   */
  public toXmlString(): string {
    return new XMLSerializer().serializeToString(this.toElement());
  }

  /**
   * Return just the file extension
   */
  public getExtension(): string {
    return this._defaultExtension;
  }
}

/**
 * Resembles an override content type
 */
export class OverrideContentType extends ContentType {
  /**
   * The name of the part
   */
  protected _partName: URI;

  /**
   * @param partName The name of the part
   * @param contentType The MIME type
   */
  constructor(partName: URI, contentType: MimeType) {
    super(contentType);
    this._partName = partName;
  }

  /**
   * Return an xml Element that can be attached to Nodes
   */
  public toElement(): Element {
    throw new NotYetImplemented();
  }

  /**
   * Return the xml Element as a string
   */
  public toXmlString(): string {
    return new XMLSerializer().serializeToString(this.toElement());
  }

}

/**
 * Describes the options for a default extension
 */
export interface DefaultExtensionContentTypeOptions {
  /**
   * The file extension
   */
  extension: string;
  /**
   * The MIME type
   */
  type: MimeType;
}

/**
 * Is used to identify a slide layout via name, since the name will be in the same language the presentation was created in
 */
export type MultiLanguageSlideLayoutTitle = Array<string>;
