export const xmlTypes = {
  officeDocument: {
    2006: {
      relationships: {
        image: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/image',
        hyperlink: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink',
        fontTable: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable',
        theme: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme',
        customXml: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml',
        styles: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles',
        settings: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings',
        webSettings: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings'
      }
    }
  },
  office: {
    2007: {
      relationships: {
        stylesWithEffects: 'http://schemas.microsoft.com/office/2007/relationships/stylesWithEffects'
      }
    }
  }
}
