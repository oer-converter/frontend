import {IAddStyleOptions} from './interfaces';

/**
 * Default options when adding a style
 */
export const DefaultAddStyleOptions: IAddStyleOptions = {
  ifAlreadyExists: 'do not add'
};
