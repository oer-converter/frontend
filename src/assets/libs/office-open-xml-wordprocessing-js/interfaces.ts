/**
 * Describes properties of texts that shall be appended to a paragraph node
 */
export interface IWordprocessingParagraphTextParameters {
  /**
   * The text that will be written to a paragraph
   */
  text: string;
  /**
   * You can give the text some properties if wanted
   */
  properties?: {
    /**
     * If provided, the text will be made a hyperlink to the specified target
     */
    hyperlink?: {
      /**
       * The url of the target
       */
      url: string;
    }
  };
}

/**
 * Possible options when adding a style
 */
export interface IAddStyleOptions {
  ifAlreadyExists: 'replace' | 'do not add';
}
