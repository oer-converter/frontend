export const beschriftungStyleXml = '<w:style w:type="paragraph" w:styleId="Beschriftung" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">\n' +
  '    <w:name w:val="caption"/>\n' +
  '    <w:basedOn w:val="Standard"/>\n' +
  '    <w:next w:val="Standard"/>\n' +
  '    <w:uiPriority w:val="35"/>\n' +
  '    <w:unhideWhenUsed/>\n' +
  '    <w:qFormat/>\n' +
  '    <w:rsid w:val="00EC0A76"/>\n' +
  '    <w:pPr>\n' +
  '      <w:spacing w:after="200" w:line="240" w:lineRule="auto"/>\n' +
  '    </w:pPr>\n' +
  '    <w:rPr>\n' +
  '      <w:i/>\n' +
  '      <w:iCs/>\n' +
  '      <w:color w:val="44546A" w:themeColor="text2"/>\n' +
  '      <w:sz w:val="18"/>\n' +
  '      <w:szCs w:val="18"/>\n' +
  '    </w:rPr>\n' +
  '  </w:style>';
