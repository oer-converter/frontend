import {FileHandlerAbstract, IAddImageFromUrlParameters, IFileHanderParameters, ISaveToFileProgress} from './file-handler.abstract';
import {AddImageAsBase64Parameters} from '../assets/libs/office-open-xml-js/interfaces';
import {IReference, ISource} from '../assets/libs/office-open-xml-presentation-js/interfaces';
import {IImageWrapper} from '../models/image-wrapper';
import {Subject} from 'rxjs';
import {Document as DocumentModel} from '../models/document.model';
import {MimeType} from '@hexxag0nal/mime-type';
import {v4 as uuidv4} from 'uuid';
import {DataUrl} from '@hexxag0nal/data-url';
import {Utils} from '@hexxag0nal/utils';
import {UUID} from '@hexxag0nal/uuid';
import {PDFDocument, PDFStream} from 'pdf-lib';
import {saveAs} from 'file-saver';

// see this https://resources.infosecinstitute.com/pdf-file-format-basic-structure/ to understand PDFs
// or https://www.oreilly.com/library/view/pdf-explained/9781449321581/ch04.html
// or https://web.archive.org/web/20141010035745/http://gnupdf.org/Introduction_to_PDF


// big fat props to https://stackoverflow.com/a/50769855/7618184 !!!!
import * as pdfjsLib from 'pdfjs-dist/build/pdf';
import {
  PDFDocumentProxy,
  getDocument as getPdfDocument,
  GlobalWorkerOptions as PdfjsGlobalWorkerOptions,
} from 'pdfjs-dist';
import {Licenses} from '../models/licenses';
import {License} from '../assets/libs/office-open-xml-js/license';
// using the worker in the node_modules does not work, probably because it can't be accessed
PdfjsGlobalWorkerOptions.workerSrc = '/assets/pdf.worker.min.js';


export class PdfHandler extends FileHandlerAbstract {
  private _pdf: PDFDocumentProxy;
  private __inited: boolean;

  // dev
  private __passedFile: FileWrapper;

  constructor(params: IFileHanderParameters) {
    super(params);
    this.__inited = false;
    this.__init(params);
  }

  /**
   * Just a helper class to avoid the .then() chains inside the constructor
   * @param params Parameters to instantiate an object of this class. Was initially passed to the original constructor
   */
  private async __init(params: IFileHanderParameters): Promise<void> {
    if (!this.__inited) {
      // dev
      this.__passedFile = params.file;


      this.__inited = true;
      const pdfAsBuffer = await new Response(params.file.file).arrayBuffer();
      // find the PDFJS docs here https://mozilla.github.io/pdf.js/api/draft/index.html
      // and examples here https://mozilla.github.io/pdf.js/examples/index.html#interactive-examples
      // or play with http://brendandahl.github.io/pdf.js.utils/browser/ to learn about the structure
      this._pdf = await getPdfDocument(pdfAsBuffer).promise;
      const mimeType = new MimeType('application/pdf');
      const guid = await this.getGuid();
      this._document = new DocumentModel(guid, mimeType);
      this._subjectSubhandlerReady.next();
    } else {
      return Promise.reject('Already inited. Why do you want to init again?!');
    }
  }

  /**
   * See FileHandlerAbstract
   */
  public async addBase64Image(image: AddImageAsBase64Parameters): Promise<any> {
    console.log('would need to add base64 image', image);
  }

  /**
   * See FileHandlerAbstract
   */
  public async addImageFromUrl(image: IAddImageFromUrlParameters): Promise<any> {
    console.log('would have to add image from url', image);
  }

  /**
   * See FileHandlerAbstract
   */
  public async addSources(sources: ISource[]): Promise<void> {
    console.log('would have to add sources', sources);
  }

  /**
   * See FileHandlerAbstract
   */
  public async addBibRefsToImages(references: IReference[]): Promise<void> {
    console.log('Would have to add BibRefs', references);
  }

  /**
   * See FileHandlerAbstract
   */
  public async getImages(): Promise<IImageWrapper[]> {
    // use https://www.extractpdf.com/ to check if actually all images are extracted
    const numberOfPages = this._pdf.numPages;
    const namesOfImageObjectsInPdf: {[key: number]: string[]} = {};
    const images: IImageWrapper[] = [];
    // use an array containing an MD5 checksum to check whether an image was already pushed to the 'images' arrray, since in a
    // PDF the the same image is embedded into each page it occurs on and is not cross-referenced
    const rawImages: Uint8ClampedArray[] = [];
    let counterInternalId = 0;
    for (let currentPageIndex = 1; currentPageIndex <= numberOfPages; currentPageIndex++) {
      // init the string array
      namesOfImageObjectsInPdf[currentPageIndex] = [];
      const page = await this._pdf.getPage(currentPageIndex);
      // @ts-ignore The function is present, dunno why TS does not recognize it
      const ops = await page.getOperatorList();

      // big props to Jason Siefken @ https://stackoverflow.com/a/39855420/7618184
      for (let i = 0; i < ops.fnArray.length; i++) {
        if (
          ops.fnArray[i] === pdfjsLib.OPS.paintJpegXObject ||
          ops.fnArray[i] === pdfjsLib.OPS.paintImageXObject ||
          ops.fnArray[i] === pdfjsLib.OPS.paintXObject
        ) {
          namesOfImageObjectsInPdf[currentPageIndex].push(ops.argsArray[i][0]);
        }
      }
      for (const nameOfImageObject of namesOfImageObjectsInPdf[currentPageIndex]) {

        const imageObject: {
          data: Uint8ClampedArray,
          height: number,
          width: number,
          kind: number
        // @ts-ignore The object exists, dunno why TS does not recognize it
        } = await page.objs.get(nameOfImageObject);


        // Compare the raw images' data to avoid duplicates
        let skipThisImage = false;
        for (const rawImage of rawImages) {
          if (Utils.areSimilarArrays(rawImage, imageObject.data)) {
            skipThisImage = true;
            break;
          }
        }
        // Only draw canvas and stuff if image is not skipped (if it has not been processed before)
        if (!skipThisImage) {
          // then the images was not already processed. Push its raw data, hence it won't be processed in another future iteration
          rawImages.push(imageObject.data);

          const width = imageObject.width;
          const height = imageObject.height;

          images.push({
            internalId: counterInternalId++,
            base64: Utils.binaryImageDataToDataUrl(imageObject.data, width, height),
            dimension: {
              width: width,
              height: height,
              ratio: width / height
            }
          });
        }
      }
    }

    return images;
  }

  /**
   * See FileHandlerAbstract
   */
  public async saveToFile(legalDisclaimerText: string, targetLicense: License, logSubject?: Subject<ISaveToFileProgress>): Promise<void> {
    console.log('Would have to save operations to file');

    const pdfAsBase64DataUrl: DataUrl = await Utils.readBlobAsDataUrl(this.__passedFile.file);
    const pdfDoc = await PDFDocument.load(pdfAsBase64DataUrl.getData());


    // small helper function that may aid in finding a way to manipulate image data
    const makeOpReadable = (opId): string => {
      for (const opKey in pdfjsLib.OPS) {
        if (pdfjsLib.OPS.hasOwnProperty(opKey) && pdfjsLib.OPS[opKey] === opId) {
          return opKey;
        }
      }
    };


    // this loop finds the images in a pdf file. For further information, please refer to the pdf-lib
    // docs @ https://pdf-lib.js.org/docs/api/ or the examples @ https://pdf-lib.js.org/#examples
    for (const [ref, object] of pdfDoc.getPage(0).doc.context.enumerateIndirectObjects()) {
      if (object instanceof PDFStream) {
        const subtypeObj = object.dict.context.obj('Subtype');
        const imageObj = object.dict.context.obj('Image');
        // check if it has the subtype object and then check if the subtype is /Image
        if (object.dict.has(subtypeObj) && object.dict.get(subtypeObj) === imageObj) {
          // uncomment the following line to see what happens if you set the whole image data to 255 => it should be a white image, right?
          // object['contents'] = new Uint8Array(object.getContentsSize()).fill(255, 0 , object.getContentsSize());



          // object.getContents() contains the image data

          // use the following line to get the image's width
          // const width: number = (object.dict.get(object.dict.context.obj('Width')) as PDFNumber).asNumber();
          // use the following line to get the image's height
          // const height: number = (object.dict.get(object.dict.context.obj('Height')) as PDFNumber).asNumber();
        }
      }
    }

    // the following lines save the current pdf's data (manipulated or not) to a file which can then be downloaded
    const pdfBytes = await pdfDoc.save();
    saveAs(new Blob([pdfBytes], {type: 'application/pdf'}), this.__passedFile.file.name);
  }

  /**
   * If a GUID is present, return it. If no GUID can be found, generate a new one and save it to the file.
   */
  protected async getGuid(): Promise<UUID> {
    // since we don't know yet how to embed a guid, we'll create a new one every time
    // todo: see #58
    const guid = this._pdf.fingerprint;
    if (!guid) {
      return new UUID(uuidv4());
    } else {
      return new UUID(guid);
    }
  }

  /**
   * Get the name of the author of the file
   */
  public async getFileAuthorName(): Promise<string> {
    const author = (await this._pdf.getMetadata()).info['Author'];
    return author ? author : 'unknown';
  }
}
