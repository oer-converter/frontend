export class Feedback {
   _id: string;
   wasHelpful: string;
   uiIsGood: string;
   iWillUseIt: string;
   recommendOthers: string;
   suggestions: string;
}
