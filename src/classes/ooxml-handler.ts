import {
  FileHandlerAbstract,
  FileHandlerActionEnum, IAddImageFromUrlParameters,
  IFileHanderParameters,
  IFileHandlerAddSourceAction, IFileHandlerPublishAsOerAction, IFileHandlerReplaceImageAction,
  ISaveToFileProgress
} from './file-handler.abstract';
import {OfficeOpenXmlPresentationJs} from '../assets/libs/office-open-xml-presentation-js/office-open-xml-presentation-js';
import {JsZipHelper} from './js-zip-helper';
import {MimeType} from '@hexxag0nal/mime-type';
import {Document} from '../models/document.model';
import {OfficeOpenXmlJs} from '../assets/libs/office-open-xml-js/office-open-xml-js';
import * as JSZip from 'jszip';
import {IImageWrapper} from '../models/image-wrapper';
import {Subject} from 'rxjs';
import {DataUrl} from '@hexxag0nal/data-url';
import {Crypto} from '@hexxag0nal/crypto';
import {Licenses, LicensesBaseHref, LicensesHumanReadable} from '../models/licenses';
import {ISource} from '../assets/libs/office-open-xml-presentation-js/interfaces';
import {IReference} from '../assets/libs/open-xml-presentation-js-depr/interfaces/IReference';
import {OpenXmlPresentationJs} from '../assets/libs/open-xml-presentation-js-depr/open-xml-presentation-js';
import {AddImageAsBase64Parameters} from '../assets/libs/office-open-xml-js/interfaces';
import {UUID} from '@hexxag0nal/uuid';
import {MainConfig} from '../config/main.config';
import {v4 as UUIDv4} from 'uuid';
import {IOfficeOpenXmlConfig, IOfficeOpenXmlPresentationConfig, OfficeOpenXmlPresentationConfig} from '../config/ooxml.config';
import {License} from '../assets/libs/office-open-xml-js/license';

export abstract class OoxmlHandler<kindOfDocument, kindOfConfig extends IOfficeOpenXmlConfig> extends FileHandlerAbstract {
  /**
   * The actual zip file
   */
  protected _zip: Promise<JSZip>;

  /**
   * An instance of the ooxml-presentation-js class
   */
  protected ooxmlFile: OfficeOpenXmlJs;

  /**
   * @param params Parameters for successfully instantiating this
   */
  protected constructor(params: IFileHanderParameters) {
    super(params);
  }

  /**
   * Get the zip file
   */
  public async getZip(): Promise<JSZip> {
    return this._zip;
  }

  /**
   * Extracts image file from the presentation file, processes them into a Base64 string and then wraps
   * the image wrapper around it
   */
  public async getImages(): Promise<IImageWrapper[]> {
    // need to await the zip file otherwise the ooxml class will not have been instantiated
    await this._zip;
    const _images: IImageWrapper[] = [];
    const _imagesLoading: Promise<void>[] = [];

    for (const imageFromFile of (await this.ooxmlFile.getImages())) {
      _imagesLoading.push(new Promise<void>(async (resolve) => {
        const tmpImg = new Image();
        tmpImg.src = imageFromFile.dataBase64.toString();
        tmpImg.onload = () => {
          _images.push({
            internalId: imageFromFile.id,
            base64: imageFromFile.dataBase64,
            source: imageFromFile.path,
            dimension: {
              width: tmpImg.width,
              height: tmpImg.height,
              ratio: tmpImg.width / tmpImg.height
            }
          });
          resolve();
        };
      }));
    }
    await Promise.all(_imagesLoading);
    return _images;
  }

  public abstract saveToFile(legalDisclaimerText: string, targetLicense: License, logSubject?: Subject<ISaveToFileProgress>): Promise<void>;

  public async _executeActionQueue(logSubject?: Subject<ISaveToFileProgress>): Promise<void> {
    // check for actions that demand to create a source on a slide. Put them all in a second queue for now and process them once all images
    // were replaced
    let _totalNumberOfActions = this._actionQueue.length;
    const _addSourceQueue: IFileHandlerAddSourceAction[] = [];
    const _logPrefix = (currentNumber: number, totalNumber: number): string => {
      return `Executing instruction ${currentNumber} / ${totalNumber}: `;
    };
    let _counterCurrentAction = 0;
    while (this._actionQueue.length > 0) {
      _counterCurrentAction++;
      let _log = _logPrefix(_counterCurrentAction, _totalNumberOfActions);
      const _action = this._actionQueue.shift();
      switch (_action.action) {
        case FileHandlerActionEnum.REPLACE_IMAGE_BY_URL:
          const _actReplace = _action as IFileHandlerReplaceImageAction;
          const _dataUrlReplacingImage: DataUrl = await DataUrl.fromImageUrl(_actReplace.replacingBy.source);
          (await this._receipt).addMediumReplacedAction({
            doNotUpload: _actReplace.doNotUpload,
            wasReplaced: {
              internalPath: _actReplace.toReplace.source,
              checksum: Crypto.md5(_actReplace.toReplace.base64.toString())
            },
            isReplacing: {
              width: _actReplace.replacingBy.dimension.width,
              height: _actReplace.replacingBy.dimension.height,
              // TODO: think about setting the MIME type
              checksum: Crypto.md5(_dataUrlReplacingImage.toString()),
              dataUrlBase64: _dataUrlReplacingImage,
              // TODO: the source API targets something like 'cc', it wants to know which of the provided APIs by the
              //  tool is the origin. Fix it.
              sourceApi: this._deriveHostNameFromUrl(_actReplace.replacingBy.source),
              sourceUrl: _actReplace.replacingBy.source,
              license: {
                name: LicensesHumanReadable[_actReplace.replacingBy.license],
                version: _actReplace.replacingBy.licenseVersion,
                deedsUrl: _actReplace.replacingBy.licenseUrl
              }
            }
          });
          const _imageReplacedBy: IImageWrapper = (_action as IFileHandlerReplaceImageAction).replacingBy;
          _addSourceQueue.push({
            action: FileHandlerActionEnum.ADD_SOURCE_CREDITS,
            mediaType: 'image',
            credits: {
              licenseHumanReadable: LicensesHumanReadable[_imageReplacedBy.license],
              author: _imageReplacedBy.author,
              description: _imageReplacedBy.description,
              originUrl: _imageReplacedBy.source,
              licenseVersion: _imageReplacedBy.licenseVersion,
              // todo get author url
              authorUrl: undefined,
              title: _actReplace.replacingBy.title
            },
            filePath: (_action as IFileHandlerReplaceImageAction).toReplace.source
          });
          // increment the total number of actions by 1 for each newly spawned ADD_SOURCE_CREDITS action
          _totalNumberOfActions++;
          _log += 'Replacing Image';

          /*_refToCurrentZip = this._addImageFromUrl(_refToCurrentZip, {
            nameAndFullPath: (_action as IFileHandlerReplaceImageAction).toReplace.source,
            urlToImage: (_action as IFileHandlerReplaceImageAction).replacingBy.source,
            dimensions: {
              width: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.width,
              height: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.height
            }
          });*/
          await this.addImageFromUrl({
            nameAndFullPath: (_action as IFileHandlerReplaceImageAction).toReplace.source,
            urlToImage: (_action as IFileHandlerReplaceImageAction).replacingBy.source,
            dimensions: {
              width: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.width,
              height: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.height
            }
          });

          if (logSubject) {
            logSubject.next({
              action: FileHandlerActionEnum.REPLACE_IMAGE_BY_URL,
              text: _log,
              current: _counterCurrentAction,
              total: _totalNumberOfActions});
          }
          break;
        // TODO: make sure it can expect an `edited` field (so far only edited ones use the AsBase64 functionality, but there is no
        //  safety-check
        case FileHandlerActionEnum.REPLACE_IMAGE_BY_BASE64:
          const _actReplaceByBase64 = _action as IFileHandlerReplaceImageAction;
          const _imageReplacedByBase64: IImageWrapper = (_action as IFileHandlerReplaceImageAction).replacingBy;
          // Replacing by image from machine uses the same procedure, but then it obviously does not have a real URL. Instead, use the
          // base64 field which resembles the original image before editing
          const _dataUrlReplacingImageOriginalBase64: DataUrl =
            _actReplaceByBase64.replacingBy.source ?
              await DataUrl.fromImageUrl(_actReplaceByBase64.replacingBy.source) : _actReplaceByBase64.replacingBy.base64;
          const _dataUrlReplacingImageEditedBase64: DataUrl = _actReplaceByBase64.replacingBy.edited.base64;
          (await this._receipt).addMediumReplacedAction({
            doNotUpload: _actReplaceByBase64.doNotUpload,
            wasReplaced: {
              internalPath: _actReplaceByBase64.toReplace.source,
              checksum: Crypto.md5(_actReplaceByBase64.toReplace.base64)
            },
            isReplacing: {
              width: _actReplaceByBase64.replacingBy.dimension.width,
              height: _actReplaceByBase64.replacingBy.dimension.height,
              // TODO: think about setting the MIME type
              checksum: Crypto.md5(_dataUrlReplacingImageOriginalBase64.toString()),
              dataUrlBase64: _dataUrlReplacingImageOriginalBase64,
              // TODO: the source API targets something like 'cc', it wants to know which of the provided APIs by the
              //  tool is the origin. Fix it.
              sourceApi: this._deriveHostNameFromUrl(_actReplaceByBase64.replacingBy.source),
              sourceUrl: _actReplaceByBase64.replacingBy.source,
              authorUserName: _actReplaceByBase64.replacingBy.edited.editor,
              license: {
                name: LicensesHumanReadable[_actReplaceByBase64.replacingBy.license],
                version: _actReplaceByBase64.replacingBy.licenseVersion,
                deedsUrl: _actReplaceByBase64.replacingBy.licenseUrl
              },
              edited: {
                width: _actReplaceByBase64.replacingBy.edited.dimension.width,
                height: _actReplaceByBase64.replacingBy.edited.dimension.height,
                checksum: Crypto.md5(_dataUrlReplacingImageEditedBase64.toString()),
                dataUrlBase64: _dataUrlReplacingImageEditedBase64 as DataUrl
              }
            }
          });
          _addSourceQueue.push({
            action: FileHandlerActionEnum.ADD_SOURCE_CREDITS,
            mediaType: 'image',
            credits: {
              licenseHumanReadable: LicensesHumanReadable[_imageReplacedByBase64.license],
              author: _imageReplacedByBase64.author,
              description: _imageReplacedByBase64.description,
              originUrl: _imageReplacedByBase64.source,
              licenseVersion: _imageReplacedByBase64.licenseVersion,
              // todo get author url
              authorUrl: undefined,
              title: _actReplaceByBase64.replacingBy.title
            },
            filePath: _actReplaceByBase64.toReplace.source,
            // if info about edit is available, add it to the source info, otherwise set it to `undefined`
            edited: _actReplaceByBase64.replacingBy.edited ? {
              editor: _actReplaceByBase64.replacingBy.edited.editor,
              actions: _actReplaceByBase64.replacingBy.edited.action
            } : undefined
          });
          // increment the total number of actions by 1 for each newly spawned ADD_SOURCE_CREDITS action
          _totalNumberOfActions++;
          _log += 'Replacing Image';
          /*_refToCurrentZip = this._addImageFromUrl(_refToCurrentZip, {
            nameAndFullPath: (_action as IFileHandlerReplaceImageAction).toReplace.source,
            urlToImage: (_action as IFileHandlerReplaceImageAction).replacingBy.source,
            dimensions: {
              width: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.width,
              height: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.height
            }
          });*/
          await this.addBase64Image({
            nameAndFullPath: (_action as IFileHandlerReplaceImageAction).toReplace.source,
            imageAsBase64: (_action as IFileHandlerReplaceImageAction).replacingBy.edited.base64.getData()
          });
          if (logSubject) {
            logSubject.next({
              action: FileHandlerActionEnum.REPLACE_IMAGE_BY_BASE64,
              text: _log,
              current: _counterCurrentAction,
              total: _totalNumberOfActions});
          }
          break;
        case FileHandlerActionEnum.ADD_SOURCE_CREDITS:
          _addSourceQueue.push((_action as IFileHandlerAddSourceAction));
          break;
        case FileHandlerActionEnum.PUBLISH_AS_OER:
          const _actOer: IFileHandlerPublishAsOerAction = _action as IFileHandlerPublishAsOerAction;
          const _madeOerDataUrl: DataUrl = DataUrl.fromString(_actOer.image.base64.toString());
          (await this._receipt).addImagePublishedAsOerAction({
            doNotUpload: _actOer.doNotUpload,
            internalPath: _actOer.image.source,
            title: _actOer.title,
            checksum: Crypto.md5(_madeOerDataUrl.toString()),
            width: _actOer.image.dimension.width,
            height: _actOer.image.dimension.height,
            author: {
              name: _actOer.author,
              url: _actOer.authorUrl,
              affiliation: _actOer.affiliation
            },
            license: {
              name: LicensesHumanReadable[_actOer.license.enum],
              version: _actOer.license.version,
              deedsUrl: LicensesBaseHref[_actOer.license.enum] + '/' + _actOer.license.version
            },
            dataUrlBase64: _madeOerDataUrl
          });
          _addSourceQueue.push({
            filePath: _actOer.image.source,
            mediaType: 'image',
            credits: {
              licenseHumanReadable: LicensesHumanReadable[_actOer.license.enum],
              licenseVersion: _actOer.license.version,
              author: _actOer.author,
              description: _actOer.description,
              originUrl: _actOer.originUrl,
              originName: _actOer.originName,
              authorUrl: _actOer.authorUrl,
              title: _actOer.title
            },
            role: _actOer.role,
            screenshotData: !_actOer.screenshotData ? undefined : {
              date: _actOer.screenshotData.date,
              description: _actOer.screenshotData.description
            }
          } as IFileHandlerAddSourceAction);
          break;
        default:
          if (logSubject) {
            logSubject.next({action: FileHandlerActionEnum.ABORTING_ON_ERROR, text: 'An error occured. See console or cry for help.'});
          }
          console.log(`Tried to execute unknown action in queue: ${_action}`);
          break;
      }
    }

    let _counterRef = 1;
    const _sources: ISource[] = [];
    const _references: IReference[] = [];
    for (const source of _addSourceQueue) {
      const _refAbbr = '' + _counterRef++;
      _sources.push({
        refAbbreviation: _refAbbr,
        hrefLicense: LicensesBaseHref[source.credits.licenseHumanReadable],
        licenseHumanReadable: source.credits.licenseHumanReadable,
        licenseVersion: source.credits.licenseVersion,
        author: source.credits.author,
        description: source.credits.description,
        originUrl: source.credits.originUrl,
        // if an originName was passed, use it. Otherwise derive one from the hostname
        originName: source.credits.originName ? source.credits.originName : this._deriveHostNameFromUrl(source.credits.originUrl),
        authorUrl: source.credits.authorUrl,
        title: source.credits.title,
        // if no info about an edit was passed, source.edited will be undefined get propagated correctly
        edited: source.edited,
        role: source.role,
        screenshotData: !source.screenshotData ? undefined : {
          date: source.screenshotData.date,
          description: source.screenshotData.description
        }
      } as ISource);
      _references.push({file: source.filePath, refAbbreviation: _refAbbr});
    }
    // _refToCurrentZip = OpenXmlPresentationJs.addBibRefsToImages(await _refToCurrentZip, _references);
    await this.addBibRefsToImages(_references);


    _counterCurrentAction++;
    if (logSubject) {
      logSubject.next({
        action: FileHandlerActionEnum.ADD_SOURCE_CREDITS,
        text: _logPrefix(_counterCurrentAction, _totalNumberOfActions) + 'Appending source',
        current: _counterCurrentAction,
        total: _totalNumberOfActions
      });
    }
    // _refToCurrentZip = await OpenXmlPresentationJs.addSourceSlides(await _refToCurrentZip, _sources);
    await this.addSources(_sources);
  }

  /**
   * If a GUID is present, return it. If no GUID can be found, generate a new one and save it to the file.
   */
  protected async getGuid(): Promise<UUID> {
    const _readGuidString = await this.ooxmlFile.getCustomData(MainConfig.variableNameGuidData);
    let guid: UUID;
    if (_readGuidString == null || !UUID.isUUID(_readGuidString)) {
      const _newGuid: UUID = new UUID(UUIDv4());
      await this.ooxmlFile.addCustomDataAsString(MainConfig.variableNameGuidData, _newGuid.toString());
      guid = _newGuid;
    } else {
      guid = (new UUID(_readGuidString));
    }
    return guid;
  }

  /**
   * This will save an image encoded in Base64 into the zip file. If name/filePath already exists, it will be overwritten!
   * @param image Object holding the name as full filePath + image name and the image encoded in Base64
   */
  public async addBase64Image(image: AddImageAsBase64Parameters): Promise<void> {
    // return this._addBase64Image(undefined, image);
    return this.ooxmlFile.addBase64Image(image);
  }

  /**
   * Add an image by its url to the file
   * @param image Image that is to be added
   */
  public async addImageFromUrl(image: IAddImageFromUrlParameters): Promise<void> {
    return this.ooxmlFile.addImageFromUrl(image);
  }

  /**
   * Get the name of the author of the file
   */
  public async getFileAuthorName(): Promise<string> {
    return this.ooxmlFile.getAuthorName();
  }

  /**
   * Easy accessor for the config file
   */
  protected abstract readConfig(): kindOfConfig;

  /**
   * A means to add some disclaimer slide/page
   * @param disclaimerText The text that is to be displayed as legal disclaimer
   * @param targetLicense The target license. Needed here to insert the correct icon for the license
   */
  public async abstract addDisclaimer(disclaimerText: string, targetLicense: License): Promise<void>;
}
