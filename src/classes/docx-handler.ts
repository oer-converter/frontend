import {FileHandlerAbstract, FileHandlerActionEnum, IFileHanderParameters, ISaveToFileProgress} from './file-handler.abstract';
import {OfficeOpenXmlPresentationJs} from '../assets/libs/office-open-xml-presentation-js/office-open-xml-presentation-js';
import {JsZipHelper} from './js-zip-helper';
import * as JSZip from 'jszip';
import {OfficeOpenXmlWordprocessingJs} from '../assets/libs/office-open-xml-wordprocessing-js/office-open-xml-wordprocessing-js';
import {EImageRole, IImageWrapper} from '../models/image-wrapper';
import {Subject} from 'rxjs';
import {Document as DocumentModel} from '../models/document.model';
import {MimeType} from '@hexxag0nal/mime-type';
import {OoxmlHandler} from './ooxml-handler';
import {IReference, ISource} from '../assets/libs/office-open-xml-presentation-js/interfaces';
import {ICreateParagraphParameters} from '../assets/libs/office-open-xml-js/interfaces';
import {IWordprocessingParagraphTextParameters} from '../assets/libs/office-open-xml-wordprocessing-js/interfaces';
import {
  IOfficeOpenXmlWordprocessingConfig,
  OfficeOpenXmlPresentationConfig,
  OfficeOpenXmlWordprocessingConfig
} from '../config/ooxml.config';
import {Licenses, LicensesBaseHref} from '../models/licenses';
import {licensingPageXml} from '../assets/libs/office-open-xml-wordprocessing-js/templates/licensing-page.xml';
import {xmlNamespaces} from '../assets/libs/office-open-xml-wordprocessing-js/namespaces';
import {ParagraphNode} from '../assets/libs/office-open-xml-js/types';
import {beschriftungStyleXml} from '../assets/libs/office-open-xml-wordprocessing-js/templates/beschriftung-style.xml';
import {License} from '../assets/libs/office-open-xml-js/license';

export class DocxHandler extends OoxmlHandler<DocxHandler, IOfficeOpenXmlWordprocessingConfig> {
  /**
   * An instance of the ooxml-presentation-js class
   */
  protected ooxmlDocument: OfficeOpenXmlWordprocessingJs;

  /**f
   * @param params Parameters for successfully instantiating this
   */
  constructor(params: IFileHanderParameters) {
    super(params);
    this._zip = JsZipHelper.getContent(this._file.file);
    this._zip.then(async zip => {
      this.ooxmlDocument = new OfficeOpenXmlWordprocessingJs(zip);
      this.ooxmlFile = this.ooxmlDocument;
      const mimeType = new MimeType('application/vnd.openxmlformats-officedocument.wordprocessingml.document');
      const guid = await this.getGuid();
      this._document = new DocumentModel(guid, mimeType);
      this._subjectSubhandlerReady.next();
    });
  }

  /**
   * Easy accessor for the config file
   */
  protected readConfig(): IOfficeOpenXmlWordprocessingConfig {
    return OfficeOpenXmlWordprocessingConfig;
  }

  /**
   * Save the current presentation to a file
   * @param legalDisclaimerText The text that shall be put onto the legal disclaimer slide
   * @param targetLicense The target license. Needed here to insert the correct icon for the license
   * @param logSubject A subject where progress can be announced
   */
  public async saveToFile(legalDisclaimerText: string, targetLicense: License, logSubject?: Subject<ISaveToFileProgress>): Promise<void> {
    await this._executeActionQueue(logSubject);
    await this.addDisclaimer(legalDisclaimerText, targetLicense);

    return JsZipHelper.zipToFile(await this.ooxmlDocument.getFile(), {fileName: this._file.file.name, progressSubject: logSubject}).then(result => {
      logSubject.next({action: FileHandlerActionEnum.READY_TO_DOWNLOAD, text: 'Done!'});
      return result;
    });
  }

  /**
   * See typedoc comment of fileHandlerAbstract
   */
  public async addSources(sources: ISource[]): Promise<void> {
    const createdSourceNodes: ParagraphNode[] = [];
    // add the sources to the content box
    for (const source of sources) {
      const params: ICreateParagraphParameters<IWordprocessingParagraphTextParameters> = {
        texts: [
          {
            text: `${OfficeOpenXmlWordprocessingConfig.prefixRefAbbr}${source.refAbbreviation}${OfficeOpenXmlWordprocessingConfig.postfixRefAbbr}: `
          }
        ]
      };
      switch (source.role) {
        case EImageRole.screenshot:
          const date = source.screenshotData.date;
          params.texts.push({
            text: `Screenshot of ${source.screenshotData.description}, taken on ${date.toLocaleDateString('en-US')}. `
          });
          break;
        default:
          params.texts.push({
            text: `Image `
          });
          break;
      }
      if (source.title) {
        params.texts.push({
          text: `"${source.title}" `
        });
      }
      if (source.author) {
        params.texts.push({
          text: 'by '
        });
        if (source.authorUrl) {
          params.texts.push({
            text: source.author + ' ',
            properties: {
              hyperlink: {
                url: source.authorUrl
              }
            }
          });
        } else {
          params.texts.push({text: source.author + ' '});
        }
      }
      // license is always given
      params.texts.push({
          text: `under the license `
        },
        {
          text: `${source.licenseHumanReadable} ${source.licenseVersion}`,
          properties: {
            hyperlink: {
              url: LicensesBaseHref[source.licenseHumanReadable] + source.licenseVersion
            }
          }
        });
      if (source.originName) {
        params.texts.push({
            text: ` via `,
          },
          {
            text: source.originName,
            properties: {
              hyperlink: {
                url: source.originUrl
              }
            }
          });
      } else if (source.originUrl) {
        params.texts.push({
            text: ` via `,
          },
          {
            text: source.originUrl,
            properties: {
              hyperlink: {
                url: source.originUrl
              }
            }
          });
      }
      // if image was edited, add info about it as seen on p. 117 of "Freie Unterrichtsmaterialien" by Jören Muuß-Merholz
      if (source.edited) {
        params.texts.push({
          text: `; Edited: ${source.edited.actions.join(', ')} by ${source.edited.editor}`
        });
      }
      // Add the created paragraph node to the array
      createdSourceNodes.push(await this.ooxmlDocument._createParagraph(params));
    }
    // now add all the sources to a new page
    await this.ooxmlDocument.addPage(createdSourceNodes);
  }

  /**
   * See typedoc comment of fileHandlerAbstract
   */
  public async addBibRefsToImages(references: IReference[]): Promise<void> {
    const documentXmlPath: string = 'word/document.xml';
    const documentXmlContent: string = await this.ooxmlDocument.getFile(documentXmlPath);
    const docDocumentXml: Document = await new DOMParser().parseFromString(documentXmlContent, 'application/xml');

    const rIdsAndAbbrevs: {rId: number, abbrev: string}[] = [];

    for (const reference of references) {
      // since we only want the name of the file without the path, we can just take the last element of the split array
      const tmp = reference.file.split('/');
      // We can just shift the array since the 'word/' directory is at the beginning
      // tmp.shift();
      const processedFileName = tmp.pop();

      rIdsAndAbbrevs.push({
        rId: await this.ooxmlDocument.getRIdOfImageByImageName(processedFileName),
        abbrev: reference.refAbbreviation
      });
    }

    const nodesABlip = Array.from(docDocumentXml.getElementsByTagName('a:blip'));
    for (const nodeABlip of nodesABlip) {
      const rEmbedOfCurrentABlip: number = Number(nodeABlip.getAttribute('r:embed').match(/\d+/));
      for (const rIdAndAbbrev of rIdsAndAbbrevs) {
        if (rEmbedOfCurrentABlip === rIdAndAbbrev.rId) {
          const paragraphLabelForImage: ParagraphNode = await this.ooxmlDocument._createParagraph({
            texts: [{
              // tslint:disable-next-line:max-line-length
              text: `${OfficeOpenXmlWordprocessingConfig.prefixRefAbbr}${rIdAndAbbrev.abbrev}${OfficeOpenXmlWordprocessingConfig.postfixRefAbbr}`
            }],
            style: 'Beschriftung'
          });

          // get the parent or grandparent or... well get the <w:p> that contains our <a:blip>
          let parentalNodeWP = nodeABlip.parentElement;
          while (parentalNodeWP.tagName !== 'w:p') {
            // If it's not the <w:p> yet, then try the next parent
            parentalNodeWP = parentalNodeWP.parentElement;
          }
          await this.ooxmlDocument.addParagraphs([paragraphLabelForImage], {
            referenceParagraph: parentalNodeWP,
            position: 'after',
            parsedDocument: docDocumentXml
          });
        }
      }
    }

    // Now add the style
    const styleNode = (await new DOMParser().parseFromString(beschriftungStyleXml, 'application/xml')).getElementsByTagName('w:style')[0];
    await this.ooxmlDocument.addStyle(styleNode);
  }

  /**
   * See abstract OoxmlHandler
   */
  public async addDisclaimer(disclaimerText: string, targetLicense: License): Promise<void> {
    return this.addLicensingPage(disclaimerText, targetLicense);
  }

  /**
   * Appends a page that tells the current license and disclaims some stuff that has been used in the file
   * @param disclaimerText The text that is to be displayed
   * @param targetLicense The target license. Needed here to insert the correct icon for the license
   */
  private async addLicensingPage(disclaimerText: string, targetLicense: License): Promise<void> {
    // tslint:disable-next-line:max-line-length
    const _ccLicenseImageAsBase64 = targetLicense.logo.getData();
    const newImageNumber: number = await this.ooxmlDocument.getNumberOfImages() + 1;
    await this.addBase64Image({
      nameAndFullPath: `word/media/image${newImageNumber}.png`,
      imageAsBase64: _ccLicenseImageAsBase64
    });
    const rIdOfCCLicenseImage: string = `rId${await this.ooxmlDocument.getRIdOfImageByImageName(`image${newImageNumber}.png`)}`;
    const docLicensingPage: Document = await new DOMParser().parseFromString(licensingPageXml, 'application/xml');
    // look for the <a:blip r:embed="__REPLACE__" ...> element to replace it with the proper rId
    for (const nodeABlip of Array.from(docLicensingPage.getElementsByTagName('a:blip'))) {
      // There should only be one, but for safety's sake check if the property r:embed is set to "__REPLACE__"
      if (nodeABlip.getAttribute('r:embed').toString() === '__REPLACE__') {
        nodeABlip.setAttributeNS(xmlNamespaces.officeDocument['2006'].relationships, 'r:embed', rIdOfCCLicenseImage);
        break;
      }
    }
    const paragraphOfLegalText: Node = await this.ooxmlDocument._createParagraph({texts: [{text: disclaimerText}]});
    const nodeLicensingPage: Node[] = (Array.from(docLicensingPage.getElementsByTagName('w:p'))as Node[])
      .concat(paragraphOfLegalText);
    await this.ooxmlDocument.addPage(nodeLicensingPage);
  }
}
