import * as JSZip from 'jszip';
import {AddImageAsBase64Parameters} from '../assets/libs/office-open-xml-js/interfaces';
import {JSZipGeneratorOptions, JSZipLoadOptions, JSZipObject} from 'jszip';
import {Subject} from 'rxjs';
import {FileHandlerActionEnum, ISaveToFileProgress} from './file-handler.abstract';
import {Utils} from '@hexxag0nal/utils';

export abstract class JsZipHelper {
  /**
   * Get the content of a zip file
   * @param file The zip file
   * @param jsZipOptions Some further options
   */
  public static getContent(file: File, jsZipOptions = null): Promise<JSZip> {
    return new JSZip().loadAsync(file, jsZipOptions);
  }

  /**
   * Saving the zip to a file, meaning that the user will be prompted a 'save file as'-window for the altered file
   * @param refToZip A reference to the JSZip object returned by getContent()
   * @param options An object holding the file name with the correct file ending that the 'save file as'-window will use as default.
   *  It can also hold a subject which can be used to disclose the progress of zipping the files
   */
  public static async zipToFile(refToZip: JSZip, options?: {fileName?: string, progressSubject?: Subject<ISaveToFileProgress>}): Promise<void> {
    // setting compression level to maximum
    const jsZipGeneratorOptions: JSZipGeneratorOptions = {
      type: 'blob',
      compression: 'DEFLATE',
      compressionOptions: {
        level: 9
      }
    };
    // this function will be called upon updates regarding the saving progression
    // can be used to tell the progress to a the user by pushing it onto an observable, for example
    const cbOnUpdate = (metadata: any): void => {
      if (options && options.progressSubject) {
        const progress: ISaveToFileProgress = {
          text: 'Zipping files: ' + metadata.percent.toFixed(2) + '%',
          action: FileHandlerActionEnum.ZIPPING_FILE,
          current: metadata.percent.toFixed(0),
          total: 100 // since the zipping is done when it reaches 100%
        };
        options.progressSubject.next(progress);
      }
      // this gives back the file that is currently zipped
      // if (metadata.currentFile) {
      //   console.log('current file = ' + metadata.currentFile);
      // }
    };
    return refToZip.generateAsync(jsZipGeneratorOptions, cbOnUpdate).then(content => {
      const defaultFileName = 'default';
      let fileName;
      if (options) {
        // if a file name was passed, use it, otherwise use the default one
        fileName = options.fileName ? options.fileName : defaultFileName;
      } else {
        fileName = defaultFileName;
      }
      return Utils.saveAs(content, fileName);
    });
  }

  /**
   * This will save an image encoded in Base64 into the zip file. If name/filePath already exists, it will be overwritten!
   * @param refToZip A reference to the JSZip object returned by getContent()
   * @param image Object holding the name as full filePath + image name and the image encoded in Base64
   */
  public static addBase64Image(refToZip: JSZip, image: AddImageAsBase64Parameters): JSZip {
    return refToZip.file(image.nameAndFullPath, image.imageAsBase64, {base64: true});
  }

  /**
   * Get a file from inside the zip
   * @param refToZip Reference to the JSZip object
   * @param path The path of the file
   */
  public static async getFile(refToZip: JSZip, path: string): Promise<JSZipObject> {
    return refToZip.file(path);
  }

  /**
   * The same as getFile(), but returning the content as string instead of the file itself
   * @param refToZip Reference to the JSZip object
   * @param path The path of the file
   */
  public static async getFileContent(refToZip: JSZip, path: string): Promise<string> {
    const _file = await refToZip.file(path);
    // check if a file was found
    if (_file) {
      return _file.async('text');
    } else {
      return null;
    }
  }

  /**
   * Create a file inside the zip
   * @param refToZip Reference to the JSZip object
   * @param path The path where the new file is to be created
   * @param content The content of the new file
   * @param options Some further options
   */
  public static async createFile(refToZip: JSZip, path: string, content: string, options?: IJsZipCreateFileOptions): Promise<JSZip> {
    const _asBase64 = options ? options.base64 : null;
    refToZip = await refToZip.file(path, content, {
      base64: _asBase64,
      createFolders: true
    });
    return refToZip;
  }
}

// TODO: re-implement it as object class to model default values accordingly #108
/**
 * Taken from https://stuk.github.io/jszip/documentation/api_jszip/file_data.html.
 */
export interface IJsZipCreateFileOptions {
  /**
   * From JsZip page:
   * Set to true if the data is base64 encoded. For example image data from a <canvas> element. Plain text and HTML do not need this option.
   * Default: false
   */
  base64?: boolean;
  /**
   * From JsZip page:
   * Set to true if the data should be treated as raw content, false if this is a text. If base64 is used, this defaults to true, if the
   * data is not a string, this will be set to true.
   * Default: false
   */
  binary?:	boolean;
  /**
   * From JsZip page:
   * The last modification date.
   * Default: The current date
   */
  date?: Date;
}
