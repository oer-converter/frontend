import * as JSZip from 'jszip';
import {EditingAction, EImageRole, IImageWrapper} from '../models/image-wrapper';
import {DomSanitizer} from '@angular/platform-browser';
import {Subject, Subscription} from 'rxjs';
import {IMakeOerParams} from '../app/make-oer/make-oer.component';
import {Licenses} from '../models/licenses';
import {History, Receipt} from '../models/receipt.model';
import {UUID} from '@hexxag0nal/uuid';
import {MimeType} from '@hexxag0nal/mime-type';
import {Crypto} from '@hexxag0nal/crypto';
import {Document} from '../models/document.model';
import {AddImageAsBase64Parameters} from '../assets/libs/office-open-xml-js/interfaces';
import {OfficeOpenXmlJs} from '../assets/libs/office-open-xml-js/office-open-xml-js';
import {MainConfig} from '../config/main.config';
import {v4 as UUIDv4} from 'uuid';
import {IReference, ISource} from '../assets/libs/office-open-xml-presentation-js/interfaces';
import {License} from '../assets/libs/office-open-xml-js/license';

/**
 * Make it available under a different name
 */
export type FileHandler = FileHandlerAbstract;

/**
 * Parameters for instantiating a file handler
 */
export interface IFileHanderParameters {
  /**
   * The file itself
   */
  file: FileWrapper;
  /**
   * The DomSanitizer to sanitize elements and nodes
   */
  domSanitizer: DomSanitizer;
  /**
   * A previous receipt
   */
  receipt?: Receipt;
}

/**
 * Parameters for showing progress on saving to file
 */
export interface ISaveToFileProgress {
  /**
   * Text for current action
   */
  text: string;
  /**
   * The type of the current action
   */
  action: FileHandlerActionEnum;
  /**
   * The number of the current action
   */
  current?: number;
  /**
   * The total number of all actions to be performed
   */
  total?: number;
}

/**
 * Possible actions
 */
export enum FileHandlerActionEnum {
  REPLACE_IMAGE_BY_URL = 'replace image by image from URL',
  REPLACE_IMAGE_BY_BASE64 = 'replace image by image from base64 encoded string',
  ADD_SOURCE_CREDITS = 'add credits/source to presentation',
  ZIPPING_FILE = 'zipping file',
  READY_TO_DOWNLOAD = 'ready to download',
  ABORTING_ON_ERROR = 'aborting due to error',
  PUBLISH_AS_OER = 'publish an object as (own) oer'
}

/**
 * Parameter for declaring an image as OER
 */
export interface IPublishAsOerActionParams {
  /**
   * The author's name
   */
  author?: string;
  /**
   * Url to the author's web presence
   */
  authorUrl?: string;
  /**
   * Title of the image
   */
  title?: string;
  /**
   * The license this image was published under
   */
  license: {
    /**
     * The corresponding enum
     */
    enum: Licenses;
    /**
     * The version of the license
     */
    version: string;
  };
  /**
   * The author's affiliation
   */
  affiliation?: string;
  /**
   * The image's description
   */
  description?: string;
  /**
   * An url where this image can be found
   */
  originUrl?: string;
  /**
   * The name of the service that hosts this image
   */
  originName?: string;
  /**
   * Whether or not it shall be announced to the backend
   */
  doNotUpload?: boolean;
  /**
   * The role of the image
   */
  role?: EImageRole;
  screenshotData?: {
    date: Date,
    description: string;
  }
}

/**
 * Base Interface. Only bears an Enum of the action to do
 */
export interface IFileHandlerAction {
  /**
   * Should be self-explanatory
   */
  action: FileHandlerActionEnum;
}

/**
 * This action is chosen if an image needs to be toReplace
 */
export interface IFileHandlerReplaceImageAction extends IFileHandlerAction {
  /**
   * The image from the file that is to be toReplace
   */
  toReplace: IImageWrapper;
  /**
   * the external image that will replace one from the file
   */
  replacingBy: IImageWrapper;
  /**
   * If you do not wish to upload that action to the database, e.g. because the chosen image for replacement actually came from a
   * db suggestion, then set this field to `true`
   */
  doNotUpload?: boolean;
}

/**
 * This action is chosen if an object, e.g. an image is original work by the user and it shall be made oer
 */
export interface IFileHandlerPublishAsOerAction extends IFileHandlerAction, IPublishAsOerActionParams {
  /**
   * The image that shall be oer
   */
  image: IImageWrapper;
  /**
   * If you do not wish to upload that action to the database, e.g. because the chosen parameter for making the image oer actually
   * came from a db suggestion, then set this field to `true`
   */
  doNotUpload?: boolean;
  /**
   * Role of the image if given
   */
  role?: EImageRole;
  screenshotData?: {
    date: Date;
    description: string;
  };
}

/**
 * This action is chosen if credits for a media file (image, clip, audio) are needed
 */
export interface IFileHandlerAddSourceAction extends IFileHandlerAction {
  /**
   * since only images are supported right now, the only available option is 'image'
   */
  mediaType: 'image';
  /**
   * An object containing the necessary info to give proper credit
   */
  credits: {
    /**
     * The License as humanly readable
     */
    licenseHumanReadable: string;
    /**
     * License version
     */
    licenseVersion: string;
    /**
     * The author of the media file
     */
    author?: string;
    /**
     * A link to the author's site if any
     */
    authorUrl?: string;
    /**
     * Description of the media file
     */
    description?: string;
    /**
     * The original location of the image
     */
    originUrl?: string;
    /**
     * The name of the hoster. If none provided and an originUrl was passed, then a name is derived from the root domain of the hostname
     */
    originName?: string;
    /**
     * The given title if any
     */
    title: string;
  };
  /**
   * filePath to the file from root
   */
  filePath: string;
  /**
   * Info about edit if the original image was edited
   */
  edited?: {
    /**
     * The name of the person that edited the image
     */
    editor: string;
    /**
     * The actions that were performed
     */
    actions: EditingAction[];
  };
  /**
   * Role of the image if given
   */
  role?: EImageRole;
  screenshotData?: {
    date: Date;
    description: string;
  };
}

/**
 * Parameters for adding an image from an url
 */
export interface IAddImageFromUrlParameters extends IAddImageParamtersAbstract {
  /**
   * The url to the desired image
   */
  urlToImage: string;
}

/**
 * Describes abstractly the needed params for replacing an image with another one
 */
interface IAddImageParamtersAbstract {
  /**
   * The name including the full path of the image within the pptx file
   */
  nameAndFullPath: string;
  /**
   * Info about the size of the image
   */
  dimensions: {
    /**
     * The image's width
     */
    width: number;
    /**
     * The image's height
     */
    height: number;
  };
}

/**
 * Abstract class which defines the functions and things a deriving class has to be able to manipulate
 */
export abstract class FileHandlerAbstract {
  /**
   * The file and some info
   */
  protected _file: FileWrapper;
  /**
   * The DOM sanitizer
   */
  protected _domSanitizer: DomSanitizer;
  /**
   * Current action queue
   */
  protected _actionQueue: IFileHandlerAction[] = []; // create empty array upon instantiation
  /**
   * Current receipt
   */
  protected _receipt: Receipt;
  /**
   * Current document's data
   */
  protected _documentGUID: Promise<UUID>;

  /**
   * Current document's data
   */
  protected _document: Document;

  protected _subjectSubhandlerReady: Subject<void> = new Subject<void>();
  protected _subscriptionSubhandlerReady: Subscription;

  /**
   * Helper that determines whether this class and the subhandler is ready
   */
  private readonly READY: Promise<void>;

  /**
   * @param params The parameter needed
   */
  protected constructor(params: IFileHanderParameters) {
    this._file = params.file;
    this._domSanitizer = params.domSanitizer;
    this.READY = new Promise<void>(async (resolve, reject) => {
      // waiting for the extending subhandler to be ready before finishing the abstract file handler
      this._subscriptionSubhandlerReady = this._subjectSubhandlerReady.subscribe(async () => {
        const history: History = params.receipt instanceof Receipt ? params.receipt.getHistory() : null;
        this._receipt = new Receipt(this._document, history);
        resolve();
      });
    });
  }

  /**
   * Return the wrapper, meaning the file and some info
   */
  public getRawFile(): FileWrapper {
    return this._file;
  }

  /**
   * Enqueue to add an image from url
   * @param replacedImage The image that shall be replaced by the other image
   * @param replacingImage The image that shall replace the other image
   * @param doNotUpload Whether or not it shall be announced to the backend
   */
  public EnqueueAddImageFromUrl(replacedImage: IImageWrapper, replacingImage: IImageWrapper, doNotUpload: boolean = false): void {
    this._actionQueue.push({
      action: FileHandlerActionEnum.REPLACE_IMAGE_BY_URL,
      toReplace: replacedImage,
      replacingBy: replacingImage,
      doNotUpload: doNotUpload
    } as IFileHandlerReplaceImageAction);
  }

  /**
   * Enqueue to add an image from a base64 encoded data url
   * @param replacedImage The image that shall be replaced by the other image
   * @param replacingImage The image that shall replace the other image
   * @param doNotUpload Whether or not it shall be announced to the backend
   */
  public EnqueueAddImageFromBase64(replacedImage: IImageWrapper, replacingImage: IImageWrapper, doNotUpload: boolean = false): void {
    this._actionQueue.push({
      action: FileHandlerActionEnum.REPLACE_IMAGE_BY_BASE64,
      toReplace: replacedImage,
      replacingBy: replacingImage,
      doNotUpload: doNotUpload
    } as IFileHandlerReplaceImageAction);
  }

  /**
   * Enqueue to declare an image from the file as OER
   * @param image The image that is declared OER
   * @param params Additional parameter
   */
  public EnqueuePublishAsOer(image: IImageWrapper, params: IMakeOerParams): void {
    this._actionQueue.push({
      action: FileHandlerActionEnum.PUBLISH_AS_OER,
      image: image,
      author: params!.author,
      title: params!.title,
      affiliation: params!.affiliation,
      license: params.license,
      authorUrl: params!.authorUrl,
      originUrl: params!.originUrl,
      originName: params!.originName,
      doNotUpload: params.doNotUpload,
      role: params.role,
      screenshotData: !params.screenshotData ? undefined : {
        date: params.screenshotData.date,
        description: params.screenshotData.description
      }
    } as IFileHandlerPublishAsOerAction);
  }

  /**
   * Derive a hostname from an url
   * @param url The url to derive the hostname from
   */
  protected _deriveHostNameFromUrl(url: string): string {
    if (url) {
      let _hostname = (new URL(url)).hostname;
      // split it at dots to cut domain parts as subdomains, tld etc.
      const _split = _hostname.split('.');
      // Take the second last element to get the root domain without tld
      // since subdomains are also part of the split we just made, we can't just take the first element
      const _l = _split.length;
      _hostname = _split[_l - 2];
      // convert the first character to uppercase
      // props to Steve Harrison and 52d6c6af @ https://stackoverflow.com/a/1026087/7618184
      _hostname = _hostname.charAt(0).toUpperCase() + _hostname.slice(1);
      return _hostname;
    } else {
      return null;
    }
  }

  /**
   * This is a pre-defined way to execute the actions in the queue, but , in example, PptxHandler got its own implementation of this
   * (Due to complexity)
   * @param logSubject This subject bears the info that will be displayed upon donwloading the edited file (e.g. 'Executing blah blah')
   */
  protected async _executeActionQueue(logSubject?: Subject<ISaveToFileProgress>): Promise<any> {
    console.log('default action queue exec');
    const _asyncActions: Promise<any>[] = [];
    console.log('length of action queue', this._actionQueue.length);
    while (this._actionQueue.length > 0) {
      let _action = this._actionQueue.shift();
      console.log('current action being executed:', _action);
      switch (_action.action) {
        case FileHandlerActionEnum.REPLACE_IMAGE_BY_URL:
          _action = _action as IFileHandlerReplaceImageAction;
          _asyncActions.push(this.addImageFromUrl({
            nameAndFullPath: (_action as IFileHandlerReplaceImageAction).toReplace.source,
            urlToImage: (_action as IFileHandlerReplaceImageAction).replacingBy.source,
            dimensions: {
              width: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.width,
              height: (_action as IFileHandlerReplaceImageAction).toReplace.dimension.height
            }
          }));
          break;
        // TODO: implement missing cases in future
        default:
          console.log(`Tried to execute unknown action in queue: ${_action}`);
          break;
      }
    }
    return Promise.all(_asyncActions);
  }

  /**
   * Get contained images
   */
  public abstract async getImages(): Promise<IImageWrapper[]>;

  /**
   * Save current state to file
   * @param legalDisclaimerText The legal disclaimer text that shall be put onto the proper slide
   * @param targetLicense The target license. Needed here to make sure the correct license icon is added
   * @param logSubject A subject to announce the progress to
   */
  public async abstract saveToFile(legalDisclaimerText: string, targetLicense: License, logSubject?: Subject<ISaveToFileProgress>): Promise<void>;

  /**
   * Add an image from base64
   * @param image The image to be added
   */
  public async abstract addBase64Image(image: AddImageAsBase64Parameters): Promise<any>;

  /**
   * Add an image from url
   * @param image The image to be added
   */
  public async abstract addImageFromUrl(image: IAddImageFromUrlParameters): Promise<any>;

  /**
   * Get the receipt in CSV format
   */
  public async getReceiptAsCsv(): Promise<string> {
    await this.READY;
    return this._receipt.toCsv();
  }

  /**
   * Get the current document's GUID
   */
  public async getDocumentGUID(): Promise<UUID> {
    await this.READY;
    return this._document.getGuid();
  }

  /**
   * Get the current receipt
   */
  public async getReceipt(): Promise<Receipt> {
    await this.READY;
    return this._receipt;
  }

  /**
   * Get the current file's author
   */
  public abstract getFileAuthorName(): Promise<string>;

  /**
   * Add sources to a document
   * @param sources The sources that are to be added
   */
  public abstract addSources(sources: ISource[]): Promise<void>;

  /**
   * Adds references to images that shows the corresponding source
   */
  public abstract addBibRefsToImages(references: IReference[]): Promise<void>;
}
