import {
  FileHandlerActionEnum,
  IFileHanderParameters,
  ISaveToFileProgress
} from './file-handler.abstract';
import {JsZipHelper} from './js-zip-helper';
import {JSZipObject} from 'jszip';
import {Subject} from 'rxjs';
import {MimeType} from '@hexxag0nal/mime-type';
import {
  OfficeOpenXmlPresentationJs
} from '../assets/libs/office-open-xml-presentation-js/office-open-xml-presentation-js';
import * as DocumentModel from '../models/document.model';
import {OoxmlHandler} from './ooxml-handler';
import {
  IMapSlideXmlRid,
  IOpenXmlPresentationAddSourceSlidesOptions, IPairRidReference,
  IPresentationParagraphTextParameters, IReference, IShapeDimensions,
  ISlide,
  ISource, ITupleXY
} from '../assets/libs/office-open-xml-presentation-js/interfaces';
import {xmlNamespaces} from '../assets/libs/office-open-xml-presentation-js/namespaces';
import {IOfficeOpenXmlPresentationConfig, OfficeOpenXmlPresentationConfig} from '../config/ooxml.config';
import {ICreateParagraphParameters} from '../assets/libs/office-open-xml-js/interfaces';
import {Licenses, LicensesBaseHref} from '../models/licenses';
import {isNull, isUndefined} from 'util';
import {License} from '../assets/libs/office-open-xml-js/license';
import {EImageRole} from '../models/image-wrapper';




export class PptxHandler extends OoxmlHandler<PptxHandler, IOfficeOpenXmlPresentationConfig> {

  /**
   * An instance of the ooxml-presentation-js class
   */
  protected ooxmlPresentation: OfficeOpenXmlPresentationJs;

  /**
   * @param params Parameters for successfully instantiating this
   */
  constructor(params: IFileHanderParameters) {
    super(params);
    this._zip = JsZipHelper.getContent(this._file.file);
    this._zip.then(async zip => {
      this.ooxmlPresentation = new OfficeOpenXmlPresentationJs(zip);
      this.ooxmlFile = this.ooxmlPresentation;
      const mimeType = new MimeType('application/vnd.openxmlformats-officedocument.presentationml.presentation');
      const guid = await this.getGuid();
      this._document = new DocumentModel.Document(guid, mimeType);
      this._subjectSubhandlerReady.next();
    });
  }

  /**
   * Easy accessor for the config file
   */
  protected readConfig(): IOfficeOpenXmlPresentationConfig {
    return OfficeOpenXmlPresentationConfig;
  }

  /**
   * Save the current presentation to a file
   * @param legalDisclaimerText The text that shall be put onto the legal disclaimer slide
   * @param targetLicense The target license. Needed here to insert the correct icon for the license
   * @param logSubject A subject where progress can be announced
   */
  public async saveToFile(legalDisclaimerText: string, targetLicense: License, logSubject?: Subject<ISaveToFileProgress>): Promise<void> {
    await this._executeActionQueue(logSubject);
    // await this.addLicensingSlide(legalDisclaimerText);
    await this.addDisclaimer(legalDisclaimerText, targetLicense);
    // tslint:disable-next-line:max-line-length
    return JsZipHelper.zipToFile(await this.ooxmlPresentation.getFile(), {fileName: this._file.file.name, progressSubject: logSubject}).then(result => {
      logSubject.next({action: FileHandlerActionEnum.READY_TO_DOWNLOAD, text: 'Done!'});
      return result;
    });
  }

  /**
   * Add a source slide
   * @param title The title of the slide
   * @param sources The sources that shall be put onto that slide
   */
  private async addSourceSlide(title: string, sources: ISource[]): Promise<void> {
    // creating ppt/slideLayouts/slideLayout#NUMBER#.xml
    const _slideLayoutXmlContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
      // tslint:disable-next-line:max-line-length
      `<p:sldLayout xmlns:a="${xmlNamespaces.drawingml.main}" xmlns:r="${xmlNamespaces.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main" type="obj" preserve="1"><p:cSld name="Titel und Inhalt"><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="2" name="Titel 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{07A39B65-9269-4EFE-862B-28239DA065F3}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="title"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:r><a:rPr lang="de-DE"/><a:t>Mastertitelformat bearbeiten</a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="3" name="Inhaltsplatzhalter 2"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{834AD2E2-43DB-44C3-A861-F81DF7C5A913}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph idx="1"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:pPr lvl="0"/><a:r><a:rPr lang="de-DE"/><a:t>Mastertextformat bearbeiten</a:t></a:r></a:p><a:p><a:pPr lvl="1"/><a:r><a:rPr lang="de-DE"/><a:t>Zweite Ebene</a:t></a:r></a:p><a:p><a:pPr lvl="2"/><a:r><a:rPr lang="de-DE"/><a:t>Dritte Ebene</a:t></a:r></a:p><a:p><a:pPr lvl="3"/><a:r><a:rPr lang="de-DE"/><a:t>Vierte Ebene</a:t></a:r></a:p><a:p><a:pPr lvl="4"/><a:r><a:rPr lang="de-DE"/><a:t>Fünfte Ebene</a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="4" name="Datumsplatzhalter 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{9D0D1EE0-497A-44F7-A1A9-CD89EEEA72BC}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="dt" sz="half" idx="10"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{5D8EFFE2-AA75-411E-B166-DA043D84DB8C}" type="datetimeFigureOut"><a:rPr lang="de-DE" smtClean="0"/><a:t>09.01.2020</a:t></a:fld><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Fußzeilenplatzhalter 4"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{C4F906FE-D013-4E7C-86F4-70426A07A45A}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ftr" sz="quarter" idx="11"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="6" name="Foliennummernplatzhalter 5"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{658B3698-3230-483C-94C7-B54187DC40E8}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="sldNum" sz="quarter" idx="12"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{9DE91646-6837-40F9-92BD-80A4980E4DD1}" type="slidenum"><a:rPr lang="de-DE" smtClean="0"/><a:t>‹Nr.›</a:t></a:fld><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="2237627844"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sldLayout>`;
    // const _docSlideLayoutXml: Document = await new DOMParser().parseFromString(_slideLayoutXmlContent, 'application/xml');



    // creating ppt/slides/slide#NUMBER#.xml
    const _templateSlideXmlContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
      // tslint:disable-next-line:max-line-length
      `<p:sld xmlns:a="${xmlNamespaces.drawingml.main}" xmlns:r="${xmlNamespaces.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="4" name="Titel 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{182C4918-090F-4E93-9E46-65C1AF21D9E6}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="title"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Inhaltsplatzhalter 4"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{6B155D67-C27B-488C-87C8-3811EA56C936}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph idx="1"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="de-DE"/></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="3284794006"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>`;
    const _docSlideXml: Document = await new DOMParser().parseFromString(_templateSlideXmlContent, 'application/xml');
    // [0] will be the title box, [1] the content box
    const _elemsSp = _docSlideXml.getElementsByTagName('p:sp');
    const _elemSpTitleBox = _elemsSp[0];
    const _elemSpContentBox = _elemsSp[1];
    const _elemTxBodyOfTitleBox = _elemSpTitleBox.getElementsByTagName('p:txBody')[0];
    const _elemTxBodyOfContentBox = _elemSpContentBox.getElementsByTagName('p:txBody')[0];
    // since we just created the slide#NUMMBER#.xml, the only <a:p> block in both boxes is the empty placeholder which has no text
    const _elemEmptyPOfTxBodyOfContentBox = _elemTxBodyOfContentBox.getElementsByTagName('a:p')[0];
    const _elemEmptyPOfTxBodyOfTitleBox = _elemTxBodyOfTitleBox.getElementsByTagName('a:p')[0];

    // remove the node that tells powerpoint that this box is empty
    _elemTxBodyOfContentBox.removeChild(_elemEmptyPOfTxBodyOfContentBox);
    _elemTxBodyOfTitleBox.removeChild(_elemEmptyPOfTxBodyOfTitleBox);

    // set the title
    _elemTxBodyOfTitleBox.appendChild(OfficeOpenXmlPresentationJs._createParagraph({texts: [{text: title}]}).paragraph);

    const _slideLayoutXmlRelsContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
      // tslint:disable-next-line:max-line-length
      '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster" Target="../slideMasters/slideMaster1.xml"/></Relationships>';
    // find the number/position of the slide layout with the title 'Title and Content'/'Titel und Inhalt'

    // we assume that slide master 1 holds all the default layouts offered by powerpoint
    // tslint:disable-next-line:max-line-length
    const _positionOfDesiredSlideLayout: number = await this.ooxmlPresentation.getPositionOfSlideLayoutByName(OfficeOpenXmlPresentationConfig.slideLayoutName, 1);
    const _slideXmlRelsContent = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
    // tslint:disable-next-line:max-line-length
    `<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout" Target="../slideLayouts/slideLayout${_positionOfDesiredSlideLayout}.xml"/></Relationships>`;
    let _docSlideXmlRels = await new DOMParser().parseFromString(_slideXmlRelsContent, 'application/xml');


    // add the sources to the content box
    for (const source of sources) {
      const params: ICreateParagraphParameters<IPresentationParagraphTextParameters> = {
        texts: [
          {
            // tslint:disable-next-line:max-line-length
            text: `${OfficeOpenXmlPresentationConfig.prefixRefAbbr}${source.refAbbreviation}${OfficeOpenXmlPresentationConfig.postfixRefAbbr}: `
          }
        ]
      };
      switch (source.role) {
        case EImageRole.screenshot:
          const date = source.screenshotData.date;
          params.texts.push({
            text: `Screenshot of ${source.screenshotData.description}, taken on ${date.toLocaleDateString('en-US')}. `
          });
          break;
        default:
          params.texts.push({
            text: `Image `
          });
          break;
      }
      if (source.title) {
        params.texts.push({
          text: `"${source.title}" `
        });
      }
      if (source.author) {
        params.texts.push({
          text: 'by '
        });
        if (source.authorUrl) {
          params.texts.push({
            text: source.author + ' ',
            properties: {
              hyperlink: {
                url: source.authorUrl,
                xmlRel: _docSlideXmlRels
              }
            }
          });
        } else {
          params.texts.push({text: source.author + ' '});
        }
      }
      // license is always given
      params.texts.push({
          text: `under the license `
        },
        {
          text: `${source.licenseHumanReadable} ${source.licenseVersion}`,
          properties: {
            hyperlink: {
              url: LicensesBaseHref[source.licenseHumanReadable] + source.licenseVersion,
              xmlRel: _docSlideXmlRels
            }
          }
        });
      if (source.originName) {
        params.texts.push({
            text: ` via `,
          },
          {
            text: source.originName,
            properties: {
              hyperlink: {
                url: source.originUrl,
                xmlRel: _docSlideXmlRels
              }
            }
          });
      } else if (source.originUrl) {
        params.texts.push({
            text: ` via `,
          },
          {
            text: source.originUrl,
            properties: {
              hyperlink: {
                url: source.originUrl,
                xmlRel: _docSlideXmlRels
              }
            }
          });
      }
      // if image was edited, add info about it as seen on p. 117 of "Freie Unterrichtsmaterialien" by Jören Muuß-Merholz
      if (source.edited) {
        params.texts.push({
          text: `; Edited: ${source.edited.actions.join(', ')} by ${source.edited.editor}`
        });
      }
      const _paragraphObject = OfficeOpenXmlPresentationJs._createParagraph(params);
      _elemTxBodyOfContentBox.appendChild(_paragraphObject.paragraph);
      _docSlideXmlRels = _paragraphObject.xmlRels;
    }

    // creating the slide object with the layout and the content of the slides, such that the object can be passed to the function
    // that appends a new slide
    const _xmlSerializer = new XMLSerializer();
    const slide: ISlide = {
      title: this.readConfig().titleOfSourceSlides,
      slide: {
        xmlString: _xmlSerializer.serializeToString(_docSlideXml),
        xmlRelsString: _xmlSerializer.serializeToString(_docSlideXmlRels)
      }
    };

    await this.ooxmlPresentation.appendSlide(slide);
  }

  /**
   * Checks if multiple slides are needed since they exceed the maximum number of sources per slide and then calls addSourceSlide
   * the proper number of times
   * @param sources The sources that shall be put onto slides
   * @param options Some options
   */
  public async addSourceSlides(sources: ISource[], options?: IOpenXmlPresentationAddSourceSlidesOptions): Promise<void> {
    let _numberOfSlidesNeeded: number;
    let _numberOfSourcesPerSlide: number;

    const _processedCreditStrings: (string[])[] = [];
    if (!options) {
      // if no options were provided, fill in default values set in the config file
      _numberOfSlidesNeeded = sources.length / OfficeOpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
      _numberOfSourcesPerSlide = OfficeOpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
    } else {
      // if options were provided
      if (
        options.maxNumberOfSourcesPerSlide === null ||
        isNull(options.maxNumberOfSourcesPerSlide) ||
        isUndefined(options.maxNumberOfSourcesPerSlide) ||
        options.maxNumberOfSourcesPerSlide === undefined
      ) {
        // if no max number option was provided, use the default value set in the config file
        _numberOfSlidesNeeded = sources.length / OfficeOpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
        _numberOfSourcesPerSlide = OfficeOpenXmlPresentationConfig.defaultMaxNumberOfSourcesPerSlide;
      } else {
        // but if a max number option was indeed provided, calc how many slides are needed in the end
        _numberOfSourcesPerSlide = options.maxNumberOfSourcesPerSlide;
        if (options.maxNumberOfSourcesPerSlide < 1) {
          // this happens if the number of allowed sources per slide is smaller than 1, because that implies that all sources shall be put
          // onto a single slide
          _numberOfSlidesNeeded = 1;
        } else {
          // otherwise calc how many slides are needed
          _numberOfSlidesNeeded = Math.ceil(sources.length / options.maxNumberOfSourcesPerSlide);
        }
      }
    }

    let _isFirstSlide = true;
    for (let i = 0; i < _numberOfSlidesNeeded; i++) {
      // the following call to splice will remove as many sources as are allowed on a single slide. If there are less sources than allowed,
      // then splice still works as expected and will not throw any error. If the number of sources per slide is smaller than 1 (meaning
      // that all sources shall be on a single slide), then set _numberOfSourcesPerSlide to the length of the sources array
      if (_numberOfSourcesPerSlide < 1) {
        _numberOfSourcesPerSlide = sources.length;
      }

      let title = '';

      // prefill the title with an empty string. If this is the first slide or the title is to be put on all the additional slides, then
      // this field will be changed to that, further below in the code
      /*const _slide: ISlideSource = {
        sources: sources.splice(0, _numberOfSourcesPerSlide)
      };*/
      const _sourcesToPass = sources.splice(0, _numberOfSourcesPerSlide);
      // check whether the title shall only be put onto the first of the slides
      if (!OfficeOpenXmlPresentationConfig.titleOnlyOnFirstSourceSlide) {
        // in this case the title ought to be on all the new slides
        title = OfficeOpenXmlPresentationConfig.titleOfSourceSlides;
      } else {
        // in this case the title shall only be on the first of the slides
        if (_isFirstSlide) {
          title = OfficeOpenXmlPresentationConfig.titleOfSourceSlides;
        }
        // once the first slide was processed, set it to false to indicate that the other slides are not the first one
        _isFirstSlide = false;
      }
      await this.addSourceSlide(title, _sourcesToPass);
    }
  }

  /**
   * This function adds those little texts around the images to show the ref to the source
   * @param references The references that shall be added
   */
  public async addBibRefsToImages(references: IReference[]): Promise<void> {
    const _slidesPath = 'ppt/slides/';
    const _relsPath = _slidesPath + '_rels/';
    const _relpathsAndRef: {relPath: string, ref: IReference}[] = [];
    // const _relPathsFromXml: string[] = [];
    // convert the references' full path to a path relative to the slide xml files, not the slide rel xml files!
    references.forEach((ref: IReference) => {
      const pathOfRefAsArray = ref.file.split('/');
      // results for example in 'image2' and 'media' respectively
      const _file = pathOfRefAsArray.pop();
      const _folder = pathOfRefAsArray.pop();
      // get the last to levels
      // _relPathsFromXml.push(`../${_folder}/${_file}`);
      _relpathsAndRef.push({
        relPath: `../${_folder}/${_file}`,
        ref: ref
      });
    });

    // will hold the promises, but already linked to the corresponding slideX
    const _mapSlideXmlRidPromises: {[key: string]: Promise<IPairRidReference[]>} = {};
    const _mapSlideXmlRid: IMapSlideXmlRid = {};

    // this will hold all the promises that are generated
    const _wait: Promise<string>[] = [];

    this.ooxmlPresentation.getZip().folder(_relsPath).forEach((relPath: string, file: JSZipObject) => {
      // since relPath looks like 'slideX.xml.rels', we want it to look like 'slideX' instead, which is the first element if we split the
      // relPath by the dots
      const _slideX = relPath.split('.').shift();
      const _f = new Promise<any>(async (resolve, reject) => {
        // init a string array that cna be written to
        const _mapsRidsReferences: IPairRidReference[] = new Array<IPairRidReference>();
        // read every slide rel and look for occurrences of the images affected by the references
        const _xml: string = await this.ooxmlPresentation.getZip().file(_relsPath + relPath).async('text');
        const _doc: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
        const _elemsRelationship = _doc.getElementsByTagName('Relationship');
        for (let i = 0; i < _elemsRelationship.length; i++) {
          // check the attribute 'Target' from each Relationship node for equivalence with any of the given reference file paths and
          // save the Id
          for (const relPathAndRefOfAffectedImage of _relpathsAndRef) {
            if (_elemsRelationship[i].getAttribute('Target') === relPathAndRefOfAffectedImage.relPath) {
              const _rId = _elemsRelationship[i].getAttribute('Id');
              _mapsRidsReferences.push({
                rId: _rId,
                ref: relPathAndRefOfAffectedImage.ref
              });
            }
          }
        }
        resolve(_mapsRidsReferences);
      });
      // push it to the _wait queue such that we can wait for it to finish
      _wait.push(_f);
      // reference the file name with the result of _f, this object will be processed later such that it will contain the absolute
      // results instead of their promises
      _mapSlideXmlRidPromises[_slideX] = _f;
    });

    // wait for all the async function before we clean up the mess in __test
    await Promise.all(_wait);
    // since we waited for all the promises to finish, we should expect a result at every key
    for (const slide in _mapSlideXmlRidPromises) {
      // safety-check
      if (_mapSlideXmlRidPromises.hasOwnProperty(slide)) {
        // save the results instead of their promises
        _mapSlideXmlRid[slide] = await _mapSlideXmlRidPromises[slide];
      }
    }

    const _xmlSerializer = new XMLSerializer();

    // now that we've mapped the slides, rIds and refs, we need to add the text fields to the slides
    for (const slide in _mapSlideXmlRid) {
      // safety-check if property really exists. If it does, check if the array is not empty
      if (_mapSlideXmlRidPromises.hasOwnProperty(slide) && _mapSlideXmlRid[slide].length) {
        const _slideXmlFullPath = _slidesPath + slide + '.xml';
        const _xml: string = await this.ooxmlPresentation.getZip().file(_slideXmlFullPath).async('text');
        const _docSlide: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
        // shortcut to the current pairs
        const _currentPairsOfRidsAndRefs: IPairRidReference[] = _mapSlideXmlRid[slide];
        // shortcut to the spTree for easy appending of the text nodes which contain the ref abbr
        const _elemSpTree = _docSlide.getElementsByTagName('p:spTree')[0];
        // all <p:pic> nodes in the slide
        const _elemsPic = _elemSpTree.getElementsByTagName('p:pic');
        // all the rIds
        const _rIds: string[] = [];
        for (const map of _mapSlideXmlRid[slide]) {
          _rIds.push(map.rId);
        }
        for (let i = 0; i < _elemsPic.length; i++) {
          // shortcut
          const _elemPic = _elemsPic[i];
          // find the child <a:blip r:embed="rIdX"> (there is only one <a:blip> child)
          const _elemBlip = _elemPic.getElementsByTagName('a:blip')[0];
          // shortcut, basically holds rIdX as string
          const _attrEmbed: string = _elemBlip.getAttribute('r:embed');
          if (_rIds.includes(_attrEmbed)) {
            // if the emebed/rId of this <pic> element matches, then get its position data
            const _elemOff = _elemPic.getElementsByTagName('a:off')[0];
            // There seems to be only one <a:off>, but multiple <a:ext> in this <pic>. The needed <a:ext> is always the sibling of the only
            // <a:off>. Ergo, use it as shortcut.
            const _elemExt = _elemOff.parentElement.getElementsByTagName('a:ext')[0];
            // save the dimension data
            const _picDimensions: IShapeDimensions = {
              offset: {
                x: Number(_elemOff.getAttribute('x')),
                y: Number(_elemOff.getAttribute('y')),
              },
              ext: {
                x: Number(_elemExt.getAttribute('cx')),
                y: Number(_elemExt.getAttribute('cy'))
              }
            };
            // calculate offset of text field
            const _offsetReferenceAbbreviation: ITupleXY = {
              x: _picDimensions.offset.x + _picDimensions.ext.x,
              y: _picDimensions.offset.y + _picDimensions.ext.y
            };


            // just another shortcut for better readability
            const _currentRid: string = _attrEmbed;
            // preset it with -1, thus it can indicate a malfunction if it still appears in the saved presentation
            let _refNumber = '-1';
            for (const _pairOfRidsAndRefs of _currentPairsOfRidsAndRefs) {
              if (_pairOfRidsAndRefs && _pairOfRidsAndRefs.rId === _currentRid) {
                _refNumber = _pairOfRidsAndRefs.ref.refAbbreviation;
              }
            }

            // template for a text field which will later hold the ref abbr
            // tslint:disable-next-line:max-line-length
            const _templateTextField = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' + `<p:sp xmlns:p="${xmlNamespaces.presentationml.main}" xmlns:r="${xmlNamespaces.officeDocument.relationships}" xmlns:a="${xmlNamespaces.drawingml.main}"><p:nvSpPr><p:cNvPr id="2" name="Textfeld 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{08035DC6-F5EC-4E63-B802-D1666C78ECDE}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="8034500" y="5423946"/><a:ext cx="442750" cy="369332"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="none" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:r><a:rPr lang="de-DE" dirty="0"/><a:t>${this.readConfig().prefixRefAbbr}${_refNumber}${this.readConfig().postfixRefAbbr}</a:t></a:r></a:p></p:txBody></p:sp>`;
            const _docTextField: Document = await new DOMParser().parseFromString(_templateTextField, 'application/xml');
            const _deltas: ITupleXY = {
              x: 7591759,
              y: 5054614
            };
            const _elemOffOfNewTextField = _docTextField.getElementsByTagName('a:off')[0];
            // this ref is not yet used but keep it for dev purposes maybe?
            // const _elemExtOfNewTextfield = _docTextField.getElementsByTagName('a:ext')[0];
            _elemOffOfNewTextField.setAttribute('x', '' + _offsetReferenceAbbreviation.x);
            _elemOffOfNewTextField.setAttribute('y', '' + _offsetReferenceAbbreviation.y);

            // do not append it to the top spTree, but instead make it a sibling of the <pic> which contains the image that was replaced.
            // This way it is made sure that the positional values will have the same relative anchor point as the <pic> in question has
            _elemPic.parentElement.appendChild(_docTextField.childNodes[0]);
          }
        }
        // write every slideX.xml and chain them together
        // this.ooxmlPresentation.getZip() = this.zipFile.file(_slideXmlFullPath, _xmlSerializer.serializeToString(_docSlide));
        // tslint:disable-next-line:max-line-length
        this.ooxmlPresentation.updateZip(this.ooxmlPresentation.getZip().file(_slideXmlFullPath, _xmlSerializer.serializeToString(_docSlide)));
      }
    }
  }

  /**
   * Creates a claimer/disclaimer slide at the very end
   * @param legalText The text that is to be displayed on the disclaimer slide
   * @param targetLicense The target license. Needed here to insert the correct icon for the license
   */
  public async addLicensingSlide(legalText: string, targetLicense: License): Promise<void> {
    const paragraphOfLegalText: Node = OfficeOpenXmlPresentationJs._createParagraph({texts: [{text: legalText}]}).paragraph;
    const _logoFileEnding: string = 'png';
    const _slideTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
      `<p:sld xmlns:a="${xmlNamespaces.drawingml.main}" xmlns:r="${xmlNamespaces.officeDocument.relationships}" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="9" name="Textfeld 8"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{CCE18A2D-B512-4BBE-9251-22F0483AE87A}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="4762500" y="3099435"/><a:ext cx="5847370" cy="1477328"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="none" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/></p:txBody></p:sp><p:pic><p:nvPicPr><p:cNvPr id="3" name="Grafik 2" descr="Badge showing the CC license"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{5781B322-F30A-4896-B4D7-258C6631DD77}"/></a:ext></a:extLst></p:cNvPr><p:cNvPicPr><a:picLocks noChangeAspect="1"/></p:cNvPicPr><p:nvPr/></p:nvPicPr><p:blipFill><a:blip r:embed="rId2"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></p:blipFill><p:spPr><a:xfrm><a:off x="695462" y="3205880"/><a:ext cx="3589371" cy="1264437"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr></p:pic></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="3243645703"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>`;

    const _slideTemplateDoc = new DOMParser().parseFromString(_slideTemplate, 'application/xml');
    const _elemTxBodyInTemplate = _slideTemplateDoc.getElementsByTagNameNS(xmlNamespaces.presentationml.main, 'txBody')[0];
    _elemTxBodyInTemplate.appendChild(paragraphOfLegalText);

    // tslint:disable-next-line:max-line-length
    const _ccLicenseImageAsBase64 = targetLicense.logo.getData();
    // data:image/png;base64,

    const _currentNumberOfImages = this.ooxmlPresentation.getNumberOfImages();
    // The image of the cc license that'll be written is a png file
    // tslint:disable-next-line:max-line-length
    // this.zipFile = JsZipHelper.addBase64Image(this.zipFile, {nameAndFullPath: `ppt/media/image${_currentNumberOfImages + 1}.${_logoFileEnding}`, imageAsBase64: _ccLicenseImageAsBase64});
    await this.ooxmlPresentation.addBase64Image({
      nameAndFullPath: `ppt/media/image${await _currentNumberOfImages + 1}.${_logoFileEnding}`,
      imageAsBase64: _ccLicenseImageAsBase64
    });


    // we assume that slide master 1 holds all the default layouts offered by powerpoint
    // tslint:disable-next-line:max-line-length
    const _positionOfDesiredSlideLayout: number = await this.ooxmlPresentation.getPositionOfSlideLayoutByName(OfficeOpenXmlPresentationConfig.slideLayoutName, 1);
    const _slideRelsTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n` +
      // tslint:disable-next-line:max-line-length
      `<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="../media/image${await _currentNumberOfImages + 1}.png"/><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout" Target="../slideLayouts/slideLayout${_positionOfDesiredSlideLayout}.xml"/></Relationships>`;

    const _docLicenseSlideXml: Document = await new DOMParser().parseFromString(_slideTemplate, 'application/xml');
    const slide: ISlide = {
      slide: {
        xmlString: new XMLSerializer().serializeToString(_slideTemplateDoc),
        xmlRelsString: _slideRelsTemplate
      }
      // xmlLayoutString: _slideLayoutTemplate,
    };

    await this.ooxmlPresentation.appendSlide(slide, {
      newLastModifiedBy: this.readConfig().newLastModifiedBy,
      changeInfoTimeModified: this.readConfig().changeInfoTimeModified,
      incrementRevision: this.readConfig().incrementRevision
    });
  }

  /**
   * Alias such that this rather generic name maps to the rather specific name for this type of document
   * @param sources The sources that are to be put onto the slide
   */
  public async addSources(sources: ISource[]): Promise<void> {
    return this.addSourceSlides(sources);
  }

  /**
   * See abstract OoxmlHandler
   */
  public async addDisclaimer(disclaimerText: string, targetLicense: License): Promise<void> {
    return this.addLicensingSlide(disclaimerText, targetLicense);
  }
}
