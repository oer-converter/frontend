/**
 * Parameters for a shown toast
 */
interface ToastsConfigInterface {
  /**
   * duration in ms before automatically dismissing the toast/snackbar
   */
  duration: number;
}

/**
 * Defining the default configuration for a toast/snackbar
 */
export const ToastsConfig: ToastsConfigInterface = {
  duration: 2000
};
