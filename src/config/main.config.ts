/**
 * Here you can set options for the overall experience of the conversion tool
 */
export const MainConfig: {
  /**
   * please make sure that you fill in the KEY of the object, not the name of the api! Otherwise it'll break!
   * Example: 'cc' instead of 'CC image search'
   * You can find the keys in 'external-image-search-api.config.ts'
   */
  defaultTargetApi: string;
  /**
   * will be displayed as tab title
   */
  name: string;
  /**
   * if true, the version number will be appended to wherever the name is used
   */
  showVersion: boolean;
  /**
   * The supported types of media files, ought to be the fileextension, i.e. 'mp4', 'jpeg', 'png' etc.
   */
  supportedMediaFormats: {
    images?: string[];
    video?: string[];
  }
  /**
   * Sets how many fraction digits of the image's ratio are shown to the user and shall be compared when checking if the ratios mismatch
   * heavily. This does not affect internal calculations based on that ratio
   */
  imageRatioFractionDigits: number;
  /**
   * Define a tolerance t for the ratio of the images. Example t = .05, then ratios of 1.86 and 1.91 match, 1.86 and 1.92 don't
   */
  imageRatioTolerance: number;
  /**
   * The name of the variable inside the document to save the GUID data
   */
  variableNameGuidData: string;
  /**
   * Config for the backend of this tool
   */
  backend: {
    url: string;
    port: number;
  }
  /**
   * Options regarding the disclaimer slide at the end
   */
  disclaimerSlide: {
    /**
     * The default text for the disclaimer slide. There are some variable available:
     * __ORIGINAL_AUTHOR__ -> The name of the author of the chosen file
     * __TOOL_NAME__ -> The tool's name
     * __TOOL_VERSION__ -> The tool's version
     * __USER_NAME__ -> The name of the current user
     * __LICENSE_FULL_NAME__ -> The full name of the license the resulting file shall be published under, e.g. Attribution 4.0 International
     * __LICENSE_SHORT_NAME__ -> The full name of the license the resulting file shall be published under, e.g. CC BY 4.0
     * Occurences of the above strings will be replaced by the proper text
     */
    defaultText: string;
  }
  /**
   * You can set a custom name for this tool.
   */
  nameToolCustom?: string;
} = {
  defaultTargetApi: 'cc',
  name: 'OER Converter',
  showVersion: true,
  supportedMediaFormats: {
    images: ['jpeg', 'jpg', 'png', 'gif']
  },
  imageRatioFractionDigits: 2,
  imageRatioTolerance: .05,
  variableNameGuidData: 'oer_tool_guid_data',
  backend: {
    url: 'http://localhost',
    port: 4201
  },
  disclaimerSlide: {
    defaultText: 'This work was originally created by __ORIGINAL_AUTHOR__.\n' +
      'Updated by __USER_NAME__ via __TOOL_NAME__ V__TOOL_VERSION__.\n' +
      'It is licensed under __LICENSE_FULL_NAME__ (__LICENSE_SHORT_NAME__).\n'
  }
};
