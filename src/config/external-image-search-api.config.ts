/**
 * Holds info about the supported external search APIs
 */
export interface ExternalImageSearchApiConfig {
  /**
   * The key is a short-term for the service used, i.e. 'cc' for CC Image Search or 'flickr' for FLickr. Should not change!
   */
  [key: string]: {
    /**
     * The name of the API as shown in texts
     */
    name: string;
    /**
     * The API endpoint
     */
    url: string;
    /**
     * A public key if necessary
     */
    publicKey?: string
  };
}

/**
 * The actual config for external image search apis
 */
export const ExternalImageSearchApis: ExternalImageSearchApiConfig = {
  cc: {
    name: 'CC image search',
    url: 'https://api.creativecommons.engineering/v1/images'
  },
  flickr: {
    name: 'Flickr Public Image Search',
    url: 'https://www.flickr.com/services/rest/', // 'https://api.flickr.com/services',
    publicKey: 'bc74556d96120702fa64809ce374042c'
  }
};
