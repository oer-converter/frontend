/**
 * Interface for the corresponding config
 */
export interface SupportedImportFileType {
  /**
   * The extension of the file, e.g. 'pptx', 'txt', 'docx' ...
   */
  extension: string;
  /**
   * The string that identifies the type, e.g. 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
   */
  type: string;
}

/**
 * The actual config of supported types
 */
export const SupportedImportFileTypesConfig: SupportedImportFileType[] = [
  {
    extension: 'pptx',
    type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  },
  {
    extension: 'docx',
    type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
  },
  {
    extension: 'pdf',
    type: 'application/pdf'
  }
];

