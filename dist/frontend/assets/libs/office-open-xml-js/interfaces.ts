import {DataUrl} from '@hexxag0nal/data-url';
import {IPresentationParagraphTextParameters} from '../office-open-xml-presentation-js/interfaces';
import {ParagraphNode} from './types';

/**
 * Holds information about an image that is used in a ooxml file
 */
export interface IImageInFile {
  dataBase64: DataUrl;
  path: string;
  id: number;
}

export interface IGetImagesOptions {
  sorted?: 'id ascending' | 'id descending';
}

/**
 * Parameters for adding an image as base64 string
 */
export interface AddImageAsBase64Parameters {
  /**
   * The name including the full path of the image within the pptx file
   */
  nameAndFullPath: string;
  /**
   * The base64 string resembling the image
   */
  imageAsBase64: string;
}

/**
 * Defines which namespace and what identifier should be used
 */
export interface INamespaceSettings {
  /**
   * The identifier of the NS. E.g. for node <w:p>, w is the identifier
   */
  identifier: string;
  /**
   * The targeted URL
   */
  target: string;
}

/**
 * Describes the parameters needed when creating a new paragraph
 */
export interface ICreateParagraphParameters<T = string> {
  /**
   * The texts this paragraph shall contain
   */
  texts: T[];
  /**
   * A style that shall be associated with the paragraph. A check if it exists needs to be done manually!
   */
  style?: string;
}

/**
 * Option to set where a paragraph shall be inserted
 */
export interface IParagraphPosition {
  referenceParagraph: ParagraphNode;
  position: 'before' | 'after';
  /**
   * In order to have valid links between the nodes, we need to pass our own parsed version
   */
  parsedDocument: Document;
}

/**
 * Parameter to set to define the the element and the attribute that shall be searched for
 */
export interface IGetParagraphContainingElementWithRIdSeekedElement {
  /**
   * If we search e.g. for an element <pic:p>, then we must set it to 'pic:p'
   */
  elementIdentifier: string;
  /**
   * The attribute including its namespace identifier
   */
  attributeIdentifier: string;
  /**
   * If passed as string, e.g. 'rId5' is expected. If a number is passed, e.g. `5` is expected
   */
  rIdOfImage: string| number;
}
