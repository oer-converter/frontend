export type ParagraphNode = Node;

export type GetFileReadMode = 'text' | 'string' | 'arraybuffer' | 'uint8array';
