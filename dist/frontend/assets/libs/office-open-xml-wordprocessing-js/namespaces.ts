/**
 * This holds the namespaces that are used
 */
export const xmlNamespaces = {
  office: {
    word: {
      2006: {
        wordml: 'http://schemas.microsoft.com/office/word/2006/wordml'
      },
      2010: {
        wordprocessingCanvas: 'http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas',
        wordprocessingDrawing: 'http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing',
        wordprocessingGroup: 'http://schemas.microsoft.com/office/word/2010/wordprocessingGroup',
        wordprocessingInk: 'http://schemas.microsoft.com/office/word/2010/wordprocessingInk',
        wordprocessingShape: 'http://schemas.microsoft.com/office/word/2010/wordprocessingShape',
        wordml: 'http://schemas.microsoft.com/office/word/2010/wordml'
      },
      2012: {
        wordml: 'http://schemas.microsoft.com/office/word/2012/wordml'
      },
      2015: {
        wordml: {
          symex: 'http://schemas.microsoft.com/office/word/2015/wordml/symex'
        }
      },
      2016: {
        cid: 'http://schemas.microsoft.com/office/word/2016/wordml/cid'
      },
      2018: {
        wordml: {
          // tslint:disable-next-line:max-line-length
          _: 'http://schemas.microsoft.com/office/word/2018/wordml', // the name _ is a workaround since the url is both leaf and branch node
          cex: 'http://schemas.microsoft.com/office/word/2018/wordml/cex'
        }
      }
    },
    drawing: {
      2014: {
        chartex: 'http://schemas.microsoft.com/office/drawing/2014/chartex'
      },
      2015: {
        9: {
          8: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2015/9/8/chartex'
          }
        },
        10: {
          21: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2015/10/21/chartex'
          }
        }
      },
      2016: {
        5: {
          9: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2016/5/9/chartex'
          },
          10: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2016/5/10/chartex'
          },
          11: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2016/5/11/chartex'
          },
          12: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2016/5/12/chartex'
          },
          13: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2016/5/13/chartex'
          },
          14: {
            chartex: 'http://schemas.microsoft.com/office/drawing/2016/5/14/chartex'
          }
        },
        ink: 'http://schemas.microsoft.com/office/drawing/2016/ink'
      },
      2017: {
        model3D: 'http://schemas.microsoft.com/office/drawing/2017/model3d'
      }
    }
  },
  markupCompatibility: {
    2006: 'http://schemas.openxmlformats.org/markup-compatibility/2006'
  },
  urn: {
    office: {
      office: 'urn:schemas-microsoft-com:office:office',
      word: 'urn:schemas-microsoft-com:office:word'
    },
    vml: 'urn:schemas-microsoft-com:vml'
  },
  officeDocument: {
    2006: {
      relationships: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships',
      docPropsVTypes: 'http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes',
      customProperties: 'http://schemas.openxmlformats.org/officeDocument/2006/custom-properties',
      math: 'http://schemas.openxmlformats.org/officeDocument/2006/math'
    }
  },
  drawingml: {
    2006: {
      wordprocessingDrawing: 'http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing',
      main: 'http://schemas.openxmlformats.org/drawingml/2006/main'
    }
  },
  wordprocessingml: {
    2006: {
      main: 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'
    }
  },
  package: {
    2006: {
      relationships: 'http://schemas.openxmlformats.org/package/2006/relationships'
    }
  }
};
