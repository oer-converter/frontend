import {OfficeOpenXmlJs} from '../office-open-xml-js/office-open-xml-js';
import * as JSZip from 'jszip';
import {
  ICreateParagraphParameters,
  IGetParagraphContainingElementWithRIdSeekedElement,
  IImageInFile,
  IParagraphPosition
} from '../office-open-xml-js/interfaces';
import {xmlNamespaces} from './namespaces';
import {ParagraphNode} from '../office-open-xml-js/types';
import {IAddStyleOptions, IWordprocessingParagraphTextParameters} from './interfaces';
import {xmlTypes} from './xml-types';
import {StyleNode} from './types';
import {DefaultAddStyleOptions} from './defaults';

/**
 * This class exposes functions to manipulate or create OOXML parts suitable for PresentationML files
 */
export class OfficeOpenXmlWordprocessingJs extends OfficeOpenXmlJs {
  constructor(refToZip: JSZip) {
    super(refToZip);
  }

  public async getImages(): Promise<IImageInFile[]> {
    return this._getImages('word/media/');
  }

  /**
   * Appends a new, empty page
   */
  public async addEmptyPage(): Promise<void> {
    return this.addPage();
  }

  /**
   * Append a page with given content to the document
   * @param content An array of paragraphs that are to be put onto the new page
   */
  public async addPage(content: ParagraphNode[] = []): Promise<void> {
    // now create each needed node
    // shortcut to the namespace
    const _nsW: string = xmlNamespaces.wordprocessingml['2006'].main;
    const _nodeWP: Element = document.createElementNS(_nsW, 'w:p');
    const _nodeWR: Element = document.createElementNS(_nsW, 'w:r');
    const _nodeWBr: Element = document.createElementNS(_nsW, 'w:br');
    _nodeWBr.setAttributeNS(_nsW, 'w:type', 'page');
    // stack together the just created nodes
    _nodeWP.appendChild(_nodeWR);
    _nodeWR.appendChild(_nodeWBr);

    // concat the passed content to an array that holds a paragraph that resembles a new page
    await this.addParagraphs(([_nodeWP] as ParagraphNode[]).concat(content));
  }

  /**
   * Creates a paragraph and manipulates the word/_rels/document.xml.rels. Supports hyperlinks.
   * @param params The text pieces that shall be put together into a single paragraph
   */
  public async _createParagraph(params: ICreateParagraphParameters<IWordprocessingParagraphTextParameters>): Promise<Node> {
    const _nsW: string = xmlNamespaces.wordprocessingml['2006'].main;
    const _nsR: string = xmlNamespaces.officeDocument['2006'].relationships;
    const newNodeWP = document.createElementNS(_nsW, 'w:p');

    // If a style is set in the params, add the proper <w:pPr> containing a <w:pStyle> node
    if (params.style) {
      const nodeWPPr = document.createElementNS(_nsW, 'w:pPr');
      newNodeWP.appendChild(nodeWPPr);
      const nodeWPStyle = document.createElementNS(_nsW, 'w:pStyle');
      nodeWPPr.appendChild(nodeWPStyle);
      nodeWPStyle.setAttributeNS(_nsW, 'w:val', 'Beschriftung');
    }

    for (const textSection of params.texts) {
      // <w:r> > <w:t> are always needed, regardless of a hyperlink being present
      const newNodeWR = document.createElementNS(_nsW, 'w:r');
      const newNodeWT = document.createElementNS(_nsW, 'w:t');
      newNodeWT.setAttribute('xml:space', 'preserve');
      newNodeWT.innerHTML = textSection.text;

      // <w:hyperlink> and its child <w:rPr> with child <w:rStyle> are only needed if hyperlink was passed
      if (textSection.properties && textSection.properties.hyperlink) {
        // announce the hyperlink to the word/_rels/document.xml.rels file
        const _documentXmlRelsContent: string = await this.zipFile.file('word/_rels/document.xml.rels').async('text');
        const _docDocumentXmlRels: Document = await new DOMParser().parseFromString(_documentXmlRelsContent, 'application/xml');
        const _nodeRelationships: Element = _docDocumentXmlRels.getElementsByTagName('Relationships')[0];
        const _nodesRelationshipPrior: HTMLCollectionOf<Element> = _nodeRelationships.getElementsByTagName('Relationship');
        // search for the <Relationship> with the target "fontTable.xml" such that we can learn its rId because that will become
        let rIdFontTableXml: number; // only saves the number part
        // the rId of the hyperlink's <Relationship> node
        for (let i = 0; i < _nodesRelationshipPrior.length; i++) {
          if (_nodesRelationshipPrior.item(i).getAttribute('Target') === 'fontTable.xml') {
            rIdFontTableXml = Number(_nodesRelationshipPrior.item(i).getAttribute('Id').match(/\d+/g));
            // I thought that for the sake of performance, we will not exit the loop and loop through it again just to identify the rIds
            // that need to be incremented. Instead, we'll loop through to the end and on the way there we'll increment any rId necessary.
            // Upon reaching the end, we'll loop through it again but stop once we got the node with 'fontTable.xml' as target again.
            // But then I realized, that we will always loop the same many times through the collection of nodes as if we just stop right
            // here and loop again through it from the beginning. So for the sake of simplicity, I'll implement the latter way of achieving
            // this behavior
            break;
          }
        }

        // now loop through the <Relationship> nodes again and increment the rIds if necessary
        for (let i = 0; i < _nodesRelationshipPrior.length; i++) {
          if (Number(_nodesRelationshipPrior.item(i).getAttribute('Id').match(/\d+/g)) >= rIdFontTableXml) {
            // extract the current numerical part of the rId, increment it and then prepend the 'rId' prefix to that number
            const newRId: string = 'rId' + (Number(_nodesRelationshipPrior.item(i).getAttribute('Id').match(/\d+/g)) + 1);
            _nodesRelationshipPrior.item(i).setAttribute('Id', newRId);
          }
        }

        // since we incremented all necessary rIds, we can now safely created and append the hyperlink's <Relationship>
        const newNodeRelationship = document.createElementNS(xmlNamespaces.package['2006'].relationships, 'Relationship');
        newNodeRelationship.setAttributeNS('', 'Target', textSection.properties.hyperlink.url);
        newNodeRelationship.setAttributeNS('', 'Type', xmlTypes.officeDocument['2006'].relationships.hyperlink);
        newNodeRelationship.setAttributeNS('', 'Id', 'rId' + rIdFontTableXml);
        newNodeRelationship.setAttributeNS('', 'TargetMode', 'External');
        // now append it to the root <Relationships>
        _nodeRelationships.appendChild(newNodeRelationship);

        // now make sure the Hyperlink style is available in /word/styles.xml, first create the style's node
        const style: Element = ((): Element => {
          const nodeWStyle: Element = document.createElementNS(_nsW, 'w:style');
          nodeWStyle.setAttributeNS(_nsW, 'w:type', 'character');
          nodeWStyle.setAttributeNS(_nsW, 'w:styleId', 'Hyperlink');
          const nodeWName: Element = document.createElementNS(_nsW, 'w:name');
          nodeWName.setAttributeNS(_nsW, 'w:val', 'Hyperlink');
          nodeWStyle.appendChild(nodeWName);
          const nodeWBasedOn: Element = document.createElementNS(_nsW, 'w:basedOn');
          nodeWBasedOn.setAttributeNS(_nsW, 'w:val', 'Absatz-Standardschriftart');
          nodeWStyle.appendChild(nodeWBasedOn);
          const nodeWUIPriority: Element = document.createElementNS(_nsW, 'w:uiPriority');
          nodeWUIPriority.setAttributeNS(_nsW, 'w:val', '99');
          nodeWStyle.appendChild(nodeWUIPriority);
          const nodeWUnhideWhenUsed: Element = document.createElementNS(_nsW, 'w:unhideWhenUsed');
          nodeWStyle.appendChild(nodeWUnhideWhenUsed);
          const nodeWRsid: Element = document.createElementNS(_nsW, 'w:rsid');
          nodeWStyle.appendChild(nodeWRsid);
          const nodeWRPr: Element = document.createElementNS(_nsW, 'w:rPr');
          const nodeWColor: Element = document.createElementNS(_nsW, 'w:color');
          nodeWColor.setAttributeNS(_nsW, 'w:val', '0563C1');
          nodeWColor.setAttributeNS(_nsW, 'w:themeColor', 'hyperlink');
          nodeWRPr.appendChild(nodeWColor);
          const nodeWU: Element = document.createElementNS(_nsW, 'w:u');
          nodeWU.setAttributeNS(_nsW, 'w:val', 'single');
          nodeWRPr.appendChild(nodeWU);
          nodeWStyle.appendChild(nodeWRPr);
          return nodeWStyle;
        })();

        // now add the style
        await this.addStyle(style);

        // now write the changes back to the file
        this.zipFile = await this.zipFile.file('word/_rels/document.xml.rels', this.serializeXml(_docDocumentXmlRels));

        const newNodeWHyperlink: Element = document.createElementNS(_nsW, 'w:hyperlink');
        newNodeWHyperlink.setAttributeNS(_nsR, 'r:id', 'rId' + rIdFontTableXml); // TODO: find out next rId

        const newNodeWRPr =  document.createElementNS(_nsW, 'w:rPr');
        const newNodeWRStyle = document.createElementNS(_nsW, 'w:rStyle');
        newNodeWRStyle.setAttributeNS(_nsW, 'w:val', 'Hyperlink');

        // if hyperlink was passed, the <w:r> needs to be a child of <w:hyperlink>
        newNodeWP.appendChild(newNodeWHyperlink);
        newNodeWHyperlink.appendChild(newNodeWR);
        newNodeWR.append(newNodeWRPr, newNodeWT);
        newNodeWRPr.appendChild(newNodeWRStyle);
      } else {
        // if no hyperlink was passed, the structure is different
        newNodeWP.appendChild(newNodeWR);
        newNodeWR.appendChild(newNodeWT);
      }
    }

    return newNodeWP;
  }

  /**
   * Adds a given paragraph to the wordprocessing file
   */
  public async addParagraphs(paragraphs: ParagraphNode[], position?: IParagraphPosition): Promise<void> {
    const documentXmlPath: string = 'word/document.xml';
    let _docDocumentXml: Document;
    if (!position) {
      const _documentXmlContent: string = await this.zipFile.file(documentXmlPath).async('text');
      _docDocumentXml = await new DOMParser().parseFromString(_documentXmlContent, 'application/xml');
    } else {
      _docDocumentXml = position.parsedDocument;
    }
    // we want to insert the nodes that resemble the now page before the <w:sectPr> child of the <body> node
    const _nodeWBody: Element = _docDocumentXml
      .getElementsByTagName('w:document')[0]
      .getElementsByTagName('w:body')[0];
    const _nodeWSectPr: Element = _nodeWBody.getElementsByTagName('w:sectPr')[0];
    if (!position) {
      // If no positional argument is passed, insert it at the end of the (visible) document
      for (const paragraph of paragraphs) {
        _nodeWBody.insertBefore(paragraph, _nodeWSectPr);
      }
    } else {
      switch (position.position) {
        case 'before':
          // Since all of the passed paragraphs will keep the given order if each of them is inserted in front of the reference node, we
          // do not need to have a temporary ref node
          for (const paragraph of paragraphs) {
            _nodeWBody.insertBefore(paragraph, position.referenceParagraph);
          }
          break;
        case 'after':
          // In order to keep the given order of the passed paragraphs, we need to set the reference, after which the next paragraph shall
          // be inserted, to the just-inserted paragraph, otherwise we would end up with the exact opposite order
          let currentRef: Node = position.referenceParagraph;
          for (const paragraph of paragraphs) {
            _nodeWBody.insertBefore(paragraph, currentRef.nextSibling);
            currentRef = paragraph;
          }
          break;
      }
    }
    this.zipFile = await this.zipFile.file(documentXmlPath, this.serializeXml(_docDocumentXml));
  }

  /**
   * Checks if a style with given ID exists in /word/styles.xml
   * @param styleId The ID of the style that is being checked
   */
  public async doesStyleExist(styleId: string): Promise<boolean> {
    const stylesXmlPath: string = 'word/styles.xml';
    const stylesXmlContent: string = await this.zipFile.file(stylesXmlPath).async('text');
    const docStylesXml: Document = await new DOMParser().parseFromString(stylesXmlContent, 'application/xml');
    const nodeWStyle = Array.from(docStylesXml.getElementsByTagName('w:style'))
      .find((elem: Element) => elem.getAttribute('w:styleId').toString() === styleId);
    return !!nodeWStyle;
  }

  /**
   * Adds a given style to the /word/styles.xml file
   */
  public async addStyle(style: StyleNode, options?: IAddStyleOptions): Promise<void> {
    const stylesXmlPath: string = 'word/styles.xml';
    const stylesXmlContent: string = await this.zipFile.file(stylesXmlPath).async('text');
    let docStylesXml: Document = await new DOMParser().parseFromString(stylesXmlContent, 'application/xml');
    let target: Object = options;
    // set default options:
    if (!options) {
      target = {};
    }
    options = Object.assign(target, DefaultAddStyleOptions);
    const styleId: string = (style as Element).getAttribute('w:styleId');
    const doesAlreadyExist = await this.doesStyleExist(styleId);
    if (doesAlreadyExist) {
      if (options.ifAlreadyExists === 'replace') {
        // remove the style node thus that the new one can be added later
        docStylesXml = this._removeStyle(docStylesXml, styleId);
      } else {
        // if 'do not add' or something illegal was passed, then just end this function
        return;
      }
    }
    // Now add the new style, regardless if it was removed before, ignored or has never existed

    // There's only one <w:styles>, acting as root
    const nodeWStyles = docStylesXml.getElementsByTagName('w:styles')[0];
    nodeWStyles.appendChild(style);
    this.zipFile = this.zipFile.file(stylesXmlPath, this.serializeXml(docStylesXml));
  }

  /**
   * Remove a style with the given styleId and write it back to the /word/style.xml file
   */
  public async removeStyle(styleId: string): Promise<void> {
    const stylesXmlPath: string = 'word/styles.xml';
    const stylesXmlContent: string = await this.zipFile.file(stylesXmlPath).async('text');
    const docStylesXml: Document = await new DOMParser().parseFromString(stylesXmlContent, 'application/xml');
    const docWithStyleRemoved = this._removeStyle(docStylesXml, styleId);
    this.zipFile = this.zipFile.file(stylesXmlPath, this.serializeXml(docWithStyleRemoved));
  }

  /**
   * Basically the same as removeStyle(), but needs the document passed and does not write it directly back to the file
   * @param docStylesXml The parsed /word/styles.xml
   * @param styleId The ID of the style that is to be removed
   */
  private _removeStyle(docStylesXml: Document, styleId: string): Document {
    const nodeWStyle = Array.from(docStylesXml.getElementsByTagName('w:style'))
      // We expect every styleId to be unique
      .find((elem: Element) => elem.getAttribute('w:styleId').toString() === styleId);
    nodeWStyle.remove();
    return docStylesXml;
  }

  /**
   * Return the number in the rId, e.g. returns 5 if Id='rId5' for a given image's name. If it is not yet referenced with an rId (e.g.
   * beceause it is a completely new image that has just been added) the function will add the correct information into the .docx file
   * such that the image can be used
   * @param imageName The name of the image including the file extension, e.g. image2.png or image6.jpg
   */
  public async getRIdOfImageByImageName(imageName: string): Promise<number> {
    const documentXmlRelsPath: string = 'word/_rels/document.xml.rels';
    const documentXmlRelsContent: string = await this.zipFile.file(documentXmlRelsPath).async('text');
    const docDocumentXmlRels: Document = await new DOMParser().parseFromString(documentXmlRelsContent, 'application/xml');
    let nodeRelationshipOfDesiredImage = Array.from(docDocumentXmlRels.getElementsByTagName('Relationship'))
      .find((elem: Element) => elem.getAttribute('Target').toString() === `media/${imageName}`);
    // if it is undefined or null, then the node does not exist
    if (!nodeRelationshipOfDesiredImage) {
      // If no node was found, then the image might have been added just recently and has not been referenced yet. In this case, create the
      // reference

      // first, find the number part of the rId of fontTable.xml
      const nodeRelationshipFontTableXml = Array.from(docDocumentXmlRels.getElementsByTagName('Relationship'))
        .find((elem: Element) => elem.getAttribute('Target').toString() === 'fontTable.xml');
      const rIdFontTableXml: number = Number(nodeRelationshipFontTableXml.getAttribute('Id').match(/\d+/));
      // just for better understanding
      const rIdOfNewImage = rIdFontTableXml;
      // now go through all the <Relationship>s and increase the rId if applicable (all rIds equal or greater than the extracted rId
      // of the fontTable.xml needs to be incremented)
      for (const nodeRelationship of Array.from(docDocumentXmlRels.getElementsByTagName('Relationship'))) {
        const currentNodeRId = Number(nodeRelationship.getAttribute('Id').toString().match(/\d+/));
        if (currentNodeRId >= rIdFontTableXml) {
          nodeRelationship.setAttributeNS('', 'Id', 'rId' + (currentNodeRId + 1));
        }
      }
      // Once we've incremented all rIds when necessary, we can insert the <Relationship> that references our new image
      nodeRelationshipOfDesiredImage = docDocumentXmlRels.createElementNS(xmlNamespaces.package['2006'].relationships, 'Relationship');
      nodeRelationshipOfDesiredImage.setAttribute('Target', `media/${imageName}`);
      nodeRelationshipOfDesiredImage.setAttribute('Type', xmlTypes.officeDocument['2006'].relationships.image);
      nodeRelationshipOfDesiredImage.setAttribute('Id', 'rId' + rIdOfNewImage);
      const nodeRelationships = docDocumentXmlRels.getElementsByTagName('Relationships')[0];
      nodeRelationships.appendChild(nodeRelationshipOfDesiredImage);
      this.zipFile = this.zipFile.file(documentXmlRelsPath, this.serializeXml(docDocumentXmlRels));
      return rIdOfNewImage;
    } else {
      // If the <Relationship> exists, return the number part of its rId
      return Number(nodeRelationshipOfDesiredImage.getAttribute('Id').match(/\d+/));
    }
  }

  /**
   * Get the number of images that are in the /word/media directory
   */
  public async getNumberOfImages(): Promise<number> {
    return await super.getNumberOfImages('word/');
  }

  // /**
  //  * Returns the paragraph node containing an element with the given rId
  //  * @param elementsToSearchFor The elements and their attributes' name, as well as the corresponding rIds to search for
  //  */
  /*public async getParagraphContainingElementWithRId(elementsToSearchFor: IGetParagraphContainingElementWithRIdSeekedElement): Promise<ParagraphNode> {
    const documentXmlPath: string = 'word/document.xml';
    const documentXmlContent: string = await this.zipFile.file(documentXmlPath).async('text');
    const docDocumentXml: Document = await new DOMParser().parseFromString(documentXmlContent, 'application/xml');

    for (const oneSeekedElement of elementsToSearchFor) {
      console.log('rId', oneSeekedElement.rIdOfImage);
      // If it is a string, e.g. 'rId5' is expected. So we need to extract the number part
      if (typeof oneSeekedElement.rIdOfImage === 'string') {
        oneSeekedElement.rIdOfImage = Number(oneSeekedElement.rIdOfImage.match(/\d+/));
      }

      const nodesOfSeekedElement = Array.from(docDocumentXml.getElementsByTagName(oneSeekedElement.elementIdentifier));
      console.log(`found nodes for ${oneSeekedElement.elementIdentifier}`, nodesOfSeekedElement);
    }
  }*/
}
