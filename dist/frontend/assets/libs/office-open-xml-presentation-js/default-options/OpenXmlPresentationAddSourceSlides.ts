import {IOpenXmlPresentationAddSourceSlidesOptions} from '../interfaces';

export const OpenXmlPresentationAddSourceSlidesDefaultOptions: IOpenXmlPresentationAddSourceSlidesOptions = {
  maxNumberOfSourcesPerSlide: 0
};
