import {EditingAction, EImageRole} from '../../../models/image-wrapper';

/**
 * Describes properties of texts that shall be appended to a paragraph node
 */
export interface IPresentationParagraphTextParameters {
  /**
   * The text that will be written to a paragraph
   */
  text: string;
  /**
   * You can give the text some properties if wanted
   */
  properties?: {
    /**
     * If provided, the text will be made a hyperlink to the specified target
     */
    hyperlink?: {
      /**
       * The url of the target
       */
      url: string;
      /**
       * The xml.rels file of the corresponding slideX.xml as document
       */
      xmlRel: Document;
    }
  };
}

/**
 * Interface for easier mapping of affected images to their rIds and those ids' occurences in the slides
 */
export interface IMapSlideXmlRid {
  /**
   * If affected images are for example rId2 and rId3 in slide2.xml and rId7 in slide3.xml, then the object would look like
   * {slide2: ['rId2', 'rId3'], slide3: ['rId7']}
   */
  [key: string]: IPairRidReference[];
}

/**
 * Defines the possible options for adding new source slides
 */
export  interface IOpenXmlPresentationAddSourceSlidesOptions {
  /**
   * Any value smaller than 1 indicates infinitely many sources per slide (resulting in exactly one slide)
   * If the number of provided sources is greater than the max number, then the remaining sources will be put onto another slide
   */
  readonly maxNumberOfSourcesPerSlide?: number;
}

/**
 * Pairs a reference object as value to an rId as key
 */
export interface IPairRidReference {
  /**
   * a reference object, needed for knowing the reference abbreviation, among other things
   */
  ref: IReference;
  /**
   * The rId in a given document
   */
  rId: string;
}

/**
 * This Interface is used when adding those little text boxes with the ref to the corner of the media file
 */
export interface IReference {
  /**
   * Path to the file from root
   */
  file: string;
  /**
   * The reference string that is to be shown, i.e. Ref01
   */
  refAbbreviation: string;
}

/**
 * A pair of numbers that resembles cartesian coordinates
 */
export interface ITupleXY {
  x: number;
  y: number;
}

/**
 * Dimension options
 */
export interface IShapeDimensions {
  /**
   * Offset from relative anchor point
   */
  offset: ITupleXY;
  /**
   * Basically the width and the height of the shape
   */
  ext: ITupleXY;
}

/**
 * Defines the field for a slide object to have. Specialized slides inherit from this interface.
 * There are some keywords available for writing the template strings, that will be replaced by actual content. Available so far:
 * - __NEXT_SLIDE_LAYOUT_NUMBER__: When passing a new slide layout to be added, this can be used to reference the new number.
 *   For example: If there are eigth layouts so far, and a ninth is being added, then __NEXT_SLIDE_LAYOUT_NUMBER__ will resolve to '9'
 */
export interface ISlide {
  /**
   * The title for the slide, even it may not be displayed
   */
  title?: string;
  /**
   * Contains info on the slide content itself
   */
  slide: {
    /**
     * XML serialized string for the slide###.xml
     */
    xmlString: string;
    /**
     * XML serialized string for the slide###.xml.rels. Can i.e. be used to manipulate targets to media files or layouts
     */
    xmlRelsString: string;
  };
  /**
   * Contains info on the layout of the slide (if a new layout is added instead of a preexisting one being used)
   */
  layout?: {
    /**
     * XML serialized string for slideLayout###.xml. Can be used to add new layouts. Not mandatory for a new slide as it can simply use one of
     * those preexisting layouts that come with the OpenXML file
     */
    xmlLayoutString: string;
    /**
     * XML serialized string for slideLayout###.xml.rels
     */
    xmlLayoutRelsString: string;
  };
}

/**
 * Defines the properties a Source is to have
 */
export interface ISource {
  /**
   * The abbreviation that shall be used to link the source to a media file occurrence in the presentation, i.e. 'Abbr01' or simply '1'
   */
  refAbbreviation: string;
  /**
   * A link to the license used
   */
  hrefLicense: string;
  /**
   * The License as humanly readable
   */
  licenseHumanReadable: string;
  /**
   * License version
   */
  licenseVersion: string;
  /**
   * The author of the media file
   */
  author: string;
  /**
   * Description of the media file
   */
  description?: string;
  /**
   * The original location of the image
   */
  originUrl: string;
  /**
   * The name of the service that hosts the image.
   * If none is provided, it will be derived from the host's domain name
   */
  originName?: string;
  /**
   * Title of the work
   */
  title?: string;
  /**
   * A link to the author's blog, website or similar
   */
  authorUrl?: string;
  /**
   * Info about edit if the original image was edited
   */
  edited?: {
    /**
     * The name of the person that edited the image
     */
    editor: string;
    /**
     * The actions that were performed
     */
    actions: EditingAction[];
  };
  /**
   * The role, i.e. photo or screenshot
   */
  role?: EImageRole;
  screenshotData?: {
    date: Date;
    description: string;
  }
}

/**
 * Parameters for a source slide
 */
export interface ISlideSource extends ISlide {
  /**
   * The sources that shall be put onto that slide
   */
  sources: ISource[];
}

/**
 * Available options when appending a slide
 */
export interface IAppendSlideOptions {
  /**
   * Update the name of the person that has updated the file, if a value is given.
   * If none is given, it is assumed that it shall not be updated
   */
  newLastModifiedBy?: string;
  /**
   * Whether the time of the last update shall be updated
   */
  changeInfoTimeModified?: boolean;
  /**
   * Whether the revision number shall be updated
   */
  incrementRevision?: boolean;
}
