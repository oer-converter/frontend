import * as JSZip from 'jszip';
import {JSZipObject} from 'jszip';
import {xmlNamespaces} from './namespaces';
import {xmlSchemata} from './schemata';
import {ISlide, IPresentationParagraphTextParameters, IAppendSlideOptions
} from './interfaces';
import {MultiLanguageSlideLayoutTitle, OfficeOpenXmlJs} from '../office-open-xml-js/office-open-xml-js';
import {ICreateParagraphParameters, IImageInFile, INamespaceSettings} from '../office-open-xml-js/interfaces';

/**
 * This class exposes functions to manipulate or create OOXML parts suitable for PresentationML files
 */
export class OfficeOpenXmlPresentationJs extends OfficeOpenXmlJs {
  constructor(refToZip: JSZip) {
    super(refToZip);
  }

  /**
   * Appends a slide via the given templates
   * @param slide Object that contains info on the slide
   */
  public async appendSlide(slide: ISlide, options?: IAppendSlideOptions): Promise<void> {
    let currentNumberOfSlides;

    // manipulating docProps/app.xml
    let _xml = await this.zipFile.file('docProps/app.xml').async('text');
    const _docAppXml: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    const _elemSlidesNumber = _docAppXml.getElementsByTagName('Slides')[0];
    currentNumberOfSlides = Number(_elemSlidesNumber.innerHTML);
    // increment number of slides by one
    _elemSlidesNumber.innerHTML = String(currentNumberOfSlides + 1);
    const _elems_vt_variant = _docAppXml.getElementsByTagName('vt:variant');
    // it seems that the <vt:i4> elemnt in question always is the last one
    // I can't rely on looking at the preceding elemnt and look at it's text content, since 'Slide Titles' and 'Folientitel' indicate that
    // the exact content depends on the most recent power point instance's, that saved this pptx, language.
    const _numberOfVariantsFound = _elems_vt_variant.length;
    const currentVariant = _elems_vt_variant[_numberOfVariantsFound - 1];
    // save the number of slide titles for future usea
    const currentNumberOfSlideTitles = Number(currentVariant.firstChild.textContent);
    // since we add a slide title to the proper xml file, we need to increase the counter in this xml file by 1
    currentVariant.firstChild.textContent = String(currentNumberOfSlideTitles + 1);
    const _elemTitlesOfParts = _docAppXml.getElementsByTagName('TitlesOfParts')[0];
    const _elem_vt_lpstr = document.createElementNS(xmlNamespaces.officeDocument.docPropsVTypes, 'vt:lpstr');
    _elem_vt_lpstr.textContent = slide.title || '';
    // appending the vt:lpstr tag at the correct position
    _elemTitlesOfParts.firstChild.appendChild(_elem_vt_lpstr);
    // update the array's size
    const _listSize = Number((_elemTitlesOfParts.firstChild as Element).getAttribute('size'));
    (_elemTitlesOfParts.firstChild as Element).setAttribute('size', (_listSize + 1).toString());
    // console.log('docProps/app.xml', _docAppXml);


    // manipulating docProps/core.xml
    _xml = await this.zipFile.file('docProps/core.xml').async('text');
    const _docCoreXml: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    if (options && options.newLastModifiedBy) {
      const _elemLastModifiedBy = _docCoreXml.getElementsByTagName('cp:lastModifiedBy')[0];
      _elemLastModifiedBy.innerHTML = options.newLastModifiedBy;
    }
    if (options && options.changeInfoTimeModified) {
      // write an ISO string
      _docCoreXml.getElementsByTagName('dcterms:modified')[0].innerHTML = new Date().toISOString();
    }
    if (options && options.incrementRevision) {
      // increment the revision number
      const currentRevsionNumber = _docCoreXml.getElementsByTagName('cp:revision')[0].innerHTML;
      _docCoreXml.getElementsByTagName('cp:revision')[0].innerHTML = String((Number(currentRevsionNumber) + 1));
    }
    // console.log('docProps/core.xml', _docCoreXml);




    // manipulating ppt/_rels/presentation.xml.rels
    _xml = await this.zipFile.file('ppt/_rels/presentation.xml.rels').async('text');
    const _docPresentationXmlRels: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    const _elemRelationships = _docPresentationXmlRels.getElementsByTagName('Relationships')[0];
    const numberOfRelationships: number = _elemRelationships.children.length;
    const _elemRelationship = document.createElementNS(xmlNamespaces.package.relationships, 'Relationship');

    // after power point repaired a file that was created with a prior build, the rId for the new slide was changed to
    // the rId of the previously last slide + 1. So I should do that as well.
    // Additionally I observed that the order of the <Relationship> don't seem to matter this time
    const _prefix = 'rId';
    let _idOfLastSlide: number;
    _elemRelationships.childNodes.forEach((node: ChildNode) => {
      const elem: Element = node as Element;
      if (elem.getAttribute('Target') === `slides/slide${currentNumberOfSlides}.xml`) {
        const _rawRId: string = elem.getAttribute('Id');
        _idOfLastSlide = Number(_rawRId.slice(_prefix.length, _rawRId.length));
      }
    });
    // loop again through all the children, this time to increment rIds if the rId is
    // at least greater than the rId of the last slide, since the new slide will be added afterwards
    _elemRelationships.childNodes.forEach((node: ChildNode, key: number) => {
      const elem: Element = node as Element;
      const _rawRId: string = elem.getAttribute('Id');
      const _id = Number(_rawRId.slice(_prefix.length, _rawRId.length));
      if (_id > _idOfLastSlide) {
        const _rawCurrentRId: string = elem.getAttribute('Id');
        const _idOfCurrentElement = Number(_rawCurrentRId.slice(_prefix.length, _rawRId.length));
        elem.setAttribute('Id', `${_prefix}${_idOfCurrentElement + 1}`);
      }
    });

    _elemRelationship.setAttribute('Id', `rId${_idOfLastSlide + 1}`);
    _elemRelationship.setAttribute('Type', xmlSchemata.slide);
    _elemRelationship.setAttribute('Target', `slides/slide${currentNumberOfSlides + 1}.xml`);
    _elemRelationships.appendChild(_elemRelationship);
    // console.log('ppt/_rels/presentation.xml.rels', _docPresentationXmlRels);


    // manipulating [Content_Types].xml
    _xml = await this.zipFile.file('[Content_Types].xml').async('text');
    const _docContentTypesXml: Document = new DOMParser().parseFromString(_xml, 'application/xml');
    // there always is only one <Types>
    const _elemTypes = _docContentTypesXml.getElementsByTagName('Types')[0];

    // add <Override> tag for the slide
    const _elemOverrideSlide = document.createElementNS(xmlNamespaces.package.contentTypes, 'Override');
    // set the attributes
    _elemOverrideSlide.setAttribute('PartName', `/ppt/slides/slide${currentNumberOfSlides + 1}.xml`);
    _elemOverrideSlide.setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.presentationml.slide+xml');

    // insert this right after the currently last slide, so find the next sibling of the last slide element such that the new
    // node can be inserted before
    let insertSlideBeforeThisNode: Node;
    let insertSlideLayoutBeforeThisNode: Node;
    let currentNumberOfSlideLayouts = 0;
    const regExpSlideLayouts = new RegExp('^\\/ppt\\/slideLayouts\\/slideLayout(\\d)+\\.xml$');
    await _elemTypes.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
      // look for the correct <Override> tag
      if ((node as Element).getAttribute('PartName') === `/ppt/slides/slide${currentNumberOfSlides}.xml`) {
        insertSlideBeforeThisNode = node.nextSibling;
      }
      // in order to save resources, also use this loop to check how many slide layouts there are in the pptx
      if (regExpSlideLayouts.test((node as Element).getAttribute('PartName'))) {
        currentNumberOfSlideLayouts++;
      }
    });

    // insert the <Override> at the correct position
    _elemTypes.insertBefore(_elemOverrideSlide, insertSlideBeforeThisNode);


    // check if a slide layout was passed at all
    if (slide.layout) {
      // add <Override> tag for the slide layout
      const _elemOverrideSlideLayout = document.createElementNS('http://schemas.openxmlformats.org/package/2006/content-types', 'Override');
      // set the attributes
      _elemOverrideSlideLayout.setAttribute('PartName', `/ppt/slideLayouts/slideLayout${currentNumberOfSlideLayouts + 1}.xml`);
      _elemOverrideSlideLayout.setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml');

      // insert this right after the currently last slide, so find the next sibling of the last slide element such that the new
      // node can be inserted before
      await _elemTypes.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        if ((node as Element).getAttribute('PartName') === `/ppt/slideLayouts/slideLayout${currentNumberOfSlideLayouts}.xml`) {
          insertSlideLayoutBeforeThisNode = node.nextSibling;
        }
      });
      // insert the <Override> at the correct position
      _elemTypes.insertBefore(_elemOverrideSlideLayout, insertSlideLayoutBeforeThisNode);
    }
    // console.log('[Content_Types].xml', _docContentTypesXml);


    // manipulating ppt/slideMasters/slideMaster1.xml
    // create the var that'll hold the document here and init it with null, thus it can safely be checked if this document exists and be
    // used if necessary
    let _docSlideMaster1Xml: Document = null;
    // only needed when a layout was passed withing the slide:ISlide argument
    if (slide.layout) {
      // add the slide master 1 to the newly created slide by referencing the new layout in the <p:sldLayoutIdLst> element
      _xml = await this.zipFile.file('ppt/slideMasters/slideMaster1.xml').async('text');
      _docSlideMaster1Xml = await new DOMParser().parseFromString(_xml, 'application/xml');
      const _elemSlideLayoutIdList = _docSlideMaster1Xml.getElementsByTagName('p:sldLayoutIdLst')[0];
      const _elemSlideLayoutId = document.createElementNS('http://schemas.openxmlformats.org/presentationml/2006/main', 'p:sldLayoutId');
      let longIdOfLastSlideLayout: number;
      _elemSlideLayoutIdList.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        if ((node as Element).getAttribute('r:id') === `rId${currentNumberOfSlideLayouts}`) {
          longIdOfLastSlideLayout = Number((node as Element).getAttribute('id'));
        }
      });
      _elemSlideLayoutId.setAttribute('id', `${longIdOfLastSlideLayout + 1}`);
      _elemSlideLayoutId.setAttributeNS(xmlNamespaces.officeDocument.relationships, 'r:id', `rId${currentNumberOfSlideLayouts + 1}`);
      _elemSlideLayoutIdList.appendChild(_elemSlideLayoutId);
    }
    // console.log('ppt/slideMasters/slideMaster1.xml', _docSlideMaster1Xml);




    // manipulating ppt/slideMasters/_rels/slideMaster1.xml.rels
    // create the var that'll hold the document here and init it with null, thus it can safely be checked if this document exists and be
    // used if necessary
    let _docSlideMaster1XmlRels: Document = null;
    // this, too, is only needed if a layout was passed
    if (slide.layout) {
      // add the slide master 1 to the newly created slide by referencing the new layout as a <Relationship> element
      _xml = await this.zipFile.file('ppt/slideMasters/_rels/slideMaster1.xml.rels').async('text');
      _docSlideMaster1XmlRels = await new DOMParser().parseFromString(_xml, 'application/xml');
      const _elemRelationships2 = _docSlideMaster1XmlRels.getElementsByTagName('Relationships')[0];
      _elemRelationships2.childNodes.forEach((node: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        const rIdRaw: string = (node as Element).getAttribute('Id');
        const rIdAsNumber: number = Number(rIdRaw.slice(_prefix.length, rIdRaw.length));
        if (rIdAsNumber > currentNumberOfSlideLayouts) {
          (node as Element).setAttribute('Id', `${_prefix}${rIdAsNumber + 1}`);
        }
      });
      const _elemRelationship2 = document.createElementNS('http://schemas.openxmlformats.org/package/2006/relationships', 'Relationship');
      _elemRelationship2.setAttribute('Id', `${_prefix}${currentNumberOfSlideLayouts + 1}`);
      _elemRelationship2.setAttribute('Type', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout');
      _elemRelationship2.setAttribute('Target', `../slideLayouts/slideLayout${currentNumberOfSlideLayouts + 1}.xml`);
      _elemRelationships2.appendChild(_elemRelationship2);
    }
    // console.log('ppt/slideMasters/_rels/slideMaster1.xml.rels', _docSlideMaster1XmlRels);






    // manipulating ppt/presentation.xml
    // adding a new <p:sldId> element as child to <p:sldIdLst> for the new slide
    _xml = await this.zipFile.file('ppt/presentation.xml').async('text');
    const _docPresentationXml: Document = await new DOMParser().parseFromString(_xml, 'application/xml');
    const _elemSlideIdsList = _docPresentationXml.getElementsByTagName('p:presentation')[0].getElementsByTagName('p:sldIdLst')[0];
    const _elemSlideId = document.createElementNS(xmlNamespaces.presentationml.main, 'p:sldId');

    // In contrast to previous observations, the IDs are not always 256 + number of slide. I guess that, when a slide is deleted that
    // was not the last one, the next slide won't have the ID of the deleted one but the currently highest ID + 1, regardless
    // of its position in the presentation. Thus, we need to find that maximal ID as base ref

    let _maxId = 256 + currentNumberOfSlides; // let's use the old approach as fallback
    const _elemsSlideId: Array<Element> = Array.from(_elemSlideIdsList.children);
    for (const _currentElementOfSlideIds of _elemsSlideId) {
      const _idOfCurrentElement = Number(_currentElementOfSlideIds.getAttribute('id'));
      if (_idOfCurrentElement > _maxId) {
        _maxId = _idOfCurrentElement;
      }
    }

    // the folowing line was used for an old observation. Not deleting it yet, but out of usage.
    // _elemSlideId.setAttribute('id', (_idOffset + currentNumberOfSlides).toString());

    _elemSlideId.setAttribute('id', (_maxId + 1).toString());
    // set the r:id attribute to the value given before in ppt/_rels/presentation.xml.rels
    _elemSlideId.setAttributeNS(xmlNamespaces.officeDocument.relationships, 'r:id', `rId${_idOfLastSlide + 1}`);

    // since some rIds were increased, we need to increase those here as well. We need to check the <p:notesMasterIdLst> and the
    // <p:handoutMasterIdLst>. So lets fetch these lists and concat them in order to be processed
    const _elemsNotesMasterId: Array<Element> = Array.from(_docPresentationXml.getElementsByTagName('p:notesMasterId'));
    const _elemsHandoutMasterId: Array<Element> = Array.from(_docPresentationXml.getElementsByTagName('p:handoutMasterId'));
    const _elemsToCheck = _elemsNotesMasterId.concat(_elemsHandoutMasterId);
    // @ts-ignore it complains that it is not of array or string type but i guess that's a mistake since
    // the HTMLCollectionOf<> can be used like an array otherwise
    for (const elemToCheck of _elemsToCheck) {
      const _rawCurrentRId: string = (elemToCheck as Element).getAttribute('r:id');
      const _idOfCurrentElement = Number(_rawCurrentRId.slice(_prefix.length, _rawCurrentRId.length));
      // if the rId if the current element is equal or greater to the new rId of the new slide, then increase it by
      // one (as done before in ppt/_rels/presentation.xml.rels)
      if (_idOfCurrentElement >= _idOfLastSlide + 1) {
        (elemToCheck as Element).setAttributeNS(
          xmlNamespaces.officeDocument.relationships,
          'r:id',
          `${_prefix}${_idOfCurrentElement + 1}`
        );
      }
    }
    _elemSlideIdsList.appendChild(_elemSlideId);
    // console.log('ppt/presentation.xml', _docPresentationXml);

    // creating ppt/slideLayouts/_rels/slideLayout#NUMBER#.xml
    // todo: this seems dependent on the number of master slides, see #34
    // again, only necessary and possible if layout was passed
    let _slideLayoutXmlRelsContent;
    if (slide.layout) {
      _slideLayoutXmlRelsContent = slide.layout.xmlLayoutRelsString;
    }
    // console.log('ppt/slideLayouts/slideLayout#NUMBER#.xml', `slideLayout${currentNumberOfSlideLayouts + 1}.xml`, _docSlideLayoutXml);


    // creating ppt/slides/_rels/slide#NUMBER#.xml.rels
    // target the slide layout xml that we've created a few steps before, if any
    let _slideXmlRelsContent = slide.slide.xmlRelsString;
    // In order to target, we need to check for special keywords that might need to be replaced (see ISlide tsDoc for more info)
    const mapOfRegExpAndReplaceValue = [[/__NEXT_SLIDE_LAYOUT_NUMBER__/, `${currentNumberOfSlideLayouts + 1}`]];
    // pair[0] will contain the pattern, pair[1] the value it is to be replaced with/by
    for (const pair of mapOfRegExpAndReplaceValue) {
      _slideXmlRelsContent = _slideXmlRelsContent.replace(pair[0] as RegExp, pair[1] as string);
    }

    const _docSlideXmlRels: Document = await new DOMParser().parseFromString(_slideXmlRelsContent, 'application/xml');
    // console.log('ppt/slides/_rels/slide#NUMBER#.xml.rels', `slide${currentNumberOfSlides + 1}.xml.rels`, _docSlideXmlRels);

    const _xmlSerializer = new XMLSerializer();
    // writing all the files to the zip;
    this.zipFile = this.zipFile
      .file(`ppt/slides/slide${currentNumberOfSlides + 1}.xml`, slide.slide.xmlString)
      .file(`ppt/slides/_rels/slide${currentNumberOfSlides + 1}.xml.rels`, _slideXmlRelsContent)
      .file('docProps/app.xml', _xmlSerializer.serializeToString(_docAppXml))
      .file('ppt/_rels/presentation.xml.rels', _xmlSerializer.serializeToString(_docPresentationXmlRels))
      .file('[Content_Types].xml', _xmlSerializer.serializeToString(_docContentTypesXml))
      .file('ppt/presentation.xml', _xmlSerializer.serializeToString(_docPresentationXml));

    // we could also check if a layout was passed, because then they following documents ought to be created, but for safety's sake
    // we'll check the existence of those documents directly
    if (_docSlideMaster1XmlRels) {
      this.zipFile = this.zipFile.file(
        'ppt/slideMasters/_rels/slideMaster1.xml.rels',
        _xmlSerializer.serializeToString(_docSlideMaster1XmlRels)
      );
    }
    if (_docSlideMaster1Xml) {
      this.zipFile = this.zipFile.file('ppt/slideMasters/slideMaster1.xml', _xmlSerializer.serializeToString(_docSlideMaster1Xml));
    }
    if (_slideLayoutXmlRelsContent) {
      this.zipFile = this.zipFile.file(
        `ppt/slideLayouts/_rels/slideLayout${currentNumberOfSlideLayouts + 1}.xml.rels`,
        _slideLayoutXmlRelsContent
      );
    }
    if (slide.layout && slide.layout.xmlLayoutString) {
      this.zipFile = this.zipFile.file(`ppt/slideLayouts/slideLayout${currentNumberOfSlideLayouts + 1}.xml`, slide.layout.xmlLayoutString);
    }
  }

  /**
   * Creates a paragraph node and the corresponding xml.rels for a slide
   * Also see https://docs.microsoft.com/en-us/dotnet/api/system.windows.documents.paragraph?view=netframework-4.8
   * @param params The params for the new paragraph node
   */
  public static _createParagraph(params: ICreateParagraphParameters<IPresentationParagraphTextParameters>): {paragraph: Node, xmlRels: Document} {
    // the paragraph node that will be returned at the end of this function
    const _newElemP = document.createElementNS(xmlNamespaces.drawingml.main, 'a:p');
    let _newXmlRels: Document = null;
    for (const textObject of params.texts) {
      const _newElemT = document.createElementNS(xmlNamespaces.drawingml.main, 'a:t');
      _newElemT.innerHTML = textObject.text;
      const _newElemRpr = document.createElementNS(xmlNamespaces.drawingml.main, 'a:rPr');
      _newElemRpr.setAttribute('lang', 'en-US');
      _newElemRpr.setAttribute('dirty', '0');
      // check if additional params are passed
      if (textObject.properties) {
        // check for making it a hyperlink
        if (textObject.properties.hyperlink) {
          // find out the new rId
          const _docSlideRel: Document = textObject.properties.hyperlink.xmlRel;
          const _newRId: number = _docSlideRel.getElementsByTagName('Relationship').length + 1;
          const _newElemRelationship = document.createElementNS(xmlNamespaces.package.relationships, 'Relationship');
          _newElemRelationship.setAttribute('Id', `rId${_newRId}`);
          _newElemRelationship.setAttribute('TargetMode', 'External');
          _newElemRelationship.setAttribute('Target', textObject.properties.hyperlink.url);
          _newElemRelationship.setAttribute('Type', xmlSchemata.hyperlink);
          // append the new <Relationship> element as a child to the root <Relationships> element
          _docSlideRel.getElementsByTagName('Relationships')[0].appendChild(_newElemRelationship);
          // set the ref to the document of the xml rels such that it can be returned at the end of this function
          _newXmlRels = _docSlideRel;

          // now create the proper hlinkClick node with the correct r:id
          const _newElemHlinkClick = document.createElementNS(xmlNamespaces.drawingml.main, 'a:hlinkClick');
          _newElemHlinkClick.setAttributeNS(xmlNamespaces.officeDocument.relationships, 'r:id', `rId${_newRId}`);
          _newElemRpr.appendChild(_newElemHlinkClick);
        }
      }
      const _newElemR = document.createElementNS(xmlNamespaces.drawingml.main, 'a:r');
      _newElemR.append(_newElemRpr, _newElemT);
      _newElemP.appendChild(_newElemR);
    }

    // return the new paragraph node
    return {
      paragraph: _newElemP,
      xmlRels: _newXmlRels
    };
  }

  /**
   * Get the number/position of a layout by its name
   * @param layoutName The name of the seeked layout
   * @param slideMaster The slide master that shall be search through. If none is given, all masters will be searched
   */
  public async getPositionOfSlideLayoutByName(layoutName: MultiLanguageSlideLayoutTitle, slideMaster?: number): Promise<number> {
    //
    // little helper to search for the desired name in the given layouts
    //
    const helperSearchThroughLayouts = (async (layouts: string[] | number[]): Promise<number> => {
      for (const layout of layouts) {
        // if it is a string, we assume it will contain the full path. If it is number, we assume it's the layout's number/position
        const _pathToLayout: string = typeof layout === 'string' ? layout : `ppt/slideLayouts/slideLayout${layout}.xml`;
        const _slideLayoutXml = await this.zipFile.file(_pathToLayout).async('text');
        const _slideLayoutDoc: Document = new DOMParser().parseFromString(_slideLayoutXml, 'application/xml');
        const _elemCSld = _slideLayoutDoc.getElementsByTagName('p:cSld')[0];
        for (const _name of layoutName) {
          if (_name === _elemCSld.getAttribute('name')) {
            if (typeof layout === 'string') {
              const _splitAtSlash = layout.split('/');
              // the file name with extension will be at the end of the split
              const _splitAtDot = _splitAtSlash.pop().split('.');
              // now remove the preceding 'slideLayout' portion of the string
              return Number(_splitAtDot[0].substring(11, _splitAtDot[0].length));
            } else if (typeof layout === 'number') {
              // since it is a number, we assume that it resembles the layout's position/number
              return layout;
            }
          }
        }
      }
      // if it looped through all the available slide layouts, then the desired one couldn't be found
      return Promise.reject(`Couldn't find slide layout '${layoutName.shift()}' or any of its variants.`);
    });
    //
    // end of little helper function
    //

    if (!slideMaster) {
      // if no slide master was passed, then push all the layouts to the array of available layouts that shall be searched
      const _availableSlideLayouts: JSZipObject[] = this.zipFile.folder('ppt/slideLayouts').file(/^slideLayout(\d)+\.xml$/);
      // map the items which are JSZipObject to their name
      return helperSearchThroughLayouts(_availableSlideLayouts.map(x => x['name']));
    } else {
      // if a slide master was passed, only look through the layouts associated with it

      // first check if the slide master provided does really exist. If it exists, it will also have a corresponding .rels.
      // since we ultimately need the content of that .rels, we can check whether or not the master exists by checking if its .rels
      // exists.
      const _slideMasterRelsString = await this.zipFile.file(`ppt/slideMasters/_rels/slideMaster${slideMaster}.xml.rels`).async('text');
      if (!_slideMasterRelsString) {
        // if null or empty, then the slide master does not exist
        return Promise.reject(`slide master number ${slideMaster} does not exist.`);
      } else {
        // parsed content to Document object
        const _slideMasterRelsDoc: Document = new DOMParser().parseFromString(_slideMasterRelsString, 'application/xml');
        const _elemRelationships = _slideMasterRelsDoc.getElementsByTagName('Relationships')[0];
        const _slideLayoutRegex = new RegExp(/slideLayouts\/slideLayout(\d)+\.xml$/);
        const _relatedLayoutNumbers: number[] = [];
        for (const elemRelationship of Array.from(_elemRelationships.childNodes)) {
          if (elemRelationship instanceof Element && _slideLayoutRegex.test(elemRelationship.getAttribute('Target'))) {
            const _target = elemRelationship.getAttribute('Target');
            // find the position of the number by checking against a regex
            // see https://www.tutorialspoint.com/typescript/typescript_string_search.htm
            const _posFirstdigit = _target.search(/(\d)+/);
            // we don't need to imageFromMachine if the search found a digit at all, because then the _slideLayoutRegex.imageFromMachine()
            // would have been false already
            // push all the single digits in here and then glue/implode/concat/join them to a string and then parse it to number
            const _digits: number[] = [];
            // now we need to check if there are more digits following
            for (let i = _posFirstdigit; true; i++) {
              if (!isNaN(Number(_target.charAt(i)))) {
                // then it is a number!
                _digits.push(Number(_target.charAt(i)));
              } else {
                // if it is not a number, then there are no more digits following and the loop can be finished
                break;
              }
            }
            _relatedLayoutNumbers.push(Number(_digits.join('')));
          }
        }

        // now we've got all the layout numbers that are associated with the desired slide master
        return helperSearchThroughLayouts(_relatedLayoutNumbers);
      }
    }
  }

  /**
   * Get the slide layout based on its number/position
   * @param position The number/position of the layout
   */
  private async _getSlideLayoutByPosition(position: number): Promise<Document> {
    const _layoutXmlContent = await this.zipFile.file(`ppt/slideLayouts/slideLayout${position}.xml`).async('text');
    return new DOMParser().parseFromString(_layoutXmlContent, 'application/xml');
  }

  /**
   * Get the number of available slide layouts
   */
  private async _getNumberOfSlideLayouts(): Promise<number> {
    return this.zipFile.folder('ppt/slideLayouts').file(/^slideLayout(\d)+\.xml$/).length;

  }

  public async getImages(/*not implemented yet options?: IGetImagesOptions*/): Promise<IImageInFile[]> {
    return this._getImages('ppt/media/'/*not implemented yet , options?: IGetImagesOptions*/);
  }

  public async getNumberOfImages(): Promise<number> {
    return await super.getNumberOfImages('ppt/');
  }
}
