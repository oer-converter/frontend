/**
 * This objects holds the schemata that are used throughout the tool
 */
export const xmlSchemata = {
  slide: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide',
  hyperlink: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink'
};
