import {EditingAction} from '../../../../models/image-wrapper';

/**
 * Defines the properties a Source is to have
 */
export interface ISource {
  /**
   * The abbreviation that shall be used to link the source to a media file occurrence in the presentation, i.e. 'Abbr01' or simply '1'
   */
  refAbbreviation: string;
  /**
   * A link to the license used
   */
  hrefLicense: string;
  /**
   * The License as humanly readable
   */
  licenseHumanReadable: string;
  /**
   * License version
   */
  licenseVersion: string;
  /**
   * The author of the media file
   */
  author: string;
  /**
   * Description of the media file
   */
  description?: string;
  /**
   * The original location of the image
   */
  originUrl: string;
  /**
   * The name of the service that hosts the image.
   * If none is provided, it will be derived from the host's domain name
   */
  originName?: string;
  /**
   * Title of the work
   */
  title?: string;
  /**
   * A link to the author's blog, website or similar
   */
  authorUrl?: string;
  /**
   * Info about edit if the original image was edited
   */
  edited?: {
    /**
     * The name of the person that edited the image
     */
    editor: string;
    /**
     * The actions that were performed
     */
    actions: EditingAction[];
  };
}
