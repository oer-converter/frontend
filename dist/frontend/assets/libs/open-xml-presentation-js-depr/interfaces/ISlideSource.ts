import {ISlide} from './ISlide';
import {ISource} from './ISource';

/**
 * Parameters for a source slide
 */
export interface ISlideSource extends ISlide {
  /**
   * The sources that shall be put onto that slide
   */
  sources: ISource[];
}
