/**
 * A pair of numbers that resembles cartesian coordinates
 */
export interface ITupleXY {
  x: number;
  y: number;
}
