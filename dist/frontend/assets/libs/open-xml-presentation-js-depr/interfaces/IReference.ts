/**
 * This Interface is used when adding those little text boxes with the ref to the corner of the media file
 */
export interface IReference {
  /**
   * Path to the file from root
   */
  file: string;
  /**
   * The reference string that is to be shown, i.e. Ref01
   */
  refAbbreviation: string;
}
