import {IReference} from './IReference';

/**
 * Pairs a reference object as value to an rId as key
 */
export interface IPairRidReference {
  /**
   * a reference object, needed for knowing the reference abbreviation, among other things
   */
  ref: IReference;
  /**
   * The rId in a given document
   */
  rId: string;
}
