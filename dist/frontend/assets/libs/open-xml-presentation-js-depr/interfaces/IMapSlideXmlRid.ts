import {IPairRidReference} from './IPairRidReference';

/**
 * Interface for easier mapping of affected images to their rIds and those ids' occurences in the slides
 */
export interface IMapSlideXmlRid {
  /**
   * If affected images are for example rId2 and rId3 in slide2.xml and rId7 in slide3.xml, then the object would look like
   * {slide2: ['rId2', 'rId3'], slide3: ['rId7']}
   */
  [key: string]: IPairRidReference[];
}
