/**
 * Defines the fields the config object needs to have
 */
import {EPositionReferenceAbbreviation} from './enums/EPositionReferenceAbbreviation';
import {MultiLanguageSlideLayoutTitle} from './open-xml-presentation-js';

export interface IOpenXmlPresentationConfig {
  /**
   * If set to true, it will set the 'last modified by' info in the presentation that is to be cleansed to the the string
   * in `newLastModifiedBy` as name for the modifier
   */
  readonly changeInfoLastModifiedBy: boolean;
  /**
   * Defines the name that is to be set for the last modifier. Only used if `changeInfoLastModifiedBy` is set to true
   */
  readonly newLastModiefiedBy?: string;
  /**
   * If set to true, it will save the current date as info when the file was last modified
   */
  readonly changeInfoTimeModified: boolean;
  /**
   * If set to true, the revision number will be increased (built-in feature by OpenXML)
   */
  readonly incrementRevision: boolean;
  /**
   * Set the title that is to be displayed on the source slides
   */
  readonly titleOfSourceSlides: string;
  /**
   * Any value smaller than 1 indicates infinitely many sources per slide (resulting in exactly one slide)
   */
  readonly defaultMaxNumberOfSourcesPerSlide: number;
  /**
   * If set to true and if there are more sources to be put onto the slide than the maximum number of sources per slide allow for, then
   * only the first slide of sources will bear the title, whilst the subsequent slides will only bear the sources
   */
  readonly titleOnlyOnFirstSourceSlide: boolean;
  /**
   * Example: to achieve a reference such as [2], you must set this prefix to '['
   */
  readonly prefixRefAbbr: string;
  /**
   * Example: to achieve a reference such as [2], you must set this postfix to ']'
   */
  readonly postfixRefAbbr: string;
  /**
   * Sets the position of the references next to the images on the slides that were replaced
   */
  readonly positionRerAbbr: EPositionReferenceAbbreviation;
  /**
   * The name of the slide layout that is to be used. Please refer to
   * https://support.office.com/en-us/article/what-is-a-slide-layout-99da5716-92ee-4b6a-a0b5-beea45150f3a and all the other
   * languages that are wished to be supported
   */
  readonly slideLayoutName: MultiLanguageSlideLayoutTitle;
}

/**
 * The actual configuration
 */
export const OpenXmlPresentationConfig: IOpenXmlPresentationConfig = {
  changeInfoLastModifiedBy: true,
  newLastModiefiedBy: 'OER Conversion Tool',
  changeInfoTimeModified: true,
  incrementRevision: true,
  titleOfSourceSlides: 'Sources',
  defaultMaxNumberOfSourcesPerSlide: 4,
  titleOnlyOnFirstSourceSlide: true,
  prefixRefAbbr: '[',
  postfixRefAbbr: ']',
  positionRerAbbr: EPositionReferenceAbbreviation.LOWER_RIGHT_CORNER,
  slideLayoutName: ['Title and Content', 'Titel und Inhalt']
};
